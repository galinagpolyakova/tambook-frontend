// flow-typed signature: 1794adc347c16744ecde37e2b5cde4a3
// flow-typed version: <<STUB>>/react-phone-input-2_v^2.9.4/flow_v0.98.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-phone-input-2'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-phone-input-2' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'react-phone-input-2/dist/lib' {
  declare module.exports: any;
}

// Filename aliases
declare module 'react-phone-input-2/dist/lib.js' {
  declare module.exports: $Exports<'react-phone-input-2/dist/lib'>;
}
