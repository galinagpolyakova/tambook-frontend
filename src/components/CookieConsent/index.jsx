// @flow
import React from "react";
import translate from "translations/translate";
import Button from "shared/components/Button";

import "./styles.scss";

export default function CookieConsent({ onClick }: { onClick: Function }) {
  return (
    <div className="cookie-consent">
      {translate(
        "This website uses cookies to ensure you get the best experience on our website."
      )}
      <Button onClick={onClick}>{translate("Got it!")}</Button>
    </div>
  );
}
