// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";

import { type ConsolidatedCourseType } from "containers/course/types";

import Button from "shared/components/Button";
import { Rating } from "shared/components/Rate";
import IconButton from "shared/components/IconButton";
import Tooltip from "shared/components/Tooltip";

import translate from "translations/translate";
import Currency from "../Currency";
import Link from "../Link";

import "./styles.scss";

type CourseProps = {
  id: $PropertyType<ConsolidatedCourseType, "id">,
  title: $PropertyType<ConsolidatedCourseType, "title">,
  location: $PropertyType<ConsolidatedCourseType, "location">,
  price: $PropertyType<ConsolidatedCourseType, "price">,
  image: $PropertyType<ConsolidatedCourseType, "image">,
  rating: $PropertyType<ConsolidatedCourseType, "rating">,
  duration: $PropertyType<ConsolidatedCourseType, "duration">,
  isBookmarked: $PropertyType<ConsolidatedCourseType, "isBookmarked">,
  isAddedToCompare: $PropertyType<ConsolidatedCourseType, "isAddedToCompare">,
  currencyType: $PropertyType<ConsolidatedCourseType, "currencyType">,
  discountedPrice: $PropertyType<ConsolidatedCourseType, "discountedPrice">,

  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function
};

class Course extends PureComponent<CourseProps> {
  addCourseToBookmarkList = (
    courseId: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.addCourseToBookmarkList) {
      this.props.addCourseToBookmarkList(courseId);
    }
  };

  removeCourseFromBookmarkList = (
    courseId: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.removeCourseFromBookmarkList) {
      this.props.removeCourseFromBookmarkList(courseId);
    }
  };

  addCourseToCompare = (id: $PropertyType<ConsolidatedCourseType, "id">) => {
    if (this.props.addCourseToCompare) {
      this.props.addCourseToCompare({ id });
    }
  };

  removeCourseFromCompare = (
    id: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.removeCourseFromCompare) {
      this.props.removeCourseFromCompare({ id });
    }
  };

  render() {
    const {
      id,
      title,
      location,
      price,
      image,
      rating,
      duration,
      isBookmarked,
      isAddedToCompare,
      currencyType,
      discountedPrice
    } = this.props;

    return (
      <div className="course">
        <div className="featured-image">
          <img src={image} alt={title} />
          <div className="hover">
            <div className="button-group">
              <Tooltip
                position={Tooltip.position.BOTTOM}
                message={translate(
                  isAddedToCompare
                    ? "Remove from compare list"
                    : "Add to compare list"
                )}
              >
                <IconButton
                  icon="exchange-alt"
                  className={classNames({ active: isAddedToCompare })}
                  onClick={() => {
                    isAddedToCompare
                      ? this.removeCourseFromCompare(id)
                      : this.addCourseToCompare(id);
                  }}
                />
              </Tooltip>
              <Tooltip
                position={Tooltip.position.BOTTOM}
                message={translate(
                  isBookmarked
                    ? "Remove from bookmark list"
                    : "Add to bookmark list"
                )}
              >
                <IconButton
                  icon="bookmark"
                  className={classNames({ active: isBookmarked })}
                  onClick={() =>
                    isBookmarked
                      ? this.removeCourseFromBookmarkList(id)
                      : this.addCourseToBookmarkList(id)
                  }
                />
              </Tooltip>
            </div>
            <div>
              <Button
                type={Button.TYPE.SECONDARY}
                size={Button.SIZE.SMALL}
                htmlType={Button.HTML_TYPE.LINK}
                link={`/course/${id}`}
              >
                {translate("More Info")}
              </Button>
            </div>
          </div>
        </div>
        <div className="course-content">
          <Link to={`/course/${id}`}>
            <div className="course-title">{title}</div>
            <div className="course-duration">
              {duration} {translate("hours / week")}
            </div>
            <div className="location-title">
              {location.city}, {location.country}
            </div>
            <Rating rate={rating.rate} reviews={rating.reviews} />

            {discountedPrice !== null && discountedPrice < price ? (
              <span className="discounted-price">
                {translate("from")} &nbsp;
                <strong>
                  <span className="old-price">
                    <Currency currencyType={currencyType}>{price}</Currency>
                  </span>
                  &nbsp;
                  <Currency currencyType={currencyType}>
                    {discountedPrice}
                  </Currency>
                </strong>
                &nbsp; / {translate("week")}
              </span>
            ) : (
              <span className="price">
                {translate("from")} &nbsp;
                <strong>
                  <Currency currencyType={currencyType}>{price}</Currency>
                </strong>
                &nbsp; / {translate("week")}
              </span>
            )}
          </Link>
          <div className="course-actions">
            <div className="button-group">
              <Tooltip
                position={Tooltip.position.BOTTOM}
                message={translate(
                  isAddedToCompare
                    ? "Remove from compare list"
                    : "Add to compare list"
                )}
              >
                <IconButton
                  icon="exchange-alt"
                  className={classNames({ active: isAddedToCompare })}
                  onClick={() => {
                    isAddedToCompare
                      ? this.removeCourseFromCompare(id)
                      : this.addCourseToCompare(id);
                  }}
                />
              </Tooltip>
              <Tooltip
                position={Tooltip.position.BOTTOM}
                message={translate(
                  isBookmarked
                    ? "Remove from bookmark list"
                    : "Add to bookmark list"
                )}
              >
                <IconButton
                  icon="bookmark"
                  className={classNames({ active: isBookmarked })}
                  onClick={() =>
                    isBookmarked
                      ? this.removeCourseFromBookmarkList(id)
                      : this.addCourseToBookmarkList(id)
                  }
                />
              </Tooltip>
            </div>
            <div>
              <Button
                type={Button.TYPE.SECONDARY}
                size={Button.SIZE.SMALL}
                htmlType={Button.HTML_TYPE.LINK}
                link={`/course/${id}`}
              >
                {translate("More Info")}
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Course;
