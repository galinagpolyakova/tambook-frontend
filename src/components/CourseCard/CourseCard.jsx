// @flow
import type { ConsolidatedCourseType } from "containers/course/types";

import React, { PureComponent } from "react";

import Button from "shared/components/Button";
import { Rating } from "shared/components/Rate";
import Card, { CardImage, CardBody } from "shared/components/Card";
import translate from "translations/translate";

import { isNotEmpty } from "shared/utils";

import missingImage from "../../assets/missing-course.jpg";

import "./styles.scss";

type CourseCardProps = ConsolidatedCourseType;

class CourseCard extends PureComponent<CourseCardProps> {
  render() {
    const {
      id,
      image,
      title,
      price,
      duration,
      rating,
      location,
      description,
      eligibleToWork
    } = this.props;
    const courseImage = isNotEmpty(image) ? image : missingImage;
    return (
      <Card className="course-card">
        <CardImage image={courseImage}>
          <span className="price">
            ${price} / {translate("pw")}
          </span>
          {eligibleToWork && (
            <span className="is-eligible">
              {translate("eligible")} <br />
              {translate("for work")}
            </span>
          )}
          <div className="location">
            <span>{location.city}, </span>
            {location.country}
          </div>
        </CardImage>
        <CardBody>
          <p className="heading-5">{title}</p>
          <div className="ratings">
            <Rating rate={rating.rate} reviews={rating.reviews} />
          </div>
          <p className="duration">
            {duration} {translate("hours / week")}
          </p>
          <div className="description">{description}</div>
          <div className="action-buttons">
            <Button
              fullWidth={true}
              type={Button.TYPE.PRIMARY}
              size={Button.SIZE.SMALL}
              htmlType={Button.HTML_TYPE.LINK}
              link={`/course/${id}`}
            >
              {translate("MORE INFO")}
            </Button>
          </div>
        </CardBody>
      </Card>
    );
  }
}

export default CourseCard;
