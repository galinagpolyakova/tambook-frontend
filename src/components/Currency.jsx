// @flow
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { getCurrencyRate } from "containers/profile/store/actions";

type CurrencyProps = {
  userCurrency: string,
  currencyType: string,
  children: any,
  rates: Object,
  getCurrencyRate: Function
};

class Currency extends Component<CurrencyProps> {
  componentDidMount() {
    const { getCurrencyRate, userCurrency } = this.props;
    getCurrencyRate(userCurrency);
  }

  componentDidUpdate() {
    const { getCurrencyRate, userCurrency } = this.props;
    getCurrencyRate(userCurrency);
  }

  render() {
    const { userCurrency, currencyType, children, rates } = this.props;
    if (rates.list === null) {
      return (
        <div className="currency-loader">
          <div />
          <div />
          <div />
          <div />
        </div>
      );
    }
    return (
      <Fragment>
        {userCurrency}&nbsp;
        {children > 0 ? (children / rates.list[currencyType]).toFixed(2) : 0}
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    userCurrency: state.profile.settings.currency,
    rates: state.profile.rates
  };
}

const Actions = {
  getCurrencyRate
};

export default connect(
  mapStateToProps,
  Actions
)(Currency);
