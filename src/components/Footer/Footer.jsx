import React, { Fragment } from "react";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Icon from "shared/components/Icon";
import translate from "translations/translate";
import Link from "../Link";

import tambookLogo from "../../assets/tambook-logo.png";

import "./styles.scss";

export default function footer() {
  return (
    <Fragment>
      <footer>
        <div className="container">
          <Row>
            <Col sm={6} md={4}>
              <h3>{translate("Contact Us")}</h3>
              <ul>
                {/*<li>
                    <span>Phone:</span> +93 123 456 789
                  </li>*/}
                <li>
                  <span>{translate("Email us")}</span>
                  &nbsp; office@tambook.com
                </li>
                <li>
                  <span>{translate("Address")}</span>
                  &nbsp;
                  {translate(
                    "3 Glenside Crescent, Eden Terrace, Auckland, NZ."
                  )}
                </li>
              </ul>
            </Col>
            {/* <Col sm={12} md="4" className="footer-widget">
                <Row>
                  <Col>
                    <h3>Top Languages</h3>
                    <ul>
                      <li>English</li>
                      <li>French</li>
                      <li>Spanish</li>
                      <li>German</li>
                      <li>Chinese</li>
                    </ul>
                  </Col>
                  <Col>
                    <h3>Popular Countries</h3>
                    <Col>
                      <ul>
                        <li>France</li>
                        <li>Spain</li>
                        <li>Ukraine</li>
                        <li>Turkey</li>
                        <li>India</li>
                      </ul>
                    </Col>
                    <Col>
                      <ul>
                        <li>France</li>
                        <li>Spain</li>
                        <li>Ukraine</li>
                        <li>Turkey</li>
                        <li>India</li>
                      </ul>
                    </Col>
                  </Col>
                </Row>
              </Col> */}
            <Col md={4} sm={6}>
              <h3>{translate("Follow us")}:</h3>
              <ul className="social-links">
                <li className="facebook">
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href="https://www.facebook.com/tambook.language.courses/"
                    className="btn-with-icon"
                  >
                    <Icon icon="facebook" />
                    facebook
                  </a>
                </li>
                <li className="linkedin">
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href="https://www.linkedin.com/company/tambook-limited/"
                    className="btn-with-icon"
                  >
                    <Icon icon="linkedin-alt" />
                    linkedin
                  </a>
                </li>
                <li className="instagram">
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href="https://www.instagram.com/tambook.language.courses/"
                    className="btn-with-icon"
                  >
                    <Icon icon="instagram" />
                    instagram
                  </a>
                </li>
              </ul>
            </Col>
            <Col md={4} sm={12} className="footer-widget">
              <h3>{translate("Resources")}</h3>
              <Row>
                <Col>
                  <ul>
                    {/*<li>
                        <Link to="/under-construction">WORKING ABROAD</Link>
                      </li>*/}
                    {/*<li>
                        <Link to="/under-construction">VISA</Link>
                      </li>*/}
                    {/*<li>
                        <Link to="/under-construction">HOW IT WORKS</Link>
                      </li>*/}
                    <li>
                      <Link to="/terms-and-conditions">
                        {translate("Terms and Conditions")}
                      </Link>
                    </li>
                    <li>
                      <a href="https://qa-school.tambook-dev.co.nz/signup">
                        {translate("Sign Up As School")}
                      </a>
                    </li>
                    <li>
                      <a href="https://school.tambook.com/login">
                        {translate("Sign In As School")}
                      </a>
                    </li>
                  </ul>
                </Col>
                <Col>
                  <ul>
                    <li>
                      <Link to="/about-us">{translate("About Us")}</Link>
                    </li>
                    <li>
                      <Link to="/contact-us">{translate("Contact Us")}</Link>
                    </li>
                    <li>
                      <Link to="/faq">{translate("FAQ")}</Link>
                    </li>
                    <li>
                      <Link to="/blogs">{translate("Blog")}</Link>
                    </li>
                    <li>
                      <Link to="/check-visa-requirements">
                        {translate("Check Visa Requirements")}
                      </Link>
                    </li>
                    <li>
                      <Link to="/reviews">{translate("Tambook Reviews")}</Link>
                    </li>
                    {/*<li>
                        <Link to="/under-construction">WHY STUDY ABROAD</Link>
                      </li>*/}
                    {/*<li>
                        <Link to="/under-construction">SUCCESS STORIES</Link>
                      </li>*/}
                  </ul>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </footer>
      <div className="footer-note">
        <div className="container">
          <img src={tambookLogo} alt="Tambook Logo" />
          <span>© 2019 {translate("All rights reserved Tambook")}.</span>
        </div>
      </div>
    </Fragment>
  );
}
