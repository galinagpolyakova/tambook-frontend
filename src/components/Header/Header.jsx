// @flow
import React, { PureComponent, Fragment, createRef } from "react";
import { type Location } from "react-router-dom";

import { type AuthState } from "containers/auth/store/reducer";

import { connect } from "react-redux";
import classnames from "classnames";

import { UserSettingsConsumer } from "../../contexts/UserSettings";

import NavBar from "shared/components/Navbar";
import NavItem from "shared/components/NavItem";
import Auth from "containers/auth/pages/auth";
import Button from "shared/components/Button";
import Icon from "shared/components/Icon";
import IconButton from "shared/components/IconButton";

import Link from "../../components/Link";

import { USER_TYPE } from "containers/auth/constants";
import translate from "translations/translate";
import UserPreferences from "../UserPreferences/UserPreferences";
import { API_URL } from "config/app";

import { authSignOut } from "containers/auth/store/action";

import tambookLogo from "../../assets/tambook-logo.png";

import "./styles.scss";

type HeaderProps = {
  isAuthenticated: boolean,
  authSignOut: Function,
  toggleAuth: Function,
  compareList: string[],
  passport: string,
  userRole: $PropertyType<AuthState, "role">,
  userStatus: $PropertyType<AuthState, "status">,
  router: Location,
  displayAuth: boolean,
  toggleUserPreference: Function
};

type HeaderState = {
  isDrawerOpen: boolean
};

class Header extends PureComponent<HeaderProps, HeaderState> {
  body: HTMLElement | null;
  drawerMenuRef = createRef();

  constructor(props) {
    super(props);

    this.state = {
      isDrawerOpen: false
    };

    this.body = document.querySelector("body");
  }

  _toggleDrawer = () => {
    this.setState(state => ({
      isDrawerOpen: !state.isDrawerOpen
    }));
  };

  componentDidMount() {
    if (this.body === null) {
      return;
    }
    this.body.addEventListener("click", this.handleClickOutside);
  }

  componentWillUnmount() {
    if (this.body === null) {
      return;
    }
    this.body.removeEventListener("click", this.handleClickOutside);
  }

  componentDidUpdate(prevProps) {
    const {
      router: { pathname },
      displayAuth
    } = this.props;
    if (pathname !== prevProps.router.pathname) {
      window.scrollTo(0, 0);
      if (displayAuth) {
        this.props.toggleAuth();
      }
    }
  }

  handleClickOutside = (e: Event) => {
    if (this.drawerMenuRef.current === null) {
      return;
    }
    if (
      this.state.isDrawerOpen === true &&
      this.drawerMenuRef.current.contains(e.target)
    ) {
      this.setState({
        isDrawerOpen: false
      });
    }
  };

  render() {
    const { isAuthenticated, compareList, passport, userRole } = this.props;
    const { isDrawerOpen } = this.state;
    return (
      <Fragment>
        <header
          className={classnames("header", { open: this.state.isDrawerOpen })}
        >
          <div className="container">
            <div className="site-logo">
              <Link to="/">
                <img src={tambookLogo} alt="Tambook Logo" />
              </Link>
              <IconButton
                icon={isDrawerOpen ? "times" : "bars"}
                onClick={this._toggleDrawer}
              />
            </div>
            <div className="menu" ref={this.drawerMenuRef}>
              <NavBar>
                <ul>
                  <NavItem>
                    <Link to="/">{translate("Home")}</Link>
                  </NavItem>
                  <NavItem>
                    <Link to="/about-us">{translate("About Us")}</Link>
                  </NavItem>
                  <NavItem>
                    <Link to="/contact-us">{translate("Contact Us")}</Link>
                  </NavItem>
                  {/*<NavItem>
                  <Link to="/under-construction">Travel Guide</Link>
                </NavItem>*/}
                  <NavItem>
                    <Link to="/need-help">{translate("Need Help?")}</Link>
                  </NavItem>
                  {/*<NavItem>
                  <Link to="/under-construction">Experiences</Link>
                </NavItem>*/}
                  {isAuthenticated ? (
                    userRole === USER_TYPE.AGENT ? (
                      <NavItem>
                        <Link to="/agent-profile/agent-information">
                          {translate("Profile")}
                        </Link>
                      </NavItem>
                    ) : (
                      <NavItem>
                        <Link to="/profile">{translate("Profile")}</Link>
                      </NavItem>
                    )
                  ) : (
                    <Fragment />
                  )}
                  <NavItem>
                    <div
                      className="country-selector"
                      onClick={this.props.toggleUserPreference}
                    >
                      <img
                        src={`${API_URL}/schools/image/upload/countries/${passport}.png?width=25&height=25`}
                        title={passport}
                        alt={passport}
                      />
                      {passport}
                    </div>
                  </NavItem>
                  <NavItem>
                    <Link to="/filter?country=&language=">
                      <Icon icon="search" />
                    </Link>
                  </NavItem>
                  <NavItem>
                    <div className="call-to-action">
                      {isAuthenticated ? (
                        <Button
                          className="btn"
                          onClick={this.props.authSignOut}
                          outline
                          type={Button.TYPE.LIGHT}
                        >
                          {translate("Logout")}
                        </Button>
                      ) : (
                        <Button
                          className="btn"
                          onClick={this.props.toggleAuth}
                          outline
                          type={Button.TYPE.LIGHT}
                        >
                          {translate("Login / Sign up")}
                        </Button>
                      )}
                    </div>
                  </NavItem>
                </ul>
              </NavBar>
            </div>
          </div>
        </header>
        <Auth />
        <UserPreferences />
        {compareList.length > 1 && (
          <div className="compare-cta-button">
            <Button htmlType={Button.HTML_TYPE.LINK} link={`/compare`}>
              {translate("Compare")}
              <span>{compareList.length}</span>
            </Button>
          </div>
        )}
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    errors: state.auth.errors,
    userRole: state.auth.role,
    isAuthSuccess: state.auth.isAuthSuccess,
    compareList: state.course.compareList,
    passport: state.profile.settings.passport,
    router: state.router.location
  };
}

const Actions = {
  authSignOut
};

class HeaderContext extends PureComponent<> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ toggleAuth, toggleUserPreference, displayAuth }) => (
          <Header
            toggleAuth={toggleAuth}
            toggleUserPreference={toggleUserPreference}
            displayAuth={displayAuth}
            {...this.props}
          />
        )}
      </UserSettingsConsumer>
    );
  }
}

export default connect(
  mapStateToProps,
  Actions
)(HeaderContext);
