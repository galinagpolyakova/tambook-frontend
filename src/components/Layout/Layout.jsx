// @flow
import React, { PureComponent, type Node } from "react";

import Header from "../Header";
import Footer from "../Footer";

import CookieConsent from "../CookieConsent";
import { type ProfileState } from "containers/profile/store/reducer";

type LayoutProps = {
  children?: Node,
  isAuthenticated: boolean,
  className: string,
  isCookieConsentGiven: $PropertyType<ProfileState, "isCookieConsentGiven">,
  updateCookieConsentStatus: Function
};

class Layout extends PureComponent<LayoutProps> {
  render() {
    const {
      children,
      isAuthenticated,
      className,
      isCookieConsentGiven,
      updateCookieConsentStatus
    } = this.props;
    return (
      <div className={className}>
        <Header isAuthenticated={isAuthenticated} />
        <div className="body-container">
          {children}
          {isCookieConsentGiven === false && (
            <CookieConsent onClick={updateCookieConsentStatus} />
          )}
        </div>
        <Footer />
      </div>
    );
  }
}

export default Layout;
