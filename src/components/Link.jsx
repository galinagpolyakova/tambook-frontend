// @flow
import React from "react";
import { Link } from "react-router-dom";
import { getLanguageFromUrl } from "../shared/helpers/translations";

const language = getLanguageFromUrl();

export default function link({ to, children, ...rest }: { to: string, children: any }) {
  return (
    <Link to={`/${language}${to}`} {...rest}>
      {children}
    </Link>
  );
}
