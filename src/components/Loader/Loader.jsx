// @flow
import React, { PureComponent } from "react";

import translate from "translations/translate";

import "./styles.scss";

type LoaderProps = {
  isLoading: boolean,
  error?: any
};

class Loader extends PureComponent<LoaderProps> {
  static defaultProps = {
    isLoading: true
  };

  render() {
    const { isLoading, error } = this.props;
    if (isLoading) {
      return (
        <div className="loader-wrapper">
          <div className="lds-ring">
            <div />
            <div />
            <div />
            <div />
          </div>
        </div>
      );
    }
    // Handle the error state
    else if (error) {
      return (
        <div>{translate("Sorry, there was a problem loading the page.")}</div>
      );
    } else {
      return null;
    }
  }
}

export default Loader;
