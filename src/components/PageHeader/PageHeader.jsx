// @flow
import type { BreadcrumbListType } from "shared/components/Breadcrumb";

import React, { PureComponent } from "react";
import Helmet from "react-helmet";
import { connect } from "react-redux";

import Breadcrumb from "shared/components/Breadcrumb";

import { isNotEmpty } from "shared/utils";

import auckland from "../../assets/auckland.jpg";

import "./styles.scss";
import { changeToTitleCase } from "../../shared/kernel/cast";
import { onPageChangeAction } from "containers/general/store/actions";

type PageHeaderProps = {
  title: string,
  subtitle?: string,
  image: string,
  subheading?: string,
  breadcrumbs: BreadcrumbListType,
  onPageChangeAction: Function
};

class PageHeader extends PureComponent<PageHeaderProps> {
  static defaultProps = {
    title: "",
    image: auckland
  };

  componentDidMount() {
    this.props.onPageChangeAction({
      title: changeToTitleCase(this.props.title)
    });
  }

  render() {
    const { title, subtitle, image, subheading, breadcrumbs } = this.props;
    return (
      <div
        className="page-header"
        style={{
          background: `linear-gradient(
            #e8bc55b3, #e8bc55b3
            ), url(${image}) no-repeat right`,
          backgroundSize: "cover"
        }}
      >
        <div className="container">
          <Breadcrumb breadcrumbs={breadcrumbs} />
          <h1>{title}</h1>
          <Helmet
            title={changeToTitleCase(title)}
            meta={[{ property: "og:title", content: changeToTitleCase(title) }]}
          />
          {isNotEmpty(subtitle) ? <h1 className="title2">{subtitle}</h1> : ""}
          {isNotEmpty(subheading) ? subheading : ""}
        </div>
        <div className="overlay" />
      </div>
    );
  }
}

const Actions = {
  onPageChangeAction
};

export default connect(
  null,
  Actions
)(PageHeader);
