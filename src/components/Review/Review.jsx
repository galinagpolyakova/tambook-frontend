// @flow
import React, { PureComponent } from "react";

import Rate from "shared/components/Rate";

import "./styles.scss";

type ReviewProps = {
  name: string,
  image: string,
  rating: number,
  comment: string
};

class Review extends PureComponent<ReviewProps> {
  render() {
    const { name, image, comment, rating } = this.props;
    return (
      <div className="review">
        <div>
          <div className="reviewer">
            <img src={image} alt={name} />
          </div>
        </div>
        <div className="review-content">
          <div className="ratings">
            <span>{name}</span>
            <Rate value={rating} />
          </div>
          <p>{comment}</p>
        </div>
      </div>
    );
  }
}

export default Review;
