// @flow
import React, { PureComponent, type Element } from "react";
import classNames from "classnames";

import { isNotEmpty } from "shared/utils";

const TYPES = {
  DEFAULT: "default",
  PRIMARY: "primary",
  INFO: "info",
  LIGHT: "light",
  SECONDARY: "secondary"
};

type WidgetProps = {
  title?: string,
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined,
  type: | typeof TYPES.DEFAULT
    | typeof TYPES.PRIMARY
    | typeof TYPES.INFO
    | typeof TYPES.LIGHT
    | typeof TYPES.SECONDARY,
  className: string
};

class Widget extends PureComponent<WidgetProps> {
  static types = TYPES;

  static defaultProps = {
    type: TYPES.DEFAULT
  };

  render() {
    const { title, children, type, className } = this.props;
    return (
      <div className={classNames("widget", className, `widget-${type}`)}>
        {isNotEmpty(title) ? <div className="title">{title}</div> : ""}
        {children}
      </div>
    );
  }
}

export default Widget;
