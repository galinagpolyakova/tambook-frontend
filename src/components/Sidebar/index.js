import Sidebar from "./Sidebar";
import Widget from "./Widget";

import "./styles.scss";

export default Sidebar;

export { Sidebar, Widget };
