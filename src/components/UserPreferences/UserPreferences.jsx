// @flow
import React, { Component } from "react";
import { connect } from "react-redux";

import "./styles.scss";

import countries from "../../shared/data/countries";
import languages from "../../shared/data/languages";

import Modal from "shared/components/Modal";
import Select from "shared/components/Select";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Link from "../../components/Link";
import Button from "shared/components/Button/Button";
import { translationOptions } from "../../shared/constants/translations";
import translate from "translations/translate";

import { UserSettingsConsumer } from "../../contexts/UserSettings";
import {
  getCurrencyList,
  saveUserPreferences,
  getCurrencyRate
} from "containers/profile/store/actions";
import {
  getLanguageFromUrl,
  getLocaleRedirectUrl
} from "../../shared/helpers/translations";

type UserPreferencesProps = {
  showModal: boolean,
  onClose: Function,
  userSettings: {
    language: string,
    currency: string,
    passport: string
  },
  currencies: Object,
  getCurrencyList: Function,
  getCurrencyRate: Function,
  saveUserPreferences: Function
};

type UserPreferencesState = {
  language: string,
  passport: string,
  currency: string
};

class UserPreferences extends Component<
  UserPreferencesProps,
  UserPreferencesState
> {
  constructor(props) {
    super(props);
    this.state = {
      language: getLanguageFromUrl(),
      passport: "",
      currency: ""
    };
    // $FlowFixMe
    this.saveUserSettings = this.saveUserSettings.bind(this);
  }

  componentDidMount() {
    const {
      userSettings: { passport, currency },
      currencies
    } = this.props;
    if (currencies === null) {
      this.props.getCurrencyList();
      this.props.getCurrencyRate(currency);
    }
    this.setState({
      passport,
      currency
    });
  }

  saveUserSettings() {
    this.props.saveUserPreferences(this.state);
    const { language } = this.state;
    const redirectUrl = getLocaleRedirectUrl(language);
    if (getLanguageFromUrl() !== language) {
      window.location.href = `${redirectUrl}`;
    }
    this.props.onClose();
  }

  render() {
    const { onClose, currencies } = this.props;
    const { language, passport, currency } = this.state;
    const countryList = [];

    for (const key in countries) {
      if (countries.hasOwnProperty(key)) {
        countryList.push({
          value: key,
          name: countries[key]
        });
      }
    }

    const currencyList = [];
    for (const key in currencies) {
      if (currencies.hasOwnProperty(key)) {
        currencyList.push({
          value: key,
          name: currencies[key]
        });
      }
    }

    const languageList = [];
    for (const key in languages) {
      if (languages.hasOwnProperty(key)) {
        languageList.push({
          value: key,
          name: languages[key]
        });
      }
    }
    return (
      <Modal className="auth-modal" showModal={true} onClose={onClose}>
        <div className="user-preferences">
          <div className="heading">
            {translate(
              "Tambook best offers are based on your citizenship. Please select website language, passport and preferred currency."
            )}
          </div>
          <div className="form-block">
            <Row className="settings-block">
              <Col md={4} sm={12}>
                <label>{translate("Language")}</label>
                <Select
                  options={translationOptions}
                  placeholder={translate("Language")}
                  selected={language}
                  onChange={language => this.setState({ language })}
                  name="language"
                />
              </Col>
              <Col md={4} sm={12}>
                <label>{translate("Passport")}</label>
                <Select
                  options={countryList}
                  placeholder={translate("Passport")}
                  selected={passport}
                  onChange={passport => this.setState({ passport })}
                  searchable
                  name="passport"
                />
              </Col>
              <Col md={4} sm={12}>
                <label>{translate("Currency")}</label>
                <Select
                  options={currencyList}
                  placeholder={translate("Currency")}
                  selected={currency}
                  onChange={currency => this.setState({ currency })}
                  searchable
                  name="currency"
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Button onClick={() => this.saveUserSettings()}>
                  {translate("Continue")}
                </Button>
              </Col>
            </Row>
          </div>
          <div className="notice">
            {translate(
              "By choosing a country and currency above, you agree to our"
            )}
            &nbsp;
            <Link target="_new" to="/terms-and-conditions">
              {translate("Terms and Conditions")}
            </Link>
            .
          </div>
        </div>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    currencies: state.profile.currencies,
    userSettings: state.profile.settings
  };
}

const Actions = {
  getCurrencyList,
  saveUserPreferences,
  getCurrencyRate
};

const UserPreferencesContainer = connect(
  mapStateToProps,
  Actions
)(UserPreferences);

export default class UserPreferencesContext extends Component<{}> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ showUserPreference, toggleUserPreference }) =>
          showUserPreference && (
            <UserPreferencesContainer
              {...this.props}
              onClose={toggleUserPreference}
            />
          )
        }
      </UserSettingsConsumer>
    );
  }
}
