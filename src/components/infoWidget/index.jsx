import React from "react";
import { Widget } from "../Sidebar";
import translate from "translations/translate";

import Icon from "shared/components/Icon";

import "./styles.scss";

export default function infoWidget() {
  return (
    <Widget title={translate("NEED HELP?")} type={Widget.types.LIGHT}>
      <p>
        {translate(
          "Our friendly and experienced student advisors will help you with all your questions"
        )}
      </p>
      <div className="contact-details">
        <div className="contact-option">
          <Icon icon="envelope" />
          OFFICE@TAMBOOK.COM
        </div>
      </div>
    </Widget>
  );
}
