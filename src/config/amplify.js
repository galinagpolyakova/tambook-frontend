export default {
  cognito: {
    identityPoolId: "us-east-1:165b7072-a885-4643-a490-95eb9b42069c",
    region: "us-east-1",
    identityPoolRegion: "us-east-1",
    userPoolId: "us-east-1_rBSndY8LT",
    userPoolWebClientId: "35vmacn917rfa4543njj73ekku",
    mandatorySignIn: false
  },
  storage: {
    AWSS3: {
      bucket: "qa-tambook",
      region: "us-east-1"
    }
  },
  social: {
    facebook: "2225110294241216",
    google:
      "147732163845-pui5kqmu3bu85d6p1sge6k89hkgpdv3d.apps.googleusercontent.com"
  }
};
