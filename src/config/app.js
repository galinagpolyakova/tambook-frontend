import { getLanguageFromUrl } from "../shared/helpers/translations";

export const SITE_URL = "https://qa.tambook-dev.co.nz";
export const API_URL = "https://qa.tambook-dev.co.nz/qa";
export const SITE_URL_WITH_LANG = `${SITE_URL}/${getLanguageFromUrl()}`;
export const MEDIA_URL = "http://qa-media.tambook-dev.co.nz";
