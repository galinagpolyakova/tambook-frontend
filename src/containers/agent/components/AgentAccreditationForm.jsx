// @flow
import React, { Component, Fragment } from "react";

import Button from "shared/components/Button";
import Input from "shared/components/Input";
import Select from "shared/components/Select";
import Checkbox from "shared/components/Checkbox";
import FileInput from "shared/components/FileInput";
import Alert from "shared/components/Alert";

import { serviceManager } from "services/manager";
import { ACCREDITATION_STATUS } from "../constants";

type AgentAccreditationFormState = {
  form: {
    paymentMethod: string,
    cardNumber: string,
    cardholderName: string,
    expiryMonth: string,
    expiryYear: string,
    cvc: string,
    uploadedDocument: [],
    phoneNumber: string,
    isAgreed: boolean
  },
  fileNames: [],
  fileCount: number,
  isUploading: boolean,
  fileInputString: string,
  fileUploadError: string,
  formErrors: {
    paymentMethod: null | string,
    cardNumber: null | string,
    cardholderName: null | string,
    expiryMonth: null | string,
    expiryYear: null | string,
    cvc: null | string,
    uploadedDocument: null | string,
    phoneNumber: null | string,
    isAgreed: null | string
  }
};

type AgentAccreditationFormProps = {
  updateAgentAccreditation: Function,
  isAccreditationDetailsSendSuccess: boolean,
  userId: string,
  isLoading: boolean,
  accreditationStatus: string
};

class AgentAccreditationForm extends Component<
  AgentAccreditationFormProps,
  AgentAccreditationFormState
> {
  state = {
    form: {
      paymentMethod: "CREDIT CARD",
      cardNumber: "",
      cardholderName: "",
      expiryMonth: "",
      expiryYear: "",
      cvc: "",
      uploadedDocument: [],
      phoneNumber: "",
      isAgreed: false
    },
    fileInputString: "No file chosen",
    fileNames: [],
    fileCount: 0,
    isUploading: false,
    fileUploadError: "",
    formErrors: {
      paymentMethod: null,
      cardNumber: null,
      cardholderName: null,
      expiryMonth: null,
      expiryYear: null,
      cvc: null,
      uploadedDocument: null,
      phoneNumber: null,
      isAgreed: null
    }
  };

  componentDidUpdate(prevProps: any) {
    const { isAccreditationDetailsSendSuccess } = this.props;
    if (
      prevProps.isAccreditationDetailsSendSuccess !==
        isAccreditationDetailsSendSuccess &&
      isAccreditationDetailsSendSuccess
    ) {
      this.handleFormFieldReset();
    }
  }

  validateForm = () => {
    const {
      paymentMethod,
      cardNumber,
      cardholderName,
      expiryMonth,
      expiryYear,
      cvc,
      phoneNumber,
      isAgreed
    } = this.state.form;

    let hasError = false;

    this.resetFormErrors();

    if (paymentMethod === "") {
      this.setFormErrors("paymentMethod", "Payment method is required.");
      hasError = true;
    }
    if (cardNumber === "") {
      this.setFormErrors("cardNumber", "Card number is required.");
      hasError = true;
    } else if (
      !cardNumber.match(
        /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/
      )
    ) {
      this.setFormErrors("cardNumber", "Provided card number is invalid");
      hasError = true;
    }
    if (cardholderName === "") {
      this.setFormErrors("cardholderName", "Cardholder's name is required.");
      hasError = true;
    }
    if (phoneNumber === "") {
      this.setFormErrors("phoneNumber", "Phone number is required.");
      hasError = true;
    }
    if (isAgreed === false) {
      this.setFormErrors("isAgreed", "This field is required.");
      hasError = true;
    }
    if (cvc === "") {
      this.setFormErrors("cvc", "CVC or CVV is required.");
      hasError = true;
    } else if (!cvc.match(/^[0-9]{3,4}$/)) {
      this.setFormErrors("cvc", "Provided CCV/CVV is invalid");
      hasError = true;
    }
    if (expiryMonth === "") {
      this.setFormErrors("expiryMonth", "Month is required.");
      hasError = true;
    }
    if (expiryYear === "") {
      this.setFormErrors("expiryYear", "Year is required.");
      hasError = true;
    }
    return hasError;
  };

  setFormErrors = (field: string, message: string) => {
    this.setState(state => {
      return {
        formErrors: {
          ...state.formErrors,
          [field]: message
        }
      };
    });
  };

  handleFormFieldReset = () => {
    this.setState(({ form }) => ({
      form: {
        ...form,
        paymentMethod: "CREDIT CARD",
        cardNumber: "",
        cardholderName: "",
        expiryMonth: "",
        expiryYear: "",
        cvc: "",
        uploadedDocument: [],
        phoneNumber: "",
        isAgreed: false
      },
      fileNames: [],
      fileInputString: "No file chosen",
      fileCount: 0
    }));
  };

  resetFormErrors = () => {
    this.setState(({ formErrors }) => ({
      formErrors: {
        ...formErrors,
        paymentMethod: null,
        cardNumber: null,
        cardholderName: null,
        expiryMonth: null,
        expiryYear: null,
        cvc: null,
        uploadedDocument: null,
        phoneNumber: null,
        isAgreed: null
      }
    }));
  };

  handleFormFieldChange = (event: any) => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.id]: event.target.value
      }
    });
  };

  readUploadedFileAsData = (inputFile: any) => {
    let AgentService = serviceManager.get("AgentService");
    let temporaryFileReader = new FileReader();
    // $FlowFixMe
    let promise = new Promise((resolve, reject) => {
      temporaryFileReader.onerror = () => {
        temporaryFileReader.abort();
        reject(
          this.setState(() => ({
            fileUploadError: "Error on reading file"
          }))
        );
      };

      temporaryFileReader.onload = () => {
        let base64 = temporaryFileReader.result;
        AgentService.uploadUserDocument({
          from: "userDoc",
          name: inputFile.name,
          type: inputFile.type,
          file: base64,
          id: this.props.userId
        }).then(result => {
          let fileData = {
            name: result.name,
            type: inputFile.type,
            url: result.url
          };
          resolve(fileData);
        });
      };
      temporaryFileReader.readAsDataURL(inputFile);
    });
    return promise;
  };

  handleFileInputChange = async (event: any) => {
    event.persist();
    if (!event.target || !event.target.files) {
      return;
    }
    let fileList: any = [0];
    fileList = event.target.files;
    try {
      let fileArray = [];
      let fileBase64 = "";
      let fileNamesArray = [];
      if (fileList.length > 0) {
        for (let i = 0; i < fileList.length; i++) {
          fileBase64 = await this.readUploadedFileAsData(fileList[i]);
          fileNamesArray[i] = fileList[i].name;
          fileArray[i] = fileBase64;
        }
      }

      this.setState(({ form }) => ({
        form: {
          ...form,
          uploadedDocument: fileArray
        },
        fileNames: fileNamesArray,
        fileCount: fileArray.length
      }));
    } catch (e) {
      this.setState(() => ({
        fileUploadError: e
      }));
    }
  };

  handleFormCheckboxChange = () => {
    const agreed = this.state.form.isAgreed;
    this.setState(({ form }) => ({
      form: {
        ...form,
        isAgreed: !agreed
      }
    }));
  };

  handleFormSelectChange = (paymentMethod: string) => {
    this.setState(({ form }) => ({
      form: {
        ...form,
        paymentMethod: paymentMethod
      }
    }));
  };

  handleFormMonthSelectChange = (month: string) => {
    this.setState(({ form }) => ({
      form: {
        ...form,
        expiryMonth: month
      }
    }));
  };

  handleFormYearSelectChange = (year: string) => {
    this.setState(({ form }) => ({
      form: {
        ...form,
        expiryYear: year
      }
    }));
  };

  handleFormSubmit = () => {
    let { updateAgentAccreditation, userId, accreditationStatus } = this.props;

    if (this.validateForm()) {
      return null;
    }

    let {
      form: {
        paymentMethod,
        cardNumber,
        cardholderName,
        expiryMonth,
        expiryYear,
        cvc,
        uploadedDocument,
        phoneNumber,
        isAgreed
      }
    } = this.state;

    if (accreditationStatus === ACCREDITATION_STATUS.DEFAULT) {
      accreditationStatus = ACCREDITATION_STATUS.PENDING;
    }

    updateAgentAccreditation({
      userId,
      paymentMethod,
      cardNumber,
      cardholderName,
      expiryMonth,
      expiryYear,
      cvc,
      uploadedDocument,
      phoneNumber,
      isAgreed,
      accreditationStatus
    });
  };

  _getSubmitButton = () => {
    const { isLoading, accreditationStatus } = this.props;
    return accreditationStatus !== ACCREDITATION_STATUS.APPROVED ? (
      <Button
        size={Button.SIZE.MEDIUM}
        type={Button.TYPE.PRIMARY}
        onClick={this.handleFormSubmit.bind(this)}
        loading={isLoading}
      >
        SEND
      </Button>
    ) : (
      <Button
        size={Button.SIZE.MEDIUM}
        type={Button.TYPE.PRIMARY}
        onClick={this.handleFormSubmit.bind(this)}
        loading={isLoading}
      >
        UPDATE
      </Button>
    );
  };

  _getFileUploadError = () => {
    const { fileUploadError } = this.state;
    if (fileUploadError !== "") {
      return (
        <Alert isFullWidth={true} type={Alert.TYPE.ERROR}>
          {fileUploadError}
        </Alert>
      );
    }
  };

  render() {
    const { form, fileNames, formErrors, fileCount } = this.state;
    let fileInputString;
    let file;
    if (fileNames.length > 0) {
      if (fileCount === 0) {
        fileInputString = "No file chosen";
      } else if (fileCount === 1) {
        fileInputString = fileNames;
      } else {
        fileInputString = fileCount + " files selected";
      }
      file = fileNames.map((name, i) => <span key={i}>{name}, </span>);
    }

    return (
      <Fragment>
        {this._getFileUploadError()}
        <form autoComplete="off">
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Payment Method</label>
                <Select
                  selected={form.paymentMethod}
                  options={["CREDIT CARD", "DIRECT"]}
                  onChange={paymentMethod => {
                    this.handleFormSelectChange(paymentMethod);
                  }}
                />
              </div>
            </div>
            <div className="col" />
          </div>
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Card Number</label>
                <Input
                  id="cardNumber"
                  type="number"
                  text={form.cardNumber}
                  onChange={this.handleFormFieldChange.bind(this)}
                  error={formErrors.cardNumber}
                  autoComplete={false}
                />
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label>Cardholder Name</label>
                <Input
                  id="cardholderName"
                  text={form.cardholderName}
                  onChange={this.handleFormFieldChange.bind(this)}
                  error={formErrors.cardholderName}
                  autoComplete={false}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="row inside-row">
                <div className="col-2">
                  <div className="form-group">
                    <label>Expiry date</label>
                    <Select
                      placeholder="MM"
                      options={[
                        "01",
                        "02",
                        "03",
                        "04",
                        "05",
                        "06",
                        "07",
                        "08",
                        "09",
                        "10",
                        "11",
                        "12"
                      ]}
                      error={formErrors.expiryMonth}
                      onChange={month => {
                        this.handleFormMonthSelectChange(month);
                      }}
                    />
                  </div>
                </div>
                <div className="col-1" />
                <div className="col-2 col-2-margin">
                  <div className="form-group">
                    <label> </label>
                    <Select
                      placeholder="YY"
                      options={[
                        "2030",
                        "2029",
                        "2028",
                        "2027",
                        "2026",
                        "2025",
                        "2024",
                        "2023",
                        "2022",
                        "2021",
                        "2020",
                        "2019"
                      ]}
                      error={formErrors.expiryYear}
                      onChange={year => {
                        this.handleFormYearSelectChange(year);
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label>CCV / CVV</label>
                <Input
                  id="cvc"
                  type="number"
                  text={form.cvc}
                  onChange={this.handleFormFieldChange.bind(this)}
                  error={formErrors.cvc}
                  autoComplete={false}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Upload Document</label>
                <FileInput
                  className="file-upload"
                  inputClass="input-file"
                  multiple={true}
                  onChange={this.handleFileInputChange}
                />
                <input
                  className="fileChosenInput"
                  type="text"
                  value={fileInputString}
                  placeholder="No file chosen"
                  disabled={true}
                />
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <div className="form-input file-name">
                  <p>{file}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Phone Number</label>
                <div className="form-input">
                  <Input
                    id="phoneNumber"
                    type="number"
                    text={form.phoneNumber}
                    onChange={this.handleFormFieldChange.bind(this)}
                    error={formErrors.phoneNumber}
                    autoComplete={false}
                  />
                </div>
              </div>
            </div>
          </div>
          <Checkbox
            text="I agree with Terms & Conditions and Privacy Policy"
            isChecked={form.isAgreed}
            onChange={this.handleFormCheckboxChange}
          />
        </form>
        {this._getSubmitButton()}
        <Button
          size={Button.SIZE.MEDIUM}
          outline
          type={Button.TYPE.INFO}
          onClick={this.handleFormFieldReset}
        >
          CANCEL
        </Button>
      </Fragment>
    );
  }
}

export default AgentAccreditationForm;
