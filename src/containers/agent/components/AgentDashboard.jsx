// @flow
import React, { Component } from "react";

import Row from "shared/components/Row";
import Col from "shared/components/Col";

import Loader from "components/Loader";
import Card, { CardBody } from "shared/components/Card";
import Alert from "shared/components/Alert";
import Modal from "shared/components/Modal";

import AgentAccreditationForm from "./AgentAccreditationForm";
import Icon from "shared/components/Icon";
import Currency from "components/Currency";

type AgentDashboardState = {
  showModal: boolean,
  update: boolean
};

type AgentDashboardProps = {
  updateAgentAccreditation: Function,
  isAccreditationDetailsSendSuccess: boolean,
  userId: string,
  name: string,
  email: string,
  isLoading: boolean,
  isBookingSummeryLoading: boolean,
  getAgentByEmail: Function,
  agentDetails: {
    accreditationStatus: string,
    agentCommission: number,
    commissionType: string,
    agentLevel: string,
    agentType: string
  },
  getBookingSummery: Function,
  successOrders: number,
  pendingOrders: number,
  totalWeeks: number,
  students: number,
  femaleStudent: number,
  sales: number,
  revenue: number
};

class AgentDashboard extends Component<
  AgentDashboardProps,
  AgentDashboardState
> {
  state = {
    showModal: false,
    update: false
  };

  componentDidMount() {
    const {
      getBookingSummery,
      agentDetails: { agentCommission }
    } = this.props;
    getBookingSummery({ agentCommission });
  }

  handleAccFormSubmit = () => {
    this.setState({
      showModal: true
    });
  };

  _onCloseModal = () => {
    this.setState({
      showModal: false
    });
  };

  _getAccreditationBanner = () => {
    const { isAccreditationDetailsSendSuccess, agentDetails } = this.props;
    if (
      isAccreditationDetailsSendSuccess === true ||
      agentDetails.accreditationStatus === "pending"
    ) {
      return (
        <Alert isFullWidth={true} type={Alert.TYPE.SUCCESS}>
          Waiting for the approval from the Tambook
        </Alert>
      );
    }
  };

  render() {
    const {
      isAccreditationDetailsSendSuccess,
      updateAgentAccreditation,
      userId,
      isLoading,
      isBookingSummeryLoading,
      agentDetails,
      totalWeeks,
      students,
      sales,
      revenue
    } = this.props;
    const { showModal } = this.state;

    if (showModal === true) {
      return (
        <Modal
          className="acc-modal"
          showModal={this.state.showModal}
          onClose={this._onCloseModal}
        >
          <div className="acc-container">
            <div className="acc-content">
              <div className="acc-body">
                <div className="form-header">
                  {this._getAccreditationBanner()}
                  <div className="heading">Request to accreditation</div>
                </div>
                <div className="sub-heading">
                  You send your request with the accreditation fee $200
                </div>
                <AgentAccreditationForm
                  userId={userId}
                  isLoading={isLoading}
                  isAccreditationDetailsSendSuccess={
                    isAccreditationDetailsSendSuccess
                  }
                  accreditationStatus={agentDetails.accreditationStatus}
                  updateAgentAccreditation={updateAgentAccreditation}
                />
              </div>
            </div>
          </div>
        </Modal>
      );
    }

    if (isBookingSummeryLoading) {
      return <Loader isLoading />;
    } else {
      return (
        <div>
          <Row>
            <Col md="4" sm="12">
              <Card className="dashboard-card">
                <CardBody className="card-body">
                  <div className="row">
                    <div className="col-4">
                      <Icon icon="person-outline" color="#64ea91" />
                    </div>
                    <div className="col-8">
                      <p className="duration">Current agent&apos;s plan</p>
                      <p className="heading-5">
                        Business {agentDetails.agentCommission} %
                      </p>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col md="4" sm="12">
              <Card className="dashboard-card">
                <CardBody className="card-body">
                  <div className="row">
                    <div className="col-4">
                      <Icon icon="coins" color="#f266ed" />
                    </div>
                    <div className="col-8">
                      <p className="duration">Total sales</p>
                      <p className="heading-5">
                        <Currency currencyType="USD">{sales}</Currency>
                      </p>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col md="4" sm="12">
              <Card className="dashboard-card">
                <CardBody className="card-body">
                  <div className="row">
                    <div className="col-4">
                      <Icon icon="dollar" color="#8fc9fb" />
                    </div>
                    <div className="col-8">
                      <p className="duration">Total profit</p>
                      <p className="heading-5">
                        <Currency currencyType="USD">{revenue}</Currency>
                      </p>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col md="4" sm="12">
              <Card className="dashboard-card">
                <CardBody className="card-body">
                  <div className="row">
                    <div className="col-4">
                      <Icon icon="people-outline" color="#f7e270" />
                    </div>
                    <div className="col-8">
                      <p className="duration">Total amount of student</p>
                      <p className="heading-5">{students}</p>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col md="4" sm="12">
              <Card className="dashboard-card">
                <CardBody className="card-body">
                  <div className="row">
                    <div className="col-4">
                      <Icon icon="alarm-clock" color="#f69899" />
                    </div>
                    <div className="col-8">
                      <p className="duration">Total course week purchased</p>
                      <p className="heading-5">{totalWeeks}</p>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col>
              {/* <Card className="dashboard-card">
                <CardBody className="card-body">
                  <div className="row">
                    <div className="col-4 ">
                      <Icon icon="female" color="#e84393" />
                    </div>
                    <div className="col-8">
                      <p className="duration">
                        {(
                          "Student diversification: rate of women participants"
                        )}
                      </p>
                      <p className="heading-5">
                        0 %
                        {((femaleStudent * 100) / students).toFixed(2)} %
                      </p>
                    </div>
                  </div>
                </CardBody>
              </Card> */}
            </Col>
          </Row>
        </div>
      );
    }
  }
}

export default AgentDashboard;
