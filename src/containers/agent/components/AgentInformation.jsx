// @flow
import React, { Component } from "react";
import PhoneInput from "react-phone-input-2";

import Loader from "components/Loader";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Button from "shared/components/Button";
import Input from "shared/components/Input";
import Select from "shared/components/Select";
import Section, { SectionHeader } from "shared/components/Section";
import AddressAutocomplete, {
  type AddressAutocompleteResponseType
} from "shared/components/AddressAutocomplete";
import Alert from "shared/components/Alert";
import Modal from "shared/components/Modal";

import { USER_STATUS } from "../../auth/constants";

import AgentAccreditationForm from "./AgentAccreditationForm";

import { ACCREDITATION_STATUS } from "../constants";

import "react-phone-input-2/dist/style.css";

type AgentInformationProps = {
  fullName: string,
  getAgentByEmail: Function,
  updateAgent: Function,
  userCurrentStatus: string,
  address: string,
  agentDetails: {
    accreditationStatus: string,
    agentLevel: string,
    agentType: string
  },
  phoneNo: string,
  address1: string,
  address2: string,
  country: string,
  zipCode: string,
  website: string,
  userEmail: string,
  userId: string,
  isLoading: boolean,
  isProfileDetailsSendSuccess: boolean,
  isAgentLoaded: boolean,
  isUserProfileLoaded: boolean,
  updateAgentAccreditation: Function,
  isAccreditationDetailsSendSuccess: boolean
};

type AgentInformationState = {
  showModal: boolean,
  update: boolean,
  latitude: number,
  altitude: number,
  isEditAddress: boolean,
  defaultCountryCode: string,
  form: {
    agentType: string,
    fullName: string,
    phoneNo: string,
    email: string,
    website: string,
    address: string,
    address1: string,
    address2: string,
    zipCode: string,
    country: string
  },
  formErrors: {
    agentType: null | string,
    fullName: null | string,
    phoneNo: null | string,
    email: null | string,
    website: null | string,
    address: null | string,
    address1: null | string,
    address2: null | string,
    zipCode: null | string,
    country: null | string
  }
};

class AgentInformation extends Component<
  AgentInformationProps,
  AgentInformationState
> {
  constructor(props: AgentInformationProps) {
    super(props);
    this.state = {
      showModal: false,
      update: false,
      currentUserStatus: null,
      latitude: 0,
      altitude: 0,
      isEditAddress: false,
      defaultCountryCode: "us",
      form: {
        agentType: "agency",
        fullName: props.fullName,
        phoneNo: "",
        email: "",
        website: "",
        address: "",
        address1: "",
        address2: "",
        zipCode: "",
        country: ""
      },
      formErrors: {
        agentType: null,
        fullName: null,
        phoneNo: null,
        email: null,
        website: null,
        address: null,
        address1: null,
        address2: null,
        zipCode: null,
        country: null
      }
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps: any) {
    const { isProfileDetailsSendSuccess } = this.props;
    if (
      prevProps.isProfileDetailsSendSuccess !== isProfileDetailsSendSuccess &&
      isProfileDetailsSendSuccess
    ) {
      window.scrollTo(0, 0);
      this.handleFormFieldReset();
    }
  }

  validateForm = () => {
    const { agentType, fullName, phoneNo, website, address } = this.state.form;

    let hasError = false;

    this.resetFormErrors();

    if (agentType === "") {
      this.setFormErrors("agentType", "Agent type is required.");
      hasError = true;
    }
    if (fullName === "" || fullName === null) {
      this.setFormErrors("fullName", "Agent name is required.");
      hasError = true;
    }
    if (phoneNo === "") {
      this.setFormErrors("phoneNo", "Phone number is required.");
      hasError = true;
    }
    if (website === "" || website === null) {
      this.setFormErrors("website", "Website is required.");
      hasError = true;
    }
    if (address === "" || address === null) {
      this.setFormErrors("address", "Address is required.");
      hasError = true;
    }
    return hasError;
  };

  showPlaceDetails = (place: AddressAutocompleteResponseType) => {
    const componentForm = {
      street_number: "short_name",
      route: "long_name",
      neighborhood: "long_name",
      sublocality_level_1: "long_name",
      sublocality_level_2: "long_name",
      locality: "long_name",
      administrative_area_level_2: "long_name",
      administrative_area_level_1: "long_name",
      country: "long_name",
      postal_code: "short_name"
    };
    let latitude;
    let longitude;
    let address = [];
    let country = "";
    let zipCode = "";

    if (place !== null) {
      latitude = place.geometry.location.lat();
      longitude = place.geometry.location.lng();

      for (let i = 0; i < place.address_components.length; i++) {
        let addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          if (addressType === "country") {
            country =
              place.address_components[i][componentForm[addressType]] || "";
          } else if (addressType === "postal_code") {
            zipCode =
              place.address_components[i][componentForm[addressType]] || "";
          }

          if (
            addressType === "street_number" ||
            addressType === "route" ||
            addressType === "sublocality_level_1" ||
            addressType === "administrative_area_level_1" ||
            addressType === "neighborhood" ||
            addressType === "locality" ||
            addressType === "administrative_area_level_2" ||
            addressType === "sublocality_level_2"
          ) {
            address.push(
              place.address_components[i][componentForm[addressType]] || ""
            );
          }
        }
      }
      let addressWithoutDuplicates = Array.from(new Set(address));
      let address_1 = "";
      let address_2 = "";

      addressWithoutDuplicates.map((line, key) => {
        if (key < Math.ceil(addressWithoutDuplicates.length / 2)) {
          if (line !== "") {
            if (key === Math.ceil(addressWithoutDuplicates.length / 2) - 1) {
              address_1 += line;
            } else {
              address_1 += line + ", ";
            }
          }
        } else {
          if (line !== "") {
            if (key === addressWithoutDuplicates.length - 1) {
              address_2 += line;
            } else {
              address_2 += line + ", ";
            }
          }
        }
        return true;
      });

      this.setState(({ form }) => ({
        form: {
          ...form,
          country,
          zipCode,
          address1: address_1,
          address2: address_2,
          address: place.formatted_address
        },
        latitude,
        altitude: longitude
      }));
    }
  };

  setFormErrors = (field: string, message: string) => {
    this.setState(state => {
      return {
        formErrors: {
          ...state.formErrors,
          [field]: message
        }
      };
    });
  };

  handleFormFieldReset = () => {
    this.setState(({ form }) => ({
      form: {
        ...form,
        agentType: "agency",
        fullName: this.props.fullName,
        website: "",
        address: "",
        address1: "",
        address2: "",
        zipCode: "",
        country: "",
        phoneNo: ""
      },
      isEditAddress: false,
      defaultCountryCode: "us"
    }));
  };

  resetFormErrors = () => {
    this.setState(({ formErrors }) => ({
      formErrors: {
        ...formErrors,
        agentType: null,
        fullName: null,
        email: null,
        website: null,
        address: null,
        address1: null,
        address2: null,
        zipCode: null,
        country: null
      }
    }));
  };

  handleFormFieldChange = (event: any) => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.id]: event.target.value
      }
    });
  };

  handleNameFieldChange = (event: any) => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.id]: event.target.value
      }
    });
  };

  handleFormSelectChange = (agentType: string) => {
    this.setState(({ form }) => ({
      form: {
        ...form,
        agentType: agentType
      }
    }));
  };

  handleFormSubmit = () => {
    const {
      updateAgent,
      userEmail,
      userCurrentStatus: userStatus
    } = this.props;
    if (this.validateForm()) {
      return null;
    }
    let {
      form: { agentType, ...form },
      latitude,
      altitude
    } = this.state;

    updateAgent({
      ...form,
      agentDetails: { agentType },
      latitude,
      altitude,
      userEmail,
      userStatus,
      isAgent: true
    });
  };

  handlePhoneInputOnChange = (phone: any) => {
    this.setState(({ form }) => ({
      form: {
        ...form,
        phoneNo: phone
      }
    }));
  };

  _getSubmitButton = () => {
    const { isLoading } = this.props;
    return (
      <Button
        size={Button.SIZE.MEDIUM}
        type={Button.TYPE.PRIMARY}
        onClick={this.handleFormSubmit.bind(this)}
        loading={isLoading}
      >
        SAVE
      </Button>
    );
  };

  _getBanner = () => {
    const { userCurrentStatus } = this.props;
    if (userCurrentStatus === USER_STATUS.PENDING) {
      return (
        <Alert isFullWidth={true} type={Alert.TYPE.SUCCESS}>
          Waiting for the approval from the Tambook
        </Alert>
      );
    }
  };

  _getAccreditationBanner = () => {
    const { isAccreditationDetailsSendSuccess, agentDetails } = this.props;
    if (
      isAccreditationDetailsSendSuccess === true ||
      agentDetails.accreditationStatus === ACCREDITATION_STATUS.PENDING
    ) {
      return (
        <Alert isFullWidth={true} type={Alert.TYPE.SUCCESS}>
          Waiting for the approval from the Tambook
        </Alert>
      );
    }
  };

  handleAccFormSubmit = () => {
    this.setState({
      showModal: true
    });
  };

  changeState = () => {
    this.setState({
      isEditAddress: true
    });
  };

  _onCloseModal = () => {
    this.setState({
      showModal: false
    });
  };

  render() {
    const {
      form,
      formErrors,
      showModal,
      isEditAddress,
      defaultCountryCode
    } = this.state;
    const {
      userEmail,
      userId,
      isAgentLoaded,
      isUserProfileLoaded,
      isAccreditationDetailsSendSuccess,
      updateAgentAccreditation,
      isLoading,
      agentDetails
    } = this.props;

    if (showModal === true) {
      return (
        <Modal
          className="acc-modal"
          showModal={showModal}
          onClose={this._onCloseModal}
        >
          <div className="acc-container">
            <div className="acc-content">
              <div className="acc-body">
                <div className="form-header">
                  {this._getAccreditationBanner()}
                  <div className="heading">Request to accreditation</div>
                </div>
                <div className="sub-heading">
                  You send your request with the accreditation fee $200
                </div>
                <AgentAccreditationForm
                  userId={userId}
                  isLoading={isLoading}
                  isAccreditationDetailsSendSuccess={
                    isAccreditationDetailsSendSuccess
                  }
                  accreditationStatus={agentDetails.accreditationStatus}
                  updateAgentAccreditation={updateAgentAccreditation}
                />
              </div>
            </div>
          </div>
        </Modal>
      );
    }

    if (!isUserProfileLoaded && !isAgentLoaded) {
      return <Loader isLoading />;
    } else {
      return (
        <div className="user-details">
          <Section className="section">
            {this._getBanner()}
            <SectionHeader heading="Information Details" position="left" />
            <p>
              {
                "After submitting your details we will review your profile and accept the account creation or update. You will receive an approval email. After that you will be able to book courses for your customers."
              }
            </p>
          </Section>
          <Row>
            <Col md={6} sm={12}>
              <div className="input-group">
                <label>Agent type</label>
                <Select
                  selected={form.agentType}
                  options={[
                    {
                      name: "Agency",
                      value: "agency"
                    },
                    {
                      name: "Single Representative",
                      value: "single"
                    }
                  ]}
                  error={formErrors.agentType}
                  onChange={agentType => {
                    this.handleFormSelectChange(agentType);
                  }}
                />
              </div>
            </Col>
            <Col md={6} sm={12}>
              <div className="input-group">
                <label>Agents Name</label>
                <Input
                  id="fullName"
                  placeholder="Enter your name"
                  text={form.fullName}
                  onChange={this.handleNameFieldChange.bind(this)}
                  error={formErrors.fullName}
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={6} sm={12}>
              <div className="input-group">
                <label>Phone Number</label>
                <div className="form-input tel-input-dropdown">
                  <PhoneInput
                    defaultCountry={defaultCountryCode}
                    inputClass="tel-input"
                    buttonClass="dropdown"
                    value={form.phoneNo}
                    onChange={this.handlePhoneInputOnChange}
                    error={form.phoneNo}
                  />
                  {formErrors.phoneNo !== null && (
                    <ul className="form-errors">
                      <li>{formErrors.phoneNo}</li>
                    </ul>
                  )}
                </div>
              </div>
            </Col>
            <Col md={6} sm={12}>
              <div className="input-group">
                <label>Email</label>
                <Input
                  id="email"
                  placeholder="Enter your e-mail address"
                  text={userEmail}
                  onChange={this.handleFormFieldChange.bind(this)}
                  disabled={true}
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={6} sm={12}>
              <div className="input-group">
                <label>Website</label>
                <Input
                  id="website"
                  placeholder="Enter your website"
                  text={form.website}
                  onChange={this.handleFormFieldChange.bind(this)}
                  error={formErrors.website}
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={12} sm={12}>
              <div className="input-group">
                <label>Address</label>
                {isEditAddress === true ? (
                  <AddressAutocomplete
                    onPlaceChanged={this.showPlaceDetails}
                    placeholder="Enter your address"
                    error={formErrors.address}
                  />
                ) : (
                  <Input
                    text={form.address1}
                    placeholder="Enter your address"
                    onFocus={this.changeState.bind(this)}
                    disabled={false}
                  />
                )}
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={6} sm={12}>
              <div className="input-group address">
                <label>Address Line 1</label>
                <Input
                  id="address1"
                  text={form.address1}
                  onChange={this.handleFormFieldChange.bind(this)}
                  error={formErrors.address1}
                  disabled={true}
                />
              </div>
            </Col>
            <Col md={6} sm={12}>
              <div className="input-group address">
                <label>Address Line 2</label>
                <Input
                  id="address2"
                  text={form.address2}
                  onChange={this.handleFormFieldChange.bind(this)}
                  error={formErrors.address2}
                  disabled={true}
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={6} sm={12}>
              <div className="input-group">
                <label>ZIP Code</label>
                <Input
                  className="address"
                  id="zipCode"
                  text={form.zipCode}
                  onChange={this.handleFormFieldChange.bind(this)}
                  error={formErrors.zipCode}
                  disabled={true}
                />
              </div>
            </Col>
            <Col md={6} sm={12}>
              <div className="input-group address">
                <label>Country</label>
                <Input
                  id="country"
                  text={form.country}
                  onChange={this.handleFormFieldChange.bind(this)}
                  error={formErrors.country}
                  disabled={true}
                />
              </div>
            </Col>
          </Row>
          <Row className="form-submit">
            <Col md={6} sm={12} />
            <Col md={6} sm={12}>
              {this._getSubmitButton()}
              <Button
                size={Button.SIZE.MEDIUM}
                outline
                type={Button.TYPE.INFO}
                onClick={this.handleFormFieldReset.bind(this)}
              >
                RESET
              </Button>
            </Col>
          </Row>
        </div>
      );
    }
  }
}

export default AgentInformation;
