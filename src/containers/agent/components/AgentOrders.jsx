// @flow
import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import format from "shared/utils/dates/format";
import Button from "shared/components/Button";
import Tooltip from "shared/components/Tooltip";

import Loader from "components/Loader/Loader";

import { getBookings } from "containers/agent/store/actions";
import Pagination from "shared/components/Pagination";
import { BOOKING_STATUS } from "containers/profile/constants";
import Currency from "components/Currency";

type CourseBookingProps = {
  agent: object,
  activeBookings: null | [],
  archivedBookings: null | [],
  unfinishedBookings: null | [],
  type: | typeof BOOKING_STATUS.ACTIVE
    | typeof BOOKING_STATUS.UNFINISHED
    | typeof BOOKING_STATUS.ARCHIVE,
  getBookings: Function
};

class CourseBooking extends PureComponent<CourseBookingProps> {
  constructor(props) {
    super(props);
    // $FlowFixMe
    this.getBookings = this.getBookings.bind(this);
  }

  componentDidMount() {
    this.getBookings();
  }

  getBookings({ page } = { page: 1 }) {
    const { type, getBookings } = this.props;

    getBookings(type, page);
  }

  render() {
    const {
      type,
      activeBookings,
      archivedBookings,
      unfinishedBookings
    } = this.props;

    let link;
    let data = null;

    switch (type) {
      case BOOKING_STATUS.ACTIVE:
        link = "/agent-profile/orders/active/";
        data = activeBookings;
        break;
      case BOOKING_STATUS.ARCHIVE:
        link = "/agent-profile/orders/archived/";
        data = archivedBookings;
        break;
      case BOOKING_STATUS.UNFINISHED:
        data = unfinishedBookings;
        break;
      default:
        link = "";
        break;
    }
    if (!data || data === null) {
      return <Loader />;
    }

    return (
      <div className="agent-orders-list">
        <div className="orders-list-header">
          <Row>
            {type === BOOKING_STATUS.UNFINISHED ? (
              <Fragment>
                <Col md="3" sm="12">
                  Title
                </Col>
                <Col md="3" sm="12">
                  Date
                </Col>
                <Col md="3" sm="12">
                  Status
                </Col>
                <Col md="3" sm="12" className="text-center">
                  Details
                </Col>
              </Fragment>
            ) : (
              <Fragment>
                <Col md="3" sm="12">
                  Title
                </Col>
                <Col md="2" sm="12">
                  Customer name
                </Col>
                <Col md="1" sm="12">
                  Date
                </Col>
                <Col md="1" sm="12">
                  Payment type
                </Col>
                <Col md="1" sm="12">
                  Status
                </Col>
                <Col md="2" sm="12">
                  Agent&apos;s interest
                </Col>
                <Col md="1" sm="12">
                  Total
                </Col>
                <Col md="1" sm="12" className="text-center">
                  Details
                </Col>
              </Fragment>
            )}
          </Row>
        </div>
        <div className="orders-list-body">
          {data.list.map(
            (
              {
                courseName,
                studentName,
                date,
                paymentType,
                bookingStatus,
                currency,
                agentCommission,
                agentCommissionLevel,
                orderTotal,
                bookingId,
                courseId
              },
              key
            ) => (
              <Row key={key} className="orders-list-row">
                {type === BOOKING_STATUS.UNFINISHED ? (
                  <Fragment>
                    <Col md="3" sm="12">
                      {courseName.length > 20 ? (
                        <Tooltip
                          position={Tooltip.position.BOTTOM}
                          message={courseName}
                        >
                          {courseName.substring(0, 10)}...
                        </Tooltip>
                      ) : (
                        courseName
                      )}
                    </Col>
                    <Col md="3" sm="12">
                      {format(date, "yyyy-MM-dd")}
                    </Col>
                    <Col md="3" sm="12" className="status text-capitalize">
                      {bookingStatus}
                    </Col>
                    <Col md="3" sm="12" className="text-center">
                      {type === BOOKING_STATUS.UNFINISHED ? (
                        <Button
                          size={Button.SIZE.SMALL}
                          htmlType={Button.HTML_TYPE.LINK}
                          link={`/booking/course/${bookingId}/${courseId}?selectedWeek=0&selectedRange=0`}
                        >
                          BOOK
                        </Button>
                      ) : (
                        <Button
                          size={Button.SIZE.SMALL}
                          htmlType={Button.HTML_TYPE.LINK}
                          link={link + bookingId}
                        >
                          SHOW
                        </Button>
                      )}
                    </Col>
                  </Fragment>
                ) : (
                  <Fragment>
                    <Col md="3" sm="12">
                      {courseName.length > 20 ? (
                        <Tooltip
                          position={Tooltip.position.BOTTOM}
                          message={courseName}
                        >
                          {courseName.substring(0, 10)}...
                        </Tooltip>
                      ) : (
                        courseName
                      )}
                    </Col>
                    <Col md="2" sm="12">
                      {studentName.length > 20 ? (
                        <Tooltip
                          position={Tooltip.position.BOTTOM}
                          message={studentName}
                        >
                          {studentName.substring(0, 10)}...
                        </Tooltip>
                      ) : (
                        studentName
                      )}
                    </Col>
                    <Col md="1" sm="12">
                      {format(date, "yyyy-MM-dd")}
                    </Col>
                    <Col md="1" sm="12">
                      {paymentType === "stripe" ? "Credit Card" : paymentType}
                    </Col>
                    <Col md="1" sm="12" className="status text-capitalize">
                      {bookingStatus}
                    </Col>
                    <Col md="2" sm="12">
                      <Currency currencyType={currency}>
                        {agentCommission}
                      </Currency>
                      &nbsp;({agentCommissionLevel}%)
                    </Col>
                    <Col md="1" sm="12">
                      <Currency currencyType={currency}>
                        {orderTotal / 100}
                      </Currency>
                    </Col>
                    <Col md="1" sm="12" className="text-center">
                      {type === BOOKING_STATUS.UNFINISHED ? (
                        <Button
                          size={Button.SIZE.SMALL}
                          htmlType={Button.HTML_TYPE.LINK}
                          link={`/booking/course/${bookingId}/${courseId}?selectedWeek=0&selectedRange=0`}
                        >
                          BOOK
                        </Button>
                      ) : (
                        <Button
                          size={Button.SIZE.SMALL}
                          htmlType={Button.HTML_TYPE.LINK}
                          link={link + bookingId}
                        >
                          SHOW
                        </Button>
                      )}
                    </Col>
                  </Fragment>
                )}
              </Row>
            )
          )}
        </div>
        <Pagination
          totalPages={data.pagination.total_pages}
          currentPage={data.pagination.page}
          onPageChanged={this.getBookings}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    activeBookings: state.agent.activeBookings,
    archivedBookings: state.agent.archivedBookings,
    unfinishedBookings: state.agent.unfinishedBookings
  };
}

const Actions = {
  getBookings
};

export default connect(
  mapStateToProps,
  Actions
)(CourseBooking);
