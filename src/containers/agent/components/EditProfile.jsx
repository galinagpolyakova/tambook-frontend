// @flow
import React, { Component, Fragment } from "react";
import PhoneInput from "react-phone-input-2";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Button from "shared/components/Button";
import Input from "shared/components/Input";
import Select from "shared/components/Select";
import AddressAutocomplete from "shared/components/AddressAutocomplete";
import Alert from "shared/components/Alert";

import { USER_STATUS } from "../../auth/constants";
import { AGENT_TYPE } from "../../agent/constants";

import countries from "shared/data/countries";
import locales from "shared/data/locales";

import "react-phone-input-2/dist/style.css";

type EditProfileProps = {
  fullName: string,
  userEmail: string,
  phoneNo: string,
  gender: string,
  birthDate: string,
  citizenship: string,
  nativeLanguage: string,
  agentDetails: {
    agentType: string
  },
  address: string,
  address1: string,
  address2: string,
  country: string,
  zipCode: string,
  website: string,
  isOnEdit: boolean,
  toggleEdit: Function,
  onCompleteEditing: Function,
  // FIXME:
  userStatus: string,
  userCurrentStatus: string,
  latitude: number,
  altitude: number
};

type EditProfileState = {
  fullName: string,
  userEmail: string,
  agentDetails: {
    agentType: string
  },
  phoneNo: string,
  address: string,
  address1: string,
  address2: string,
  country: string,
  zipCode: string,
  website: string,
  latitude: number,
  altitude: number
};

class EditProfile extends Component<EditProfileProps, EditProfileState> {
  constructor(props: EditProfileProps) {
    super(props);
    // eslint-disable-next-line
    const { isOnEdit, toggleEdit, onCompleteEditing, ...state } = this.props;

    this.state = {
      ...state
    };
    // $FlowFixMe
    this.handleInputChange = this.handleInputChange.bind(this);
    // $FlowFixMe
    this.handlePhoneInputOnChange = this.handlePhoneInputOnChange.bind(this);
  }

  handleInputChange(event: SyntheticInputEvent<HTMLInputElement>) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handlePhoneInputOnChange(phoneNo: any) {
    this.setState({
      phoneNo
    });
  }

  showPlaceDetails = (place: {
    address_components: Array<Object>,
    formatted_address: string
  }) => {
    const componentForm = {
      street_number: "short_name",
      route: "long_name",
      neighborhood: "long_name",
      sublocality_level_1: "long_name",
      sublocality_level_2: "long_name",
      locality: "long_name",
      administrative_area_level_2: "long_name",
      administrative_area_level_1: "long_name",
      country: "long_name",
      postal_code: "short_name"
    };

    let latitude = place.geometry.location.lat();
    let longitude = place.geometry.location.lng();
    let address = [];
    let country = "";
    let zipCode = "";
    for (let i = 0; i < place.address_components.length; i++) {
      let addressType = place.address_components[i].types[0];
      if (componentForm[addressType]) {
        if (addressType === "country") {
          country =
            place.address_components[i][componentForm[addressType]] || "";
        } else if (addressType === "postal_code") {
          zipCode =
            place.address_components[i][componentForm[addressType]] || "";
        }

        if (
          addressType === "street_number" ||
          addressType === "route" ||
          addressType === "sublocality_level_1" ||
          addressType === "administrative_area_level_1" ||
          addressType === "neighborhood" ||
          addressType === "locality" ||
          addressType === "administrative_area_level_2" ||
          addressType === "sublocality_level_2"
        ) {
          address.push(
            place.address_components[i][componentForm[addressType]] || ""
          );
        }
      }
    }

    let addressWithoutDuplicates = Array.from(new Set(address));
    let address_1 = "";
    let address_2 = "";

    addressWithoutDuplicates.map((line, key) => {
      if (key < addressWithoutDuplicates.length / 2) {
        if (line !== "") {
          if (key === addressWithoutDuplicates.length / 2 - 1) {
            address_1 += line;
          } else {
            address_1 += line + ", ";
          }
        }
      } else {
        if (line !== "") {
          if (key === addressWithoutDuplicates.length - 1) {
            address_2 += line;
          } else {
            address_2 += line + ", ";
          }
        }
      }
      return true;
    });

    this.setState({
      latitude,
      country,
      zipCode,
      altitude: longitude,
      address1: address_1,
      address2: address_2,
      address: place.formatted_address
    });
  };

  _getBanner = () => {
    const { userCurrentStatus } = this.props;
    if (userCurrentStatus === USER_STATUS.PENDING) {
      return (
        <Alert isFullWidth={true} type={Alert.TYPE.SUCCESS}>
          Waiting for the approval from the Tambook
        </Alert>
      );
    }
  };

  render() {
    const { isOnEdit } = this.props;
    const {
      fullName,
      userEmail,
      phoneNo,
      address1,
      address2,
      agentDetails: { agentType },
      address,
      country,
      zipCode,
      website
    } = this.state;
    const countryList = [];
    for (const key in countries) {
      if (countries.hasOwnProperty(key)) {
        countryList.push({
          value: key,
          name: countries[key]
        });
      }
    }
    const localeList = [];
    for (const key in locales) {
      if (locales.hasOwnProperty(key)) {
        localeList.push({
          value: key,
          name: locales[key]
        });
      }
    }
    return (
      <Row>
        {this._getBanner()}
        <Col sm={12} className="user-edit-details">
          {isOnEdit ? (
            <Fragment>
              <Row>
                <Col md={4} sm={12}>
                  Name
                </Col>
                <Col>
                  <Input
                    text={fullName}
                    onChange={this.handleInputChange}
                    id="fullName"
                  />
                </Col>
              </Row>
            </Fragment>
          ) : (
            <Row>
              <Col>
                <div className="name">{fullName}</div>
              </Col>
            </Row>
          )}
          <Row>
            <Col md={4} sm={12}>
              Email
            </Col>
            <Col>{userEmail}</Col>
          </Row>
          <Row>
            <Col md={4} sm={12}>
              Phone
            </Col>
            {isOnEdit ? (
              <Col>
                <PhoneInput
                  defaultCountry="us"
                  inputClass="tel-input"
                  buttonClass="dropdown"
                  value={phoneNo}
                  onChange={text => this.handlePhoneInputOnChange(text)}
                />
              </Col>
            ) : (
              <Col>{phoneNo}</Col>
            )}
          </Row>
          <Row>
            <Col md={4} sm={12}>
              Agent type
            </Col>
            {isOnEdit ? (
              <Col>
                <Select
                  options={[
                    {
                      name: "Agency",
                      value: "agency"
                    },
                    {
                      name: "Single Representative",
                      value: "single"
                    }
                  ]}
                  id="agentType"
                  placeholder="Select the agent type"
                  onChange={agentType =>
                    this.setState({ agentDetails: { agentType } })
                  }
                  selected={agentType}
                />
              </Col>
            ) : (
              <Col className="text-capitalize">
                {agentType === AGENT_TYPE.SINGLE
                  ? "Single Representative"
                  : "Agency"}
              </Col>
            )}
          </Row>
          <Row>
            <Col md={4} sm={12}>
              Website
            </Col>
            {isOnEdit ? (
              <Col>
                <Input
                  placeholder="Enter your website"
                  id="website"
                  text={website}
                  onChange={this.handleInputChange}
                />
              </Col>
            ) : (
              <Col>{website}</Col>
            )}
          </Row>
          <Row>
            <Col md={4} sm={12}>
              Address
            </Col>
            {isOnEdit ? (
              <Col>
                <AddressAutocomplete
                  onPlaceChanged={this.showPlaceDetails}
                  placeholder="Enter your address"
                />
                <span className="helper">
                  {
                    "Leave blank if you don't want to change it. Current Address:"
                  }
                  {this.state.address}
                </span>
              </Col>
            ) : (
              <Col>{address}</Col>
            )}
          </Row>
          {isOnEdit ? (
            <Row>
              <Col md={4} sm={12}>
                Address line 1
              </Col>
              <Col>
                <Input id="website" text={address1} disabled={true} />
              </Col>
            </Row>
          ) : null}

          {isOnEdit ? (
            <Row>
              <Col md={4} sm={12}>
                Address line 2
              </Col>
              <Col>
                <Input id="address2" text={address2} disabled={true} />
              </Col>
            </Row>
          ) : null}
          <Row>
            <Col md={4} sm={12}>
              ZIP Code
            </Col>
            {isOnEdit ? (
              <Col>
                <Input id="zipCode" text={zipCode} disabled={true} />
              </Col>
            ) : (
              <Col>{zipCode}</Col>
            )}
          </Row>
          <Row>
            <Col md={4} sm={12}>
              Country
            </Col>
            {isOnEdit ? (
              <Col>
                <Input id="country" text={country} disabled={true} />
              </Col>
            ) : (
              <Col>{country}</Col>
            )}
          </Row>
          <Button
            className="update-profile"
            onClick={
              isOnEdit
                ? () => this.props.onCompleteEditing(this.state)
                : this.props.toggleEdit
            }
          >
            {isOnEdit ? "Update Profile" : "Edit Profile"}
          </Button>
        </Col>
      </Row>
    );
  }
}

export default EditProfile;
