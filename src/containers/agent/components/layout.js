// @flow
import type { Node } from "react";
import React from "react";
import Link from "components/Link";
import classnames from "classnames";

import PageHeader from "components/PageHeader";

import "../styles.scss";

type LayoutProps = {
  currentLink: string,
  currentTitle: string,
  children: Node
};

export default function layout({
  currentLink,
  currentTitle,
  children
}: LayoutProps) {
  let title;
  let activeLink;
  let pages = [
    { link: "dashboard", title: "Dashboard" },
    { link: "orders", title: "Orders" },
    { link: "agent-information", title: "Agent information" }
  ];

  if (currentLink === "order-details") {
    title = "order-details";
    activeLink = "orders";
  } else {
    title = currentTitle;
    activeLink = currentLink;
  }
  return (
    <div className="agent-page">
      <PageHeader
        title={title}
        breadcrumbs={[
          {
            title: "agent's profile",
            link: "/agent-profile"
          },
          {
            title
          }
        ]}
      />
      <div className="container">
        <div className="header-menu">
          <ul>
            {pages.map(({ title, link }, key) => (
              <li
                key={key}
                className={classnames({ active: link === activeLink })}
              >
                <Link to={`/agent-profile/${link}`}>{title}</Link>
              </li>
            ))}
          </ul>
        </div>
        {children}
      </div>
    </div>
  );
}
