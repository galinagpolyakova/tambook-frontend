export const ACCREDITATION_STATUS = {
  DEFAULT: "default",
  PENDING: "pending",
  APPROVED: "approved"
};

export const USER_ACCOUNT_STATUS = {
  DEFAULT: "default",
  PENDING: "pending",
  APPROVED: "approved"
};

export const AGENT_TYPE = {
  AGENCY: "agency",
  SINGLE: "single"
};
