// @flow
import React, { Component } from "react";
import { connect } from "react-redux";

import Layout from "../components/layout";
import AgentInformation from "../components/AgentInformation";
import EditProfile from "../components/EditProfile";

import { USER_STATUS } from "../../auth/constants";

import {
  getAgentByEmail,
  updateAgent,
  updateAgentAccreditation
} from "../store/actions";

type AgentInfoProps = {
  isLoading: boolean,
  isProfileDetailsSendSuccess: boolean,
  agent: Object,
  // FIXME:
  userStatus: string,
  getAgentByEmail: Function,
  updateAgent: Function,
  isAgentLoaded: boolean,
  isUserProfileLoaded: boolean,
  updateAgentAccreditation: Function,
  isAccreditationDetailsSendSuccess: boolean
};

type agentInfoState = {
  isOnEdit: boolean
};

class AgentInfo extends Component<AgentInfoProps, agentInfoState> {
  constructor(props: AgentInfoProps) {
    super(props);
    this.state = {
      isOnEdit: false
    };
    // $FlowFixMe
    this._updateProfile = this._updateProfile.bind(this);
    // $FlowFixMe
    this._toggleEdit = this._toggleEdit.bind(this);
  }

  componentDidMount() {
    this.props.getAgentByEmail();
  }

  _toggleEdit() {
    this.setState({ isOnEdit: true });
  }

  _updateProfile(payload) {
    const {
      agent: { userId, userStatus }
    } = this.props;
    this.props.updateAgent({
      ...payload,
      userId,
      userStatus,
      isAgent: true
    });
    this.setState({ isOnEdit: false });
  }

  render() {
    const { isOnEdit } = this.state;
    const {
      updateAgent,
      agent,
      isLoading,
      isProfileDetailsSendSuccess,
      isAgentLoaded,
      updateAgentAccreditation,
      isAccreditationDetailsSendSuccess,
      isUserProfileLoaded,
      userStatus
    } = this.props;
    return (
      <Layout currentLink="agent-information" currentTitle="Agent information">
        <div className="agent-information">
          {agent !== null &&
            (userStatus === USER_STATUS.DEFAULT ? (
              <AgentInformation
                {...agent}
                isAgentLoaded={isAgentLoaded}
                updateAgent={updateAgent}
                isProfileDetailsSendSuccess={isProfileDetailsSendSuccess}
                isLoading={isLoading}
                userCurrentStatus={userStatus}
                updateAgentAccreditation={updateAgentAccreditation}
                isAccreditationDetailsSendSuccess={
                  isAccreditationDetailsSendSuccess
                }
                isUserProfileLoaded={isUserProfileLoaded}
              />
            ) : (
              <EditProfile
                {...agent}
                userCurrentStatus={userStatus}
                isOnEdit={isOnEdit}
                toggleEdit={this._toggleEdit}
                onCompleteEditing={this._updateProfile}
              />
            ))}
        </div>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    agent: state.agent.data,
    isProfileDetailsSendSuccess: state.agent.isProfileDetailsSendSuccess,
    isLoading: state.agent.isLoading,
    isAgentLoaded: state.agent.isAgentLoaded,
    isUserProfileLoaded: state.agent.isUserProfileLoaded,
    isAccreditationDetailsSendSuccess:
      state.agent.isAccreditationDetailsSendSuccess,
    userStatus: state.auth.status
  };
}

const Actions = {
  getAgentByEmail,
  updateAgent,
  updateAgentAccreditation
};

export default connect(
  mapStateToProps,
  Actions
)(AgentInfo);
