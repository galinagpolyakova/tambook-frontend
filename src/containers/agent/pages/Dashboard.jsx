// @flow
import React, { Component } from "react";

import { type BookingSummeryType } from "../types";

import Loader from "components/Loader";
import Layout from "../components/layout";
import AgentDashboard from "../components/AgentDashboard";

import { connect } from "react-redux";
import {
  updateAgentAccreditation,
  getAgentByEmail,
  getBookingSummery
} from "../store/actions";

type DashboardProps = {
  getAgentByEmail: Function,
  getBookingSummery: Function,
  agent: Object,
  isLoading: boolean,
  isBookingSummeryLoading: boolean,
  updateAgentAccreditation: Function,
  isAccreditationDetailsSendSuccess: boolean,
  bookingSummery: BookingSummeryType
};

type DashboardState = {};

class Dashboard extends Component<DashboardProps, DashboardState> {
  componentDidMount() {
    this.props.getAgentByEmail();
  }

  render() {
    const {
      agent,
      bookingSummery,
      getBookingSummery,
      updateAgentAccreditation,
      isAccreditationDetailsSendSuccess,
      isBookingSummeryLoading,
      isLoading
    } = this.props;

    return (
      <Layout currentLink="dashboard" currentTitle="Dashboard">
        <div className="dashboard">
          {isLoading ? (
            <Loader isLoading />
          ) : (
            agent !== null && (
              <AgentDashboard
                getBookingSummery={getBookingSummery}
                {...agent}
                {...bookingSummery}
                isLoading={isLoading}
                isBookingSummeryLoading={isBookingSummeryLoading}
                updateAgentAccreditation={updateAgentAccreditation}
                isAccreditationDetailsSendSuccess={
                  isAccreditationDetailsSendSuccess
                }
              />
            )
          )}
        </div>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    agent: state.agent.data,
    isLoading: state.agent.isLoading,
    isAccreditationDetailsSendSuccess:
      state.agent.isAccreditationDetailsSendSuccess,
    bookingSummery: state.agent.bookingSummery,
    isBookingSummeryLoading: state.agent.isBookingSummeryLoading
  };
}

const Actions = {
  updateAgentAccreditation,
  getAgentByEmail,
  getBookingSummery
};

export default connect(
  mapStateToProps,
  Actions
)(Dashboard);
