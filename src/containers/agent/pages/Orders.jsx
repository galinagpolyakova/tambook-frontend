// @flow
import React, { Component } from "react";
import { connect } from "react-redux";

import Tabs from "shared/components/Tabs";

import Loader from "components/Loader";

import Layout from "../components/layout";
import AgentOrders from "../components/AgentOrders";
import { BOOKING_STATUS } from "containers/profile/constants";

import { getAgentByEmail } from "../store/actions";

type OrdersProps = {
  agent: null | object,
  getAgentByEmail: Function
};

class Orders extends Component<OrdersProps> {
  componentDidMount() {
    this.props.getAgentByEmail();
  }
  render() {
    const { agent } = this.props;
    if (agent === null) {
      return <Loader />;
    }
    return (
      <Layout currentLink="orders" currentTitle="Orders">
        <div className="agent-orders">
          <Tabs
            items={[
              {
                title: "Active",
                content: (
                  <AgentOrders type={BOOKING_STATUS.ACTIVE} agent={agent} />
                )
              },
              {
                title: "Unfinished",
                content: (
                  <AgentOrders type={BOOKING_STATUS.UNFINISHED} agent={agent} />
                )
              },
              {
                title: "Archived",
                content: (
                  <AgentOrders type={BOOKING_STATUS.ARCHIVE} agent={agent} />
                )
              }
            ]}
          />
        </div>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    agent: state.agent.data
  };
}

const Actions = {
  getAgentByEmail
};

export default connect(
  mapStateToProps,
  Actions
)(Orders);
