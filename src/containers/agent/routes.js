// import lib
import { lazy } from "react";
import { USER_TYPE } from "containers/auth/constants";

export default [
  {
    path: "/agent-profile/",
    exact: true,
    auth: true,
    roles: [USER_TYPE.AGENT],
    component: lazy(() => import("./pages/AgentInfo"))
  },
  {
    path: "/agent-profile/agent-information",
    exact: true,
    auth: true,
    roles: [USER_TYPE.AGENT],
    component: lazy(() => import("./pages/AgentInfo"))
  },
  {
    path: "/agent-profile/dashboard",
    exact: true,
    auth: true,
    roles: [USER_TYPE.AGENT],
    component: lazy(() => import("./pages/Dashboard"))
  },
  {
    path: "/agent-profile/orders",
    exact: true,
    auth: true,
    roles: [USER_TYPE.AGENT],
    component: lazy(() => import("./pages/Orders"))
  },
  {
    path: "/agent-profile/orders/:status/:id",
    exact: true,
    auth: true,
    roles: [USER_TYPE.AGENT],
    component: lazy(() => import("../profile/pages/Booking"))
  }
];
