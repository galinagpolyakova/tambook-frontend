// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";

export class AgentService {
  api: ApiServiceInterface;

  endpoint: string = "/agent";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);
    return response;
  }

  getAgentByEmail(payload: Object) {
    return this.api.post(`/user/search`, payload);
  }

  uploadUserDocument(payload: Object) {
    return this.api.post(`/user/uploads`, payload);
  }

  updateAgentAccreditationDetails(payload: Object) {
    return this.api.patch(`${this.endpoint}/update-accreditation`, payload);
  }
}
