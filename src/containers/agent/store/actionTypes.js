export const PREFIX = "@agent/";

export const GET_USER_PROFILE_SUCCESS = `${PREFIX}GET_USER_PROFILE_SUCCESS`;

export const GET_AGENT_BY_EMAIL_INIT = `${PREFIX}GET_AGENT_BY_EMAIL_INIT`;
export const GET_AGENT_BY_EMAIL_SUCCESS = `${PREFIX}GET_AGENT_BY_EMAIL_SUCCESS`;
export const GET_AGENT_BY_EMAIL_FAILURE = `${PREFIX}GET_AGENT_BY_EMAIL_FAILURE`;

export const UPDATE_AGENT_INIT = `${PREFIX}UPDATE_AGENT_INIT`;
export const UPDATE_AGENT_SUCCESS = `${PREFIX}UPDATE_AGENT_SUCCESS`;
export const UPDATE_AGENT_FAILURE = `${PREFIX}UPDATE_AGENT_FAILURE`;

export const UPDATE_AGENT_ACCREDITATION_INIT = `${PREFIX}UPDATE_AGENT_ACCREDITATION_INIT`;
export const UPDATE_AGENT_ACCREDITATION_SUCCESS = `${PREFIX}UPDATE_AGENT_ACCREDITATION_SUCCESS`;
export const UPDATE_AGENT_ACCREDITATION_FAILURE = `${PREFIX}UPDATE_AGENT_ACCREDITATION_FAILURE`;

export const GET_BOOKINGS_INIT = `${PREFIX}GET_BOOKINGS_INIT`;
export const GET_BOOKINGS_SUCCESS = `${PREFIX}GET_BOOKINGS_SUCCESS`;
export const GET_BOOKINGS_FAILURE = `${PREFIX}GET_BOOKINGS_FAILURE`;

export const GET_BOOKING_INIT = `${PREFIX}GET_BOOKING_INIT`;
export const GET_BOOKING_SUCCESS = `${PREFIX}GET_BOOKING_SUCCESS`;
export const GET_BOOKING_FAILURE = `${PREFIX}GET_BOOKING_FAILURE`;

export const GET_BOOKING_SUMMERY_INIT = `${PREFIX}GET_BOOKING_SUMMERY_INIT`;
export const GET_BOOKING_SUMMERY_SUCCESS = `${PREFIX}GET_BOOKING_SUMMERY_SUCCESS`;
export const GET_BOOKING_SUMMERY_FAILURE = `${PREFIX}GET_BOOKING_SUMMERY_FAILURE`;

export const CLEAR_AGENT_DATA = `${PREFIX}CLEAR_AGENT_DATA`;
