import { Auth } from "aws-amplify";

import { SITE_URL_WITH_LANG } from "config/app";
import { USER_STATUS } from "../../auth/constants";
import {
  GET_AGENT_BY_EMAIL_INIT,
  GET_AGENT_BY_EMAIL_SUCCESS,
  GET_AGENT_BY_EMAIL_FAILURE,
  UPDATE_AGENT_INIT,
  UPDATE_AGENT_FAILURE,
  UPDATE_AGENT_SUCCESS,
  UPDATE_AGENT_ACCREDITATION_SUCCESS,
  UPDATE_AGENT_ACCREDITATION_FAILURE,
  UPDATE_AGENT_ACCREDITATION_INIT,
  GET_BOOKINGS_INIT,
  GET_BOOKINGS_SUCCESS,
  GET_BOOKINGS_FAILURE,
  GET_BOOKING_INIT,
  GET_BOOKING_SUCCESS,
  GET_BOOKING_FAILURE,
  GET_BOOKING_SUMMERY_INIT,
  GET_BOOKING_SUMMERY_SUCCESS,
  GET_BOOKING_SUMMERY_FAILURE,
  CLEAR_AGENT_DATA
} from "./actionTypes";
import { changeUserStatus } from "../../auth/store/action";

Auth.configure({
  auth0: {
    domain: "tambook.au.auth0.com",
    clientID: "zIrPXSnI9nFKV459IJ7Ure8VYvzYQAbG",
    redirectUri: SITE_URL_WITH_LANG,
    responseType: "token id_token", // for now we only support implicit grant flow
    scope: "openid profile email", // the scope used by your app
    rredirectUri: SITE_URL_WITH_LANG
  }
});

function getAgentByEmailInit() {
  return {
    type: GET_AGENT_BY_EMAIL_INIT
  };
}

function getAgentByEmailSuccess(payload) {
  return {
    type: GET_AGENT_BY_EMAIL_SUCCESS,
    payload
  };
}

function getAgentByEmailFailure(payload) {
  return {
    type: GET_AGENT_BY_EMAIL_FAILURE,
    payload
  };
}

export function getAgentByEmail() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getAgentByEmailInit());
    let AgentService = serviceManager.get("AgentService");
    Auth.currentAuthenticatedUser().then(({ attributes: { email } }) => {
      AgentService.getAgentByEmail({ email: email })
        .then(agent => {
          dispatch(getAgentByEmailSuccess(agent));
        })
        .catch(error => {
          dispatch(getAgentByEmailFailure({ error }));
        });
    });
  };
}

function updateAgentInit() {
  return {
    type: UPDATE_AGENT_INIT
  };
}

function updateAgentAccreditationSuccess(payload) {
  return {
    type: UPDATE_AGENT_ACCREDITATION_SUCCESS,
    payload
  };
}

function updateAgentAccreditationFailure(payload) {
  return {
    type: UPDATE_AGENT_ACCREDITATION_FAILURE,
    payload
  };
}

function updateAgentAccreditationInit() {
  return {
    type: UPDATE_AGENT_ACCREDITATION_INIT
  };
}

export function updateAgentAccreditation(data) {
  return (dispatch, getState, serviceManager) => {
    dispatch(updateAgentAccreditationInit());
    let AgentService = serviceManager.get("AgentService");
    AgentService.updateAgentAccreditationDetails(data)
      .then(res => {
        dispatch(updateAgentAccreditationSuccess(res));
      })
      .catch(error => {
        dispatch(updateAgentAccreditationFailure({ error }));
      });
  };
}

function updateAgentSuccess(payload) {
  return {
    type: UPDATE_AGENT_SUCCESS,
    payload
  };
}

function updateAgentFailure(payload) {
  return {
    type: UPDATE_AGENT_FAILURE,
    payload
  };
}

export function updateAgent(data) {
  let cognitoAttributes;

  if (data.userStatus === USER_STATUS.DEFAULT) {
    cognitoAttributes = {
      "custom:status": USER_STATUS.PENDING
    };
  } else {
    cognitoAttributes = {
      "custom:status": data.userStatus
    };
  }

  return (dispatch, getState, serviceManager) => {
    dispatch(updateAgentInit());
    let AuthService = serviceManager.get("AuthService");
    AuthService.updateUser(data)
      .then(() => {
        Auth.currentAuthenticatedUser().then(user => {
          Auth.updateUserAttributes(user, cognitoAttributes).then(() => {
            Auth.currentAuthenticatedUser({ bypassCache: true }).then(
              ({ attributes: { "custom:status": status } }) => {
                dispatch(updateAgentSuccess(data));
                dispatch(changeUserStatus({ status }));
              }
            );
          });
        });
      })
      .catch(error => {
        dispatch(updateAgentFailure({ error }));
      });
  };
}

function getBookingsInit(payload) {
  return {
    type: GET_BOOKINGS_INIT,
    payload
  };
}

function getBookingsSuccess(payload) {
  return {
    type: GET_BOOKINGS_SUCCESS,
    payload
  };
}

function getBookingsFailure(payload) {
  return {
    type: GET_BOOKINGS_FAILURE,
    payload
  };
}

export function getBookings(type, page) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getBookingsInit({ type }));
    let bookingService = serviceManager.get("BookingService");
    bookingService
      .getAgentBooking({ type, page })
      .then(bookings => {
        dispatch(getBookingsSuccess({ type, bookings }));
      })
      .catch(() => {
        dispatch(getBookingsFailure({ type }));
      });
  };
}

function getBookingSummeryInit() {
  return {
    type: GET_BOOKING_SUMMERY_INIT
  };
}

function getBookingSummerySuccess(payload) {
  return {
    type: GET_BOOKING_SUMMERY_SUCCESS,
    payload
  };
}

function getBookingSummeryFailure(payload) {
  return {
    type: GET_BOOKING_SUMMERY_FAILURE,
    payload
  };
}

export function getBookingSummery(data) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getBookingSummeryInit());
    let bookingService = serviceManager.get("BookingService");
    bookingService
      .getAgentBookingSummery(data)
      .then(result => {
        dispatch(getBookingSummerySuccess(result.response.data));
      })
      .catch(err => {
        dispatch(getBookingSummeryFailure(err));
      });
  };
}

function getBookingInit() {
  return {
    type: GET_BOOKING_INIT
  };
}

function getBookingSuccess(payload) {
  return {
    type: GET_BOOKING_SUCCESS,
    payload
  };
}

function getBookingFailure(payload) {
  return {
    type: GET_BOOKING_FAILURE,
    payload
  };
}

export function getBooking(bookingId) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getBookingInit());
    let BookingService = serviceManager.get("BookingService");
    BookingService.getBooking(bookingId)
      .then(res => {
        let data = res.result;
        dispatch(
          getBookingSuccess({
            bookingId,
            data
          })
        );
      })
      .catch(err => {
        dispatch(getBookingFailure(err));
      });
  };
}

export function clearAgentData() {
  return {
    type: CLEAR_AGENT_DATA
  };
}
