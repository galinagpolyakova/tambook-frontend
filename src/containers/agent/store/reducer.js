// @flow
import type { Action } from "shared/types/ReducerAction";
import { type PaginationType } from "containers/agent/store/node_modules/shared/components/Pagination";
import {
  GET_AGENT_BY_EMAIL_SUCCESS,
  GET_AGENT_BY_EMAIL_FAILURE,
  GET_AGENT_BY_EMAIL_INIT,
  UPDATE_AGENT_INIT,
  UPDATE_AGENT_FAILURE,
  UPDATE_AGENT_SUCCESS,
  UPDATE_AGENT_ACCREDITATION_SUCCESS,
  UPDATE_AGENT_ACCREDITATION_FAILURE,
  UPDATE_AGENT_ACCREDITATION_INIT,
  GET_BOOKINGS_INIT,
  GET_BOOKINGS_SUCCESS,
  GET_BOOKINGS_FAILURE,
  GET_BOOKING_INIT,
  GET_BOOKING_SUCCESS,
  GET_BOOKING_FAILURE,
  GET_BOOKING_SUMMERY_INIT,
  GET_BOOKING_SUMMERY_SUCCESS,
  GET_BOOKING_SUMMERY_FAILURE,
  CLEAR_AGENT_DATA
} from "./actionTypes";
import { BOOKING_STATUS } from "containers/profile/constants";

export type AgentState = {
  isLoading: boolean,
  isAgentLoading: boolean,
  isBookingSummeryLoading: boolean,
  isLoaded: boolean,
  isAgentLoaded: boolean,
  isUserProfileLoaded: boolean,
  isProfileDetailsSendSuccess: boolean,
  isAccreditationDetailsSendSuccess: boolean,
  error: null | string,
  data: {
    website: string,
    userEmail: string,
    altitude: string,
    gender: string,
    nativeLanguage: string,
    birthDate: string,
    citizenship: string,
    userType: string,
    lastName: string,
    createdAt: number,
    phoneNo: string,
    country: string,
    firstName: string,
    agentDetails: {
      accreditationStatus: string,
      agentLevel: string,
      agentCommission: number,
      commissionType: string,
      agentType: string
    },
    city: string,
    updatedAt: number,
    userId: number,
    accreditationInfo: {
      attachedDocument: [],
      message: string,
      paymentMethod: string,
      stripeToken: string,
      phoneNumber: string,
      isAgreed: false
    },
    zipCode: string,
    userStatus: string,
    latitude: string,
    address1: string,
    address2: string
  } | null,
  uploadDocResult: Object,
  booking: Object,
  pagination: PaginationType,
  bookingTotal: number,
  bookingSummery: Object
};

const initialState: AgentState = {
  isLoading: false,
  isLoaded: false,
  isAgentLoaded: false,
  isAgentLoading: false,
  isUploading: false,
  isUserProfileLoaded: false,
  isBookingSummeryLoading: false,
  isProfileDetailsSendSuccess: false,
  isAccreditationDetailsSendSuccess: false,
  error: null,
  uploadDocResult: {
    success: false,
    url: null,
    path: null,
    name: null
  },
  data: null,
  booking: {
    bookingId: null,
    data: null
  },
  pagination: null,
  bookingTotal: 0,
  bookingSummery: {
    successOrders: 0,
    pendingOrders: 0,
    totalWeeks: 0,
    students: 0,
    sales: 0,
    revenue: 0
  },
  activeBookings: null,
  archivedBookings: null,
  unfinishedBookings: null
};

function getAgentByEmailInit(state) {
  return {
    ...state,
    isLoading: true,
    isAgentLoading: true
  };
}

function getAgentByEmailSuccess(state: AgentState, data) {
  return {
    ...state,
    isAgentLoaded: true,
    isLoading: false,
    isAgentLoading: false,
    data
  };
}

function getAgentByEmailFailure(state: AgentState, { error }): AgentState {
  return {
    ...state,
    isLoading: false,
    isAgentLoading: false,
    error
  };
}

function updateAgentInit(state) {
  return {
    ...state,
    isLoading: true,
    isProfileDetailsSendSuccess: false
  };
}

function updateAgentSuccess(state: AgentState, payload) {
  return {
    ...state,
    isLoading: false,
    isProfileDetailsSendSuccess: true,
    data: {
      ...state.data,
      ...payload
    }
  };
}

function updateAgentFailure(state: AgentState, { error }): AgentState {
  return {
    ...state,
    isLoading: false,
    isProfileDetailsSendSuccess: false,
    error
  };
}

function updateAgentAccreditationSuccess(state: AgentState) {
  return {
    ...state,
    isLoading: false,
    isAccreditationDetailsSendSuccess: true
  };
}

function updateAgentAccreditationFailure(
  state: AgentState,
  { error }
): AgentState {
  return {
    ...state,
    isLoading: false,
    isAccreditationDetailsSendSuccess: false,
    error
  };
}

function updateAgentAccreditationInit(state) {
  return {
    ...state,
    isLoading: true,
    isAccreditationDetailsSendSuccess: false
  };
}

function getBookingsSuccess(state: AgentState, { type, bookings }) {
  let dataType = "";

  switch (type) {
    case BOOKING_STATUS.ACTIVE:
      dataType = "activeBookings";
      break;
    case BOOKING_STATUS.ARCHIVE:
      dataType = "archivedBookings";
      break;
    case BOOKING_STATUS.UNFINISHED:
      dataType = "unfinishedBookings";
      break;
    default:
      dataType = undefined;
      break;
  }
  return {
    ...state,
    loading: false,
    [dataType]: bookings
  };
}

function getBookingsInit(state: AgentState, { type }) {
  let dataType = "";

  switch (type) {
    case BOOKING_STATUS.ACTIVE:
      dataType = "activeBookings";
      break;
    case BOOKING_STATUS.ARCHIVE:
      dataType = "archivedBookings";
      break;
    case BOOKING_STATUS.UNFINISHED:
      dataType = "unfinishedBookings";
      break;
    default:
      dataType = undefined;
      break;
  }
  return {
    ...state,
    loading: true,
    [dataType]: null
  };
}

function getBookingsFailure(state: AgentState, { type }) {
  let dataType = "";

  switch (type) {
    case BOOKING_STATUS.ACTIVE:
      dataType = "activeBookings";
      break;
    case BOOKING_STATUS.ARCHIVE:
      dataType = "archivedBookings";
      break;
    case BOOKING_STATUS.UNFINISHED:
      dataType = "unfinishedBookings";
      break;
    default:
      dataType = undefined;
      break;
  }
  return {
    ...state,
    loading: false,
    [dataType]: null
  };
}

function getBookingInit(state: AgentState) {
  return {
    ...state,
    isLoading: true
  };
}

function getBookingSuccess(state: AgentState, { bookingId, data }) {
  return {
    ...state,
    isLoading: false,
    isLoaded: true,
    booking: {
      bookingId,
      data
    }
  };
}

function getBookingFailure(state: AgentState, { error }) {
  return {
    ...state,
    error,
    isLoading: false,
    booking: []
  };
}

function getBookingSummeryInit(state) {
  return {
    ...state,
    isBookingSummeryLoading: true
  };
}

function getBookingSummerySuccess(state: AgentState, bookingSummery) {
  return {
    ...state,
    isBookingSummeryLoading: false,
    bookingSummery
  };
}

function getBookingSummeryFailure(state: AgentState, { error }): AgentState {
  return {
    ...state,
    isBookingSummeryLoading: false,
    error
  };
}

const reducer = (
  state: AgentState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case UPDATE_AGENT_INIT:
      return updateAgentInit(state);
    case GET_AGENT_BY_EMAIL_INIT:
      return getAgentByEmailInit(state);
    case GET_AGENT_BY_EMAIL_SUCCESS:
      return getAgentByEmailSuccess(state, payload);
    case GET_AGENT_BY_EMAIL_FAILURE:
      return getAgentByEmailFailure(state, payload);
    case UPDATE_AGENT_SUCCESS:
      return updateAgentSuccess(state, payload);
    case UPDATE_AGENT_FAILURE:
      return updateAgentFailure(state, payload);
    case UPDATE_AGENT_ACCREDITATION_SUCCESS:
      return updateAgentAccreditationSuccess(state);
    case UPDATE_AGENT_ACCREDITATION_FAILURE:
      return updateAgentAccreditationFailure(state, payload);
    case UPDATE_AGENT_ACCREDITATION_INIT:
      return updateAgentAccreditationInit(state);
    case GET_BOOKINGS_INIT:
      return getBookingsInit(state, payload);
    case GET_BOOKINGS_SUCCESS:
      return getBookingsSuccess(state, payload);
    case GET_BOOKINGS_FAILURE:
      return getBookingsFailure(state, payload);
    case GET_BOOKING_INIT:
      return getBookingInit(state);
    case GET_BOOKING_SUCCESS:
      return getBookingSuccess(state, payload);
    case GET_BOOKING_FAILURE:
      return getBookingFailure(state, payload);
    case GET_BOOKING_SUMMERY_INIT:
      return getBookingSummeryInit(state);
    case GET_BOOKING_SUMMERY_SUCCESS:
      return getBookingSummerySuccess(state, payload);
    case GET_BOOKING_SUMMERY_FAILURE:
      return getBookingSummeryFailure(state, payload);
    case CLEAR_AGENT_DATA:
      return (state = initialState);
    default:
      return state;
  }
};

export default reducer;
