// @flow
import { type ApplicationState } from "store/reducers";

export const getBooking = (state: ApplicationState) => state.agent.booking;
export const getBookingData = (state: ApplicationState) =>
  state.agent.booking.data;
