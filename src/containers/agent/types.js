// @flow
export type Order = {
  id: string,
  title: string,
  customerName: string,
  date: string,
  paymentType: string,
  status: string,
  agentsInterest: number,
  total: number,
  details: string
};
export type OrderList = Array<Order>;
export type UserType = {
  name: string,
  email: string,
  status: string
};

export type BookingSummeryType = {
  successOrders: 0,
  pendingOrders: 0,
  totalWeeks: 0,
  students: 0,
  femaleStudent: 0,
  sales: 0,
  revenue: 0
};

export type UploadDocResultType = {
  success: boolean,
  url: string,
  path: string,
  name: string
};

export type CurrentAgentType = {
  accreditationInfo: {
    attachedDocument: [],
    message: string,
    paymentMethod: string,
    cardNumber: string,
    cardholderName: string,
    expiryMonth: string,
    expiryYear: string,
    cvc: string,
    phoneNumber: string,
    isAgreed: boolean
  },
  address: string,
  address1: string,
  address2: string,
  agentDetails: {
    accreditationStatus: string,
    agentLevel: string,
    agentType: string
  },
  altitude: string,
  citizenship: string,
  city: string,
  country: string,
  createdAt: number,
  firstName: string,
  gender: string,
  lastName: string,
  latitude: string,
  phoneNo: string,
  updatedAt: number,
  userEmail: string,
  userId: string,
  userStatus: string,
  userType: string,
  website: string,
  zipCode: string,
  birthDate: string,
  nativeLanguage: string
};

export type Booking = {
  bookingId: string,
  title: string,
  details: string,
  createdDate: string,
  bookingStatus: string,
  total: number,
  paymentType: string,
  bookingSummery: {
    currency: string,
    agentCommission: number
  },
  studentInfo: {
    dob: string,
    email: string,
    firstName: string,
    lastName: string,
    phoneNo: string
  },
  course: {
    courseShortName: string
  },
  orderTotal: number
};

export type BookingList = Array<Booking>;

export type StudentInfoType = {
  firstName: string,
  lastName: string,
  dob: string,
  email: string,
  phoneNo: string,
  passportImage: string,
  country: string
};
export type BookingType = {
  userEmail: string,
  createdAt: number,
  studentInfo: StudentInfoType,
  schoolId: string,
  travelDetails: {},
  bookingId: string,
  school: {
    country: string,
    zipCode: string,
    courseType: [],
    secondaryAddress: [],
    code: string,
    contactInfo: {
      WebAddress: string,
      ContactNumber: string,
      Email: string,
      prefix: string
    },
    minStudentAge: number,
    city: string,
    txt_preview_RU: string,
    imageAnonse: string,
    txt_preview_PT: string,
    latitude: number,
    placeId: string,
    txt_detail_PT: string,
    rating: number,
    txt_detail_RU: string,
    contactPerson: {
      ContactNumber: string,
      LastName: string,
      Email: string,
      FirstName: string,
      prefix: string
    },
    language: [],
    secondaryContact: [],
    reviews: [],
    videoUrl: string,
    logo: string,
    id: string,
    schoolName: string,
    NZQA_catogory: number,
    enrolmentFee: number,
    updatedAt: number,
    longitude: number,
    images: [],
    txt_preview_EN: string,
    address: string,
    txt_detail_EN: string,
    address1: string,
    txt_detail_ES: string,
    active: string,
    paymentSetting: {
      stripe: {
        bank_account: string
      }
    },
    url: string,
    documentList: [],
    name: string,
    txt_preview_ES: string,
    accommodation: [],
    location: number,
    facilities: []
  },
  paymentStatus: string,
  course: {
    currency: string,
    country: string,
    minCourseLength: string,
    courseType: string,
    minStudentAge: number,
    textAnonse_pt: string,
    minEnglishLevel: string,
    textFull_es: string,
    rating: string,
    language: [],
    courseShortName: string,
    workEligibility: boolean,
    reviews: [],
    tambookDiscount: string,
    school: string,
    mnemonicCode: string,
    textFull_en: string,
    id: string,
    images: [],
    textAnonse_en: string,
    workEligibilityDiscription: string,
    created: string,
    imageFull: string,
    active: string,
    numberOfStudents: number,
    startDates_noFormated: string,
    textFull_pt: string,
    schoolDetail: {
      name: string,
      activities: [],
      shortDescription: string
    },
    name: string,
    lastModified: string,
    courseDiscount: [],
    city: string,
    imageAnonse: string,
    textAnonse_ru: string,
    isfixedPrice: boolean,
    hoursNumber: number,
    isfeatured: boolean,
    isDeleted: string,
    coursePrice: [],
    priceFrom: string,
    enrollmentFee: number,
    textAnonse_es: string,
    workEligibilityHoursPerWeek: string,
    maxCourseLength: string,
    url: string,
    textFull_ru: string,
    intensity: number,
    schedule: [],
    startDates: [],
    location: number
  },
  updatedAt: number,
  courseId: string,
  paymentLog: {
    log: {
      balance_transaction: string,
      metadata: {},
      livemode: boolean,
      destination: string,
      description: string,
      failure_message: string,
      fraud_details: {},
      source: {
        address_zip_check: string,
        country: string,
        last4: string,
        funding: string,
        metadata: {},
        address_country: string,
        address_state: string,
        exp_month: number,
        exp_year: number,
        address_city: string,
        tokenization_method: string,
        cvc_check: string,
        address_line2: string,
        address_line1: string,
        fingerprint: string,
        name: string,
        id: string,
        address_line1_check: string,
        address_zip: string,
        dynamic_last4: string,
        brand: string,
        object: string,
        customer: null
      },
      amount_refunded: number,
      refunds: {
        has_more: false,
        data: [],
        total_count: number,
        url: string,
        object: string
      },
      statement_descriptor: string,
      transfer_data: string,
      receipt_url: string,
      shipping: string,
      review: string,
      captured: boolean,
      currency: string,
      refunded: boolean,
      id: string,
      outcome: {
        reason: string,
        risk_level: string,
        seller_message: string,
        network_status: string,
        type: string,
        risk_score: number
      },
      order: string,
      dispute: string,
      amount: number,
      failure_code: string,
      transfer_group: string,
      on_behalf_of: string,
      created: number,
      source_transfer: string,
      receipt_number: string,
      application: string,
      receipt_email: string,
      paid: true,
      application_fee: string,
      payment_intent: string,
      invoice: string,
      application_fee_amount: number,
      object: string,
      customer: string,
      status: string
    },
    paymentParams: {
      description: string,
      amount: number,
      currency: string,
      application_fee: number,
      source: string,
      receipt_email: string
    }
  },
  accommodation: {
    alergies: string,
    endDate: string,
    foodRestrictions: string,
    homeStayWeeks: number,
    medicalConditions: string,
    olderAdults: string,
    olderChildren: string,
    pets: string,
    smoke: string,
    startDate: string,
    type: string,
    youngChildren: number
  },
  bookingStatus: string,
  id: string,
  bookingSummery: {
    agentCommission: number,
    bookingOwner: string,
    bookedBy: string,
    weeks: number,
    stripeCharges: number,
    tambookCharge: number,
    cardToken: string,
    bookingId: string,
    paymentType: string,
    weekDiscount: number,
    schoolId: string,
    accommodationPrice: number,
    paymentMethod: string,
    currency: string,
    weekPrice: number,
    enrolmentFee: number,
    startDate: string
  }
};
