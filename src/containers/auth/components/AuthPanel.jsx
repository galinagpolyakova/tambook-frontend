// @flow
import React, { Component, Fragment } from "react";
import { withFederated } from "aws-amplify-react";

import Input from "shared/components/Input";
import Icon from "shared/components/Icon";
import Button from "shared/components/Button";
import Checkbox from "shared/components/Checkbox";
import translate from "translations/translate";
import Alert from "shared/components/Alert";
import config from "config/amplify";

import { hasWhiteSpace } from "shared/utils";
import { isEmail, isName } from "shared/kernel/cast";
import { AUTH_TYPE } from "../constants";

type AuthPanelPropsType = {
  isLoading: boolean,
  loadingAgent: boolean,
  loadingStudent: boolean,
  notifications: null | Object,
  isCapsLockActive: boolean,
  onSignInSubmit: Function,
  onSignUpSubmit: Function,
  toggleAuthType: Function,
  handleAuthStateChange: Function,
  authType: string,
  forgotPassword: Function,
  forgotPasswordSubmit: Function,
  isSubmitted: boolean
};

type AuthPanelState = {
  values: {
    email: string,
    password: string,
    name: string,
    code: string,
    newPassword: string,
    confirmNewPassword: string
  },
  errors: {
    email: null | string,
    password: null | string,
    name: null | string,
    code: null | string,
    newPassword: null | string,
    confirmNewPassword: null | string
  }
};

class AuthPanel extends Component<AuthPanelPropsType, AuthPanelState> {
  constructor() {
    super();

    this.state = {
      values: {
        email: "",
        password: "",
        name: "",
        code: "",
        newPassword: "",
        confirmNewPassword: ""
      },
      errors: {
        email: null,
        password: null,
        name: null,
        code: null,
        newPassword: null,
        confirmNewPassword: null
      }
    };
    // $FlowFixMe
    this.resetForm = this.resetForm.bind(this);
    // $FlowFixMe
    this.validateForm = this.validateForm.bind(this);
    // $FlowFixMe
    this.setFormErrors = this.setFormErrors.bind(this);
    // $FlowFixMe
    this.handleInputChange = this.handleInputChange.bind(this);
    // $FlowFixMe
    this.handleSignInSubmit = this.handleSignInSubmit.bind(this);
    // $FlowFixMe
    this.getNotifications = this.getNotifications.bind(this);
    // $FlowFixMe
    this.handleSignUpSubmit = this.handleSignUpSubmit.bind(this);
    // $FlowFixMe
    this.toggleAuthType = this.toggleAuthType.bind(this);
    // $FlowFixMe
    this.handleForgotPasswordSubmit = this.handleForgotPasswordSubmit.bind(
      this
    );
  }

  componentDidMount() {
    this.resetForm();
    this._loadFacebookSDK();
  }

  _loadFacebookSDK() {
    window.fbAsyncInit = function() {
      window.FB.init({
        appId: config.social.facebook,
        autoLogAppEvents: true,
        xfbml: true,
        version: "v3.1"
      });
    };

    (function(d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      // $FlowFixMe
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");
  }

  resetForm() {
    this.setState({
      errors: {
        email: null,
        password: null,
        name: null,
        code: null,
        newPassword: null,
        confirmNewPassword: null
      }
    });
  }

  validateForm() {
    const {
      values: { name, email, password, code, newPassword, confirmNewPassword }
    } = this.state;
    const { authType, isSubmitted } = this.props;

    let hasError = false;

    this.resetForm();

    if (email === "") {
      this.setFormErrors("email", translate("Email is required."));
      hasError = true;
    } else if (hasWhiteSpace(email)) {
      this.setFormErrors("email", translate("Email cannot contain spaces."));
      hasError = true;
    } else if (!isEmail(email)) {
      this.setFormErrors("email", translate("Email is invalid."));
      hasError = true;
    }

    if (authType === AUTH_TYPE.SIGN_UP || authType === AUTH_TYPE.SIGN_IN) {
      if (password === "") {
        this.setFormErrors("password", translate("Password is required."));
        hasError = true;
      }
    }

    if (authType === AUTH_TYPE.SIGN_UP) {
      if (name === "") {
        this.setFormErrors("name", translate("Name is required."));
        hasError = true;
      } else if (!isName(name)) {
        this.setFormErrors("name", translate("Name is invalid."));
        hasError = true;
      }
      if (
        !/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-.]).{8,20}$/.test(
          password
        )
      ) {
        this.setFormErrors(
          "password",
          translate(
            "Password must contain at least 8 characters including a number, a special character, lower case and upper case letters."
          )
        );
        hasError = true;
      } else if (hasWhiteSpace(password)) {
        this.setFormErrors(
          "password",
          translate("Password cannot contain spaces.")
        );
        hasError = true;
      }
    }

    if (authType === AUTH_TYPE.FORGOT_PASSWORD && isSubmitted) {
      if (code === "") {
        this.setFormErrors("code", translate("Verification code is required."));
        hasError = true;
      } else if (hasWhiteSpace(code)) {
        this.setFormErrors("code", translate("Code cannot contain spaces."));
        hasError = true;
      }

      if (newPassword === "") {
        this.setFormErrors(
          "newPassword",
          translate("New password is required.")
        );
        hasError = true;
      } else if (
        !/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-.]).{8,20}$/.test(
          newPassword
        )
      ) {
        this.setFormErrors(
          "newPassword",
          translate(
            "Password must contain at least 8 characters including a number, a special character, lower case and upper case letters."
          )
        );
        hasError = true;
      } else if (hasWhiteSpace(newPassword)) {
        this.setFormErrors(
          "newPassword",
          translate("Password cannot contain spaces.")
        );
        hasError = true;
      }

      if (confirmNewPassword === "") {
        this.setFormErrors(
          "confirmNewPassword",
          translate("Confirm password is required.")
        );
        hasError = true;
      } else if (confirmNewPassword !== newPassword) {
        this.setFormErrors(
          "confirmNewPassword",
          translate("The new password and confirmation password do not match.")
        );
        hasError = true;
      }
    }
    return hasError;
  }

  setFormErrors(field: string, message: string) {
    this.setState(({ errors }) => ({
      errors: {
        ...errors,
        [field]: message
      }
    }));
  }

  handleInputChange(event: SyntheticInputEvent<HTMLInputElement>) {
    this.setState({
      values: {
        ...this.state.values,
        [event.target.id]: event.target.value
      }
    });
  }

  handleSignInSubmit(role: string) {
    if (!this.validateForm()) {
      const { email, password } = this.state.values;
      const { onSignInSubmit } = this.props;
      onSignInSubmit({
        username: email.trim().toLowerCase(),
        password,
        role
      });
    }
  }

  handleSignUpSubmit(role: string) {
    if (!this.validateForm()) {
      const { email, name, password } = this.state.values;
      const { onSignUpSubmit } = this.props;
      onSignUpSubmit({
        email: email.trim().toLowerCase(),
        name,
        password,
        role
      });
    }
  }

  handleForgotPasswordSubmit() {
    if (!this.validateForm()) {
      const { email, code, newPassword } = this.state.values;
      const { isSubmitted, forgotPassword, forgotPasswordSubmit } = this.props;
      if (!isSubmitted) {
        forgotPassword({
          email: email.trim().toLowerCase()
        });
      } else {
        forgotPasswordSubmit({
          email: email.trim().toLowerCase(),
          code,
          newPassword
        });
      }
    }
  }

  getNotifications() {
    const { notifications } = this.props;
    if (notifications !== null) {
      return notifications.message;
    }
    return null;
  }

  toggleAuthType(authType: string) {
    const { toggleAuthType } = this.props;

    this.resetForm();
    toggleAuthType(authType);
  }

  render() {
    const { values, errors } = this.state;
    const {
      isCapsLockActive,
      loadingStudent,
      loadingAgent,
      authType,
      isSubmitted,
      isLoading
    } = this.props;

    const Buttons = props => (
      <Fragment>
        <Button
          htmlType={Button.HTML_TYPE.BUTTON}
          type={Button.TYPE.FACEBOOK}
          onClick={props.facebookSignIn}
          className="btn-with-icon"
        >
          <Icon icon="facebook" />
          {translate("LOGIN WITH FACEBOOK")}
        </Button>
        <Button
          htmlType={Button.HTML_TYPE.BUTTON}
          type={Button.TYPE.GOOGLE}
          onClick={props.googleSignIn}
          className="btn-with-icon"
        >
          <Icon icon="google" />
          {translate("LOGIN WITH GOOGLE")}
        </Button>
      </Fragment>
    );

    const Federated = withFederated(Buttons);

    const federatedConfig = {
      google_client_id: config.social.google,
      facebook_app_id: config.social.facebook
    };

    return (
      <Fragment>
        <form autoComplete="off">
          {authType === AUTH_TYPE.SIGN_UP && (
            <div className="form-group">
              <label>{translate("Name")}</label>
              <Input
                placeholder={translate("Full name")}
                id="name"
                text={values.name}
                onChange={this.handleInputChange}
                error={errors.name}
                autoComplete={false}
              />
            </div>
          )}
          <div className="form-group">
            <label>{translate("Email")}</label>
            <Input
              placeholder="Eg: user@example.com"
              id="email"
              text={values.email}
              onChange={this.handleInputChange}
              error={errors.email}
              autoComplete={false}
            />
          </div>
          {authType === AUTH_TYPE.SIGN_UP || authType === AUTH_TYPE.SIGN_IN ? (
            <div className="form-group">
              <label>{translate("Password")}</label>
              <Input
                placeholder={translate(
                  "at least 6 characters including a number, a special character, lower case and upper case letters."
                )}
                id="password"
                text={values.password}
                onChange={this.handleInputChange}
                type="password"
                error={errors.password}
                autoComplete={false}
              />
              {isCapsLockActive && (
                <span className="warning">
                  <Icon icon="exclamation-circle" />
                  &nbsp;
                  {translate("Warning: Caps lock enabled.")}
                </span>
              )}
            </div>
          ) : (
            authType === AUTH_TYPE.FORGOT_PASSWORD &&
            isSubmitted && (
              <Fragment>
                <div className="form-group">
                  <label>{translate("Verification Code")}</label>
                  <Input
                    id="code"
                    placeholder="Eg: 123456"
                    onChange={this.handleInputChange}
                    error={errors.code}
                    autoComplete={false}
                    name="code"
                  />
                </div>
                <div className="form-group">
                  <label>{translate("New Password")}</label>
                  <Input
                    id="newPassword"
                    type="password"
                    placeholder={translate(
                      "at least 6 characters including a number, a special character, lower case and upper case letters."
                    )}
                    onChange={this.handleInputChange}
                    error={errors.newPassword}
                    autoComplete={false}
                    name="password"
                  />
                  {isCapsLockActive && (
                    <span className="warning">
                      <Icon icon="exclamation-circle" />{" "}
                      {translate("Warning: Caps lock enabled.")}
                    </span>
                  )}
                </div>
                <div className="form-group">
                  <label>{translate("Confirm new Password")}</label>
                  <Input
                    id="confirmNewPassword"
                    type="password"
                    placeholder={translate(
                      "at least 6 characters including a number, a special character, lower case and upper case letters."
                    )}
                    onChange={this.handleInputChange}
                    error={errors.confirmNewPassword}
                    autoComplete={false}
                    name="confirm_password"
                  />
                  {isCapsLockActive && (
                    <span className="warning">
                      <Icon icon="exclamation-circle" />{" "}
                      {translate("Warning: Caps lock enabled.")}
                    </span>
                  )}
                </div>
              </Fragment>
            )
          )}
          {authType === AUTH_TYPE.SIGN_IN ? (
            <Fragment>
              <Checkbox
                text={translate("Register now")}
                isChecked={false}
                onChange={() => this.toggleAuthType(AUTH_TYPE.SIGN_UP)}
              />
              <div className="secondary-link">
                {translate("Forgot your password?")} &nbsp;
                <span
                  onClick={() => this.toggleAuthType(AUTH_TYPE.FORGOT_PASSWORD)}
                  className="link"
                >
                  {translate("Reset password.")}
                </span>
              </div>
            </Fragment>
          ) : authType === AUTH_TYPE.SIGN_UP ? (
            <Checkbox
              text={translate("Register now")}
              isChecked={true}
              onChange={() => this.toggleAuthType(AUTH_TYPE.SIGN_IN)}
            />
          ) : (
            authType === AUTH_TYPE.FORGOT_PASSWORD && (
              <Fragment>
                <div className="secondary-link">
                  {translate("Go back to")}
                  <span
                    onClick={() => this.props.toggleAuthType(AUTH_TYPE.SIGN_IN)}
                    className="link"
                  >
                    {translate("Login")}
                  </span>
                </div>
                {isSubmitted && (
                  <Alert type={Alert.TYPE.SUCCESS}>
                    {translate(
                      "Please check your email account, We have sent you a verification code to reset your password!"
                    )}
                  </Alert>
                )}
              </Fragment>
            )
          )}
        </form>
        {this.getNotifications()}
        {authType === AUTH_TYPE.SIGN_UP ? (
          <Fragment>
            <div className="button-container">
              <Fragment>
                <Button
                  htmlType={Button.HTML_TYPE.SUBMIT}
                  onClick={() => this.handleSignUpSubmit("student")}
                  loading={loadingStudent}
                >
                  {translate("SIGN UP AS STUDENT")}
                </Button>
              </Fragment>
            </div>
            <div className="button-container">
              <Fragment>
                <Button
                  htmlType={Button.HTML_TYPE.SUBMIT}
                  onClick={() => this.handleSignUpSubmit("agent")}
                  loading={loadingAgent}
                >
                  {translate("SIGN UP AS AGENT")}
                </Button>
              </Fragment>
            </div>
          </Fragment>
        ) : authType === AUTH_TYPE.SIGN_IN ? (
          <div className="button-container">
            <Button
              htmlType={Button.HTML_TYPE.BUTTON}
              onClick={() => this.handleSignInSubmit("student")}
              loading={loadingStudent}
            >
              {translate("Login")}
            </Button>
            <div className="button-group">
              <Federated
                federated={federatedConfig}
                onStateChange={this.props.handleAuthStateChange}
              />
            </div>
          </div>
        ) : (
          authType === AUTH_TYPE.FORGOT_PASSWORD && (
            <div className="button-container">
              <Button
                htmlType={Button.HTML_TYPE.BUTTON}
                onClick={() => this.handleForgotPasswordSubmit()}
                loading={isLoading}
              >
                {translate("SUBMIT")}
              </Button>
            </div>
          )
        )}
      </Fragment>
    );
  }
}

export default AuthPanel;
