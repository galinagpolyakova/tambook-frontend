export const USER_TYPE = {
  STUDENT: "student",
  AGENT: "agent"
};

export const AUTH_TYPE = {
  SIGN_IN: "SIGN_IN",
  SIGN_UP: "SIGN_UP",
  FORGOT_PASSWORD: "FORGOT_PASSWORD",
  VERIFY_ACCOUNT: "VERIFY_ACCOUNT"
};

export const USER_STATUS = {
  DEFAULT: "default",
  PENDING: "pending",
  APPROVED: "approved"
};
