// @flow
import type { SerializableInterface } from "../../shared/SerializableInterface";

import type { UserProps } from "./types";

export class Auth implements SerializableInterface<UserProps> {
  username: $PropertyType<UserProps, "username">;
  password: $PropertyType<UserProps, "password">;
  name: $PropertyType<UserProps, "name">;
  email: $PropertyType<UserProps, "email">;
  role: $PropertyType<UserProps, "role">;
  schoolId: $PropertyType<UserProps, "schoolId">;
  phone: $PropertyType<UserProps, "phone">;
  gender: $PropertyType<UserProps, "gender">;
  citizenship: $PropertyType<UserProps, "citizenship">;
  language: $PropertyType<UserProps, "language">;
  birth_date: $PropertyType<UserProps, "birth_date">;
  profile_image: $PropertyType<UserProps, "profile_image">;
  created_by: $PropertyType<UserProps, "created_by">;
  website: $PropertyType<UserProps, "website">;
  address: $PropertyType<UserProps, "address">;
  user_type: $PropertyType<UserProps, "user_type">;
  status: $PropertyType<UserProps, "status">;
  country: $PropertyType<UserProps, "country">;
  city: $PropertyType<UserProps, "city">;

  constructor() {}

  static fromAuthApi() {}

  toJSON() {
    return this;
  }
}
