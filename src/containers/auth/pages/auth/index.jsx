// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import {
  authSignIn,
  authSignUp,
  clearNotifications,
  federatedSignInSuccess,
  authForgotPassword,
  authForgotPasswordSubmit,
  resetNotifications
} from "../../store/action";

import { AUTH_TYPE } from "../../constants";

import authImage from "assets/auckland.jpg";
import Modal from "shared/components/Modal";
import Alert from "shared/components/Alert";
import Link from "components/Link";
import AuthPanel from "../../components/AuthPanel";
import translate from "translations/translate";

import { UserSettingsConsumer } from "contexts/UserSettings";
import { isString } from "shared/utils";

import "./styles.scss";

const EVENT_KEY_UP = "keyup";

type AuthProps = {
  error: Object | null,
  loading: boolean,
  loadingAgent: boolean,
  loadingStudent: boolean,
  isAuthSuccess: boolean,
  isEmailSent: boolean,
  isForgotPasswordSubmitted: boolean,
  authSignUp: Function,
  authSignIn: Function,
  toggleAuth: Function,
  clearNotifications: Function,
  federatedSignInSuccess: Function,
  authForgotPassword: Function,
  authForgotPasswordSubmit: Function,
  resetNotifications: Function
};

type AuthState = {
  authState: | typeof AUTH_TYPE.SIGN_IN
    | typeof AUTH_TYPE.SIGN_UP
    | typeof AUTH_TYPE.FORGOT_PASSWORD,
  isLoading: boolean,
  error: string | null,
  isCapsLockActive: boolean
};

class AuthBlock extends PureComponent<AuthProps, AuthState> {
  state = {
    authState: AUTH_TYPE.SIGN_IN,
    isLoading: false,
    error: null,
    isCapsLockActive: false
  };

  componentDidMount() {
    document.addEventListener(EVENT_KEY_UP, this._wasCapsLockDeactivated);
    this.props.resetNotifications();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isAuthSuccess !== prevProps.isAuthSuccess &&
      this.props.isAuthSuccess
    ) {
      prevState.authState === AUTH_TYPE.SIGN_UP ||
      prevState.authState === AUTH_TYPE.FORGOT_PASSWORD
        ? this.setState({ authState: AUTH_TYPE.SIGN_IN })
        : this.props.toggleAuth();
    }
  }

  componentWillUnmount() {
    document.removeEventListener(EVENT_KEY_UP, this._wasCapsLockDeactivated);
  }

  _wasCapsLockDeactivated = (event: any) => {
    if (event.getModifierState) {
      this.setState({
        isCapsLockActive:
          event.getModifierState("CapsLock") &&
          this.state.isCapsLockActive === false
      });
    }
  };

  _toggleAuthType = authState => {
    if (authState !== undefined) {
      if (this.props.error !== null || this.props.isEmailSent) {
        this.props.clearNotifications();
      }
      this.setState({
        authState
      });
    }
  };

  _handleAuthStateChange = state => {
    if (state === "signedIn") {
      this.props.federatedSignInSuccess();
    }
  };

  _getFormFields = () => {
    const { authState, isCapsLockActive } = this.state;
    const { loading, loadingAgent, loadingStudent } = this.props;

    switch (authState) {
      case AUTH_TYPE.SIGN_UP:
        return (
          <AuthPanel
            isLoading={loading}
            loadingAgent={loadingAgent}
            loadingStudent={loadingStudent}
            notifications={this._getNotifications()}
            isCapsLockActive={isCapsLockActive}
            onSignInSubmit={this.props.authSignIn}
            onSignUpSubmit={this.props.authSignUp}
            toggleAuthType={this._toggleAuthType}
            handleAuthStateChange={this._handleAuthStateChange}
            forgotPassword={this.props.authForgotPassword}
            forgotPasswordSubmit={this.props.authForgotPasswordSubmit}
            isSubmitted={this.props.isForgotPasswordSubmitted}
            authType={AUTH_TYPE.SIGN_UP}
          />
        );
      case AUTH_TYPE.FORGOT_PASSWORD:
        return (
          <AuthPanel
            isLoading={loading}
            loadingAgent={loadingAgent}
            loadingStudent={loadingStudent}
            notifications={this._getNotifications()}
            isCapsLockActive={isCapsLockActive}
            onSignInSubmit={this.props.authSignIn}
            onSignUpSubmit={this.props.authSignUp}
            toggleAuthType={this._toggleAuthType}
            handleAuthStateChange={this._handleAuthStateChange}
            forgotPassword={this.props.authForgotPassword}
            forgotPasswordSubmit={this.props.authForgotPasswordSubmit}
            isSubmitted={this.props.isForgotPasswordSubmitted}
            authType={AUTH_TYPE.FORGOT_PASSWORD}
          />
        );
      default:
        return (
          <AuthPanel
            isLoading={loading}
            loadingAgent={loadingAgent}
            loadingStudent={loadingStudent}
            notifications={this._getNotifications()}
            isCapsLockActive={isCapsLockActive}
            onSignInSubmit={this.props.authSignIn}
            onSignUpSubmit={this.props.authSignUp}
            toggleAuthType={this._toggleAuthType}
            handleAuthStateChange={this._handleAuthStateChange}
            forgotPassword={this.props.authForgotPassword}
            forgotPasswordSubmit={this.props.authForgotPasswordSubmit}
            isSubmitted={this.props.isForgotPasswordSubmitted}
            authType={AUTH_TYPE.SIGN_IN}
          />
        );
    }
  };

  _getNotifications = () => {
    const { error, isEmailSent } = this.props;
    if (error !== null) {
      if (isString(error)) {
        return {
          message: <Alert type={Alert.TYPE.ERROR}>{error}</Alert>,
          code: ""
        };
      } else {
        if (error.code === "UserNotFoundException") {
          return {
            message: (
              <Alert type={Alert.TYPE.ERROR}>
                {translate("The user does not exist.")}
              </Alert>
            ),
            code: error.code
          };
        }
        return {
          message: <Alert type={Alert.TYPE.ERROR}>{error.message}</Alert>,
          code: error.code
        };
      }
    }
    if (isEmailSent) {
      return {
        message: (
          <Alert type={Alert.TYPE.SUCCESS}>
            {translate("Check Your Email.")}
          </Alert>
        ),
        code: null
      };
    }
    return null;
  };

  render() {
    const { toggleAuth } = this.props;
    return (
      <Modal className="auth-modal" showModal={true} onClose={toggleAuth}>
        <div className="auth-container">
          <div className="auth-content">
            <div className="auth-body">
              <div className="form-header">
                <div className="heading">
                  {translate("Welcome To Tambook!")}
                </div>
              </div>

              <div className="sub-heading">
                {this.state.authState === AUTH_TYPE.FORGOT_PASSWORD
                  ? translate("Reset your password")
                  : translate("Please login or create an account")}
              </div>
              {this._getFormFields()}

              <div className="footer-links">
                <Link target="_new" to="/terms-and-conditions">
                  {translate("Terms and Conditions")}
                </Link>
              </div>
            </div>
          </div>
          <div
            className="auth-image"
            style={{
              background: `linear-gradient(
                      rgba(0, 160, 186, 0.43),
                      rgba(0, 160, 186, 0.43)
                      )
                      , center / cover no-repeat url(${authImage})`
            }}
          />
        </div>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    error: state.auth.error,
    loading: state.auth.loading,
    loadingAgent: state.auth.loadingAgent,
    loadingStudent: state.auth.loadingStudent,
    isAuthSuccess: state.auth.isAuthSuccess,
    isForgotPasswordSubmitted: state.auth.isForgotPasswordSubmitted,
    isEmailSent: state.auth.isEmailSent
  };
}

const Actions = {
  authSignIn,
  authSignUp,
  clearNotifications,
  federatedSignInSuccess,
  authForgotPassword,
  authForgotPasswordSubmit,
  resetNotifications
};

const Auth = connect(
  mapStateToProps,
  Actions
)(AuthBlock);

export default class AuthContext extends PureComponent<{}> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ displayAuth, toggleAuth }) => {
          return (
            displayAuth && <Auth {...this.props} toggleAuth={toggleAuth} />
          );
        }}
      </UserSettingsConsumer>
    );
  }
}
