// @flow
import React, { Component } from "react";
import { connect } from "react-redux";

import { queryParamsParse } from "shared/helpers/url";
import Section from "shared/components/Section";
import Alert from "shared/components/Alert";
import Loader from "components/Loader";

import translate from "translations/translate";

import { authVerifyAccount } from "../../store/action";

type ConfirmAccountProps = {
  isAuthSuccess: boolean,
  error: null | Object,
  location: Object,
  filters: null | Object,
  authVerifyAccount: Function,
  getFilterParams: Function
};

class ConfirmAccount extends Component<ConfirmAccountProps> {
  componentDidMount() {
    const queryParams = queryParamsParse(this.props.location.search);
    const { email: username, code } = queryParams;
    if (username !== undefined && code !== undefined) {
      this.props.authVerifyAccount({
        username,
        code
      });
    }
    if (this.props.filters === null) {
      this.props.getFilterParams();
    }
  }

  getPageContent = () => {
    if (this.props.isAuthSuccess) {
      return (
        <Alert type={Alert.TYPE.SUCCESS}>
          {translate("Your account has been successfully verified.")}
        </Alert>
      );
    } else {
      if (this.props.error === null) {
        return <Loader isLoading={true} />;
      } else {
        return (
          <Alert type={Alert.TYPE.ERROR}>{this.props.error.message}</Alert>
        );
      }
    }
  };

  render() {
    return (
      <div className="container">
        <Section>{this.getPageContent()}</Section>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthSuccess: state.auth.isAuthSuccess,
    error: state.auth.error
  };
}

const Actions = {
  authVerifyAccount
};

export default connect(
  mapStateToProps,
  Actions
)(ConfirmAccount);
