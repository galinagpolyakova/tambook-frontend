// import lib
import { lazy } from "react";

export default [
  {
    path: "/confirm",
    exact: true,
    component: lazy(() => import("./pages/confirm"))
  }
];
