// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";

export class AuthService {
  api: ApiServiceInterface;

  endpoint: string = "/user";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);
    return response;
  }

  addUserToDb(payload: Object) {
    return this.api.post(`${this.endpoint}/create`, payload);
  }

  updateUser(payload: Object) {
    return this.api.patch(`${this.endpoint}/update`, payload);
  }
}

export default AuthService;
