import { push } from "connected-react-router";

import {
  AUTH_INIT,
  AGENT_INIT,
  STUDENT_INIT,
  AUTH_SIGN_IN_SUCCESS,
  AUTH_FAILURE,
  AUTH_SIGN_UP_SUCCESS,
  IS_USER_AUTHENTICATED_SUCCESS,
  IS_USER_AUTHENTICATED_FAILURE,
  AUTH_SIGN_OUT_SUCCESS,
  CLEAR_NOTIFICATIONS,
  AUTH_FORGOT_PASSWORD_SUCCESS,
  AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS,
  AUTH_CONFIRM_SIGN_UP_SUCCESS,
  IS_ADD_USER__TO_DB_SUCCESS,
  IS_ADD_USER_TO_DB_FAILURE,
  CHANGE_USER_STATUS,
  AUTH_AGENT_SIGN_UP_INIT,
  AUTH_STUDENT_SIGN_UP_INIT,
  RESET_NOTIFICATIONS
} from "./actionTypes";

import { clearAgentData } from "../../agent/store/actions";
import { clearStudentData } from "../../profile/store/actions";

import { getLanguageFromUrl } from "shared/helpers/translations";

import { Auth } from "aws-amplify";
import { USER_TYPE, USER_STATUS } from "../constants";
import { SITE_URL_WITH_LANG } from "config/app";
import { getPageFromUrl } from "shared/helpers/url";

Auth.configure({
  auth0: {
    domain: "tambook.au.auth0.com",
    clientID: "zIrPXSnI9nFKV459IJ7Ure8VYvzYQAbG",
    redirectUri: SITE_URL_WITH_LANG,
    responseType: "token id_token", // for now we only support implicit grant flow
    scope: "openid profile email", // the scope used by your app
    rredirectUri: SITE_URL_WITH_LANG
  }
});

export function resetNotifications() {
  return {
    type: RESET_NOTIFICATIONS
  };
}

function authInit() {
  return {
    type: AUTH_INIT
  };
}
function agentInit() {
  return {
    type: AGENT_INIT
  };
}
function studentInit() {
  return {
    type: STUDENT_INIT
  };
}

function authSignInSuccess(payload) {
  return {
    type: AUTH_SIGN_IN_SUCCESS,
    payload
  };
}

function authFailure(payload) {
  return {
    type: AUTH_FAILURE,
    payload
  };
}

export function authSignIn({ username, password, role }) {
  return (dispatch, getState, serviceManager) => {
    if (role === USER_TYPE.AGENT) {
      dispatch(agentInit());
    } else {
      dispatch(studentInit());
    }
    Auth.signIn(username, password, role)
      .then(data => {
        Auth.currentAuthenticatedUser()
          .then(user => {
            return Auth.userAttributes(user);
          })
          .then(attributes => {
            let role, status;
            attributes.map(function(item) {
              if (item.Name === "custom:role") {
                role = item.Value;
              }
              if (item.Name === "custom:status") {
                status = item.Value;
              }
              return true;
            });
            if (
              role &&
              (role === USER_TYPE.STUDENT || role === USER_TYPE.AGENT)
            ) {
              serviceManager.get("ApiService").authToken =
                data.signInUserSession.idToken.jwtToken;
              dispatch(
                authSignInSuccess({ email: data.username, role, status })
              );
              if (role === USER_TYPE.AGENT && status === USER_STATUS.DEFAULT) {
                dispatch(
                  push(
                    `/${getLanguageFromUrl()}/agent-profile/agent-information`
                  )
                );
              } else {
                if (getPageFromUrl() === "confirm") {
                  dispatch(push(`/${getLanguageFromUrl()}/`));
                }
              }
            } else {
              dispatch(authFailure({ error: "The user does not exist." }));
            }
          })
          .catch(error => dispatch(authFailure({ error })));
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

function authSignUpSuccess(payload) {
  return {
    type: AUTH_SIGN_UP_SUCCESS,
    payload
  };
}

function isAddUserToDbSuccess(payload) {
  return {
    type: IS_ADD_USER__TO_DB_SUCCESS,
    payload
  };
}

function isAddUserToDbFailure(payload) {
  return {
    type: IS_ADD_USER_TO_DB_FAILURE,
    payload
  };
}

export function authSignUp({ password, email, name, role }) {
  return (dispatch, getState, serviceManager) => {
    if (role === USER_TYPE.AGENT) {
      dispatch(agentInit());
      dispatch({
        type: AUTH_AGENT_SIGN_UP_INIT
      });
    } else {
      dispatch(studentInit());
      dispatch({
        type: AUTH_STUDENT_SIGN_UP_INIT
      });
    }
    let roles = "";
    let AuthService = serviceManager.get("AuthService");
    if (role === "student") {
      roles = USER_TYPE.STUDENT;
    } else {
      roles = USER_TYPE.AGENT;
    }
    Auth.signUp({
      username: email,
      password,
      attributes: {
        email,
        name,
        "custom:role": roles,
        "custom:status": USER_STATUS.DEFAULT
      }
    })
      .then(() => {
        AuthService.addUserToDb({ email, name, role })
          .then(res => {
            isAddUserToDbSuccess({ res });
          })
          .catch(error => {
            isAddUserToDbFailure({ error });
          });
        dispatch(authSignUpSuccess());
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

function isUserAuthenticatedSuccess(payload) {
  return {
    type: IS_USER_AUTHENTICATED_SUCCESS,
    payload
  };
}

function isUserAuthenticatedFailure() {
  return {
    type: IS_USER_AUTHENTICATED_FAILURE
  };
}

export function isUserAuthenticated() {
  return (dispatch, getState, serviceManager) => {
    Auth.currentAuthenticatedUser({ bypassCache: true })
      .then(({ username: email, attributes }) => {
        let role = USER_TYPE.STUDENT;
        let status = USER_STATUS.APPROVED;

        if (attributes) {
          role = attributes["custom:role"];
          status = attributes["custom:status"];
        }
        dispatch(isUserAuthenticatedSuccess({ email, role, status }));
        Auth.currentSession()
          .then(session => {
            serviceManager.get("ApiService").authToken =
              session.idToken.jwtToken;
          })
          .catch(() => {
            Auth.currentCredentials().then(result => {
              const credentials = Auth.essentialCredentials(result);
              serviceManager.get("ApiService").authToken =
                credentials.sessionToken;
            });
          });
      })
      .catch(error => {
        dispatch(isUserAuthenticatedFailure(error));
      });
  };
}

function authSignOutSuccess() {
  return { type: AUTH_SIGN_OUT_SUCCESS };
}

export function authSignOut() {
  return dispatch => {
    Auth.signOut().then(
      dispatch(authSignOutSuccess()),
      dispatch(clearAgentData()),
      dispatch(clearStudentData())
    );
  };
}

export function clearNotifications() {
  return {
    type: CLEAR_NOTIFICATIONS
  };
}

export function federatedSignInSuccess() {
  return dispatch => {
    Auth.currentAuthenticatedUser().then(({ email }) => {
      dispatch(
        isUserAuthenticatedSuccess({
          email,
          role: USER_TYPE.STUDENT,
          status: USER_STATUS.APPROVED
        })
      );
    });
  };
}

function authForgotPasswordSuccess(payload) {
  return {
    type: AUTH_FORGOT_PASSWORD_SUCCESS,
    payload
  };
}

export function authForgotPassword({ email }) {
  return dispatch => {
    dispatch(authInit());
    Auth.forgotPassword(email)
      .then(() => {
        dispatch(authForgotPasswordSuccess({ email }));
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

function authForgotPasswordSubmitSuccess(payload) {
  return {
    type: AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS,
    payload
  };
}

export function authForgotPasswordSubmit({ email, code, newPassword }) {
  return dispatch => {
    dispatch(authInit());
    Auth.forgotPasswordSubmit(email, code, newPassword)
      .then(() => {
        dispatch(authForgotPasswordSubmitSuccess());
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

function authConfirmSignUpSuccess() {
  return {
    type: AUTH_CONFIRM_SIGN_UP_SUCCESS
  };
}

export function authVerifyAccount({ username, code }) {
  return dispatch => {
    dispatch(authInit());
    Auth.confirmSignUp(username, code)
      .then(() => {
        dispatch(authConfirmSignUpSuccess());
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

export function changeUserStatus(payload) {
  return {
    type: CHANGE_USER_STATUS,
    payload
  };
}
