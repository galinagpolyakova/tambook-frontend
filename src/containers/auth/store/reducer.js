// @flow
import type { Action } from "shared/types/ReducerAction";

import {
  AUTH_INIT,
  AGENT_INIT,
  STUDENT_INIT,
  AUTH_SIGN_IN_SUCCESS,
  AUTH_FAILURE,
  AUTH_SIGN_UP_SUCCESS,
  AUTH_SIGN_OUT_SUCCESS,
  IS_USER_AUTHENTICATED_SUCCESS,
  IS_USER_AUTHENTICATED_FAILURE,
  CLEAR_NOTIFICATIONS,
  AUTH_FORGOT_PASSWORD_SUCCESS,
  AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS,
  AUTH_CONFIRM_SIGN_UP_SUCCESS,
  IS_ADD_USER__TO_DB_SUCCESS,
  IS_ADD_USER_TO_DB_FAILURE,
  CHANGE_USER_STATUS,
  RESET_NOTIFICATIONS
} from "./actionTypes";
import { USER_TYPE, USER_STATUS } from "../constants";

export type AuthState = {
  loading: boolean,
  loadingAgent: boolean,
  loadingStudent: boolean,
  isAuthenticated: boolean,
  isUserInitiated: boolean,
  isAuthSuccess: boolean,
  error: null | string,
  email: string,
  role: typeof USER_TYPE.AGENT | typeof USER_TYPE.STUDENT,
  status: | typeof USER_STATUS.APPROVED
    | typeof USER_STATUS.DEFAULT
    | typeof USER_STATUS.PENDING,
  isForgotPasswordSubmitted: boolean,
  isEmailSent: boolean
};

const initialState: AuthState = {
  loading: false,
  loadingAgent: false,
  loadingStudent: false,
  isAuthenticated: false,
  isUserInitiated: false,
  isAuthSuccess: false,
  error: null,
  email: "",
  role: "",
  status: "",
  isForgotPasswordSubmitted: false,
  isEmailSent: false
};

function resetNotifications(state) {
  return {
    ...state,
    error: null
  };
}

function authInit(state: AuthState): AuthState {
  return {
    ...state,
    loading: true,
    error: null,
    isAuthSuccess: false
  };
}

function agentInit(state: AuthState): AuthState {
  return {
    ...state,
    loadingAgent: true,
    error: null,
    isAuthSuccess: false
  };
}
function studentInit(state: AuthState): AuthState {
  return {
    ...state,
    loadingStudent: true,
    error: null,
    isAuthSuccess: false
  };
}

function authSignInSuccess(
  state: AuthState,
  { email, role, status }
): AuthState {
  return {
    ...state,
    loadingAgent: false,
    loadingStudent: false,
    loading: false,
    isAuthenticated: true,
    isAuthSuccess: true,
    email,
    role,
    status
  };
}

function authSignUpSuccess(state: AuthState): AuthState {
  return {
    ...state,
    loading: false,
    loadingAgent: false,
    loadingStudent: false,
    isAuthSuccess: true,
    isEmailSent: true
  };
}

function authFailure(state: AuthState, { error }): AuthState {
  return {
    ...state,
    error,
    loading: false,
    loadingAgent: false,
    loadingStudent: false
  };
}

function isUserAuthenticatedSuccess(
  state: AuthState,
  { email, role, status = null }
): AuthState {
  return {
    ...state,
    email,
    role,
    status,
    isAuthenticated: true,
    isUserInitiated: true,
    isAuthSuccess: true,
    loading: false
  };
}

function authSignOutSuccess(state: AuthState): AuthState {
  return {
    ...state,
    isAuthenticated: false,
    isAuthSuccess: false,
    loading: false
  };
}

function isUserAuthenticatedFailure(state: AuthState): AuthState {
  return {
    ...state,
    isUserInitiated: true,
    loading: false
  };
}

function clearNotifications(state: AuthState): AuthState {
  return {
    ...state,
    error: null,
    isEmailSent: false
  };
}

function authForgotPasswordSuccess(state: AuthState): AuthState {
  return {
    ...state,
    isForgotPasswordSubmitted: true,
    loading: false
  };
}

function authForgotPasswordSubmitSuccess(state: AuthState) {
  return {
    ...state,
    isAuthSuccess: true,
    loading: false
  };
}

function authConfirmSignUpSuccess(state: AuthState) {
  return {
    ...state,
    isAuthSuccess: true,
    loading: false,
    isEmailSent: false
  };
}

function isAddUserToDbSuccess(state: AuthState) {
  return {
    ...state,
    isAuthSuccess: true
  };
}

function isAddUserToDbFailure(state: AuthState) {
  return {
    ...state,
    isAuthSuccess: true
  };
}

function changeUserStatus(state: AuthState, { status }) {
  return {
    ...state,
    status
  };
}

const reducer = (
  state: AuthState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case AUTH_INIT:
      return authInit(state);
    case RESET_NOTIFICATIONS:
      return resetNotifications(state);
    case AGENT_INIT:
      return agentInit(state);
    case STUDENT_INIT:
      return studentInit(state);
    case AUTH_SIGN_IN_SUCCESS:
      return authSignInSuccess(state, payload);
    case AUTH_SIGN_UP_SUCCESS:
      return authSignUpSuccess(state);
    case AUTH_FAILURE:
      return authFailure(state, payload);
    case IS_USER_AUTHENTICATED_SUCCESS:
      return isUserAuthenticatedSuccess(state, payload);
    case IS_USER_AUTHENTICATED_FAILURE:
      return isUserAuthenticatedFailure(state);
    case AUTH_SIGN_OUT_SUCCESS:
      return authSignOutSuccess(state);
    case CLEAR_NOTIFICATIONS:
      return clearNotifications(state);
    case AUTH_FORGOT_PASSWORD_SUCCESS:
      return authForgotPasswordSuccess(state);
    case AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS:
      return authForgotPasswordSubmitSuccess(state);
    case AUTH_CONFIRM_SIGN_UP_SUCCESS:
      return authConfirmSignUpSuccess(state);
    case IS_ADD_USER__TO_DB_SUCCESS:
      return isAddUserToDbSuccess(state);
    case IS_ADD_USER_TO_DB_FAILURE:
      return isAddUserToDbFailure(state);
    case CHANGE_USER_STATUS:
      return changeUserStatus(state, payload);
    default:
      return state;
  }
};

export default reducer;
