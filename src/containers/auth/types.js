// @flow
export type SignInProps = {
  username: string,
  password: string
};

export type UserProps = {
  username: string,
  password: string,
  name: string,
  email: string,
  role: string,
  schoolId: string,
  phone: string,
  gender: string,
  citizenship: string,
  language: string,
  birth_date: string,
  profile_image: string,
  created_by: string,
  website: string,
  address: string,
  user_type: string,
  status: string,
  country: string,
  city: string,
};
