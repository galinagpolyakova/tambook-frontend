// @flow
import React, { PureComponent } from "react";
import Link from "components/Link";
import type { ConsolidatedBlogType } from "../../types";

import Button from "shared/components/Button";
import BlogFilter from "../../pages/filter";
import translate from "translations/translate";

import "./styles.scss";

type BlogGridItemProps = {
  id: $PropertyType<ConsolidatedBlogType, "id">,
  activeFrom: $PropertyType<ConsolidatedBlogType, "activeFrom">,
  image: $PropertyType<ConsolidatedBlogType, "image">,
  blogData: $PropertyType<ConsolidatedBlogType, "blogData">,
  getShortDescription: Function
};

class BlogGridItem extends PureComponent<BlogGridItemProps> {
  render() {
    const {
      id,
      image,
      activeFrom,
      blogData: { title, textPreview },
      getShortDescription
    } = this.props;
    return (
      <div className="blog">
        <div className="featured-image">
          {image && image !== "" && <img src={image} alt={title} />}
          <div className="hover">
            <div>
              <Button
                type={Button.TYPE.SECONDARY}
                size={Button.SIZE.SMALL}
                htmlType={Button.HTML_TYPE.LINK}
                link={`/blog/${id}`}
              >
                {translate("MORE INFO")}
              </Button>
            </div>
          </div>
        </div>
        <div className="blog-content">
          <Link to={`/blog/${id}`}>
            <div className="blog-title">{title}</div>
            <div className="blog-active-date">{activeFrom}</div>
            <div className="blog-description">
              {getShortDescription(textPreview, BlogFilter.types.TEXT_DETAIL)}
            </div>
          </Link>
        </div>
      </div>
    );
  }
}

export default BlogGridItem;
