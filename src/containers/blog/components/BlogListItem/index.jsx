// @flow
import React, { PureComponent } from "react";

import type { ConsolidatedBlogType } from "../../types";

import Button from "shared/components/Button";
import Col from "shared/components/Col";
import Row from "shared/components/Row";
import BlogFilter from "../../pages/filter";
import translate from "translations/translate";
import Link from "components/Link";

import "./styles.scss";

type BlogListItemProps = {
  id: $PropertyType<ConsolidatedBlogType, "id">,
  activeFrom: $PropertyType<ConsolidatedBlogType, "activeFrom">,
  image: $PropertyType<ConsolidatedBlogType, "image">,
  blogData: $PropertyType<ConsolidatedBlogType, "blogData">,
  getShortDescription: Function
};

class BlogListItem extends PureComponent<BlogListItemProps> {
  render() {
    const {
      id,
      activeFrom,
      image,
      blogData: { title, textPreview },
      getShortDescription
    } = this.props;
    return (
      <div className="blog-list-item">
        <div className="featured-image">
          {image && image !== "" && <img src={image} alt={title} />}
        </div>
        <div className="blog-content">
          <Row className="detail-block">
            <Col sm={12} md="8">
              <Link to={`/blog/${id}`}>
                <div className="blog-title">{title}</div>
                <div className="blog-active-from">{activeFrom}</div>
                <div className="blog-description">
                  {getShortDescription(
                    textPreview,
                    BlogFilter.types.TEXT_DETAIL
                  )}
                </div>
              </Link>
            </Col>
            <Col sm={12} md="4" className="blog-cta">
              <Button
                type={Button.TYPE.SECONDARY}
                size={Button.SIZE.SMALL}
                htmlType={Button.HTML_TYPE.LINK}
                link={`/blog/${id}`}
              >
                {translate("MORE INFO")}
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default BlogListItem;
