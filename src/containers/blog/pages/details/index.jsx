// @flow
import { type BlogType } from "../../types";

import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Parser from "html-react-parser";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton
} from "react-share";

import PageHeader from "components/PageHeader";
import Loader from "components/Loader";
import { isEmail } from "shared/kernel/cast";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Icon from "shared/components/Icon";
import Input from "shared/components/Input";
import Button from "shared/components/Button";
import Alert from "shared/components/Alert";
import ImageSlider from "shared/components/ImageSlider";

import Link from "components/Link";

import { getBlogPost, subscribeToBlog } from "../../store/actions";
import { getConsolidatedBlog, isBlogLoaded } from "../../store/selectors";
import { SITE_URL_WITH_LANG } from "config/app";
import translate from "translations/translate";

import "./styles.scss";

export type BlogDetailsProps = {
  match: {
    params: {
      blogId: number
    }
  },
  getBlogPost: Function,
  subscribeToBlog: Function,
  blog: BlogType,
  isLoaded: boolean,
  isBlogLoadingFailure: boolean,
  isSubscriptionSuccess: boolean,
  isSubscriptionLoading: boolean
};

type BlogDetailsState = {
  subscribe: {
    email: string,
    emailError: null | string
  }
};
class BlogDetails extends PureComponent<BlogDetailsProps, BlogDetailsState> {
  constructor() {
    super();

    this.state = {
      subscribe: {
        email: "",
        emailError: null
      }
    };
    // $FlowFixMe
    this.handleSubscribeInput = this.handleSubscribeInput.bind(this);
    // $FlowFixMe
    this.validateSubscribeEmail = this.validateSubscribeEmail.bind(this);
    // $FlowFixMe
    this.setError = this.setError.bind(this);
    // $FlowFixMe
    this.resetError = this.resetError.bind(this);
    // $FlowFixMe
    this.handleSubscribeSubmit = this.handleSubscribeSubmit.bind(this);
  }
  componentDidMount() {
    const {
      isLoaded,
      getBlogPost,
      match: {
        params: { blogId }
      }
    } = this.props;
    if (!isLoaded) getBlogPost(blogId);
  }

  componentDidUpdate(prevProps) {
    const { isSubscriptionSuccess } = this.props;
    if (
      prevProps.isSubscriptionSuccess !== isSubscriptionSuccess &&
      isSubscriptionSuccess
    ) {
      this.setState(({ subscribe }) => ({
        subscribe: {
          ...subscribe,
          email: ""
        }
      }));
    }
  }

  handleSubscribeInput(event) {
    this.setState({
      subscribe: {
        ...this.state.subscribe,
        email: event.target.value
      }
    });
  }

  validateSubscribeEmail() {
    const { email } = this.state.subscribe;

    let hasError = false;

    this.resetError();

    if (email === "") {
      this.setError(translate("Email is required."));
      hasError = true;
    } else if (!isEmail(email)) {
      this.setError(translate("Email is invalid."));
      hasError = true;
    }
    return hasError;
  }

  setError(message: string) {
    this.setState(({ subscribe }) => ({
      subscribe: {
        ...subscribe,
        emailError: message
      }
    }));
  }

  resetError() {
    this.setState(({ subscribe }) => ({
      subscribe: {
        ...subscribe,
        emailError: null
      }
    }));
  }

  handleSubscribeSubmit() {
    const { subscribeToBlog } = this.props;
    const {
      subscribe: { email }
    } = this.state;

    if (!this.validateSubscribeEmail()) {
      subscribeToBlog({
        email: email
      });
    }
  }

  render() {
    const {
      blog,
      isLoaded,
      isSubscriptionSuccess,
      isSubscriptionLoading,
      isBlogLoadingFailure,
      match: {
        params: { blogId }
      }
    } = this.props;

    const {
      subscribe: { email, emailError }
    } = this.state;

    if (!isLoaded) {
      return <Loader isLoading />;
    }

    if (isBlogLoadingFailure) {
      return (
        <Redirect
          to={{
            pathname: `/404`
          }}
        />
      );
    }

    return (
      <div>
        <PageHeader
          breadcrumbs={[
            {
              title: translate("BLOG"),
              link: "/blogs"
            },
            {
              title: translate("DETAIL")
            }
          ]}
        />
        <div className="blog-article-container">
          <Row>
            <Col sm={12} md="8">
              <Row>
                <Col>
                  <div className="back-to-blog">
                    <Link to="/blogs">{"< " + translate("Back to Blog")}</Link>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="blog-publish-date">{blog.activeFrom}</div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="blog-title">{blog.blogData.title}</div>
                </Col>
              </Row>
              <Row>
                <Col className="course-images">
                  <ImageSlider autoplay slides={blog.images} />
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="blog-text">
                    {Parser(blog.blogData.textDetail)}
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="back-to-blog">
                    <Link to="/blogs">{"< " + translate("Back to Blog")}</Link>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col sm={12} md="4">
              <div className="side-container">
                <Row>
                  <Col>
                    <div className="social-links-heading">
                      {translate("SHARE")}
                    </div>
                    <div className="social-links-container">
                      <ul className="social-links">
                        <li className="social-link-item facebook">
                          <FacebookShareButton
                            url={`${SITE_URL_WITH_LANG}/blog/${blogId}`}
                          >
                            <Icon icon="facebook" />
                          </FacebookShareButton>
                        </li>
                        <li className="social-link-item linkedin">
                          <LinkedinShareButton
                            url={`${SITE_URL_WITH_LANG}/blog/${blogId}`}
                          >
                            <Icon icon="linkedin-alt" />
                          </LinkedinShareButton>
                        </li>
                        <li className="social-link-item twitter">
                          <TwitterShareButton
                            url={`${SITE_URL_WITH_LANG}/blog/${blogId}`}
                          >
                            <Icon icon="twitter" />
                          </TwitterShareButton>
                        </li>
                      </ul>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div className="subscribe-title">
                      {translate("DON'T MISS A THING:")}
                    </div>
                    <div className="subscribe-description">
                      {translate(
                        "Subscribe to get our latest best offers, promotions and news"
                      )}
                    </div>
                    <div className="subscribe-input-container">
                      <Input
                        id="email"
                        placeholder="e.g user@example.com"
                        text={email}
                        onChange={event => this.handleSubscribeInput(event)}
                        error={emailError}
                      />
                    </div>
                    <div className="subscribe-button">
                      <Button
                        sm={12}
                        md={Button.SIZE.MEDIUM}
                        type={Button.TYPE.PRIMARY}
                        onClick={this.handleSubscribeSubmit}
                        loading={isSubscriptionLoading}
                      >
                        {translate("SUBSCRIBE")}
                      </Button>
                    </div>
                    {isSubscriptionSuccess && (
                      <Alert isFullWidth={true} type={Alert.TYPE.SUCCESS}>
                        {translate("Subscribed successfully. Thank you.")}
                      </Alert>
                    )}
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoaded: isBlogLoaded(state, props),
    isSubscriptionSuccess: state.blog.isSubscriptionSuccess,
    isSubscriptionLoading: state.blog.isSubscriptionLoading,
    isBlogLoadingFailure: state.blog.isBlogLoadingFailure,
    blog: getConsolidatedBlog(state)
  };
};

const Actions = {
  getBlogPost,
  subscribeToBlog
};

export default connect(
  mapStateToProps,
  Actions
)(BlogDetails);
