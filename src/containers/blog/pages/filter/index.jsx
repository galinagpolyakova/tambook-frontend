// @flow
import React, { PureComponent, createRef } from "react";
import { connect } from "react-redux";

import Pagination, { type PaginationType } from "shared/components/Pagination";
import type { ConsolidatedBlogsType, ConsolidatedBlogType } from "../../types";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton
} from "react-share";

import PageHeader from "components/PageHeader";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Button from "shared/components/Button";
import Input from "shared/components/Input";
import Icon from "shared/components/Icon";
import { ItemsLayout, ItemsLayoutToggler } from "shared/components/ItemsLayout";
import { UserSettingsConsumer } from "contexts/UserSettings";
import Loader from "components/Loader";
import { queryParamsParse, stringifyQueryParams } from "shared/helpers/url";
import BlogGridItem from "../../components/BlogGridItem";
import BlogListItem from "../../components/BlogListItem";
import { filterBlogs, getFilterTags } from "../../store/actions";
import { getLanguageFromUrl } from "shared/helpers/translations";
import { SITE_URL_WITH_LANG } from "config/app";
import translate from "translations/translate";

import "./styles.scss";
import IconButton from "shared/components/IconButton";

type BlogFilterProps = {
  filterBlogs: Function,
  blogs: ConsolidatedBlogsType,
  loading: boolean,
  pagination: PaginationType,
  location: {
    search: string
  },
  history: any,
  tags: Array<string>,
  selectedTags: Array<string>,
  getFilterTags: Function
};

type BlogFilterState = {
  search: string
};

class BlogFilter extends PureComponent<BlogFilterProps, BlogFilterState> {
  YOUTUBE_LINK = "https://www.youtube.com/embed/_gFI6k0Zny8";
  ALL_TAG = "all";

  static types = {
    TITLE: "title",
    TEXT_DETAIL: "textDetail"
  };

  constructor(props) {
    super(props);

    // $FlowFixMe
    this.getBlogPosts = this.getBlogPosts.bind(this);
    // $FlowFixMe
    this.getShortDescription = this.getShortDescription.bind(this);
    // $FlowFixMe
    this.handleTagClick = this.handleTagClick.bind(this);
    // $FlowFixMe
    this.isTagSelected = this.isTagSelected.bind(this);
    // $FlowFixMe
    this.handleFormFieldChange = this.handleFormFieldChange.bind(this);
    // $FlowFixMe
    this.searchBlogPosts = this.searchBlogPosts.bind(this);

    const { search } = queryParamsParse(props.location.search);
    this.state = {
      search: search ? search : ""
    };
  }

  myRef = createRef();

  componentDidMount() {
    const {
      location: { search }
    } = this.props;
    const { page = 1, tags, ...filters } = queryParamsParse(search);
    this.props.filterBlogs(
      { ...filters, tags: tags ? tags.split(",") : [] },
      { page }
    );
    this.props.getFilterTags();
  }

  isTagSelected(tag) {
    const {
      location: { search }
    } = this.props;

    let { tags } = queryParamsParse(search);
    let foundTag = false;
    if (tags) {
      const selectedTags = tags.split(",");

      selectedTags.map(selected => {
        if (selected === tag) {
          foundTag = true;
        }
        return null;
      });
    }
    return foundTag;
  }

  getBlogPosts(filter) {
    const language = getLanguageFromUrl();

    const query = {
      ...queryParamsParse(this.props.location.search),
      ...filter
    };

    const { page = 1, tags = [], ...filters } = query;
    this.props.filterBlogs(
      {
        ...filters,
        tags: tags && !Array.isArray(tags) ? tags.split(",") : tags
      },
      { page }
    );

    this.props.history.push({
      pathname: `/${language}/blogs`,
      search: stringifyQueryParams(query, { arrayFormat: "bracket" })
    });
    window.scrollTo(0, this.myRef.current.offsetTop - 40);
  }

  getShortDescription(data, type) {
    let modifiedData = null;

    if (type === BlogFilter.types.TITLE && data) {
      modifiedData =
        data.length > 30 ? data.substring(0, 30).concat("...") : data;
    }
    if (type === BlogFilter.types.TEXT_DETAIL && data) {
      modifiedData =
        data.length > 200 ? data.substring(0, 200).concat("...") : data;
    }
    return modifiedData;
  }

  handleTagClick(tag) {
    const {
      location: { search }
    } = this.props;

    let { tags } = queryParamsParse(search);

    tags = !tags || tag === this.ALL_TAG ? [] : tags.split(",");

    if (tag !== this.ALL_TAG) {
      const index = tags.indexOf(tag);
      tags =
        index === -1 ? [...tags, tag] : tags.filter(filter => filter !== tag);
    }

    this.getBlogPosts({ tags, page: 1, search: undefined });
  }

  handleFormFieldChange = (event: any) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  searchBlogPosts() {
    const { search } = this.state;
    this.getBlogPosts({ search, page: 1, tags: undefined });
  }

  render() {
    const { blogs, pagination, tags, selectedTags } = this.props;
    const { search } = this.state;

    if (tags === null || blogs === null) {
      return <Loader isLoading />;
    }

    return (
      <div>
        <PageHeader
          title="BLOG"
          breadcrumbs={[
            {
              title: translate("BLOG")
            }
          ]}
        />
        <UserSettingsConsumer>
          {({ viewMode, changeViewMode }) => (
            <div className="container page-container">
              <div className="tag-search-container">
                <div className="tag-container">
                  <Button
                    type={
                      selectedTags.length > 0
                        ? Button.TYPE.INFO
                        : Button.TYPE.PRIMARY
                    }
                    size={Button.SIZE.SMALL}
                    outline={true}
                    onClick={() => this.handleTagClick(this.ALL_TAG)}
                  >
                    {translate("All")}
                  </Button>
                  {tags.map(tag => {
                    return (
                      <Button
                        key={tag}
                        type={
                          this.isTagSelected(tag)
                            ? Button.TYPE.PRIMARY
                            : Button.TYPE.INFO
                        }
                        size={Button.SIZE.SMALL}
                        outline={true}
                        onClick={() => this.handleTagClick(tag)}
                      >
                        {tag}
                      </Button>
                    );
                  })}
                </div>
                <div className="search-container">
                  <div className="search-bar">
                    <Input
                      placeholder={translate("search")}
                      text={search}
                      onChange={this.handleFormFieldChange}
                      id="search"
                    />
                    <IconButton icon="search" onClick={this.searchBlogPosts} />
                  </div>
                  <div className="layout-switcher">
                    <div className="layout-text">{translate("VIEW")}: </div>
                    <ItemsLayoutToggler
                      viewMode={viewMode}
                      changeViewMode={changeViewMode}
                    />
                  </div>
                </div>
              </div>
              <div className="video-container">
                <Row>
                  <Col sm={12} md="4">
                    <h3>
                      {translate("WATCH HOW TAMBOOK HELPS YOU STUDY OVERSEAS")}
                    </h3>
                    <p>
                      {translate(
                        "Welcome to our blog! Here you'll find useful tips and tricks that'll help you plan your next adventure. To get started, take a look at our short video 'How to book a course using Tambook"
                      )}
                    </p>
                    <Button
                      htmlType={Button.HTML_TYPE.LINK}
                      link="/filter?country=&language="
                      fullWidth
                    >
                      {translate("SEARCH FOR COURSES NOW")}
                    </Button>
                    <div className="social-links-container">
                      <ul className="social-links">
                        <li className="social-link-item facebook">
                          <FacebookShareButton
                            url={`${SITE_URL_WITH_LANG}/blogs`}
                          >
                            <Icon icon="facebook" />
                          </FacebookShareButton>
                        </li>
                        <li className="social-link-item linkedin">
                          <LinkedinShareButton
                            url={`${SITE_URL_WITH_LANG}/blogs`}
                          >
                            <Icon icon="linkedin-alt" />
                          </LinkedinShareButton>
                        </li>
                        <li className="social-link-item twitter">
                          <TwitterShareButton
                            url={`${SITE_URL_WITH_LANG}/blogs`}
                          >
                            <Icon icon="twitter" />
                          </TwitterShareButton>
                        </li>
                      </ul>
                    </div>
                  </Col>
                  <Col sm={12} md="8">
                    <div className="promo-video">
                      <iframe
                        title="school-promo"
                        width="100%"
                        height="450"
                        src={this.YOUTUBE_LINK}
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                        frameBorder="0"
                      />
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="blog-view" ref={this.myRef}>
                <Row>
                  <Col>
                    <ItemsLayout
                      items={blogs}
                      viewMode={viewMode}
                      gridByLine={4}
                      gridItemComponent={(props: {
                        item: ConsolidatedBlogType
                      }) => {
                        return (
                          <BlogGridItem
                            {...props.item}
                            getShortDescription={this.getShortDescription}
                          />
                        );
                      }}
                      listItemComponent={(props: {
                        item: ConsolidatedBlogType
                      }) => {
                        return (
                          <BlogListItem
                            {...props.item}
                            getShortDescription={this.getShortDescription}
                          />
                        );
                      }}
                    />
                    {pagination !== null && (
                      <Pagination
                        totalPages={pagination.pages}
                        currentPage={pagination.currentPage}
                        onPageChanged={this.getBlogPosts}
                      />
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          )}
        </UserSettingsConsumer>
      </div>
    );
  }
}

const Actions = {
  filterBlogs,
  getFilterTags
};

function mapStateToProps(state) {
  return {
    blogs: state.blog.blogs,
    loading: state.blog.loading,
    pagination: state.blog.pagination,
    tags: state.blog.tags,
    selectedTags: state.blog.selectedTags
  };
}

export default connect(
  mapStateToProps,
  Actions
)(BlogFilter);
