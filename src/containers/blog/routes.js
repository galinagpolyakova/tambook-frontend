//import lib
import { lazy } from "react";

export default [
  {
    path: "/blog/:blogId",
    className: "blog-details-page",
    exact: true,
    component: lazy(() => import("./pages/details"))
  },
  {
    path: "/blogs",
    className: "blog-filter-page",
    exact: true,
    component: lazy(() => import("./pages/filter"))
  }
];
