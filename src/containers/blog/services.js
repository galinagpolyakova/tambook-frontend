// @flow
import { type ApiServiceInterface } from "../../shared/services/ApiServiceInterface";

import type {
  ApiBlogType,
  BlogType,
  ApiBlogsType,
  ConsolidatedBlogType
} from "./types";
import missingImage from "../../assets/missing-course.jpg";
import { Blog } from "./model";
import { API_URL } from "config/app";
import { isCompleteUrl } from "shared/utils";

export class BlogService {
  api: ApiServiceInterface;

  endpoint: string = "/blog";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  _transformBlogData({
    textPreview,
    textDetail,
    title,
    id,
    activeFrom,
    images
  }: any): ConsolidatedBlogType {
    const image = images.length
      ? isCompleteUrl(images[0].path)
        ? `${API_URL}/schools/image${images[0].path}?width=327&height=225`
        : `${API_URL}/schools/image/${images[0].path}?width=327&height=225`
      : missingImage;
    return {
      id,
      activeFrom,
      image,
      blogData: {
        title,
        textDetail,
        textPreview
      }
    };
  }

  _normalizeBlog(apiBlog: ApiBlogType): BlogType {
    return Blog.fromApi(apiBlog);
  }

  _normalizeBlogs(apiBlogs: ApiBlogsType): any {
    return apiBlogs.map(apiBlog => this._transformBlogData(apiBlog));
  }

  getBlogPost(blogID: number): Promise<BlogType> {
    return this.api.get(`${this.endpoint}/${blogID}`).then(response => {
      return this._normalizeBlog(response.result);
    });
  }

  subscribeToBlog(payload: Object) {
    return this.api.post(`${this.endpoint}/subscribe`, payload);
  }

  filterBlogs(payload: Object = {}, query: Object = {}) {
    return this.api.post(this.endpoint, payload, query).then(response => {
      return {
        blogs: this._normalizeBlogs(response.result),
        pagination: {
          items: response.pagination.total,
          currentPage: response.pagination.current,
          pages: response.pagination.total_pages,
          pageSize: response.pagination.per_page
        },
        selectedTags: response.tags
      };
    });
  }

  getFilterTags() {
    return this.api.get(`${this.endpoint}/tags`).then(response => {
      return {
        tags: response.result.en
      };
    });
  }
}
