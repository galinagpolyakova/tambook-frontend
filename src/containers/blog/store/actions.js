import {
  GET_BLOG_INIT,
  GET_BLOG_SUCCESS,
  GET_BLOG_FAILURE,
  SUBSCRIBE_BLOG_INIT,
  SUBSCRIBE_BLOG_SUCCESS,
  SUBSCRIBE_BLOG_FAILURE,
  FILTER_BLOG_INIT,
  FILTER_BLOG_SUCCESS,
  FILTER_BLOG_FAILURE,
  GET_FILTER_TAGS_INIT,
  GET_FILTER_TAGS_SUCCESS,
  GET_FILTER_TAGS_FAILURE
} from "./actionTypes";

function getBlogPostInit() {
  return {
    type: GET_BLOG_INIT
  };
}

function getBlogPostSuccess(payload) {
  return {
    type: GET_BLOG_SUCCESS,
    payload
  };
}

function getBlogPostFailure(payload) {
  return {
    type: GET_BLOG_FAILURE,
    payload
  };
}

export function getBlogPost(id) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getBlogPostInit());

    let blogService = serviceManager.get("BlogService");

    blogService
      .getBlogPost(id)
      .then(data => {
        dispatch(
          getBlogPostSuccess({
            id,
            data
          })
        );
      })
      .catch(err => {
        dispatch(getBlogPostFailure(err));
      });
  };
}

function subscribeToBlogInit() {
  return {
    type: SUBSCRIBE_BLOG_INIT
  };
}

function subscribeToBlogSuccess() {
  return {
    type: SUBSCRIBE_BLOG_SUCCESS
  };
}

function subscribeToBlogFailure(payload) {
  return {
    type: SUBSCRIBE_BLOG_FAILURE,
    payload
  };
}

export function subscribeToBlog(email) {
  return (dispatch, getState, serviceManager) => {
    dispatch(subscribeToBlogInit());

    let blogService = serviceManager.get("BlogService");

    blogService
      .subscribeToBlog(email)
      .then(() => dispatch(subscribeToBlogSuccess()))
      .catch(err => dispatch(subscribeToBlogFailure(err)));
  };
}

function filterBlogsInit() {
  return {
    type: FILTER_BLOG_INIT
  };
}

function filterBlogsSuccess(payload) {
  return {
    type: FILTER_BLOG_SUCCESS,
    payload
  };
}

function filterBlogsFailure(payload) {
  return {
    type: FILTER_BLOG_FAILURE,
    payload
  };
}

export function filterBlogs(payload, query) {
  return (dispatch, getState, serviceManager) => {
    dispatch(filterBlogsInit());

    let blogService = serviceManager.get("BlogService");
    blogService
      .filterBlogs(payload, query)
      .then(response => dispatch(filterBlogsSuccess(response)))
      .catch(err => dispatch(filterBlogsFailure(err)));
  };
}

function getFilterTagsInit() {
  return {
    type: GET_FILTER_TAGS_INIT
  };
}

function getFilterTagsSuccess(payload) {
  return {
    type: GET_FILTER_TAGS_SUCCESS,
    payload
  };
}

function getFilterTagsFailure(payload) {
  return {
    type: GET_FILTER_TAGS_FAILURE,
    payload
  };
}

export function getFilterTags() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getFilterTagsInit());

    let blogService = serviceManager.get("BlogService");

    blogService
      .getFilterTags()
      .then(response => dispatch(getFilterTagsSuccess(response)))
      .catch(err => dispatch(getFilterTagsFailure(err)));
  };
}
