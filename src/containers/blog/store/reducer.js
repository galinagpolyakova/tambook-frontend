// @flow
import { type Action } from "shared/types/ReducerAction";
import { type PaginationType } from "shared/components/Pagination";

import {
  GET_BLOG_INIT,
  GET_BLOG_SUCCESS,
  GET_BLOG_FAILURE,
  SUBSCRIBE_BLOG_INIT,
  SUBSCRIBE_BLOG_SUCCESS,
  SUBSCRIBE_BLOG_FAILURE,
  FILTER_BLOG_INIT,
  FILTER_BLOG_SUCCESS,
  FILTER_BLOG_FAILURE,
  GET_FILTER_TAGS_INIT,
  GET_FILTER_TAGS_SUCCESS,
  GET_FILTER_TAGS_FAILURE
} from "./actionTypes";

export type BlogState = {
  notifications: null | string[],
  blogs: Array<any>,
  blog: Object,
  loading: boolean,
  isLoaded: boolean,
  isBlogLoadingFailure: boolean,
  isSubscriptionSuccess: boolean,
  pagination: PaginationType,
  tags: null | Array<string>,
  isTagsLoaded: boolean,
  selectedTags: null | Array<string>
};

const initialState: BlogState = {
  notifications: null,
  blog: {
    id: null,
    data: null
  },
  blogs: [],
  loading: false,
  isLoaded: false,
  isBlogLoadingFailure: false,
  isSubscriptionSuccess: false,
  isSubscriptionLoading: false,
  pagination: null,
  tags: null,
  isTagsLoaded: false,
  selectedTags: []
};

function filterBlogsInit(state: BlogState) {
  return {
    ...state,
    loading: true,
    blogs: null
  };
}

function filterBlogsSuccess(
  state: BlogState,
  { blogs, pagination, selectedTags }
) {
  return {
    ...state,
    loading: false,
    blogs,
    pagination,
    selectedTags
  };
}

function filterBlogsFailure(state: BlogState) {
  return {
    ...state,
    loading: false,
    isLoaded: true
  };
}

function getBlogPostInit(state: BlogState) {
  return {
    ...state,
    loading: true,
    isLoaded: false
  };
}

function getBlogPostSuccess(state: BlogState, { id, data }) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    blog: {
      id,
      data
    }
  };
}

function getBlogPostFailure(state: BlogState, { notifications }) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    isBlogLoadingFailure: true,
    notifications
  };
}

function subscribeToBlogInit(state: BlogState) {
  return {
    ...state,
    isSubscriptionLoading: true
  };
}

function subscribeToBlogSuccess(state: BlogState) {
  return {
    ...state,
    isSubscriptionSuccess: true,
    isSubscriptionLoading: false
  };
}

function subscribeToBlogFailure(state: BlogState) {
  return {
    ...state,
    isSubscriptionLoading: false
  };
}

function getFilterTagsInit(state: BlogState) {
  return {
    ...state,
    isTagsLoaded: false
  };
}

function getFilterTagsSuccess(state: BlogState, { tags }) {
  return {
    ...state,
    isTagsLoaded: true,
    tags
  };
}

function getFilterTagsFailure(state: BlogState) {
  return {
    ...state,
    isTagsLoaded: false
  };
}

const reducer = (
  state: BlogState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case GET_BLOG_INIT:
      return getBlogPostInit(state);
    case GET_BLOG_SUCCESS:
      return getBlogPostSuccess(state, payload);
    case GET_BLOG_FAILURE:
      return getBlogPostFailure(state, payload);
    case SUBSCRIBE_BLOG_INIT:
      return subscribeToBlogInit(state);
    case SUBSCRIBE_BLOG_SUCCESS:
      return subscribeToBlogSuccess(state);
    case SUBSCRIBE_BLOG_FAILURE:
      return subscribeToBlogFailure(state);
    case FILTER_BLOG_INIT:
      return filterBlogsInit(state);
    case FILTER_BLOG_SUCCESS:
      return filterBlogsSuccess(state, payload);
    case FILTER_BLOG_FAILURE:
      return filterBlogsFailure(state);
    case GET_FILTER_TAGS_INIT:
      return getFilterTagsInit(state);
    case GET_FILTER_TAGS_SUCCESS:
      return getFilterTagsSuccess(state, payload);
    case GET_FILTER_TAGS_FAILURE:
      return getFilterTagsFailure(state);
    default:
      return state;
  }
};

export default reducer;
