// @flow
export type BlogType = {
  activeFrom: string,
  createdDate: number,
  id: number,
  images: Array<string>,
  status: string,
  tags: string,
  blogData: {
    textDetail: string,
    title: string
  }
};

export type ApiBlogType = {
  activeFrom: string,
  createdDate: number,
  id: number,
  images: Array<{
    path: string,
    lang: string,
    type: string,
    id: number
  }>,
  status: string,
  tags: string,
  textDetail_en: string,
  title_en: string,
  textDetail_ru: string,
  title_ru: string,
  textDetail_es: string,
  title_es: string,
  textDetail_pt: string,
  title_pt: string,
  title: string,
  textDetail: string,
  textPreview: string
};

export type ApiBlogsType = Array<ApiBlogType>;

export type ConsolidatedBlogType = {
  activeFrom: string,
  id: number,
  image: string,
  blogData: {
    textDetail: string,
    title: string,
    textPreview: string
  }
};

export type ConsolidatedBlogsType = Array<ConsolidatedBlogType>;
