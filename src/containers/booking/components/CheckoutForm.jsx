// @flow
import React, { Component } from "react";
import { CardElement, injectStripe } from "react-stripe-elements";
import poweredByStripe from "assets/powered_by_stripe.svg";
import Button from "shared/components/Button";
import translate from "translations/translate";

type CheckoutFormProps = {
  stripe: any,
  handleResult: Function,
  isLoading: boolean
};

type CheckoutFormState = {
  errorMessage: string
};

class CheckoutForm extends Component<CheckoutFormProps, CheckoutFormState> {
  constructor(props: CheckoutFormProps) {
    super(props);
    this.state = {
      errorMessage: "",
      loading: false
    };
  }

  handleChange = ({ error }) => {
    if (error) {
      this.setState({ errorMessage: error.message });
    }
  };

  handleSubmit = evt => {
    evt.preventDefault();
    this.setState({ loading: true });
    if (this.props.stripe) {
      this.props.stripe.createToken().then(payload => {
        this.setState({ loading: false });
        this.props.handleResult(payload);
      });
    } else {
      this.setState({ loading: false });
    }
  };

  render() {
    const createOptions = () => {
      return {
        style: {
          base: {
            fontSize: "16px",
            color: "#c0c1ca",
            letterSpacing: "0.025em",
            "::placeholder": {
              color: "#fbfbfb"
            }
          },
          invalid: {
            color: "#a9a9a9"
          }
        }
      };
    };

    return (
      <div>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div className="card">
            <div className="chip">
              <div className="side left" />
              <div className="side right" />
              <div className="vertical top" />
              <div className="vertical bottom" />
            </div>
            <div className="card-container">
              <CardElement
                hidePostalCode={true}
                onChange={this.handleChange}
                {...createOptions()}
              />
            </div>
            <div className="error" role="alert">
              {this.state.errorMessage}
            </div>
            <div className="lines-up" />
          </div>
          <Button
            htmlType={Button.HTML_TYPE.SUBMIT}
            loading={this.props.isLoading || this.state.loading}
          >
            {translate("Pay")}
          </Button>
        </form>
        <img src={poweredByStripe} className="powered-by" alt="Stripe" />
      </div>
    );
  }
}

export default injectStripe(CheckoutForm);
