// @flow
import React, { PureComponent, Fragment } from "react";

import { type CourseType } from "containers/course/types";

import RadioButton from "shared/components/RadioButton";
import Button from "shared/components/Button";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Select from "shared/components/Select";

import Currency from "components/Currency";

import { DatePicker } from "shared/components/DatePicker/DatePicker";
import format from "shared/utils/dates/format";
import addWeeks from "shared/utils/dates/addWeeks";
import translate from "translations/translate";

const ACCOMMODATION_OPTIONS = {
  SELF_ARRANGE: "selfArrange",
  IS_ABOVE_18: "above18",
  IS_BELOW_18: "under18"
};

export type AccommodationType = {
  type: | typeof ACCOMMODATION_OPTIONS.SELF_ARRANGE
    | typeof ACCOMMODATION_OPTIONS.IS_ABOVE_18
    | typeof ACCOMMODATION_OPTIONS.IS_BELOW_18,
  startDate: null | Date,
  homeStayWeeks: number | null,
  youngChildren: string,
  olderChildren: string,
  olderAdults: string,
  pets: string,
  smoke: string,
  foodRestrictions: string,
  alergies: string,
  medicalConditions: string
};

type AccommodationFormProps = {
  homestays: $PropertyType<CourseType, "homestays">,
  currencyType: $PropertyType<CourseType, "currencyType">,
  selectedWeek: number | null,
  options: Array<any>,
  nextStep: Function,
  prevStep: Function,
  onAccommodationChange: Function,
  onSubmit: Function,
  onPageLoad: Function,
  data: AccommodationType
};

type AccommodationFormState = {
  values: AccommodationType,
  errors: {
    startDate: null | string
  }
};

class AccommodationForm extends PureComponent<
  AccommodationFormProps,
  AccommodationFormState
> {
  static ACCOMMODATION_OPTIONS = ACCOMMODATION_OPTIONS;
  static ACCOMMODATION_VALUES = {
    [ACCOMMODATION_OPTIONS.SELF_ARRANGE]: "I'll arrange accommodation myself",
    [ACCOMMODATION_OPTIONS.IS_BELOW_18]: "Homestay for students younger 18",
    [ACCOMMODATION_OPTIONS.IS_ABOVE_18]: "Homestay for students 18 and above"
  };

  static SELECT_OPTIONS = [
    { name: translate("NOT SPECIFIED"), value: "NOT SPECIFIED" },
    { name: translate("YES"), value: "YES" },
    { name: translate("NO"), value: "NO" }
  ];
  constructor(props: AccommodationFormProps) {
    super(props);
    this.state = {
      values: {
        type: props.data.type,
        startDate: props.data.startDate,
        homeStayWeeks: props.data.homeStayWeeks,
        youngChildren: props.data.youngChildren,
        olderChildren: props.data.olderChildren,
        olderAdults: props.data.olderAdults,
        pets: props.data.pets,
        smoke: props.data.smoke,
        foodRestrictions: props.data.foodRestrictions,
        alergies: props.data.alergies,
        medicalConditions: props.data.medicalConditions
      },
      errors: {
        startDate: null
      },
      showPreferences: false
    };
  }

  componentDidMount() {
    this.props.onPageLoad(2);
  }

  handleFormFieldChange(name: string, value: any) {
    if (name === "type" || name === "homeStayWeeks") {
      this.props.onAccommodationChange(name, value);
    }

    this.setState({
      values: {
        ...this.state.values,
        [name]: value
      }
    });
  }

  validateForm = () => {
    const {
      values: { startDate }
    } = this.state;

    let hasError = false;

    this.resetFormErrors();

    if (startDate === null) {
      this.setFormErrors("startDate", translate("Start date is required."));
      hasError = true;
    }
    return hasError;
  };

  setFormErrors = (field: string, message: string) => {
    this.setState(state => {
      return {
        errors: {
          ...state.errors,
          [field]: message
        }
      };
    });
  };

  resetFormErrors = () => {
    this.setState({
      errors: {
        startDate: null
      }
    });
  };

  getHomeStayPreferences = () => {
    const { options } = this.props;
    const {
      errors,
      values: {
        startDate,
        homeStayWeeks,
        youngChildren,
        olderChildren,
        olderAdults,
        pets,
        smoke,
        foodRestrictions,
        alergies,
        medicalConditions
      }
    } = this.state;
    return (
      <div>
        <div className="homestay-duration">
          <p className="heading-4">{translate("HOMESTAY DURATION")}</p>
          <Row>
            <Col sm={12} md="6">
              <div className="homestay-date-label">
                {translate("Start Date")}
              </div>
              <div className="homestay-date-picker">
                <DatePicker
                  onToggleDate={homeStayStartDate => {
                    this.handleFormFieldChange("startDate", homeStayStartDate);
                  }}
                  initialSelection={startDate}
                  error={errors.startDate}
                />
              </div>
            </Col>
            <Col sm={12} md="6">
              <div className="homestay-date-label">
                {translate("How Many Weeks")}
              </div>
              <div className="homestay-date-picker">
                <Select
                  onChange={homeStayWeeks =>
                    this.handleFormFieldChange("homeStayWeeks", homeStayWeeks)
                  }
                  selected={homeStayWeeks}
                  options={options}
                  placeholder={translate("Select")}
                />
              </div>
            </Col>
          </Row>
        </div>
        <div className="homestay-preferences">
          <p className="heading-4">{translate("HOMESTAY PREFERENCES")}</p>
          <Row>
            <Col sm={12} md="4">
              <div className="homestay-preferences-label">
                {translate("Family with younger children")}
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={AccommodationForm.SELECT_OPTIONS}
                  selected={youngChildren}
                  onChange={value =>
                    this.handleFormFieldChange("youngChildren", value)
                  }
                />
              </div>
            </Col>
            <Col sm={12} md="4">
              <div className="homestay-preferences-label">
                {translate("Family with older children")}
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={AccommodationForm.SELECT_OPTIONS}
                  selected={olderChildren}
                  onChange={value =>
                    this.handleFormFieldChange("olderChildren", value)
                  }
                />
              </div>
            </Col>
            <Col sm={12} md="4">
              <div className="homestay-preferences-label">
                {translate("Older adults, Children left home")}
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={AccommodationForm.SELECT_OPTIONS}
                  selected={olderAdults}
                  onChange={value =>
                    this.handleFormFieldChange("olderAdults", value)
                  }
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm={12} md="4">
              <div className="homestay-preferences-label">
                {translate("Do you have pets?")}
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={AccommodationForm.SELECT_OPTIONS}
                  selected={pets}
                  onChange={value => this.handleFormFieldChange("pets", value)}
                />
              </div>
            </Col>
            <Col sm={12} md="4">
              <div className="homestay-preferences-label">
                {translate("Do you smoke?")}
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={AccommodationForm.SELECT_OPTIONS}
                  selected={smoke}
                  onChange={value => this.handleFormFieldChange("smoke", value)}
                />
              </div>
            </Col>
            <Col sm={12} md="4">
              <div className="homestay-preferences-label">
                {translate("Do you have any food restrictions?")}
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={AccommodationForm.SELECT_OPTIONS}
                  selected={foodRestrictions}
                  onChange={value =>
                    this.handleFormFieldChange("foodRestrictions", value)
                  }
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm={12} md="4">
              <div className="homestay-preferences-label">
                {translate("Do you have any allergies?")}
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={AccommodationForm.SELECT_OPTIONS}
                  selected={alergies}
                  onChange={value =>
                    this.handleFormFieldChange("alergies", value)
                  }
                />
              </div>
            </Col>
            <Col sm={12} md="4">
              <div className="homestay-preferences-label">
                {translate("Do you have any medical conditions")}
              </div>
              <div className="homestay-preferences-select">
                <Select
                  options={AccommodationForm.SELECT_OPTIONS}
                  selected={medicalConditions}
                  onChange={value =>
                    this.handleFormFieldChange("medicalConditions", value)
                  }
                />
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  };

  nextStep = () => {
    const { values } = this.state;

    const { nextStep, onSubmit } = this.props;

    if (values.type === ACCOMMODATION_OPTIONS.SELF_ARRANGE) {
      nextStep();
    } else if (!this.validateForm()) {
      if (values.startDate === null) {
        return;
      }
      onSubmit({
        ...values,
        endDate: format(
          addWeeks(new Date(values.startDate), values.homeStayWeeks),
          "yyyy-MM-dd"
        )
      });
    }
  };

  render() {
    const { homestays, currencyType, prevStep } = this.props;
    const {
      values: { type }
    } = this.state;
    return (
      <div className="booking-course-accommodation">
        <p className="heading-4">{translate("ACCOMMODATION")}</p>
        <Row>
          <Col>
            <RadioButton
              label={translate(
                AccommodationForm.ACCOMMODATION_VALUES[
                  ACCOMMODATION_OPTIONS.SELF_ARRANGE
                ]
              )}
              checked={
                type === ACCOMMODATION_OPTIONS.SELF_ARRANGE ? true : false
              }
              onChange={() =>
                this.handleFormFieldChange(
                  "type",
                  ACCOMMODATION_OPTIONS.SELF_ARRANGE
                )
              }
            />
          </Col>
        </Row>
        {homestays.length > 0 && (
          <Fragment>
            <Row>
              <Col sm={12} md="4">
                <RadioButton
                  label={translate(
                    AccommodationForm.ACCOMMODATION_VALUES[
                      ACCOMMODATION_OPTIONS.IS_BELOW_18
                    ]
                  )}
                  checked={
                    type === ACCOMMODATION_OPTIONS.IS_BELOW_18 ? true : false
                  }
                  onChange={() =>
                    this.handleFormFieldChange(
                      "type",
                      ACCOMMODATION_OPTIONS.IS_BELOW_18
                    )
                  }
                />
              </Col>
              {homestays[0].under18Price && homestays[0].under18Price !== 0 ? (
                <Col sm={12} md="4">
                  <span className="homestay-notes">
                    @&nbsp;
                    <Currency currencyType={currencyType}>
                      {homestays[0].under18Price}
                    </Currency>
                    {translate("p/w")} +&nbsp;
                    <Currency currencyType={currencyType}>
                      {homestays[0].homestayFee}
                    </Currency>
                    {translate("fee")}
                  </span>
                </Col>
              ) : null}
            </Row>
            <Row>
              <Col sm={12} md="4">
                <RadioButton
                  label={translate(
                    AccommodationForm.ACCOMMODATION_VALUES[
                      ACCOMMODATION_OPTIONS.IS_ABOVE_18
                    ]
                  )}
                  checked={
                    type === ACCOMMODATION_OPTIONS.IS_ABOVE_18 ? true : false
                  }
                  onChange={() =>
                    this.handleFormFieldChange(
                      "type",
                      ACCOMMODATION_OPTIONS.IS_ABOVE_18
                    )
                  }
                />
              </Col>
              {homestays[0].above18Price && homestays[0].above18Price !== 0 ? (
                <Col sm={12} md="4">
                  <span className="homestay-notes">
                    @&nbsp;
                    <Currency currencyType={currencyType}>
                      {homestays[0].above18Price}
                    </Currency>
                    {translate("p/w")} +&nbsp;
                    <Currency currencyType={currencyType}>
                      {homestays[0].homestayFee}
                    </Currency>
                    {translate("fee")}
                  </span>
                </Col>
              ) : null}
            </Row>
          </Fragment>
        )}
        {type !== ACCOMMODATION_OPTIONS.SELF_ARRANGE &&
          this.getHomeStayPreferences()}
        <div className="button-block">
          <Button
            size={Button.SIZE.MEDIUM}
            type={Button.TYPE.SECONDARY}
            onClick={prevStep}
          >
            {translate("STEP BACK")}
          </Button>

          <Button
            size={Button.SIZE.MEDIUM}
            type={Button.TYPE.SECONDARY}
            onClick={this.nextStep.bind(this)}
          >
            {translate("NEXT STEP")}
          </Button>
        </div>
      </div>
    );
  }
}

export default AccommodationForm;
