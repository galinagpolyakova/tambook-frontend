// @flow
import React, { PureComponent, Fragment } from "react";

import { type CourseType, type CourseFeeType } from "containers/course/types";

import Select from "shared/components/Select";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Currency from "components/Currency";
import Icon from "shared/components/Icon";
import Button from "shared/components/Button";
import { Rating } from "shared/components/Rate";
import { DatePicker } from "shared/components/DatePicker/DatePicker";

import addWeeks from "shared/utils/dates/addWeeks";
import format from "shared/utils/dates/format";
import translate from "translations/translate";

type BookingDetailFormProps = {
  course: CourseType,
  courseStartDate: Date | null,
  selectedWeek: number | null,
  isFixedCourse: boolean,
  fees: any,
  selectedRange: number | null,
  range: CourseFeeType | null,
  options: Array<any>,
  onRangeSelect: Function,
  handleCourseStartDate: Function,
  handleSelectedWeek: Function,
  nextStep: Function,
  onPageLoad: Function
};
type BookingDetailFormState = {
  formErrors: {
    courseStartDate: null | string
  }
};
class BookingDetailForm extends PureComponent<
  BookingDetailFormProps,
  BookingDetailFormState
> {
  state = {
    formErrors: {
      courseStartDate: null,
      selectedWeek: null
    }
  };

  componentDidMount() {
    this.props.onPageLoad(1);
  }

  validateForm = () => {
    const { courseStartDate, selectedWeek } = this.props;

    let hasError = false;

    this.resetFormErrors();

    if ((selectedWeek === null) | (selectedWeek === "0")) {
      this.setFormErrors(
        "selectedWeek",
        translate("Course duration is required.")
      );
      hasError = true;
    }

    if (courseStartDate === null) {
      this.setFormErrors(
        "courseStartDate",
        translate("Start date is required.")
      );
      hasError = true;
    }
    return hasError;
  };

  setFormErrors = (field: string, message: string) => {
    this.setState(state => {
      return {
        formErrors: {
          ...state.formErrors,
          [field]: message
        }
      };
    });
  };

  resetFormErrors = () => {
    this.setState({
      formErrors: {
        courseStartDate: null,
        selectedWeek: null
      }
    });
  };

  nextStep = () => {
    const { nextStep } = this.props;
    if (!this.validateForm()) {
      nextStep();
    }
  };

  render() {
    const {
      course,
      courseStartDate,
      selectedWeek,
      isFixedCourse,
      fees,
      selectedRange,
      range,
      options,
      onRangeSelect,
      handleCourseStartDate,
      handleSelectedWeek
    } = this.props;
    const { formErrors } = this.state;
    return (
      <Fragment>
        <p className="heading-4">{translate("YOUR BOOKING DETAILS")}</p>
        <p className="heading-6">{course.title}</p>
        <Rating
          rate={course.rating.rate}
          reviews={course.rating.reviews}
          sm={12}
          md={Rating.size.SMALL}
        />
        {!isFixedCourse && (
          <div className="booking-course-duration">
            <div className="duration-selector">
              <Row>
                <Col sm={12} md={4}>
                  <div className="duration-selector-title">
                    {translate("WEEK RANGE")}
                  </div>
                </Col>
                <Col sm={12} md={4}>
                  <Select
                    options={fees}
                    onChange={key => onRangeSelect(key)}
                    selected={selectedRange}
                    placeholder={translate("WEEK RANGE")}
                  />
                </Col>
              </Row>
            </div>
          </div>
        )}
        {selectedWeek !== null && range !== null && (
          <div className="booking-course-duration">
            <div className="duration-selector">
              <Row>
                <Col sm={12} md={4}>
                  <div className="duration-selector-title">
                    {translate("HOW MANY WEEKS")}
                  </div>
                </Col>
                <Col sm={12} md={4}>
                  <Select
                    onChange={selectedWeek => handleSelectedWeek(selectedWeek)}
                    selected={selectedWeek}
                    options={options}
                    placeholder={translate("HOW MANY WEEKS")}
                  />
                  {formErrors.selectedWeek !== null && (
                    <ul className="form-errors">
                      <li>{formErrors.selectedWeek}</li>
                    </ul>
                  )}
                </Col>
                <Col sm={12} md={4}>
                  <div className="duration-selector-content">
                    <span>
                      @&nbsp;
                      <Currency currencyType={course.currencyType}>
                        {range.price}
                      </Currency>
                      /{translate("Week")}
                    </span>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        )}
        <div className="booking-course-duration">
          <div className="duration-selector">
            <Row>
              <Col sm={12} md={4}>
                <div className="duration-selector-title">
                  {translate("PICK YOUR START DATE")}
                </div>
              </Col>
              <Col sm={12} md={4}>
                <div className="date-selector-content">
                  <DatePicker
                    onToggleDate={courseStartDate => {
                      handleCourseStartDate(courseStartDate);
                    }}
                    initialSelection={courseStartDate}
                    error={formErrors.courseStartDate}
                    rrule={course.rrule}
                    startingDates={course.startDates}
                  />
                </div>
              </Col>
              {courseStartDate !== null && selectedWeek !== "0" && (
                <Col sm={12} md={4}>
                  <div className="duration-selector-content duration-end-date">
                    <div className="icon-prefix">
                      <Icon icon="arrow-right" />
                    </div>
                    <Icon icon="calendar-alt" />
                    <span className="duration-selector-date">
                      {format(
                        addWeeks(new Date(courseStartDate), selectedWeek),
                        "yyyy-MM-dd"
                      )}
                    </span>
                  </div>
                </Col>
              )}
            </Row>
          </div>
        </div>
        <div className="note">
          *
          {translate(
            "If you require a visa to enter New Zealand or Australia, please make sure you have allowed enough time to obtain one."
          )}
        </div>
        <div className="step-nav button-single">
          <Button
            sm={12}
            md={Button.SIZE.MEDIUM}
            type={Button.TYPE.SECONDARY}
            onClick={this.nextStep.bind(this)}
          >
            {translate("NEXT STEP")}
          </Button>
        </div>
      </Fragment>
    );
  }
}

export default BookingDetailForm;
