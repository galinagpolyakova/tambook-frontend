// @flow
import React, { PureComponent } from "react";

import Button from "shared/components/Button";
import RadioButton from "shared/components/RadioButton";

import translate from "translations/translate";

type PaymentSchemeFormProps = {
  prevStep: Function,
  handleFormSubmit: Function,
  handlePaymentMethodChange: Function,
  loading: boolean,
  onPageLoad: Function
};

class PaymentSchemeForm extends PureComponent<PaymentSchemeFormProps> {
  STRIPE = "stripe";

  componentDidMount() {
    const { handlePaymentMethodChange } = this.props;
    handlePaymentMethodChange(this.STRIPE);
    this.props.onPageLoad(5);
  }

  render() {
    const { prevStep, handleFormSubmit, loading } = this.props;
    return (
      <div>
        <p className="heading-4">{translate("PAYMENT SCHEME")}</p>
        <div className="payment-scheme-container">
          <RadioButton
            label={translate("Full (100%) course prepayment")}
            checked={true}
          />
        </div>
        <p className="heading-4">{translate("PAYMENT METHOD")}</p>
        <div className="payment-method-container">
          <RadioButton label={translate("Credit Card")} checked={true} />
        </div>
        <div className="button-block">
          <Button
            size={Button.SIZE.MEDIUM}
            type={Button.TYPE.SECONDARY}
            onClick={prevStep}
          >
            {translate("STEP BACK")}
          </Button>
          <Button
            size={Button.SIZE.MEDIUM}
            type={Button.TYPE.PRIMARY}
            onClick={handleFormSubmit}
            loading={loading}
          >
            {translate("CONFIRM AND PAY")}
          </Button>
        </div>
      </div>
    );
  }
}

export default PaymentSchemeForm;
