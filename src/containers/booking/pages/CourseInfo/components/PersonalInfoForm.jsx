// @flow
import { type UserSettingsType } from "profile/store/reducer";

import React, { PureComponent, Fragment } from "react";
import PhoneInput from "react-phone-input-2";
import InputMask from "react-input-mask";

import { MEDIA_URL } from "config/app";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Input from "shared/components/Input";
import Select from "shared/components/Select";
import Checkbox from "shared/components/Checkbox";
import Button from "shared/components/Button";
import Alert from "shared/components/Alert";
import countries from "shared/data/countries";
import { isEmail } from "shared/kernel/cast";
import isValid from "shared/utils/dates/isValid";
import compareAsc from "shared/utils/dates/compareAsc";

import { USER_TYPE } from "containers/auth/constants";

import { serviceManager } from "services/manager";

import translate from "translations/translate";

import "react-phone-input-2/dist/style.css";

export type PersonalInfoType = {
  email: string,
  firstName: string,
  lastName: string,
  dob: string,
  phoneNo: string,
  nativeLanguage: string,
  passportNo: string,
  passportImage: string,
  country: string,
  promotions: boolean
};

type PersonalInfoFormProps = {
  role: string,
  studentInfo: PersonalInfoType,
  passport: $PropertyType<UserSettingsType, "passport">,
  onSubmit: Function,
  handleUserPassportChange: Function,
  onPageLoad: Function,
  termsAndConditions: string
};

type PersonalInfoFormState = {
  uploading: boolean,
  values: PersonalInfoType,
  errors: {
    email: null | string,
    firstName: null | string,
    lastName: null | string,
    dob: null | string,
    phoneNo: null | string,
    nativeLanguage: null | string,
    passportNo: null | string,
    passportImage: null | string,
    country: null | string,
    isTermsAccepted: boolean
  },
  hasErrors: boolean
};

class PersonalInfoForm extends PureComponent<
  PersonalInfoFormProps,
  PersonalInfoFormState
> {
  constructor(props: PersonalInfoFormProps) {
    super(props);
    const {
      studentInfo: {
        email,
        firstName,
        lastName,
        dob,
        phoneNo,
        nativeLanguage,
        passportNo,
        passportImage,
        promotions,
        isTermsAccepted
      },
      passport: country
    } = props;
    this.state = {
      values: {
        email,
        firstName,
        lastName,
        dob,
        phoneNo,
        nativeLanguage,
        passportNo,
        passportImage,
        country,
        promotions,
        isTermsAccepted
      },
      errors: {
        email: null,
        firstName: null,
        lastName: null,
        dob: null,
        phoneNo: null,
        nativeLanguage: null,
        passportNo: null,
        passportImage: null,
        country: null
      },
      hasErrors: false,
      uploading: false
    };
    // $FlowFixMe
    this.onChange = this.onChange.bind(this);
    // $FlowFixMe
    this.handleInputChange = this.handleInputChange.bind(this);
    // $FlowFixMe
    this.handleFormFieldChange = this.handleFormFieldChange.bind(this);
    // $FlowFixMe
    this.resetPassportImageError = this.resetPassportImageError.bind(this);
  }

  componentDidMount() {
    this.props.onPageLoad(4);
  }

  async onChange(event: SyntheticInputEvent<HTMLInputElement>) {
    this.setState({
      uploading: true
    });

    this.resetPassportImageError();

    let passportImage = "";
    if (event.target.files.length > 0) {
      this.getBase64(event.target.files[0], (result, name) => {
        passportImage = result;

        let bookingService = serviceManager.get("BookingService");

        bookingService
          .uploadPassport({
            file: passportImage,
            name: name,
            type: "image/jpeg"
          })
          .then(({ path }) => {
            this.handleFormFieldChange("passportImage", path);
            this.setState({
              uploading: false
            });
          })
          .catch(() => {
            this.setFormErrors("passportImage", translate("Failed to upload"));
          });
      });
    }
  }

  getBase64(file: any, callBack: Function) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function() {
      callBack(reader.result, file.name);
    };
    reader.onerror = function() {
      this.setFormErrors("passportImage", translate("Failed to upload"));
    };
  }

  handleInputChange(event: SyntheticInputEvent<HTMLInputElement>) {
    this.setState({
      values: {
        ...this.state.values,
        [event.target.id]: event.target.value
      }
    });
  }

  handleFormFieldChange(name: string, value: string) {
    if (name === "country") {
      this.props.handleUserPassportChange(value);
    }
    this.setState(({ values }) => ({
      values: {
        ...values,
        [name]: value
      }
    }));
  }

  resetPassportImageError() {
    this.setState(({ errors }) => ({
      errors: {
        ...errors,
        passportImage: null
      }
    }));
  }

  setFormErrors(field: string, message: string) {
    this.setState(state => {
      return {
        errors: {
          ...state.errors,
          [field]: message
        },
        hasErrors: true
      };
    });
  }

  validateForm() {
    const {
      values: {
        email,
        firstName,
        nativeLanguage,
        lastName,
        dob,
        phoneNo,
        passportNo,
        passportImage,
        country,
        isTermsAccepted
      }
    } = this.state;

    let hasError = false;

    this.resetFormErrors();

    if (email === "") {
      this.setFormErrors("email", translate("Email is required."));
      hasError = true;
    } else if (!isEmail(email)) {
      this.setFormErrors("email", translate("Email is invalid."));
      hasError = true;
    }

    if (firstName === "") {
      this.setFormErrors("firstName", translate("First Name is required."));
      hasError = true;
    }

    if (lastName === "") {
      this.setFormErrors("lastName", translate("Last Name is required."));
      hasError = true;
    }
    if (dob === "") {
      this.setFormErrors("dob", translate("Date of birth is required."));
      hasError = true;
    } else if (!isValid(new Date(dob))) {
      this.setFormErrors(
        "dob",
        translate("Date of birth format is incorrect.")
      );
      hasError = true;
    } else if (1 !== compareAsc(new Date(), dob)) {
      this.setFormErrors("dob", translate("Date of birth is invalid."));
      hasError = true;
    }

    if (nativeLanguage === "") {
      this.setFormErrors(
        "nativeLanguage",
        translate("Native language is required.")
      );
      hasError = true;
    }

    if (phoneNo === "") {
      this.setFormErrors("phoneNo", translate("Phone number is required."));
      hasError = true;
    }

    if (passportNo === "") {
      this.setFormErrors(
        "passportNo",
        translate("Passport number is required.")
      );
      hasError = true;
    }

    if (passportImage === "") {
      this.setFormErrors(
        "passportImage",
        translate("Passport image is required.")
      );
      hasError = true;
    }

    if (country === "") {
      this.setFormErrors("country", translate("Passport is required."));
      hasError = true;
    }

    if (!isTermsAccepted) {
      this.setState({
        hasErrors: true
      });
      hasError = true;
    }
    return hasError;
  }

  resetFormErrors() {
    this.setState({
      errors: {
        email: null,
        firstName: null,
        lastName: null,
        dob: null,
        phoneNo: null,
        nativeLanguage: null,
        passportNo: null,
        passportImage: null,
        country: null,
        isTermsAccepted: null
      },
      hasErrors: false
    });
  }

  submitForm = () => {
    if (!this.validateForm()) {
      this.props.onSubmit(this.state.values, true);
    }
  };

  goBack = () => {
    this.props.onSubmit(this.state.values, false);
  };

  render() {
    const { role, termsAndConditions } = this.props;
    const { uploading, values, errors, hasErrors } = this.state;

    const countryList = [];
    for (const key in countries) {
      if (countries.hasOwnProperty(key)) {
        countryList.push({
          value: key,
          name: countries[key]
        });
      }
    }
    return (
      <Fragment>
        <p className="heading-4">{translate("STUDENT PERSONAL INFORMATION")}</p>
        <Row>
          <Col sm={12} md={6}>
            <div className="input-group">
              <label>{translate("First Name")}</label>
              <Input
                placeholder={translate("Enter your first name")}
                id="firstName"
                text={values.firstName}
                onChange={this.handleInputChange}
                error={errors.firstName}
              />
            </div>
          </Col>
          <Col sm={12} md={6}>
            <div className="input-group">
              <label>{translate("Last Name")}</label>
              <Input
                placeholder={translate("Enter your last name")}
                id="lastName"
                text={values.lastName}
                onChange={this.handleInputChange}
                error={errors.lastName}
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm={12} md={6}>
            <div className="input-group">
              <label>{translate("Date of Birth")}</label>
              <div className="form-input">
                <InputMask
                  mask="9999-99-99"
                  maskChar="_"
                  placeholder="yyyy-mm-dd"
                  onChange={event =>
                    this.handleFormFieldChange("dob", event.target.value)
                  }
                  value={values.dob}
                />
                {errors.dob !== null && (
                  <ul className="form-errors">
                    <li>{errors.dob}</li>
                  </ul>
                )}
              </div>
            </div>
          </Col>
          <Col sm={12} md={6}>
            <div className="input-group">
              <label>{translate("E-mail Address")}</label>
              <Input
                placeholder={translate("Enter your e-mail address")}
                id="email"
                text={values.email}
                error={errors.email}
                onChange={this.handleInputChange}
                disabled={role === USER_TYPE.STUDENT ? true : false}
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm={12} md={6}>
            <div className="input-group">
              <label>{translate("Native Language")}</label>
              <Input
                placeholder={translate("Enter your Native Language")}
                id="nativeLanguage"
                text={values.nativeLanguage}
                onChange={this.handleInputChange}
                error={errors.nativeLanguage}
              />
            </div>
          </Col>
          <Col sm={12} md={6}>
            <div className="input-group">
              <label>{translate("Phone Number")}</label>
              <div className="form-input">
                <PhoneInput
                  id="phoneNo"
                  defaultCountry={"us"}
                  inputClass="tel-input"
                  buttonClass={"dropdown"}
                  value={values.phoneNo}
                  onChange={phoneNo =>
                    this.handleFormFieldChange("phoneNo", phoneNo)
                  }
                />
                {errors.phoneNo !== null && (
                  <ul className="form-errors">
                    <li>{errors.phoneNo}</li>
                  </ul>
                )}
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm={12} md={6}>
            <div className="input-group">
              <label>{translate("Passport Number")}</label>
              <Input
                placeholder={translate("Enter your passport number")}
                id="passportNo"
                text={values.passportNo}
                onChange={this.handleInputChange}
                error={errors.passportNo}
              />
            </div>
          </Col>
          <Col sm={12} md={6}>
            <div className="input-group">
              <label>{translate("Passport Image")}</label>
              <Input
                id="passportImage"
                type="file"
                accept="image/png"
                onChange={this.onChange}
                error={errors.passportImage}
              />
              {values.passportImage !== "" && (
                <img
                  src={`${MEDIA_URL}/${values.passportImage}`}
                  alt="Passport"
                />
              )}
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm={12} md="6">
            <div className="input-group">
              <label>{translate("Passport")}</label>
              <Select
                options={countryList}
                selected={values.country}
                error={errors.country}
                placeholder={translate("Select your country")}
                onChange={country =>
                  this.handleFormFieldChange("country", country)
                }
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <Checkbox
              isChecked={values.promotions}
              onChange={() =>
                this.handleFormFieldChange("promotions", !values.promotions)
              }
              text={translate(
                "I want to receive Tambook promotional offers in the future"
              )}
            />
            <Checkbox
              isChecked={values.isTermsAccepted}
              onChange={() =>
                this.handleFormFieldChange(
                  "isTermsAccepted",
                  !values.isTermsAccepted
                )
              }
              text={translate("I have read and accepted")}
            >
              &nbsp;
              <a
                href={`${MEDIA_URL}/${termsAndConditions}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {translate("terms and conditions")}
              </a>
            </Checkbox>
          </Col>
        </Row>
        {hasErrors && (
          <Alert type={Alert.TYPE.ERROR}>
            {translate(
              "Please fill all required fields and accept terms and conditions to continue."
            )}
          </Alert>
        )}

        <div className="button-block">
          <Button
            size={Button.SIZE.MEDIUM}
            type={Button.TYPE.SECONDARY}
            onClick={this.goBack}
            loading={uploading}
          >
            {translate("STEP BACK")}
          </Button>
          <Button
            size={Button.SIZE.MEDIUM}
            type={Button.TYPE.SECONDARY}
            onClick={this.submitForm}
            loading={uploading}
          >
            {translate("NEXT STEP")}
          </Button>
        </div>
      </Fragment>
    );
  }
}

export default PersonalInfoForm;
