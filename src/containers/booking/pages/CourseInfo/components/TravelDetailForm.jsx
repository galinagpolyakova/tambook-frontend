// @flow
import React, { Component } from "react";

import RadioButton from "shared/components/RadioButton";
import Button from "shared/components/Button";

import translate from "translations/translate";

type TravelDetailFormProps = {
  prevStep: Function,
  nextStep: Function,
  onPageLoad: Function
};

export default class TravelDetailForm extends Component<TravelDetailFormProps> {
  componentDidMount() {
    this.props.onPageLoad(3);
  }

  render() {
    const { prevStep, nextStep } = this.props;

    return (
      <div className="booking-course-travel-details">
        <p className="heading-4">{translate("TRAVEL DETAILS")}</p>
        <div className="booking-course-travel-details-section">
          <p className="heading-6">{translate("INSURANCE")}</p>
          <p>
            {translate(
              "It is a legal requirement for all students studying abroad to have full medical and travel insurance coverage for the entire period they are studying overseas."
            )}
          </p>
          <RadioButton
            label={translate("I'll arrange insurance myself")}
            checked={true}
          />
        </div>
        <div className="booking-course-travel-details-section">
          <p className="heading-6">{translate("AIRPORT PICK UP")}</p>
          <RadioButton
            label={translate(
              "I'll arrange transportation from the airport myself"
            )}
            checked={true}
          />
        </div>
        <div className="step-nav button-block">
          <Button
            size={Button.SIZE.MEDIUM}
            type={Button.TYPE.SECONDARY}
            onClick={prevStep}
          >
            {translate("STEP BACK")}
          </Button>
          <Button
            size={Button.SIZE.MEDIUM}
            type={Button.TYPE.SECONDARY}
            onClick={nextStep}
          >
            {translate("NEXT STEP")}
          </Button>
        </div>
      </div>
    );
  }
}
