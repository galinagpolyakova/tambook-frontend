// @flow
import type {
  ConsolidatedHomestayType,
  CourseType,
  DiscountType,
  CourseFeeType
} from "containers/course/types";
import { type UserProps } from "containers/auth/types";

import React, { PureComponent, Fragment } from "react";
import Sidebar, { Widget } from "components/Sidebar";
import Currency from "components/Currency";
import AccommodationForm, { type AccommodationType } from "./AccommodationForm";
import translate from "translations/translate";
import { USER_TYPE } from "containers/auth/constants";

const STRIPE = "stripe";

type BookingSidebarProps = {
  selectedWeek: $PropertyType<AccommodationType, "homeStayWeeks">,
  homestayType: $PropertyType<AccommodationType, "type">,
  courseFee: $PropertyType<CourseFeeType, "price">,
  discount: $PropertyType<DiscountType, "discountedPrice">,
  homeStayWeeks: $PropertyType<AccommodationType, "homeStayWeeks">,
  paymentMethod: typeof STRIPE,
  currencyType: $PropertyType<CourseType, "currencyType">,
  enrolmentFee: $PropertyType<CourseType, "enrolmentFee">,
  homeStayFees: {
    under18Price: $PropertyType<ConsolidatedHomestayType, "under18Price">,
    homestayFee: $PropertyType<ConsolidatedHomestayType, "homestayFee">,
    above18Price: $PropertyType<ConsolidatedHomestayType, "above18Price">
  },
  agentCommission: number,
  role: $PropertyType<UserProps, "role">
};

export default class BookingSidebar extends PureComponent<BookingSidebarProps> {
  static STRIPE = STRIPE;

  render() {
    const {
      selectedWeek,
      courseFee,
      discount,
      homestayType,
      homeStayWeeks,
      paymentMethod,
      currencyType,
      enrolmentFee,
      homeStayFees: { under18Price, homestayFee, above18Price },
      agentCommission,
      role
    } = this.props;

    if (selectedWeek === null || homeStayWeeks === null) {
      return null;
    }

    const courseFeePerWeek = discount === 0 ? courseFee : discount;

    const accommodationCharge =
      homestayType === AccommodationForm.ACCOMMODATION_OPTIONS.IS_BELOW_18
        ? under18Price * homeStayWeeks + homestayFee
        : above18Price * homeStayWeeks + homestayFee;

    const chargesWithoutAccommodation =
      selectedWeek * courseFeePerWeek + enrolmentFee + accommodationCharge;

    const chargesWithAccommodation =
      selectedWeek * courseFeePerWeek + enrolmentFee;

    const paymentGatewayCharges =
      paymentMethod === BookingSidebar.STRIPE ? 1.065 : 1;

    const transactionCharges = AccommodationForm.ACCOMMODATION_OPTIONS
      .SELF_ARRANGE
      ? chargesWithAccommodation * 0.065
      : chargesWithoutAccommodation * 0.065;

    const agentCommissionCharges =
      ((selectedWeek * courseFeePerWeek) / 100) * agentCommission;

    const total =
      homestayType === AccommodationForm.ACCOMMODATION_OPTIONS.SELF_ARRANGE
        ? (chargesWithAccommodation - agentCommissionCharges) *
          paymentGatewayCharges
        : (chargesWithoutAccommodation - agentCommissionCharges) *
          paymentGatewayCharges;
    return (
      <Sidebar>
        {selectedWeek !== null && courseFee && (
          <Widget
            title={translate("COURSE FEES")}
            className="course-fee-widget"
          >
            <div className="course-fees">
              <div className="course-fee-item">
                <span className="course-fee-title">
                  {translate("Course fee")}
                </span>
                <span className="course-fee">
                  <Currency currencyType={currencyType}>
                    {selectedWeek * courseFee}
                  </Currency>
                </span>
              </div>
              <div className="course-fee-item">
                <span className="course-fee-title">
                  {translate("School registration fee")}
                </span>
                <span className="course-fee">
                  <Currency currencyType={currencyType}>
                    {enrolmentFee}
                  </Currency>
                </span>
              </div>
              {discount !== 0 && (
                <Fragment>
                  <div className="course-fee-item regular-price">
                    <span className="course-fee-title">
                      {translate("Regular Price")}
                    </span>
                    <span className="course-fee">
                      <Currency currencyType={currencyType}>
                        {selectedWeek * courseFee + enrolmentFee}
                      </Currency>
                    </span>
                  </div>
                  <div className="course-fee-item">
                    <span className="course-fee-title">
                      {translate("Savings")}
                    </span>
                    <span className="course-fee">
                      -
                      <Currency currencyType={currencyType}>
                        {selectedWeek * (courseFee - discount)}
                      </Currency>
                    </span>
                  </div>
                </Fragment>
              )}
              {homestayType !==
                AccommodationForm.ACCOMMODATION_OPTIONS.SELF_ARRANGE &&
                accommodationCharge !== 0 && (
                  <div className="course-fee-item">
                    <span className="course-fee-title">
                      {translate("Accommodation")}
                    </span>
                    <span className="course-fee">
                      <Currency currencyType={currencyType}>
                        {accommodationCharge}
                      </Currency>
                    </span>
                  </div>
                )}
              {paymentMethod === BookingSidebar.STRIPE && (
                <div className="course-fee-item">
                  <span className="course-fee-title">
                    {translate("Transaction Charges")}
                  </span>
                  <span className="course-fee">
                    <Currency currencyType={currencyType}>
                      {transactionCharges}
                    </Currency>
                  </span>
                </div>
              )}
              {role === USER_TYPE.AGENT && (
                <div className="course-fee-item">
                  <span className="course-fee-title">
                    {translate("Agent Commission")}
                  </span>
                  <span className="course-fee">
                    <Currency currencyType={currencyType}>
                      {agentCommissionCharges}
                    </Currency>
                  </span>
                </div>
              )}
              <div className="course-fee-item total">
                <span className="course-fee-title">{translate("Total")}</span>
                <span className="course-fee">
                  <Currency currencyType={currencyType}>{total}</Currency>
                </span>
              </div>
              <div className="course-fee-item total">
                <span className="course-fee-title">
                  {translate("Actual Course Price")}
                </span>
                <span className="course-fee">
                  <Fragment>
                    {currencyType}&nbsp;
                    {total.toFixed(2)}
                  </Fragment>
                </span>
              </div>
            </div>
          </Widget>
        )}
      </Sidebar>
    );
  }
}
