// @flow
import { type CourseType, type CourseFeeType } from "containers/course/types";
import { type AuthState } from "containers/auth/store/reducer";
import { type AgentState } from "containers/agent/store/reducer";

import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Steps from "shared/components/Steps";
import addWeeks from "shared/utils/dates/addWeeks";
import format from "shared/utils/dates/format";

import translate from "translations/translate";

import PageHeader from "components/PageHeader";
import Loader from "components/Loader";

import BookingDetailForm from "./components/BookingDetailForm";
import AccommodationForm, {
  type AccommodationType
} from "./components/AccommodationForm";
import TravelDetailForm from "./components/TravelDetailForm";
import PersonalInfoForm, {
  type PersonalInfoType
} from "./components/PersonalInfoForm";
import PaymentSchemeForm from "./components/PaymentSchemeForm";
import BookingSidebar from "./components/sidebar";

import { USER_TYPE } from "containers/auth/constants";
import { queryParamsParse } from "shared/helpers/url";

import { getCourse } from "containers/course/store/actions";
import { updateCourseBooking, courseBookingStep } from "../../store/actions";
import { changeUserPassport } from "containers/profile/store/actions";
import { getAgentByEmail } from "containers/agent/store/actions";

import { getConsolidatedCourse } from "containers/course/store/selectors";
import { ASYNC_STATUS } from "constants/async";
import { type AsyncStatusType } from "types/general";

import "./styles.scss";

type CourseInfoProps = {
  updateCourseBooking: Function,
  match: {
    params: {
      courseId: number
    }
  },
  email: $PropertyType<AuthState, "email">,
  role: $PropertyType<AuthState, "role">,
  getCourse: Function,
  course: CourseType,
  startDates: any,
  passport: string,
  location: any,
  currencyType: string,
  loading: boolean,
  agent: $PropertyType<AgentState, "data">,
  status: AsyncStatusType,
  changeUserPassport: Function,
  getAgentByEmail: Function,
  courseBookingStep: Function
};

type CourseInfoState = {
  studentInfo: PersonalInfoType,
  accommodation: AccommodationType,
  payment: {
    method: string
  },
  isRangeSelected: boolean,
  selectedWeek: null | number,
  selectedRange: number | null,
  courseStartDate: Date | null,
  step: number
};

class CourseInfo extends PureComponent<CourseInfoProps, CourseInfoState> {
  STEP = 4;

  constructor(props) {
    super(props);
    this.state = {
      studentInfo: {
        email: props.role === USER_TYPE.STUDENT ? props.email : "",
        firstName: "",
        lastName: "",
        dob: "",
        phoneNo: "",
        nativeLanguage: "",
        passportNo: "",
        passportImage: "",
        country: "",
        promotions: false,
        isTermsAccepted: false
      },
      accommodation: {
        type: AccommodationForm.ACCOMMODATION_OPTIONS.SELF_ARRANGE,
        startDate: null,
        homeStayWeeks: 1,
        youngChildren: AccommodationForm.SELECT_OPTIONS[0].value,
        olderChildren: AccommodationForm.SELECT_OPTIONS[0].value,
        olderAdults: AccommodationForm.SELECT_OPTIONS[0].value,
        pets: AccommodationForm.SELECT_OPTIONS[0].value,
        smoke: AccommodationForm.SELECT_OPTIONS[0].value,
        foodRestrictions: AccommodationForm.SELECT_OPTIONS[0].value,
        alergies: AccommodationForm.SELECT_OPTIONS[0].value,
        medicalConditions: AccommodationForm.SELECT_OPTIONS[0].value
      },
      payment: {
        method: ""
      },
      selectedRange: null,
      isRangeSelected: false,
      selectedWeek: null,
      courseStartDate: null,
      step: 1
    };

    // $FlowFixMe
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    // $FlowFixMe
    this.onRangeSelect = this.onRangeSelect.bind(this);
    // $FlowFixMe
    this.handleCourseStartDate = this.handleCourseStartDate.bind(this);
    // $FlowFixMe
    this.handleSelectedWeek = this.handleSelectedWeek.bind(this);
    // $FlowFixMe
    this.nextStep = this.nextStep.bind(this);
    // $FlowFixMe
    this.prevStep = this.prevStep.bind(this);
    // $FlowFixMe
    this.handlePaymentMethodChange = this.handlePaymentMethodChange.bind(this);
    // $FlowFixMe
    this.getStepForms = this.getStepForms.bind(this);
    // $FlowFixMe
    this.onPersonalInfoFormSubmit = this.onPersonalInfoFormSubmit.bind(this);
    // $FlowFixMe
    this.onAccommodationChange = this.onAccommodationChange.bind(this);
    // $FlowFixMe
    this.onAccommodationFormSubmit = this.onAccommodationFormSubmit.bind(this);
  }

  componentDidMount() {
    const {
      match: {
        params: { courseId }
      },
      getCourse,
      role,
      agent
    } = this.props;
    // $FlowFixMe
    const query = queryParamsParse(this.props.location.search);
    getCourse(courseId);

    if ("selectedWeek" in query) {
      this.setState({
        selectedWeek: query.selectedWeek
      });
    } else {
      this.setState({
        selectedWeek: 0
      });
    }
    if ("selectedRange" in query) {
      this.onRangeSelect(query.selectedRange);
    }
    if (role === USER_TYPE.AGENT && agent === null) {
      this.props.getAgentByEmail();
    }
  }

  handleFormSubmit() {
    const {
      match: {
        params: { courseId, bookingId }
      },
      course: { schoolId }
    } = this.props;
    const {
      selectedWeek: weeks,
      courseStartDate: startDate,
      studentInfo,
      accommodation
    } = this.state;

    this.props.updateCourseBooking({
      bookingId,
      courseId,
      email: studentInfo.email,
      accommodation,
      travelDetails: {},
      bookingSummery: {
        weeks,
        schoolId,
        startDate,
        endDate:
          startDate !== null
            ? format(addWeeks(new Date(startDate), weeks), "yyyy-MM-dd")
            : null
      },
      studentInfo: {
        ...studentInfo,
        dob: format(new Date(studentInfo.dob), "yyyy-MM-dd")
      }
    });
  }

  onRangeSelect(selectedRange: number) {
    this.setState({
      selectedRange,
      isRangeSelected: true
    });
  }

  getWeekRange = (
    min: $PropertyType<CourseFeeType, "weeksMin">,
    max: $PropertyType<CourseFeeType, "weeksMax">
  ) => {
    let ranges = [];

    let start = min === null ? 1 : parseInt(min);
    const end = max === null ? 50 : parseInt(max);

    while (start <= end) {
      ranges.push(start);
      start++;
    }

    return ranges;
  };

  handleCourseStartDate(courseStartDate) {
    this.setState({
      courseStartDate: courseStartDate
    });
  }

  handleSelectedWeek(selectedWeek) {
    this.setState({
      selectedWeek: selectedWeek
    });
  }

  nextStep() {
    const { step } = this.state;

    if (step !== this.STEP) {
      this.setState(() => ({
        step: step + 1
      }));
    }
  }

  prevStep() {
    this.setState(({ step }) => ({
      step: step - 1
    }));
  }

  handlePaymentMethodChange(method) {
    this.setState(({ payment }) => ({
      payment: {
        ...payment,
        method: method
      }
    }));
  }

  onPersonalInfoFormSubmit(studentInfo, gotoNext) {
    const step = gotoNext ? 5 : 3;
    this.setState({
      studentInfo,
      step
    });
  }

  onAccommodationChange(name, value) {
    this.setState(state => ({
      accommodation: {
        ...state.accommodation,
        [name]: value
      }
    }));
  }

  onAccommodationFormSubmit(accommodation) {
    const step = 3;
    this.setState({
      accommodation,
      step
    });
  }

  getStepForms() {
    const { course, passport, loading, role } = this.props;

    let {
      courseStartDate,
      selectedWeek,
      selectedRange,
      studentInfo,
      accommodation
    } = this.state;

    const fees = course.courseFees.map((courseFee, key) => ({
      name:
        courseFee.weeksMin === courseFee.weeksMax
          ? `${courseFee.weeksMin} weeks`
          : `${courseFee.weeksMin} - ${courseFee.weeksMax} weeks`,
      value: key
    }));

    let range = null;
    let options = [];

    if (selectedRange !== null) {
      range = course.courseFees[selectedRange];
      options = this.getWeekRange(range.weeksMin, range.weeksMax);
    }

    const isFixedCourse = range === null || range.weeksMin === null;

    return [
      {
        title: translate("Course"),
        content: (
          <BookingDetailForm
            course={course}
            courseStartDate={courseStartDate}
            selectedWeek={selectedWeek}
            isFixedCourse={isFixedCourse}
            fees={fees}
            selectedRange={selectedRange}
            range={range}
            options={options}
            onRangeSelect={this.onRangeSelect}
            handleCourseStartDate={this.handleCourseStartDate}
            handleSelectedWeek={this.handleSelectedWeek}
            nextStep={this.nextStep}
            onPageLoad={this.props.courseBookingStep}
          />
        )
      },
      {
        title: translate("Accommodation"),
        content: (
          <AccommodationForm
            homestays={course.homestays}
            currencyType={course.currencyType}
            options={options}
            selectedWeek={selectedWeek}
            onAccommodationChange={this.onAccommodationChange}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            onSubmit={this.onAccommodationFormSubmit}
            data={accommodation}
            onPageLoad={this.props.courseBookingStep}
          />
        )
      },
      {
        title: translate("Travel"),
        content: (
          <TravelDetailForm
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            onPageLoad={this.props.courseBookingStep}
          />
        )
      },
      {
        title: translate("Personal Info"),
        content: (
          <PersonalInfoForm
            termsAndConditions={course.termsAndConditions}
            onSubmit={this.onPersonalInfoFormSubmit}
            passport={passport}
            handleUserPassportChange={this.props.changeUserPassport}
            studentInfo={studentInfo}
            role={role}
            onPageLoad={this.props.courseBookingStep}
          />
        )
      },
      {
        title: translate("Payment"),
        content: (
          <PaymentSchemeForm
            handlePaymentMethodChange={this.handlePaymentMethodChange}
            handleFormSubmit={this.handleFormSubmit}
            prevStep={this.prevStep}
            nextStep={this.nextStep}
            loading={loading}
            onPageLoad={this.props.courseBookingStep}
          />
        )
      }
    ];
  }

  render() {
    const { course, status, passport, agent, role } = this.props;
    let {
      selectedWeek,
      selectedRange,
      step,
      accommodation: { type, homeStayWeeks },
      payment: { method }
    } = this.state;
    let discount = 0;

    if (course === null || status === ASYNC_STATUS.LOADING) {
      return <Loader isLoading />;
    }

    let range = null;

    if (selectedRange !== null) {
      course.discountList.map(discountItem => {
        if (discountItem.weeksMin === null && discountItem.weeksMax === null) {
          if (discountItem.countryOfOrigin.includes(passport)) {
            discount = discountItem.discountedPrice;
          }
        } else {
          if (
            selectedWeek !== null &&
            discountItem.weeksMin <= selectedWeek &&
            discountItem.weeksMax >= selectedWeek &&
            discountItem.countryOfOrigin.includes(passport)
          ) {
            discount = discountItem.discountedPrice;
          }
        }
        return true;
      });
      range = course.courseFees[selectedRange];
    }

    if (range === null || course === null) {
      return null;
    }
    return (
      <div className="course-booking-page">
        <PageHeader
          title={translate("COURSE BOOKING")}
          breadcrumbs={[
            {
              title: translate("Booking")
            }
          ]}
        />
        <div className="container">
          <Row className="course-details-columns">
            <Col sm="12" md="8">
              <Steps steps={this.getStepForms()} current={step} />
            </Col>
            <Col sm="12" md="4" className="sidebar-container">
              <BookingSidebar
                selectedWeek={selectedWeek}
                courseFee={range.price}
                currencyType={course.currencyType}
                enrolmentFee={course.enrolmentFee}
                homeStayFees={{
                  under18Price:
                    course.homestays[0] && course.homestays[0].under18Price
                      ? course.homestays[0].under18Price
                      : 0,
                  homestayFee:
                    course.homestays[0] && course.homestays[0].homestayFee
                      ? course.homestays[0].homestayFee
                      : 0,
                  above18Price:
                    course.homestays[0] && course.homestays[0].above18Price
                      ? course.homestays[0].above18Price
                      : 0
                }}
                discount={discount}
                homestayType={type}
                homeStayWeeks={homeStayWeeks}
                paymentMethod={method}
                agentCommission={
                  agent !== null && agent.agentDetails
                    ? agent.agentDetails.agentCommission
                    : 0
                }
                role={role}
              />
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    course: getConsolidatedCourse(state),
    status: state.course.status,
    loading: state.booking.loading,
    email: state.auth.email,
    role: state.auth.role,
    passport: state.profile.settings.passport,
    agent: state.agent.data
  };
};

const Actions = {
  getCourse,
  updateCourseBooking,
  changeUserPassport,
  getAgentByEmail,
  courseBookingStep
};

export default connect(
  mapStateToProps,
  Actions
)(CourseInfo);
