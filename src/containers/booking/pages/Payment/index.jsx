// @flow
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { StripeProvider, Elements } from "react-stripe-elements";

import PageHeader from "components/PageHeader";
import Alert from "shared/components/Alert";
import Button from "shared/components/Button";
import translate from "translations/translate";

import CheckoutForm from "../../components/CheckoutForm";

import { finalizePayment, showPaymentError } from "../../store/actions";

import { STRIPE_KEY } from "config/stripe";

import "../../styles.scss";
import { USER_TYPE } from "containers/auth/constants";

type PaymentProps = {
  isPaymentSuccess: boolean,
  isPaymentLoading: boolean,
  finalizePayment: Function,
  showPaymentError: Function,
  role: $PropertyType<AuthState, "role">,
  match: any,
  error: null | string
};

class Payment extends Component<PaymentProps> {
  render() {
    const {
      match: {
        params: { bookingId }
      },
      isPaymentLoading,
      error,
      isPaymentSuccess,
      role
    } = this.props;
    return (
      <Fragment>
        <PageHeader
          title={translate("COURSE BOOKING")}
          breadcrumbs={[
            {
              title: translate("Booking")
            }
          ]}
        />
        <div className="container payment-page">
          {isPaymentSuccess ? (
            <div className="text-center payment-success">
              <Alert isFullWidth={true} type={Alert.TYPE.SUCCESS}>
                {translate(
                  "Your payment has been successfully processed. Thank you."
                )}
              </Alert>
              <Button
                htmlType={Button.HTML_TYPE.LINK}
                link={
                  role === USER_TYPE.AGENT
                    ? "/agent-profile/orders"
                    : "/profile"
                }
              >
                {translate("BACK TO BOOKINGS")}
              </Button>
            </div>
          ) : (
            <StripeProvider apiKey={STRIPE_KEY}>
              <div className="checkout">
                <Elements>
                  <CheckoutForm
                    isLoading={isPaymentLoading}
                    handleResult={({ token, error }) => {
                      error
                        ? this.props.showPaymentError({
                            error: error.message
                          })
                        : this.props.finalizePayment({
                            cardToken: token.id,
                            bookingId
                          });
                    }}
                  />
                </Elements>
                {error && (
                  <Alert isFullWidth={true} type={Alert.TYPE.ERROR}>
                    {translate(error)}
                  </Alert>
                )}
              </div>
            </StripeProvider>
          )}
        </div>
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    isPaymentSuccess: state.booking.isPaymentSuccess,
    isPaymentLoading: state.booking.isPaymentLoading,
    role: state.auth.role,
    error: state.booking.error
  };
}

const Action = {
  finalizePayment,
  showPaymentError
};

export default connect(
  mapStateToProps,
  Action
)(Payment);
