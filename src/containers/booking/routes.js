// import lib
import { lazy } from "react";
import { USER_TYPE } from "containers/auth/constants";

export default [
  {
    path: "/booking/course/:bookingId/:courseId",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT, USER_TYPE.AGENT],
    component: lazy(() => import("./pages/CourseInfo"))
  },
  {
    path: "/booking/payment/:bookingId",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT, USER_TYPE.AGENT],
    component: lazy(() => import("./pages/Payment"))
  }
];
