// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";

export class BookingService {
  api: ApiServiceInterface;

  endpoint: string = "/booking";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  _normalizeBooking(apiBooking: Object) {
    return {
      id: apiBooking.bookingId,
      title: apiBooking.course.name,
      duration: apiBooking.bookingSummery.weeks,
      date: apiBooking.createdDate,
      status: apiBooking.paymentStatus,
      courseID: apiBooking.course.id
    };
  }

  _normalizeBookings(apiBookings: Array<Object>) {
    return apiBookings.map(apiBooking =>
      this._normalizeBooking(apiBooking._source)
    );
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  bookACourse(payload: Object) {
    return this.api.put(this.endpoint, payload);
  }

  updateCourseBooking(id: string, payload: Object) {
    return this.api.put(`${this.endpoint}/${id}`, payload);
  }

  finalizePayment(payload: Object) {
    return this.api.post(`${this.endpoint}/payment`, payload);
  }

  getBookings(filters: Object = {}) {
    return this.api
      .get(`${this.endpoint}`, filters)
      .then(({ result, pagination: { total_pages: totalPages }, current }) => ({
        list: this._normalizeBookings(result),
        pagination: {
          current,
          totalPages
        }
      }))
      .catch(() => ({
        list: [],
        pagination: { current: 0, totalPages: 0 }
      }));
  }

  uploadPassport(payload: Object) {
    return this.api.post(`/user/uploads`, payload);
  }

  _normalizeAgentBooking({
    agentInfo: {
      agentDetails: { agentCommission: agentCommissionLevel }
    },
    course: { courseShortName: courseName },
    studentInfo: { firstName, lastName },
    createdDate,
    bookingSummery: { currency, agentCommission, agentCommissionInUSD },
    paymentType,
    bookingStatus,
    bookingId,
    course: { id: courseId },
    orderTotal
  }) {
    return {
      studentName: `${firstName} ${lastName}`,
      date: createdDate,
      agentCommission,
      currency,
      agentCommissionInUSD,
      agentCommissionLevel,
      courseName,
      paymentType,
      bookingStatus,
      bookingId,
      orderTotal,
      courseId
    };
  }

  _normalizeAgentUnfinishedBooking({
    course: { courseShortName: courseName },
    createdDate,
    bookingStatus,
    bookingId,
    course: { id: courseId }
  }) {
    return {
      date: createdDate,
      courseName,
      bookingStatus,
      bookingId,
      courseId
    };
  }

  _normalizeAgentBookings(bookings, type) {
    if (type === "Unfinished") {
      return bookings.map(({ _source }) =>
        this._normalizeAgentUnfinishedBooking(_source)
      );
    } else {
      return bookings.map(({ _source }) =>
        this._normalizeAgentBooking(_source)
      );
    }
  }

  getAgentBooking(filters: Object = {}) {
    return this.api
      .get(`${this.endpoint}/agent-booking`, filters)
      .then(({ result, pagination }) => {
        return {
          list: this._normalizeAgentBookings(result, filters.type),
          pagination
        };
      });
  }

  getAgentBookingSummery(filters: Object = {}) {
    return this.api
      .get(`${this.endpoint}/booking-summary`, {}, { ...filters })
      .then(response => {
        return {
          response
        };
      });
  }

  getBooking(bookingId: any) {
    return this.api.get(`${this.endpoint}/${bookingId}`);
  }
}
