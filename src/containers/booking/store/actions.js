import {
  BOOK_A_COURSE_INIT,
  BOOK_A_COURSE_FAILURE,
  FINALIZE_PAYMENT_SUCCESS,
  FINALIZE_PAYMENT_FAILURE,
  FINALIZE_PAYMENT_INIT,
  SHOW_PAYMENT_ERROR,
  BOOKING_COURSE_STEP,
  BOOKING_ACCOMMODATION_STEP,
  BOOKING_TRAVEL_STEP,
  BOOKING_PERSONAL_INFO_STEP,
  BOOKING_PAYMENT_STEP,
  BOOK_A_COURSE_SUCCESS
} from "./actionTypes";
import { push } from "connected-react-router";
import { getLanguageFromUrl } from "shared/helpers/translations";

function bookACourseInit() {
  return {
    type: BOOK_A_COURSE_INIT
  };
}

function bookACourseFailure() {
  return {
    type: BOOK_A_COURSE_FAILURE
  };
}

function bookACourseSuccess() {
  return {
    type: BOOK_A_COURSE_SUCCESS
  };
}

export function bookACourse(
  bookingSummery,
  courseId,
  selectedWeek,
  selectedRange
) {
  return (dispatch, getState, serviceManager) => {
    dispatch(bookACourseInit());

    let bookingService = serviceManager.get("BookingService");
    bookingService
      .bookACourse(bookingSummery)
      .then(({ result: { bookingId } }) => {
        if (bookingId !== null) {
          dispatch(bookACourseSuccess());
          dispatch(
            push(
              `/${getLanguageFromUrl()}/booking/course/${bookingId}/${courseId}?selectedWeek=${selectedWeek}&selectedRange=${selectedRange}`
            )
          );
        } else {
          dispatch(bookACourseFailure());
        }
      })
      .catch(() => {
        dispatch(bookACourseFailure());
      });
  };
}

export function updateCourseBooking(bookingSummery) {
  return (dispatch, getState, serviceManager) => {
    dispatch(bookACourseInit());

    let bookingService = serviceManager.get("BookingService");
    bookingService
      .bookACourse(bookingSummery)
      .then(({ result: { bookingId } }) => {
        if (bookingId !== null) {
          dispatch(bookACourseSuccess());
          dispatch(
            push(`/${getLanguageFromUrl()}/booking/payment/${bookingId}`)
          );
        } else {
          dispatch(bookACourseFailure());
        }
      })
      .catch(() => {
        dispatch(bookACourseFailure());
      });
  };
}

function finalizePaymentSuccess() {
  return {
    type: FINALIZE_PAYMENT_SUCCESS
  };
}
function finalizePaymentFailure(payload) {
  return {
    type: FINALIZE_PAYMENT_FAILURE,
    payload
  };
}
function finalizePaymentInit() {
  return {
    type: FINALIZE_PAYMENT_INIT
  };
}

export function finalizePayment(bookingSummery) {
  return (dispatch, getState, serviceManager) => {
    dispatch(finalizePaymentInit());

    let bookingService = serviceManager.get("BookingService");
    let profileService = serviceManager.get("ProfileService");

    bookingService
      .finalizePayment({ bookingSummery })
      .then(({ success, error, result }) => {
        if (success) {
          const {
            bookingSummary: { bookingId, bookingOwner, bookingType },
            course: {
              id: courseId,
              textAnonse_en,
              country,
              schoolDetail: { name }
            }
          } = result;

          if ("direct" === bookingType) {
            profileService.notifyPaymentConfirmation({
              notificationType: "booking_status_update",
              message: `The order "${textAnonse_en} at ${name} in ${country}" has successfully proceed.`,
              meta: {},
              notifyUser: bookingOwner
            });
          }

          dispatch(
            finalizePaymentSuccess({
              bookingId,
              courseId
            })
          );
        } else {
          dispatch(finalizePaymentFailure({ error }));
        }
      })
      .catch(({ message: error }) => {
        dispatch(finalizePaymentFailure({ error }));
      });
  };
}

export function showPaymentError(payload) {
  return {
    type: SHOW_PAYMENT_ERROR,
    payload
  };
}

export function courseBookingStep(step) {
  switch (step) {
    case 1:
      return {
        type: BOOKING_COURSE_STEP
      };
    case 2:
      return {
        type: BOOKING_ACCOMMODATION_STEP
      };
    case 3:
      return {
        type: BOOKING_TRAVEL_STEP
      };
    case 4:
      return {
        type: BOOKING_PERSONAL_INFO_STEP
      };
    case 5:
      return {
        type: BOOKING_PAYMENT_STEP
      };
    default:
      break;
  }
}
