// @flow
import type { Action } from "shared/types/ReducerAction";

import {
  BOOK_A_COURSE_INIT,
  BOOK_A_COURSE_FAILURE,
  FINALIZE_PAYMENT_SUCCESS,
  FINALIZE_PAYMENT_FAILURE,
  FINALIZE_PAYMENT_INIT,
  SHOW_PAYMENT_ERROR,
  BOOK_A_COURSE_SUCCESS
} from "./actionTypes";

export type BookingState = {
  loading: boolean,
  isBookingSuccess: boolean
};

const initialState: BookingState = {
  loading: false,
  isPaymentLoading: false,
  isBookingSuccess: false,
  isPaymentSuccess: false,
  bookingId: null,
  bookingPayload: {},
  error: null
};

function bookACourseInit(state) {
  return {
    ...state,
    loading: true,
    isPaymentLoading: false,
    isPaymentSuccess: false
  };
}

function bookACourseFailure(state) {
  return {
    ...state,
    loading: false,
    error: "Something went wrong. Please try again."
  };
}

function finalizePaymentInit(state) {
  return {
    ...state,
    error: null,
    isPaymentLoading: true
  };
}

function finalizePaymentSuccess(state) {
  return {
    ...state,
    isPaymentLoading: false,
    isPaymentSuccess: true
  };
}

function finalizePaymentFailure(state, { error }) {
  return {
    ...state,
    error,
    isPaymentLoading: false
  };
}

function showPaymentError(state, { error }) {
  return {
    ...state,
    error
  };
}

function bookACourseSuccess(state) {
  return {
    ...state,
    loading: false
  };
}

const reducer = (
  state: BookingState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case BOOK_A_COURSE_INIT:
      return bookACourseInit(state);
    case BOOK_A_COURSE_SUCCESS:
      return bookACourseSuccess(state);
    case BOOK_A_COURSE_FAILURE:
      return bookACourseFailure(state);
    case FINALIZE_PAYMENT_SUCCESS:
      return finalizePaymentSuccess(state);
    case FINALIZE_PAYMENT_INIT:
      return finalizePaymentInit(state);
    case FINALIZE_PAYMENT_FAILURE:
      return finalizePaymentFailure(state, payload);
    case SHOW_PAYMENT_ERROR:
      return showPaymentError(state, payload);
    default:
      return state;
  }
};

export default reducer;
