// @flow
import { type CourseType, type CourseFeeType } from "../types";
import { type AuthState } from "../../auth/store/reducer";

import React, { PureComponent, Fragment } from "react";
import classnames from "classnames";

import RadioButton from "shared/components/RadioButton";
import Button from "shared/components/Button";
import Widget from "components/Sidebar/Widget";
import Select from "shared/components/Select";
import Currency from "components/Currency";
import Alert from "shared/components/Alert";

import { USER_STATUS, USER_TYPE } from "../../auth/constants";
import { ASYNC_STATUS } from "constants/async";
import translate from "translations/translate";

type CourseFeeProps = {
  fees: $PropertyType<CourseType, "courseFees">,
  enrolmentFee: $PropertyType<CourseType, "enrolmentFee">,
  bookACourse: Function,
  discountList: $PropertyType<CourseType, "discountList">,
  passport: string,
  isPaymentEnabled: boolean,
  isTermsAndConditionsAdded: boolean,
  currencyType: $PropertyType<CourseType, "currencyType">,
  notifySchoolStatus: string,
  userRole: $PropertyType<AuthState, "role">,
  userStatus: $PropertyType<AuthState, "status">,
  isBookingLoading: boolean,
  onClickNotifySchool: Function,
  isInterestExpressed: boolean
};

type CourseFeeState = {
  range: {
    weeksMin: number,
    weeksMax: number,
    price: number
  },
  isRangeSelected: boolean,
  selectedWeek: number | null,
  selectedRange: number,
  isFixedPrice: boolean,
  isBookingLoading: boolean
};

class CourseFee extends PureComponent<CourseFeeProps, CourseFeeState> {
  constructor() {
    super();

    this.state = {
      range: {
        weeksMin: 0,
        weeksMax: 0,
        price: 0
      },
      selectedRange: 0,
      isRangeSelected: false,
      selectedWeek: 0,
      isFixedPrice: false,
      discount: 0
    };
    // $FlowFixMe
    this.getDiscountedPrice = this.getDiscountedPrice.bind(this);
  }

  onRangeSelect = (selectedRange: number) => {
    let range = this.props.fees[selectedRange];
    if (range.weeksMin === null) {
      this.setState({
        isFixedPrice: true,
        selectedWeek: 0,
        selectedRange
      });
    }
    this.setState({
      range,
      isRangeSelected: true,
      selectedRange,
      selectedWeek: 0
    });
  };

  componentDidMount() {
    this.onRangeSelect(0);
  }

  getWeekRange = (
    min: $PropertyType<CourseFeeType, "weeksMin">,
    max: $PropertyType<CourseFeeType, "weeksMax">
  ) => {
    let ranges = [];

    let start = min === null ? 1 : parseInt(min);
    const end = max === null ? 50 : parseInt(max);

    while (start <= end) {
      ranges.push(start);
      start++;
    }

    return ranges;
  };

  getBookNowButton = (defaultSelectedWeek: number) => {
    const {
      bookACourse,
      isPaymentEnabled,
      isTermsAndConditionsAdded,
      onClickNotifySchool,
      notifySchoolStatus,
      userRole,
      userStatus,
      isBookingLoading,
      isInterestExpressed
    } = this.props;
    let { selectedRange, selectedWeek } = this.state;
    selectedWeek = selectedWeek === 0 ? defaultSelectedWeek : selectedWeek;
    if (userRole === USER_TYPE.AGENT && userStatus !== USER_STATUS.APPROVED) {
      return (
        <Fragment>
          <Alert isFullWidth={true} type={Alert.TYPE.ERROR}>
            {translate(
              "Please wait until the Tambook Admin approves your account"
            )}
          </Alert>
        </Fragment>
      );
    } else {
      return isPaymentEnabled && isTermsAndConditionsAdded ? (
        <Button
          onClick={() => bookACourse(selectedWeek, selectedRange)}
          fullWidth
          loading={isBookingLoading}
        >
          {translate("BOOK NOW")}
        </Button>
      ) : (
        <Fragment>
          <Alert>
            {translate(
              "We are unable to book this course as the school has not completed the onboarding process."
            )}
          </Alert>
          {isInterestExpressed ? (
            <Alert isFullWidth={true} type={Alert.TYPE.SUCCESS}>
              {translate(
                "We have sent your request to notify the school. You will be able to book the course once the school has added the booking details"
              )}
            </Alert>
          ) : (
            <Button
              onClick={onClickNotifySchool}
              fullWidth
              loading={notifySchoolStatus === ASYNC_STATUS.LOADING}
            >
              {translate("Express Interest")}
            </Button>
          )}
          {notifySchoolStatus === ASYNC_STATUS.FAILURE && (
            <Fragment>
              <Alert isFullWidth={true} type={Alert.TYPE.ERROR}>
                {translate("Something went wrong. Please try again.")}
              </Alert>
            </Fragment>
          )}
        </Fragment>
      );
    }
  };

  getDiscountedPrice({ weeksMin, weeksMax }, passport) {
    const { discountList } = this.props;
    let discountedPrices = [];

    if (discountList.length > 0) {
      discountList.map(discountItem => {
        if (
          discountItem.weeksMin === weeksMin &&
          discountItem.weeksMax === weeksMax &&
          discountItem.countryOfOrigin.includes(passport)
        ) {
          discountedPrices.push(discountItem.discountedPrice);
        }
        return true;
      });
    }

    const discountedPrice =
      discountedPrices.length > 0 ? Math.min(...discountedPrices) : null;

    return discountedPrice;
  }

  render() {
    const {
      fees,
      enrolmentFee,
      discountList,
      passport,
      currencyType
    } = this.props;
    const {
      range: { weeksMin, weeksMax, price },
      isRangeSelected,
      selectedRange,
      isFixedPrice,
      selectedWeek: selectedCourseWeek
    } = this.state;
    let discount = 0;
    let discountRanges = [];

    const options = this.getWeekRange(weeksMin, weeksMax);

    let selectedWeek = selectedCourseWeek;
    if (selectedCourseWeek === 0) {
      selectedWeek = options[0];
    }

    discountList.map(discountItem => {
      if (discountItem.weeksMin === null && discountItem.weeksMax === null) {
        if (discountItem.countryOfOrigin.includes(passport)) {
          discount = discountItem.discountedPrice;
        }
      } else {
        if (selectedWeek !== null) {
          if (
            discountItem.weeksMin <= selectedWeek &&
            discountItem.weeksMax >= selectedWeek &&
            discountItem.countryOfOrigin.includes(passport)
          ) {
            discount = discountItem.discountedPrice;
          }
        }
      }
      return true;
    });

    fees.map(courseFee => {
      let discountedPrice = this.getDiscountedPrice(courseFee, passport);
      discountRanges.push(discountedPrice);
      return true;
    });

    return (
      <Widget title={translate("COURSE FEES")} className="course-fees">
        <ul className="fees-list">
          <Fragment>
            {!isFixedPrice &&
              fees.map((courseFee, key) => (
                <li
                  key={key}
                  className={classnames("list-item", {
                    active: key === selectedRange
                  })}
                >
                  <div className="duration">
                    <RadioButton
                      onChange={() => this.onRangeSelect(key)}
                      value={key}
                      checked={key === selectedRange}
                      name="course-duration"
                      label={
                        courseFee.weeksMin === courseFee.weeksMax
                          ? `${courseFee.weeksMin} weeks`
                          : `${courseFee.weeksMin} - ${
                              courseFee.weeksMax
                            } weeks`
                      }
                    />
                  </div>
                  {discountRanges[key] !== null &&
                  discountRanges[key] < courseFee.price ? (
                    <div className="discounted-prices">
                      <div className="old-price">
                        <Currency currencyType={currencyType}>
                          {courseFee.price}
                        </Currency>
                        /{translate("week")}
                      </div>
                      <div className="price">
                        <span>
                          <Currency currencyType={currencyType}>
                            {discountRanges[key]}
                          </Currency>
                          /
                        </span>
                        {translate("week")}
                      </div>
                    </div>
                  ) : (
                    <div className="price">
                      <span>
                        <Currency currencyType={currencyType}>
                          {courseFee.price}
                        </Currency>
                        /
                      </span>
                      {translate("week")}
                    </div>
                  )}
                </li>
              ))}
            {isRangeSelected && (
              <Fragment>
                <li className="list-item">
                  <span className="course-fee-title">
                    <Select
                      onChange={selectedWeek => this.setState({ selectedWeek })}
                      selected={selectedWeek}
                      options={options}
                    />
                    <span>{translate("weeks")}</span>
                  </span>
                  <span className="course-fee">
                    <Currency currencyType={currencyType}>{price}</Currency>
                  </span>
                </li>
              </Fragment>
            )}
          </Fragment>

          {isRangeSelected && selectedWeek !== null && (
            <Fragment>
              <li className="list-item">
                <span className="course-fee-title">
                  {translate("Course fee")}
                </span>
                <span className="course-fee">
                  <Currency currencyType={currencyType}>
                    {selectedWeek * price}
                  </Currency>
                </span>
              </li>
              {enrolmentFee !== 0 && enrolmentFee !== null && (
                <li className="list-item">
                  <span className="course-fee-title">
                    {translate("School registration fee")}
                  </span>
                  <span className="course-fee">
                    <Currency currencyType={currencyType}>
                      {enrolmentFee}
                    </Currency>
                  </span>
                </li>
              )}
              {discount !== 0 && (
                <li className="list-item">
                  <span className="course-fee-title">
                    {translate("Regular price")}
                  </span>
                  <span className="course-fee">
                    <Currency currencyType={currencyType}>
                      {selectedWeek * price + enrolmentFee}
                    </Currency>
                  </span>
                </li>
              )}
              {discount !== 0 && (
                <li className="list-item">
                  <span className="course-fee-title">
                    {translate("Savings")}
                  </span>
                  <span className="course-fee">
                    -&nbsp;
                    <Currency currencyType={currencyType}>
                      {selectedWeek * (price - discount)}
                    </Currency>
                  </span>
                </li>
              )}
              <li className="total list-item">
                <span className="course-fee-title">{translate("Total")}</span>
                <span className="course-fee">
                  <Currency currencyType={currencyType}>
                    {discount !== 0
                      ? discount * selectedWeek + enrolmentFee
                      : price * selectedWeek + enrolmentFee}
                  </Currency>
                </span>
              </li>
            </Fragment>
          )}
        </ul>
        {this.getBookNowButton(options[0])}
      </Widget>
    );
  }
}

export default CourseFee;
