// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Parser from "html-react-parser";

import { getCourseActivities, getCourseFacilities } from "../store/actions";

import Tabs from "shared/components/Tabs";
import Section from "shared/components/Section";

import translate from "translations/translate";

type CourseInformationProps = {
  getCourseActivities: Function,
  getCourseFacilities: Function,
  information: {
    description: string,
    activities: string[],
    facilities: string[]
  },
  activities: Array<{
    name: string,
    image: string
  }>,
  facilities: Array<{
    name: string,
    image: string
  }>
};

class CourseInformation extends PureComponent<CourseInformationProps> {
  componentDidMount() {
    const {
      information: { activities, facilities }
    } = this.props;
    if (activities !== undefined && activities.length > 0) {
      this.props.getCourseActivities({
        list: activities.toString()
      });
    }
    if (facilities !== undefined && facilities.length > 0) {
      this.props.getCourseFacilities({
        list: facilities.toString()
      });
    }
  }
  render() {
    const { information, activities, facilities } = this.props;
    const activitiesContent = activities.map((activity, key) => (
      <div className="facility" key={key}>
        <img src={activity.image} alt={activity.name} />
        <span>{activity.name}</span>
      </div>
    ));

    const facilitiesContent = facilities.map((facility, key) => (
      <div className="facility" key={key}>
        <img src={facility.image} alt={facility.name} />
        <span>{facility.name}</span>
      </div>
    ));
    const items = [
      {
        title: translate("Description"),
        content: Parser(information.description)
      },
      {
        title: translate("Activities"),
        content: (
          <div className="facilities-list">
            {activities.length > 0 ? (
              activitiesContent
            ) : (
              <span>{translate("No Activities")}</span>
            )}
            <div className="clearfix" />
          </div>
        )
      },
      {
        title: translate("Facilities"),
        content: (
          <div className="facilities-list">
            {facilities.length > 0 ? (
              facilitiesContent
            ) : (
              <span>{translate("No Facilities")}</span>
            )}
            <div className="clearfix" />
          </div>
        )
      }
    ];
    return (
      <Section className="course-details">
        <Tabs items={items} />
      </Section>
    );
  }
}

function mapStateToProps(state) {
  return {
    activities: state.course.course.activities,
    facilities: state.course.course.facilities
  };
}

const Action = {
  getCourseActivities,
  getCourseFacilities
};

export default connect(
  mapStateToProps,
  Action
)(CourseInformation);
