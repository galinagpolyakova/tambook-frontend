// @flow
import type { CourseType } from "../types";
import { type AuthState } from "../../auth/store/reducer";

import React, { PureComponent } from "react";

import countries from "shared/data/countries";

import Button from "shared/components/Button";
import Sidebar, { Widget } from "components/Sidebar";
import Currency from "components/Currency";
import Rate from "shared/components/Rate";

//import Homestay from "components/Homestay/Homestay";
import InfoWidget from "components/infoWidget";
import CourseFee from "./CourseFee";
import Map from "shared/components/Map.jsx";
import translate from "translations/translate.js";
import { UserSettingsConsumer } from "contexts/UserSettings";

type CourseSidebarProps = {
  courseId: number,
  location: {
    city: string,
    country: string
  },
  courseFees: $PropertyType<CourseType, "courseFees">,
  workEligibility: $PropertyType<CourseType, "workEligibility">,
  passport: string,
  language: string,
  age: $PropertyType<CourseType, "minAge">,
  rating: number,
  isBookmarked: boolean,
  isBookmarkLoading: boolean,
  price: $PropertyType<CourseType, "startingPrice">,
  googleMap: $PropertyType<CourseType, "googleMap">,
  homestays: $PropertyType<CourseType, "homestays">,
  enrolmentFee: $PropertyType<CourseType, "enrolmentFee">,
  selectedCurrency: string,
  discountList: $PropertyType<CourseType, "discountList">,
  isPaymentEnabled: $PropertyType<CourseType, "isPaymentEnabled">,
  isTermsAndConditionsAdded: $PropertyType<CourseType,
    "isTermsAndConditionsAdded">,
  currencyType: $PropertyType<CourseType, "currencyType">,
  googleCordinates: $PropertyType<CourseType, "googleCordinates">,
  notifySchoolStatus: string,
  userRole: $PropertyType<AuthState, "role">,
  userStatus: $PropertyType<AuthState, "status">,
  isBookingLoading: boolean,
  onAddToList: Function,
  onRemoveFromList: Function,
  bookACourse: Function,
  onClickNotifySchool: Function,
  isInterestExpressed: boolean
};

class CourseSidebar extends PureComponent<CourseSidebarProps> {
  render() {
    const {
      courseId,
      location,
      courseFees,
      workEligibility,
      passport,
      language,
      age,
      rating,
      price,
      googleMap,
      // homestays,
      isBookmarked,
      enrolmentFee,
      bookACourse,
      selectedCurrency,
      discountList,
      isPaymentEnabled,
      isTermsAndConditionsAdded,
      currencyType,
      googleCordinates,
      isBookmarkLoading,
      onClickNotifySchool,
      notifySchoolStatus,
      userRole,
      userStatus,
      isBookingLoading,
      isInterestExpressed
    } = this.props;
    return (
      <Sidebar>
        <UserSettingsConsumer>
          {({ toggleUserPreference }) => (
            <Widget className="filter">
              <div className="country">
                {translate("Passport")}:&nbsp;
                <span onClick={toggleUserPreference}>
                  {passport === ""
                    ? translate("Not selected")
                    : translate(countries[passport])}
                </span>
              </div>
              <div className="currency">
                {translate("Currency")}:{" "}
                <span onClick={toggleUserPreference}>
                  {translate(selectedCurrency)}
                </span>
              </div>
            </Widget>
          )}
        </UserSettingsConsumer>
        <Widget title={translate("DETAILS")} className="summary">
          <div className="summary-line">
            <span>{translate("Language")}: </span> {language}
          </div>
          <div className="summary-line">
            <span>{translate("Price")}: </span>
            <Currency currencyType={currencyType}>{price}</Currency> /&nbsp;
            {translate("week")}
          </div>
          <div className="summary-line">
            <span>{translate("Location")}: </span> {location.city}
            ,&nbsp;
            {location.country}
          </div>
          <div className="summary-line">
            <span>{translate("Eligible For Work")}:</span>&nbsp;
            {workEligibility ? "YES" : "NO"}
          </div>
          <div className="summary-line">
            <span>{translate("Rating")}:</span>
            <div className="ratings">
              <Rate value={rating} />
            </div>
          </div>
          <div className="summary-line">
            <span>{translate("Min Age")}:</span> {age} {translate("years")}
          </div>
          <Button
            onClick={() =>
              isBookmarked
                ? this.props.onRemoveFromList()
                : this.props.onAddToList()
            }
            size={Button.SIZE.SMALL}
            className="add-to-list"
            loading={isBookmarkLoading}
          >
            <i />
            {isBookmarked
              ? translate("Remove From List")
              : translate("Add To List")}
          </Button>
        </Widget>

        <CourseFee
          fees={courseFees}
          courseId={courseId}
          discountList={discountList}
          enrolmentFee={enrolmentFee}
          bookACourse={bookACourse}
          passport={passport}
          isPaymentEnabled={isPaymentEnabled}
          isTermsAndConditionsAdded={isTermsAndConditionsAdded}
          currencyType={currencyType}
          onClickNotifySchool={onClickNotifySchool}
          notifySchoolStatus={notifySchoolStatus}
          userRole={userRole}
          userStatus={userStatus}
          isBookingLoading={isBookingLoading}
          isInterestExpressed={isInterestExpressed}
        />
        {googleCordinates.latitude !== null &&
        googleCordinates.longitude !== null ? (
          <Widget>
            <div style={{ minHeight: "200px", width: "100%" }}>
              <Map
                lat={googleCordinates.latitude}
                lng={googleCordinates.longitude}
              />
            </div>
          </Widget>
        ) : (
          googleMap !== null && (
            <Widget>
              <iframe
                title="map"
                src={googleMap}
                width="100%"
                height="250"
                frameBorder="0"
              />
            </Widget>
          )
        )}
        {/*<Widget
          title="ACCOMMODATION"
          type={Widget.types.LIGHT}
          className="homestay-widget"
        >
          {homestays.map((homestay, key) => (
            <Homestay key={key} {...homestay} currencyType={currencyType} />
          ))}
          </Widget>*/}
        <Widget>
          <Button
            htmlType={Button.HTML_TYPE.LINK}
            link="/check-visa-requirements"
            fullWidth
          >
            {translate("Check Visa Requirements")}
          </Button>
        </Widget>
        <InfoWidget />
      </Sidebar>
    );
  }
}

export default CourseSidebar;
