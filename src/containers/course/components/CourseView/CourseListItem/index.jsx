// @flow
import { type ConsolidatedCourseType } from "../../../types";

import React, { PureComponent, Fragment } from "react";
import classNames from "classnames";

import { Rating } from "shared/components/Rate";
import Col from "shared/components/Col";
import Row from "shared/components/Row";
import Button from "shared/components/Button";
import IconButton from "shared/components/IconButton";
import Tooltip from "shared/components/Tooltip";

import Currency from "components/Currency";
import translate from "translations/translate";

import "./styles.scss";

type CourseProps = {
  id: $PropertyType<ConsolidatedCourseType, "id">,
  title: $PropertyType<ConsolidatedCourseType, "title">,
  location: $PropertyType<ConsolidatedCourseType, "location">,
  price: $PropertyType<ConsolidatedCourseType, "price">,
  image: $PropertyType<ConsolidatedCourseType, "image">,
  rating: $PropertyType<ConsolidatedCourseType, "rating">,
  description: $PropertyType<ConsolidatedCourseType, "description">,
  duration: $PropertyType<ConsolidatedCourseType, "duration">,
  isBookmarked: $PropertyType<ConsolidatedCourseType, "isBookmarked">,
  currencyType: $PropertyType<ConsolidatedCourseType, "currencyType">,
  isAddedToCompare: $PropertyType<ConsolidatedCourseType, "isAddedToCompare">,
  discountedPrice: $PropertyType<ConsolidatedCourseType, "discountedPrice">,

  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function
};

class Course extends PureComponent<CourseProps> {
  addCourseToBookmarkList = (
    courseId: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.addCourseToBookmarkList) {
      this.props.addCourseToBookmarkList(courseId);
    }
  };

  removeCourseFromBookmarkList = (
    courseId: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.removeCourseFromBookmarkList) {
      this.props.removeCourseFromBookmarkList(courseId);
    }
  };

  addCourseToCompare = (id: $PropertyType<ConsolidatedCourseType, "id">) => {
    if (this.props.addCourseToCompare) {
      this.props.addCourseToCompare({ id });
    }
  };

  removeCourseFromCompare = (
    id: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.removeCourseFromCompare) {
      this.props.removeCourseFromCompare({ id });
    }
  };

  render() {
    const {
      id,
      title,
      location,
      price,
      image,
      rating,
      description,
      duration,
      isBookmarked,
      currencyType,
      isAddedToCompare,
      discountedPrice
    } = this.props;

    return (
      <div className="course-list-item">
        <div className="featured-image">
          <img src={image} alt={title} />
        </div>
        <div className="course-content">
          <Row className="detail-block">
            <Col sm={12} md="8">
              <div className="course-duration">
                {duration} {translate("hours / week")}
              </div>
              <div className="course-title">{title}</div>
              <div className="location-title">
                {location.city}, {location.country}
              </div>
              <Rating rate={rating.rate} reviews={rating.reviews} />
              <div className="course-description">{description}</div>
            </Col>
            <Col sm={12} md="4" className="course-cta">
              <div className="price">
                {translate("from")}
                <span>
                  {discountedPrice !== null && discountedPrice < price ? (
                    <Fragment>
                      <span className="old-price">
                        <Currency currencyType={currencyType}>{price}</Currency>
                      </span>
                      <Currency currencyType={currencyType}>
                        {discountedPrice}
                      </Currency>
                    </Fragment>
                  ) : (
                    <Currency currencyType={currencyType}>{price}</Currency>
                  )}
                </span>
                {translate("week")}
              </div>
              <div className="button-group">
                <Tooltip
                  position={Tooltip.position.BOTTOM}
                  message={translate(
                    isAddedToCompare
                      ? "Remove from compare list"
                      : "Add to compare list"
                  )}
                >
                  <IconButton
                    icon="exchange-alt"
                    className={classNames({ active: isAddedToCompare })}
                    onClick={() => {
                      isAddedToCompare
                        ? this.removeCourseFromCompare(id)
                        : this.addCourseToCompare(id);
                    }}
                  />
                </Tooltip>
                <Tooltip
                  position={Tooltip.position.BOTTOM}
                  message={translate(
                    isBookmarked
                      ? "Remove from bookmark list"
                      : "Add to bookmark list"
                  )}
                >
                  <IconButton
                    icon="bookmark"
                    className={classNames({ active: isBookmarked })}
                    onClick={() =>
                      isBookmarked
                        ? this.removeCourseFromBookmarkList(id)
                        : this.addCourseToBookmarkList(id)
                    }
                  />
                </Tooltip>
              </div>
              <Button
                type={Button.TYPE.SECONDARY}
                size={Button.SIZE.SMALL}
                htmlType={Button.HTML_TYPE.LINK}
                link={`/course/${id}`}
              >
                {translate("More Info")}
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Course;
