// @flow
import type {
  ConsolidatedCourseType,
  ConsolidatedCoursesListType
} from "../../types";
import React, { PureComponent, Fragment } from "react";

import CourseListItem from "./CourseListItem";
import CourseGridItem from "components/Course";
import translate from "translations/translate";

import SortButton from "shared/components/SortButton";
import { ItemsLayout, ItemsLayoutToggler } from "shared/components/ItemsLayout";
import Pagination, { type PaginationType } from "shared/components/Pagination";

import { UserSettingsConsumer } from "contexts/UserSettings";

import "./styles.scss";

type CourseViewProps = {
  ratingSort: string | null,
  priceSort: string | null,
  selectedSorting: string | null,
  courses: ConsolidatedCoursesListType,
  pagination: PaginationType,
  showFilter: boolean,

  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function,
  filter: Function,
  onPageChanged: Function
};

class CourseView extends PureComponent<CourseViewProps> {
  static defaultProps = {
    showFilter: true
  };
  render() {
    const {
      courses,
      filter,
      pagination,
      onPageChanged,
      addCourseToCompare,
      removeCourseFromCompare,
      addCourseToBookmarkList,
      removeCourseFromBookmarkList,
      ratingSort,
      priceSort,
      showFilter,
      selectedSorting
    } = this.props;

    return (
      <UserSettingsConsumer>
        {({ viewMode, changeViewMode }) => (
          <Fragment>
            {showFilter && (
              <div className="page-filters">
                <div className="layout-switcher">
                  <SortButton
                    onClick={filter}
                    filter="priceSort"
                    sorting={priceSort}
                    sortingType={SortButton.SELECTED_SORTING.PRICE}
                    selectedSorting={selectedSorting}
                  >
                    {translate("Sort by price")}
                  </SortButton>
                  <SortButton
                    onClick={filter}
                    filter="ratingSort"
                    sorting={ratingSort}
                    sortingType={SortButton.SELECTED_SORTING.RATING}
                    selectedSorting={selectedSorting}
                  >
                    {translate("Sort by ranking")}
                  </SortButton>
                </div>
                <div className="layout-switcher">
                  <div className="label">{translate("View")}: </div>
                  <ItemsLayoutToggler
                    viewMode={viewMode}
                    changeViewMode={changeViewMode}
                  />
                </div>
              </div>
            )}

            <ItemsLayout
              items={courses}
              viewMode={viewMode}
              gridByLine={3}
              gridItemComponent={(props: { item: ConsolidatedCourseType }) => {
                return (
                  <CourseGridItem
                    {...props.item}
                    addCourseToBookmarkList={addCourseToBookmarkList}
                    removeCourseFromBookmarkList={removeCourseFromBookmarkList}
                    addCourseToCompare={addCourseToCompare}
                    removeCourseFromCompare={removeCourseFromCompare}
                  />
                );
              }}
              listItemComponent={(props: { item: ConsolidatedCourseType }) => {
                return (
                  <CourseListItem
                    {...props.item}
                    addCourseToBookmarkList={addCourseToBookmarkList}
                    removeCourseFromBookmarkList={removeCourseFromBookmarkList}
                    addCourseToCompare={addCourseToCompare}
                    removeCourseFromCompare={removeCourseFromCompare}
                  />
                );
              }}
            />
            {pagination !== null && (
              <Pagination
                totalPages={pagination.pages}
                currentPage={pagination.currentPage}
                onPageChanged={onPageChanged}
              />
            )}
          </Fragment>
        )}
      </UserSettingsConsumer>
    );
  }
}

export default CourseView;
