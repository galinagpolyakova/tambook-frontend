// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import Parser from "html-react-parser";

import Accordion from "shared/components/Accordion";
import Section, { SectionBody, SectionHeader } from "shared/components/Section";
import translate from "translations/translate";
import Loader from "components/Loader";
import { getFaq } from "../store/actions";

type FaqSectionProps = {
  country: string,
  faq: Array<Object>,
  faqLoading: boolean,
  getFaq: Function
};

class FaqSection extends Component<FaqSectionProps> {
  static defaultProps = {
    faqLoading: false,
    faq: []
  };

  componentDidMount() {
    const { getFaq, country } = this.props;
    getFaq({ country });
  }

  getQuestionsList() {
    const { faq } = this.props;

    return faq.map(({ title, content }) => ({
      title: title,
      content: <div>{Parser(content)}</div>
    }));
  }

  render() {
    const { faqLoading, faq } = this.props;

    if (faqLoading) {
      return <Loader isLoading />;
    }

    if (!(faq.length > 0)) {
      return "";
    }

    return (
      <Section>
        <SectionHeader
          heading={translate("IF YOU HAVE ANY QUESTIONS")}
          position="left"
        />
        <SectionBody>
          <Accordion items={this.getQuestionsList()} />
        </SectionBody>
      </Section>
    );
  }
}

const Actions = {
  getFaq
};

const mapStateToProps = state => {
  return {
    faq: state.course.faq,
    isFaqLoading: state.course.isFaqLoading
  };
};

export default connect(
  mapStateToProps,
  Actions
)(FaqSection);
