// @flow
import React, { Component } from "react";
import classNames from "classnames";
import Checkbox from "shared/components/Checkbox";

type FilterGroupProps = {
  title: string,
  filter: string,
  items: Array<any>,
  clickHandler: Function,
  selected: string
};

type FilterGroupState = {
  isOpen: boolean
};

class FilterGroup extends Component<FilterGroupProps, FilterGroupState> {
  onClickHandler = (value: Object) => {
    this.props.clickHandler(value);
  };
  render() {
    const { title, filter, items, selected } = this.props;
    return (
      <div className={classNames("filter-group")}>
        <div className="filter-title">{title}</div>
        <ul className="filters">
          {items.map((item, key) => (
            <li
              key={key}
              // $FlowFixMe
              className={classNames({ active: item[filter] === selected })}
            >
              <Checkbox
                isChecked={item[filter] === selected}
                onChange={() =>
                  this.onClickHandler({
                    [filter]: item[filter] === selected ? "" : item[filter]
                  })
                }
              />

              <div className="filter-text">
                {item[filter]} <span className="count">({item.count})</span>
              </div>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default FilterGroup;
