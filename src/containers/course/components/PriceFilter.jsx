// @flow
import React, { PureComponent, Fragment } from "react";
import Slider, { createSliderWithTooltip } from "rc-slider";
import Button from "shared/components/Button";
import translate from "translations/translate";

const Range = createSliderWithTooltip(Slider.Range);

type PriceFilterProps = {
  searchCourses: Function,
  selectedMinPrice: number,
  selectedMaxPrice: number,
  minPrice: number,
  maxPrice: number,
  rates: Object,
  userCurrency: string
};
type PriceFilterState = {
  minPrice: number,
  maxPrice: number
};

class PriceFilter extends PureComponent<PriceFilterProps, PriceFilterState> {
  static defaultProps = {
    minPrice: 0,
    maxPrice: 4000
  };
  constructor(props: PriceFilterProps) {
    super(props);

    const { selectedMinPrice, selectedMaxPrice } = this.props;

    this.state = {
      minPrice: selectedMinPrice
        ? this.getValuesInUserCurrency(selectedMinPrice)
        : this.getValuesInUserCurrency(props.minPrice),
      maxPrice: selectedMaxPrice
        ? this.getValuesInUserCurrency(selectedMaxPrice)
        : this.getValuesInUserCurrency(props.maxPrice)
    };
  }
  onChange = (value: Array<number>) => {
    const [minPrice, maxPrice] = value;
    this.setState({
      minPrice,
      maxPrice
    });
  };

  getValuesInUserCurrency(value: number) {
    if (this.props.rates === null) return value;
    return parseInt((value / this.props.rates["USD"]).toFixed(0));
  }

  getValuesInUSD(value: number) {
    if (this.props.rates === null) return value;
    return parseInt((value * this.props.rates["USD"]).toFixed(0));
  }

  filterByPrice = () => {
    const { minPrice, maxPrice } = this.state;
    this.props.searchCourses({
      minPrice: this.getValuesInUSD(minPrice),
      maxPrice: this.getValuesInUSD(maxPrice)
    });
  };

  render() {
    let { minPrice, maxPrice, userCurrency } = this.props;
    minPrice = this.getValuesInUserCurrency(this.props.minPrice);
    maxPrice = this.getValuesInUserCurrency(this.props.maxPrice);
    return (
      <Fragment>
        <div className="price-slider">
          <Range
            min={minPrice}
            max={maxPrice}
            value={[this.state.minPrice, this.state.maxPrice]}
            onChange={this.onChange}
          />
        </div>
        <div className="price-range">
          <span className="price-min">
            {userCurrency}&nbsp;{this.state.minPrice}
          </span>
          <span className="price-max">
            {userCurrency}&nbsp;{this.state.maxPrice}
          </span>
        </div>
        <Button
          size={Button.SIZE.SMALL}
          type={Button.TYPE.PRIMARY}
          onClick={this.filterByPrice}
        >
          {translate("Filter")}
        </Button>
      </Fragment>
    );
  }
}

export default PriceFilter;
