// @flow
import React, { PureComponent, Fragment } from "react";
import Checkbox from "shared/components/Checkbox";
import Rate from "shared/components/Rate";

type RatingsFilterProps = {
  searchCourses: Function,
  ratings: string
};
type RatingsFilterState = {
  rating: Array<number>
};

class RatingsFilter extends PureComponent<
  RatingsFilterProps,
  RatingsFilterState
> {
  state = {
    rating: []
  };

  toggleRating = (rating: number) => {
    let newState = {
      ...this.state
    };
    if (this.state.rating.includes(rating)) {
      newState.rating = this.state.rating.filter(rate => {
        return rate !== rating;
      });
    } else {
      newState = {
        ...newState,
        rating: [...newState.rating, rating]
      };
    }
    this.setState(newState);

    this.props.searchCourses({ rating: newState.rating.toString() });
  };

  componentDidMount() {
    const { ratings } = this.props;
    if (ratings !== undefined) {
      const rating = ratings.split(",").map(Number);
      this.setState({
        rating
      });
    }
  }

  render() {
    return (
      <Fragment>
        <div className="rating">
          <Checkbox
            isChecked={this.state.rating.includes(5)}
            onChange={() => this.toggleRating(5)}
          />
          <Rate value={5} />
        </div>
      </Fragment>
    );
  }
}

export default RatingsFilter;
