// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import type { ConsolidatedCoursesListType } from "../types";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Section, { SectionBody, SectionHeader } from "shared/components/Section";

import Course from "components/Course";
import translate from "translations/translate";

import {
  getRelatedCourses,
  addCourseToBookmarkList,
  removeCourseFromBookmarkList,
  addCourseToCompare,
  removeCourseFromCompare
} from "../store/actions";
import { getConsolidatedRelatedCourses } from "../store/selectors";
import { isAuthenticated } from "../../auth/store/selectors";
import { UserSettingsConsumer } from "contexts/UserSettings";

type RelatedCoursesProps = {
  courses: ConsolidatedCoursesListType,
  isAuthenticated: boolean,
  country: string,
  language: string,
  except: number,
  toggleAuth: Function,
  getRelatedCourses: Function,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  removeCourseFromCompare: Function,
  addCourseToCompare: Function
};

class RelatedCourses extends PureComponent<RelatedCoursesProps> {
  static defaultProps = {
    country: "",
    language: "",
    except: 0
  };

  constructor() {
    super();

    // $FlowFixMe
    this.addCourseToBookmarkList = this.addCourseToBookmarkList.bind(this);
  }
  componentDidMount() {
    const { country, language, except } = this.props;
    this.props.getRelatedCourses({
      country,
      language,
      except
    });
  }

  addCourseToBookmarkList(courseID) {
    if (this.props.addCourseToBookmarkList !== undefined) {
      if (this.props.isAuthenticated) {
        this.props.addCourseToBookmarkList(courseID);
      } else {
        this.props.toggleAuth();
      }
    }
  }

  render() {
    const {
      courses,
      removeCourseFromBookmarkList,
      addCourseToCompare,
      removeCourseFromCompare
    } = this.props;
    const rendered = courses.map((item, index) => {
      return (
        <Col md={3} sm={12} key={index}>
          <Course
            {...item}
            addCourseToBookmarkList={this.addCourseToBookmarkList}
            removeCourseFromBookmarkList={removeCourseFromBookmarkList}
            addCourseToCompare={addCourseToCompare}
            removeCourseFromCompare={removeCourseFromCompare}
          />
        </Col>
      );
    });
    return (
      courses.length > 0 && (
        <Section className="related-courses">
          <SectionHeader
            position="left"
            heading={translate("May Interest You")}
          />
          <SectionBody>
            <Row>{rendered}</Row>
          </SectionBody>
        </Section>
      )
    );
  }
}
function mapStateToProps(state) {
  return {
    courses: getConsolidatedRelatedCourses(state),
    isAuthenticated: isAuthenticated(state)
  };
}

const Action = {
  getRelatedCourses,
  addCourseToBookmarkList,
  removeCourseFromBookmarkList,
  addCourseToCompare,
  removeCourseFromCompare
};

const RelatedCourseContainer = connect(
  mapStateToProps,
  Action
)(RelatedCourses);

type RelatedCourseContextProps = {
  country: string,
  language: string,
  except: number
};

export default class RelatedCourseContext extends PureComponent<RelatedCourseContextProps> {
  static defaultProps = {
    country: "",
    language: "",
    except: 0
  };
  render() {
    return (
      <UserSettingsConsumer>
        {({ toggleAuth }) => (
          <RelatedCourseContainer {...this.props} toggleAuth={toggleAuth} />
        )}
      </UserSettingsConsumer>
    );
  }
}
