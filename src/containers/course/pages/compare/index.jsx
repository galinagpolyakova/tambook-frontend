// @flow
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import IconButton from "shared/components/IconButton";
import PageHeader from "components/PageHeader";
import Alert from "shared/components/Alert/Alert";
import Section from "shared/components/Section";
import translate from "translations/translate";
import Currency from "components/Currency";
import Link from "components/Link";

import {
  getComparedCourseList,
  removeCourseFromCompare
} from "../../store/actions";

import "./styles.scss";
import Button from "shared/components/Button/Button";

type CompareCoursesProps = {
  list: string[],
  compareListCourses: Object[],

  getComparedCourseList: Function,
  removeCourseFromCompare: Function
};

class CompareCourses extends Component<CompareCoursesProps> {
  componentDidMount() {
    if (this.props.list.length > 0) {
      this.props.getComparedCourseList({
        list: this.props.list
      });
    }
  }
  componentDidUpdate(prevProps) {
    if (this.props.list !== prevProps.list && this.props.list.length > 0) {
      this.props.getComparedCourseList({
        list: this.props.list
      });
    }
  }
  render() {
    const { list, compareListCourses } = this.props;
    return (
      <div>
        <PageHeader
          title={translate("Compare Courses")}
          breadcrumbs={[
            {
              title: translate("Compare Courses")
            }
          ]}
        />
        <div className="container">
          {list.length === 0 ? (
            <Section>
              <Alert>{translate("No courses in compare list")}</Alert>
            </Section>
          ) : (
            <Fragment>
              <Button
                onClick={() =>
                  this.props.removeCourseFromCompare({ id: "ALL" })
                }
              >
                {translate("Clear All")}
              </Button>
              <section className="courses-comparison-table">
                <div className="courses-compare-table">
                  <div className="features">
                    <div className="features-list">
                      <div className="top-info" />
                      <div>{translate("School")}</div>
                      <div>{translate("Price")}</div>
                      <div>{translate("Destination")}</div>
                      <div>{translate("Intensity")}</div>
                      <div>{translate("Duration")}</div>
                      <div>{translate("Ability to work")}</div>
                      <div>{translate("Course type")}</div>
                    </div>
                  </div>
                  <div className="courses-wrapper">
                    <div
                      className="courses-columns"
                      style={{
                        width: `${compareListCourses.length * 310}px`
                      }}
                    >
                      {compareListCourses.map(
                        ({
                          id,
                          image,
                          title,
                          price,
                          location,
                          intensity,
                          duration,
                          eligibleToWork,
                          type,
                          currencyType,
                          schoolName
                        }) => (
                          <div className="course-item" key={id}>
                            <div className="features-list features-list-content">
                              <div className="top-info">
                                <IconButton
                                  icon="times-circle"
                                  className="close"
                                  onClick={() =>
                                    this.props.removeCourseFromCompare({ id })
                                  }
                                />
                                <img src={image} alt={title} />
                                <Link to={`/course/${id}`}>
                                  <h3>{title}</h3>
                                </Link>
                              </div>
                              <div>{schoolName}</div>
                              <div>
                                <Currency currencyType={currencyType}>
                                  {price}
                                </Currency>
                              </div>
                              <div>{location}</div>
                              <div>
                                {intensity}&nbsp;
                                {translate("HR/WK")}
                              </div>
                              <div>
                                {duration}
                                {duration !== "-" &&
                                  ` ${translate("hours / week")}`}
                              </div>
                              <div>{eligibleToWork ? "Yes" : "No"}</div>
                              <div>{type}</div>
                            </div>
                          </div>
                        )
                      )}
                    </div>
                  </div>
                </div>
              </section>
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    list: state.course.compareList,
    compareListCourses: state.course.compareListCourse
  };
}

const Actions = {
  getComparedCourseList,
  removeCourseFromCompare
};

export default connect(
  mapStateToProps,
  Actions
)(CompareCourses);
