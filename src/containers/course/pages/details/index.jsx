// @flow
import { type CourseType } from "../../types";
import { type AuthState } from "containers/auth/store/reducer";

import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import ImageSlider from "shared/components/ImageSlider";
import Rate, { Rating } from "shared/components/Rate";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Section, { SectionBody, SectionHeader } from "shared/components/Section";
import Alert from "shared/components/Alert";

import PageHeader from "components/PageHeader";
import Loader from "components/Loader";
import Review from "components/Review";
import Currency from "components/Currency";

import translate from "translations/translate";

import notFound from "assets/404.svg";

import CourseSidebar from "../../components/CourseSidebar";
import RelatedCourses from "../../components/RelatedCourses";
import FaqSection from "../../components/FaqSection";
import CourseInformation from "../../components/CourseInformation";

import {
  addCourseToBookmarkList,
  getCourse,
  getBookmarkCourseList,
  removeCourseFromBookmarkList,
  notifySchool
} from "../../store/actions";
import {
  getCourseId,
  getConsolidatedCourse,
  isInterestExpressed
} from "../../store/selectors";
import { isAuthenticated } from "containers/auth/store/selectors";
import { UserSettingsConsumer } from "contexts/UserSettings";

import { USER_STATUS, USER_TYPE } from "containers/auth/constants";
import { isUserAuthenticated } from "containers/auth/store/action";
import { ASYNC_STATUS } from "constants/async";
import { type AsyncStatusType } from "types/general";
import { bookACourse } from "containers/booking/store/actions";

import "./styles.scss";

export type CourseDetailsProps = {
  course: CourseType,
  courseId: number,
  status: AsyncStatusType,
  isBookmarkLoading: boolean,
  isAuthenticated: boolean,
  isCourseBookmarked: boolean,
  match: {
    params: {
      courseId: number
    }
  },
  history: any,
  selectedCurrency: string,
  passport: string,
  notifySchoolStatus: string,
  userRole: $PropertyType<AuthState, "role">,
  userStatus: $PropertyType<AuthState, "status">,
  isBookingLoading: boolean,
  getBookmarkCourseList: Function,
  removeCourseFromBookmarkList: Function,
  toggleAuth: Function,
  getCourse: Function,
  addCourseToBookmarkList: Function,
  notifySchool: Function,
  isUserAuthenticated: Function,
  bookACourse: Function,
  isInterestExpressed: boolean
};

class CourseDetails extends PureComponent<CourseDetailsProps> {
  componentDidMount() {
    const {
      match: {
        params: { courseId }
      },
      userStatus,
      userRole
    } = this.props;
    this.props.getCourse(courseId);
    this.props.getBookmarkCourseList();
    if (userRole === USER_TYPE.AGENT && userStatus === !USER_STATUS.APPROVED) {
      this.props.isUserAuthenticated();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.courseId !== this.props.match.params.courseId) {
      this.props.getCourse(this.props.match.params.courseId);
    }
  }

  addCourseToBookmarkList = () => {
    const { courseId } = this.props;
    if (this.props.isAuthenticated) {
      this.props.addCourseToBookmarkList(courseId);
    } else {
      this.props.toggleAuth();
    }
  };

  removeCourseFromBookmarkList = () => {
    const { courseId } = this.props;
    this.props.removeCourseFromBookmarkList(courseId);
  };

  bookACourse = (selectedWeek, selectedRange) => {
    const {
      courseId,
      course: { schoolId }
    } = this.props;
    if (this.props.isAuthenticated) {
      this.props.bookACourse(
        { courseId, bookingSummery: { schoolId } },
        courseId,
        selectedWeek,
        selectedRange
      );
    } else {
      this.props.toggleAuth();
    }
  };

  onClickNotifySchool = () => {
    const {
      course,
      passport,
      isAuthenticated,
      notifySchool,
      selectedCurrency
    } = this.props;

    notifySchool(
      {
        schoolName: course.schoolName,
        courseTitle: course.title,
        passport,
        currency: selectedCurrency,
        isAuthorized: isAuthenticated,
        toMail: course.email
      },
      course.id
    );
  };

  render() {
    const {
      course,
      selectedCurrency,
      passport,
      isBookmarkLoading,
      notifySchoolStatus,
      userRole,
      userStatus,
      status,
      isBookingLoading,
      isInterestExpressed
    } = this.props;
    const {
      match: {
        params: { courseId }
      }
    } = this.props;
    if (status === ASYNC_STATUS.FAILURE) {
      return (
        <div className="container course-error">
          <Section>
            <img src={notFound} alt="Not Found" />
            <div className="heading-2">{translate("Course not found")}</div>
          </Section>
        </div>
      );
    }
    if (course === null || status === ASYNC_STATUS.LOADING) {
      return <Loader isLoading />;
    }

    return (
      <div>
        <PageHeader
          title={course.title}
          breadcrumbs={[
            {
              title: translate("Courses"),
              link: "/filter"
            },
            {
              title: translate("Detail")
            }
          ]}
        />
        <div className="container course-page-container">
          <div className="course-header">
            <Row fullWidth>
              <Col md="8" sm="12">
                {course.intensity !== null && (
                  <span className="course-duration">
                    {translate("FULL TIME")}:&nbsp;{course.intensity}&nbsp;
                    {translate("HR/WK")}
                  </span>
                )}
                <p className="title">{course.schoolName}</p>
                <Rating
                  rate={course.rating.rate}
                  reviews={course.rating.reviews}
                />
              </Col>
              <Col md="4" sm="12">
                <div className="price">
                  {translate("Price from")}
                  <span>
                    <Currency currencyType={course.currencyType}>
                      {course.startingPrice}
                    </Currency>
                  </span>
                  /{translate("week")}
                </div>
              </Col>
            </Row>
          </div>
          <div className="course-page-content">
            <Row>
              <Col md="8" sm="12">
                <div className="course-images">
                  <ImageSlider autoplay slides={course.images} />
                </div>
                <CourseInformation information={course.information} />
                {course.youtubeLink && (
                  <Section className="promo-video">
                    <iframe
                      title="school-promo"
                      width="100%"
                      height="450"
                      src={course.youtubeLink}
                      allow="autoplay; encrypted-media"
                      allowFullScreen="allowfullscreen"
                    />
                  </Section>
                )}
                <FaqSection country={course.location.country} />
                <Section className="reviews-section hide-on-mobile">
                  <SectionHeader
                    position="left"
                    heading={
                      <Fragment>
                        <span>{translate("Reviews")}</span>
                        <Rate value={course.rating.rate} />
                      </Fragment>
                    }
                  />
                  <SectionBody>
                    {course.reviews.length === 0 ? (
                      <Alert>{translate("No reviews")}</Alert>
                    ) : (
                      course.reviews.map((review, index) => (
                        <Review
                          key={index}
                          name={review.student}
                          rating={review.rating}
                          image={review.image}
                          comment={review.comment}
                        />
                      ))
                    )}
                  </SectionBody>
                </Section>
              </Col>
              <Col md="4" sm="12">
                <CourseSidebar
                  courseId={courseId}
                  rating={course.rating.rate}
                  location={course.location}
                  courseFees={course.courseFees}
                  workEligibility={course.workEligibility}
                  passport={passport}
                  language={course.language}
                  age={course.minAge}
                  price={course.startingPrice}
                  googleMap={course.googleMap}
                  homestays={course.homestays}
                  discountList={course.discountList}
                  isBookmarked={course.isBookmarked}
                  onAddToList={this.addCourseToBookmarkList}
                  enrolmentFee={course.enrolmentFee}
                  onRemoveFromList={this.removeCourseFromBookmarkList}
                  bookACourse={this.bookACourse}
                  selectedCurrency={selectedCurrency}
                  isPaymentEnabled={course.isPaymentEnabled}
                  isTermsAndConditionsAdded={course.isTermsAndConditionsAdded}
                  currencyType={course.currencyType}
                  googleCordinates={course.googleCordinates}
                  isBookmarkLoading={isBookmarkLoading}
                  onClickNotifySchool={this.onClickNotifySchool.bind(this)}
                  notifySchoolStatus={notifySchoolStatus}
                  userRole={userRole}
                  userStatus={userStatus}
                  isBookingLoading={isBookingLoading}
                  isInterestExpressed={isInterestExpressed}
                />
              </Col>
            </Row>
            <Section className="reviews-section show-on-mobile">
              <SectionHeader
                position="left"
                heading={
                  <Fragment>
                    <span>{translate("Reviews")}</span>
                    <Rate value={course.rating.rate} />
                  </Fragment>
                }
              />
              <SectionBody>
                <div className="review-container">
                  {course.reviews.length === 0 ? (
                    <Alert>{translate("No reviews")}</Alert>
                  ) : (
                    course.reviews.map((review, index) => (
                      <Review
                        key={index}
                        name={review.student}
                        rating={review.rating}
                        image={review.image}
                        comment={review.comment}
                      />
                    ))
                  )}
                </div>
              </SectionBody>
            </Section>
            <RelatedCourses
              country={course.location.country}
              language={course.language}
              except={courseId}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    course: getConsolidatedCourse(state),
    courseId: getCourseId(state, props),
    status: state.course.status,
    isAuthenticated: isAuthenticated(state),
    isBookmarkLoading: state.course.isBookmarkLoading,
    selectedCurrency: state.profile.settings.currency,
    passport: state.profile.settings.passport,
    userRole: state.auth.role,
    userStatus: state.auth.status,
    notifySchoolStatus: state.course.notifySchoolStatus,
    isBookingLoading: state.booking.loading,
    isInterestExpressed: isInterestExpressed(state, props)
  };
};

const Actions = {
  getCourse,
  addCourseToBookmarkList,
  getBookmarkCourseList,
  removeCourseFromBookmarkList,
  notifySchool,
  isUserAuthenticated,
  bookACourse
};

const CourseDetailsContainer = connect(
  mapStateToProps,
  Actions
  // $FlowFixMe
)(withRouter(CourseDetails));

export type CourseDetailsContextProps = {
  getCourse: Function,
  addCourseToBookmarkList: Function,
  course: CourseType,
  courseId: number,
  status: AsyncStatusType,
  isAuthenticated: boolean,
  isCourseBookmarked: boolean,
  match: {
    params: {
      courseId: number
    }
  },
  getBookmarkCourseList: Function,
  removeCourseFromBookmarkList: Function
};

export default class CourseDetailsContext extends PureComponent<CourseDetailsContextProps> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ toggleAuth }) => (
          // $FlowFixMe
          <CourseDetailsContainer {...this.props} toggleAuth={toggleAuth} />
        )}
      </UserSettingsConsumer>
    );
  }
}
