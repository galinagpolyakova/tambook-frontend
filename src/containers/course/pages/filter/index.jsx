// @flow
import { type ConsolidatedCoursesListType } from "../../types";
import { type PaginationType } from "shared/components/Pagination";

import React, { PureComponent, Fragment, createRef } from "react";
import { connect } from "react-redux";
import classNames from "classnames";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Select from "shared/components/Select";
import Checkbox from "shared/components/Checkbox";
import IconButton from "shared/components/IconButton";

import translate from "translations/translate";
import Sidebar, { Widget } from "components/Sidebar";
import PageHeader from "components/PageHeader";
import Loader from "components/Loader";
import PriceFilter from "../../components/PriceFilter";
import FilterGroup from "../../components/FilterGroup";
import CourseView from "../../components/CourseView";
import { type AsyncStatusType } from "types/general";

import {
  filterCourses,
  getFilterParams,
  getBookmarkCourseList,
  addCourseToBookmarkList,
  removeCourseFromBookmarkList,
  addCourseToCompare,
  removeCourseFromCompare
} from "../../store/actions";
import { isAuthenticated } from "containers/auth/store/selectors";
import {
  isCourseParamsLoaded,
  getConsolidatedCourses,
  isCourseParamLoaded
} from "../../store/selectors";
import { UserSettingsConsumer } from "contexts/UserSettings";

import { queryParamsParse, stringifyQueryParams } from "shared/helpers/url";

import "rc-slider/assets/index.css";
import "./styles.scss";
import { getLanguageFromUrl } from "shared/helpers/translations";
import { ASYNC_STATUS } from "constants/async";

type CoursesFilterProps = {
  courses: ConsolidatedCoursesListType,
  filters: Object,
  isLoaded: boolean,
  isParamsLoaded: boolean,
  location: {
    search: string
  },
  history: any,
  viewMode: any,
  coursesTotal: number,
  status: AsyncStatusType,
  pagination: PaginationType,
  isAuthenticated: boolean,
  userCurrency: string,
  rates: Object,
  filterCourses: Function,
  getFilterParams: Function,
  getBookmarkCourseList: Function,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  toggleAuth: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function
};

type CoursesFilterState = {
  showFilters: boolean
};

class CoursesFilter extends PureComponent<
  CoursesFilterProps,
  CoursesFilterState
> {
  state = {
    showFilters: false
  };

  myRef = createRef();

  componentDidMount() {
    this.props.filterCourses(queryParamsParse(this.props.location.search));
    if (this.props.filters === null) {
      this.props.getFilterParams(queryParamsParse(this.props.location.search));
    }
    if (this.props.isAuthenticated) {
      this.props.getBookmarkCourseList();
    }
  }

  searchCourses = (filter: Object) => {
    const language = getLanguageFromUrl();
    filter.page = filter.page ? filter.page : 1;

    const query = {
      ...queryParamsParse(this.props.location.search),
      ...filter
    };
    const searchString = stringifyQueryParams(query);
    this.props.history.push({
      pathname: `/${language}/filter`,
      search: searchString
    });
    this.props.filterCourses(query);
    this.props.getFilterParams(query);
    window.scrollTo(0, this.myRef.current.offsetTop - 40);
  };

  togglePremiumCoursesFilter = e => {
    e.preventDefault();
    // $FlowFixMe
    let { premiumCourses } = queryParamsParse(this.props.location.search);
    const changedPremiumCourses = parseInt(premiumCourses) === 1 ? 0 : 1;
    this.searchCourses({ premiumCourses: changedPremiumCourses });
  };

  isPremiumCoursesFilterActive = () => {
    // $FlowFixMe
    let { premiumCourses } = queryParamsParse(this.props.location.search);
    return parseInt(premiumCourses) === 1;
  };

  getFilterContent = () => {
    const { isLoaded, filters, userCurrency, rates } = this.props;
    // $FlowFixMe
    const {
      minPrice,
      maxPrice,
      country,
      language,
      courseType,
      courseIntensity,
      isEveningClass,
      schoolName,
      workDuringCourse
    } = queryParamsParse(this.props.location.search);
    if (isLoaded) {
      const courseTypeOptions = [
        {
          name: translate("All"),
          value: ""
        },
        ...filters.courseType.map(filter => filter.type)
      ];
      const schoolNamesOptions = [
        {
          name: translate("All"),
          value: ""
        },
        ...filters.schoolNames.map(filter => filter.schoolName)
      ];
      const courseScheduleOptions = [
        {
          name: translate("All"),
          value: ""
        },
        ...filters.courseSchedule.map(filter => ({
          name: filter.schedule,
          value: filter.isEveningClass
        }))
      ];

      const courseIntensityOptions = [
        { name: translate("All"), value: "" },
        { name: translate("Low (8—19 hours)"), value: "8—19" },
        { name: translate("Standard (20—25 hours)"), value: "20—25" },
        { name: translate("High (26—45 hours)"), value: "26—45" }
      ];

      return (
        <Fragment>
          <Widget title={translate("Categories")}>
            <div className="filter-list">
              <FilterGroup
                items={filters.languages}
                clickHandler={this.searchCourses}
                selected={language}
                title={translate("Languages")}
                filter="language"
              />
              <FilterGroup
                title={translate("Countries")}
                filter="country"
                selected={country}
                items={filters.countries}
                clickHandler={this.searchCourses}
              />
            </div>
          </Widget>
          <Widget
            title={translate("Advanced Filters")}
            className="advance-filters"
          >
            <div className="form-group">
              <label>{translate("Select A Course Type")}</label>
              <Select
                name="course-type"
                options={courseTypeOptions}
                selected={courseType}
                onChange={courseType => this.searchCourses({ courseType })}
                defaultSelected={true}
                searchable
              />
            </div>
            <div className="form-group">
              <label>{translate("Select A Course Schedule")}</label>
              <Select
                name="course-schedule"
                options={courseScheduleOptions}
                selected={isEveningClass}
                onChange={isEveningClass =>
                  this.searchCourses({ isEveningClass })
                }
                defaultSelected={true}
                searchable
              />
            </div>
            <div className="form-group">
              <label>{translate("Course Intensity")}</label>
              <Select
                name="course-intensity"
                placeholder={translate("Course intensity")}
                options={courseIntensityOptions}
                selected={courseIntensity}
                onChange={courseIntensity =>
                  this.searchCourses({ courseIntensity })
                }
                defaultSelected={true}
              />
            </div>
            <div className="form-group">
              <label>{translate("Select A School")}</label>
              <Select
                name="course-school"
                placeholder={translate("Select a school")}
                options={schoolNamesOptions}
                selected={schoolName}
                onChange={schoolName => this.searchCourses({ schoolName })}
                searchable
                defaultSelected={true}
              />
            </div>
            <div className="form-group">
              <label>{translate("Eligible For Work")}</label>
              <Select
                name="course-school"
                placeholder={translate("Eligible For Work")}
                options={[
                  { value: "", name: translate("Not Relevant") },
                  {
                    value: "1",
                    name: translate("Yes")
                  },
                  { value: "0", name: translate("No") }
                ]}
                defaultSelected={true}
                selected={workDuringCourse}
                onChange={workDuringCourse =>
                  this.searchCourses({ workDuringCourse })
                }
              />
            </div>
          </Widget>
          <Widget
            title={translate("Weekly Price Range")}
            className="price-filter"
          >
            <PriceFilter
              searchCourses={this.searchCourses}
              selectedMinPrice={minPrice}
              selectedMaxPrice={maxPrice}
              userCurrency={userCurrency}
              rates={rates}
            />
          </Widget>
          <Widget
            title={translate("Premium Courses")}
            className="ratings-filter"
          >
            <Checkbox
              isChecked={this.isPremiumCoursesFilterActive()}
              onChange={this.togglePremiumCoursesFilter}
              text={translate("Yes")}
            />
          </Widget>
        </Fragment>
      );
    } else {
      return <Loader />;
    }
  };

  addCourseToBookmarkList = courseID => {
    if (this.props.addCourseToBookmarkList !== undefined) {
      if (this.props.isAuthenticated) {
        this.props.addCourseToBookmarkList(courseID);
      } else {
        this.props.toggleAuth();
      }
    }
  };
  render() {
    const {
      courses,
      coursesTotal,
      status,
      pagination,
      addCourseToCompare,
      removeCourseFromCompare
    } = this.props;
    const { showFilters } = this.state;

    const { priceSort, ratingSort, selectedSorting } = queryParamsParse(
      this.props.location.search
    );

    return (
      <div className="course-filter page">
        <PageHeader
          title={translate("Find Your Perfect Course")}
          subheading={`${translate("We Found")}: ${coursesTotal} ${translate(
            "Courses"
          )}`}
          breadcrumbs={[
            {
              title: translate("Courses")
            }
          ]}
        />
        <div className="container" ref={this.myRef}>
          <Row>
            <Col
              sm={12}
              md="3"
              className={classNames("filter-sidebar", { open: showFilters })}
            >
              <Sidebar>{this.getFilterContent()}</Sidebar>
            </Col>
            <Col>
              <Row>
                {status === ASYNC_STATUS.LOADING || courses === null ? (
                  <Loader isLoading />
                ) : (
                  <CourseView
                    ratingSort={ratingSort}
                    priceSort={priceSort}
                    selectedSorting={selectedSorting}
                    filter={this.searchCourses}
                    courses={courses}
                    addCourseToBookmarkList={this.addCourseToBookmarkList}
                    removeCourseFromBookmarkList={
                      this.props.removeCourseFromBookmarkList
                    }
                    addCourseToCompare={addCourseToCompare}
                    removeCourseFromCompare={removeCourseFromCompare}
                    pagination={pagination}
                    onPageChanged={this.searchCourses}
                  />
                )}
              </Row>
            </Col>
          </Row>
        </div>
        <IconButton
          icon={showFilters ? "outdent" : "filter"}
          className="filter-toggle"
          onClick={() =>
            this.setState(state => ({ showFilters: !state.showFilters }))
          }
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    courses: getConsolidatedCourses(state),
    isLoaded: isCourseParamsLoaded(state),
    status: state.course.status,
    filters: state.course.filters,
    isParamsLoaded: isCourseParamLoaded(state),
    coursesTotal:
      state.course.pagination !== null ? state.course.pagination.items : 0,
    pagination: state.course.pagination,
    isAuthenticated: isAuthenticated(state),
    compareList: state.course.compareList,
    userCurrency: state.profile.settings.currency,
    rates: state.profile.rates.list
  };
}

const Actions = {
  filterCourses,
  getFilterParams,
  getBookmarkCourseList,
  addCourseToBookmarkList,
  removeCourseFromBookmarkList,
  addCourseToCompare,
  removeCourseFromCompare
};

const CoursesFilterContainer = connect(
  mapStateToProps,
  Actions
)(CoursesFilter);

type CoursesFilterContextProps = {
  courses: ConsolidatedCoursesListType,
  filterCourses: Function,
  getFilterParams: Function,
  filters: Object,
  isLoaded: boolean,
  isParamsLoaded: boolean,
  location: {
    search: string
  },
  history: any,
  viewMode: any,
  coursesTotal: number,
  status: AsyncStatusType,
  pagination: PaginationType,
  isAuthenticated: boolean,
  getBookmarkCourseList: Function,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function
};

export default class CoursesFilterContext extends PureComponent<CoursesFilterContextProps> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ toggleAuth }) => (
          <CoursesFilterContainer {...this.props} toggleAuth={toggleAuth} />
        )}
      </UserSettingsConsumer>
    );
  }
}
