// import lib
import { lazy } from "react";

export default [
  {
    path: "/course/:courseId",
    className: "course-details-page",
    exact: true,
    component: lazy(() => import("./pages/details"))
  },
  {
    path: "/filter",
    className: "course-filter-page",
    exact: true,
    component: lazy(() => import("./pages/filter"))
  },
  {
    path: "/compare",
    className: "course-filter-page",
    exact: true,
    component: lazy(() => import("./pages/compare"))
  }
];
