// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";
import type {
  ApiCoursesListType,
  ConsolidatedCoursesListType,
  ConsolidatedCourseType
} from "./types";
import { Course } from "./model";
import { isNotEmpty, isCompleteUrl } from "shared/utils";
import { SITE_URL, API_URL } from "config/app";

import missingSquare from "../../assets/missing-square.jpg";
import missingImage from "assets/missing-course.jpg";

type NormalizedCoursesType = { courses: ConsolidatedCoursesListType };

export type GetCoursesResponse = {
  currentPage: number,
  nextPage: number,
  totalCount: number
} & NormalizedCoursesType;

export class CourseService {
  api: ApiServiceInterface;

  endpoint: string = "/courses";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  _transformCourseData({
    imageAnonse,
    courseID: id,
    name: title,
    shortDescription: description,
    length: duration,
    workDuringCourse: eligibleToWork,
    city,
    country,
    ratings,
    price: { price, currencyType },
    discountedPrices
  }: any): any {
    const image = isNotEmpty(imageAnonse)
      ? isCompleteUrl(imageAnonse)
        ? `${API_URL}/schools/image${imageAnonse}?width=327&height=225`
        : `${API_URL}/schools/image/${imageAnonse}?width=327&height=225`
      : missingImage;
    return {
      id,
      title,
      description,
      duration,
      eligibleToWork,
      location: {
        city,
        country
      },
      price,
      currencyType,
      image,
      rating: {
        rate: isNotEmpty(ratings) ? ratings.rate : 0,
        reviews: isNotEmpty(ratings) ? ratings.reviewCount : 0
      },
      discountList:
        discountedPrices.length > 0
          ? discountedPrices.map(({ discountedPrice, countryOfOrigin }) => {
              return {
                discountedPrice,
                countryOfOrigin
              };
            })
          : []
    };
  }

  _transformCompareCourseData({
    imageAnonse,
    intensity,
    courseID: id,
    name: title,
    length,
    workDuringCourse: eligibleToWork,
    city,
    country,
    price: { price, currencyType },
    type,
    schoolDetails: { name: schoolName }
  }: any): any {
    const image = isNotEmpty(imageAnonse)
      ? isCompleteUrl(imageAnonse)
        ? `${API_URL}/schools/image${imageAnonse}?width=214&height=147`
        : `${API_URL}/schools/image/${imageAnonse}?width=214&height=147`
      : missingImage;
    return {
      id,
      title,
      duration: length ? length : "-",
      eligibleToWork,
      location: `${city}, ${country}`,
      price,
      image,
      type,
      currencyType,
      intensity,
      schoolName
    };
  }

  _normalizeCourse(apiCourse: ApiCoursesListType): ConsolidatedCourseType {
    // $FlowFixMe
    return Course.fromCourseApi(apiCourse);
  }

  _normalizeCourses(apiCourses: any): NormalizedCoursesType {
    const courses = apiCourses.map(apiCourse => {
      return this._transformCourseData(apiCourse._source);
    });
    return {
      courses
    };
  }

  _normalizeCompareCourses(apiCourses: any): NormalizedCoursesType {
    const courses = apiCourses.map(apiCourse =>
      this._transformCompareCourseData(apiCourse._source)
    );
    return {
      courses
    };
  }

  _normalizeCourseActivity(apiActivity: any, isAvailable: boolean): any {
    // $FlowFixMe
    return {
      name: apiActivity.name,
      image: isAvailable ? `${SITE_URL}${apiActivity.images.x2}` : missingSquare
    };
  }

  _normalizeCourseActivities(apiActivities: any): any {
    const activities = [];

    apiActivities.activities.map(apiCourse => {
      return activities.push(this._normalizeCourseActivity(apiCourse, true));
    });

    apiActivities.unavailable.map(apiCourse => {
      return activities.push(this._normalizeCourseActivity(apiCourse, false));
    });
    return {
      activities
    };
  }

  _normalizeCourseFacility(apiFacility: any, isAvailable: boolean): any {
    // $FlowFixMe
    return {
      name: apiFacility.name,
      image: isAvailable ? `${SITE_URL}${apiFacility.images.x2}` : missingSquare
    };
  }

  _normalizeCourseFacilities(apiActivities: any): any {
    const facilities = [];

    apiActivities.facilities.map(apiCourse => {
      return facilities.push(this._normalizeCourseFacility(apiCourse, true));
    });

    apiActivities.unavailable.map(apiCourse => {
      return facilities.push(this._normalizeCourseFacility(apiCourse, false));
    });
    return {
      facilities
    };
  }

  getCourse(courseID: number): Promise<ConsolidatedCourseType> {
    return this.api.get(`${this.endpoint}/fetch/${courseID}`).then(response => {
      return this._normalizeCourse(response.result);
    });
  }

  filterCourses(filters: Object = {}) {
    return this.api.get(`${this.endpoint}/filter`, filters).then(response => {
      return {
        ...this._normalizeCourses(response.result),
        pagination: {
          items: response.total,
          currentPage: filters.page ? filters.page : 1,
          pages: response.pagination.total_pages,
          pageSize: response.pagination.per_page
        }
      };
    });
  }

  getFeaturedCourses() {
    return this.api
      ._fetch("get", `${this.endpoint}/featured`)
      .then(response => {
        return {
          ...this._normalizeCourses(response.result)
        };
      });
  }

  getFilterParams(filters: Object = {}) {
    return this.api
      .get(`${this.endpoint}/filterParams`, filters)
      .then(response => {
        return { ...response.array };
      });
  }

  getRelatedCourses(filters: Object = {}) {
    return this.api
      ._fetch("get", `${this.endpoint}/filter`, {}, { ...filters, size: 4 })
      .then(response => {
        return {
          ...this._normalizeCourses(response.result)
        };
      });
  }

  getCourseActivities(filters: Object = {}) {
    return this.api
      .get("/schools/features/activities", filters)
      .then(response => {
        return {
          ...this._normalizeCourseActivities(response.result)
        };
      });
  }

  getCourseFacilities(filters: Object = {}) {
    return this.api
      .get("/schools/features/facilities", filters)
      .then(response => {
        return {
          ...this._normalizeCourseFacilities(response.result)
        };
      });
  }

  addCourseToBookmarkList(payload: Object) {
    return this.api.post("/bookmark/course", payload);
  }

  removeCourseFromBookmarkList(payload: Object) {
    return this.api.post("/bookmark/delete/course", payload);
  }

  getBookmarkCourseList() {
    return this.api.get("/bookmark/course").then(results => results.data);
  }

  getBookmarkedCourses(filters: Object = {}) {
    return this.api
      .get(`/bookmark/courses/details`, { ...filters, size: 9 })
      .then(response => {
        return {
          ...this._normalizeCourses(response.data),
          pagination: {
            items: response.total,
            currentPage: filters.page ? filters.page : 1,
            pages: response.pagination.total_pages,
            pageSize: response.pagination.per_page
          }
        };
      });
  }

  getComparedCourseList(payload: any) {
    return this.api
      .post(`/bookmark/compare/open/course`, payload)
      .then(response => {
        return {
          ...this._normalizeCompareCourses(response.compairList)
        };
      });
  }

  getCurrencyList() {
    return this.api.get("/courses/currency/types");
  }

  getCurrencyRates(currency: string) {
    return this.api.get(`/courses/currency/rates/${currency}`);
  }

  notifySchool(payload: any) {
    return this.api.post(`/schools/notify`, payload);
  }

  getFaq(payload: any) {
    return this.api.get(`/faq/countrywise`, payload).then(response => {
      return this.normalizeFaqList(response.data[0]);
    });
  }

  normalizeFaqList(ApiFaq: any) {
    const faq = [];

    if (ApiFaq) {
      ApiFaq.list.map(({ question, answer }) => {
        if (isNotEmpty(question) && isNotEmpty(answer)) {
          faq.push({
            title: question,
            content: answer
          });
        }
        return true;
      });
    }
    return faq;
  }
}
