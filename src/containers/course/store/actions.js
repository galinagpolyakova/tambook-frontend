import {
  GET_COURSE_INIT,
  GET_COURSE_SUCCESS,
  GET_COURSE_FAILURE,
  FILTER_COURSES_INIT,
  FILTER_COURSES_SUCCESS,
  FILTER_COURSES_FAILURE,
  GET_FILTER_PARAMS_INIT,
  GET_FILTER_PARAMS_SUCCESS,
  GET_FILTER_PARAMS_FAILURE,
  GET_FEATURED_COURSES_INIT,
  GET_FEATURED_COURSES_SUCCESS,
  GET_FEATURED_COURSES_FAILURE,
  GET_RELATED_COURSES_INIT,
  GET_RELATED_COURSES_SUCCESS,
  GET_RELATED_COURSES_FAILURE,
  GET_COURSE_ACTIVITIES_INIT,
  GET_COURSE_ACTIVITIES_SUCCESS,
  GET_COURSE_ACTIVITIES_FAILURE,
  ADD_COURSE_TO_BOOKMARK_LIST_INIT,
  ADD_COURSE_TO_BOOKMARK_LIST_SUCCESS,
  ADD_COURSE_TO_BOOKMARK_LIST_FAILURE,
  GET_BOOKMARK_COURSE_LIST_INIT,
  GET_BOOKMARK_COURSE_LIST_SUCCESS,
  GET_BOOKMARK_COURSE_LIST_FAILURE,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_INIT,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_SUCCESS,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_FAILURE,
  ADD_COURSE_TO_COMPARE,
  LOAD_COURSES_IN_COMPARE,
  REMOVE_COURSE_FROM_COMPARE,
  GET_COMPARED_COURSE_LIST_SUCCESS,
  GET_COURSE_FACILITIES_INIT,
  GET_COURSE_FACILITIES_SUCCESS,
  GET_COURSE_FACILITIES_FAILURE,
  NOTIFY_SCHOOL_INIT,
  NOTIFY_SCHOOL_SUCCESS,
  NOTIFY_SCHOOL_FAILURE,
  GET_FAQ_INIT,
  GET_FAQ_SUCCESS,
  GET_FAQ_FAILURE
} from "./actionTypes";

import { ASYNC_STATUS } from "constants/async";
import { areObjectsEqual } from "shared/kernel/cast";

function getCourseInit(payload) {
  return {
    type: GET_COURSE_INIT,
    payload
  };
}

function getCourseSuccess(payload) {
  return {
    type: GET_COURSE_SUCCESS,
    payload
  };
}

function getCourseFailure(payload) {
  return {
    type: GET_COURSE_FAILURE,
    payload
  };
}

export function getCourse(id) {
  return (dispatch, getState, serviceManager) => {
    const {
      course: { course, status }
    } = getState();
    if (
      (course === null || course.id !== id) &&
      status !== ASYNC_STATUS.LOADING
    ) {
      dispatch(getCourseInit({ id }));
      let courseService = serviceManager.get("CourseService");
      courseService
        .getCourse(id)
        .then(data =>
          dispatch(
            getCourseSuccess({
              id,
              data
            })
          )
        )
        .catch(err => {
          dispatch(getCourseFailure(err));
        });
    }
  };
}

function filterCoursesInit() {
  return {
    type: FILTER_COURSES_INIT
  };
}

function filterCoursesSuccess(payload) {
  return {
    type: FILTER_COURSES_SUCCESS,
    payload
  };
}

function filterCoursesFailure(payload) {
  return {
    type: FILTER_COURSES_FAILURE,
    payload
  };
}

export function filterCourses(searchParams) {
  return (dispatch, getState, serviceManager) => {
    dispatch(filterCoursesInit());

    let courseService = serviceManager.get("CourseService");

    courseService
      .filterCourses(searchParams)
      .then(payload => {
        dispatch(filterCoursesSuccess(payload));
      })
      .catch(err => {
        dispatch(filterCoursesFailure(err));
      });
  };
}

function getFilterParamsInit() {
  return {
    type: GET_FILTER_PARAMS_INIT
  };
}

function getFilterParamsSuccess(payload) {
  return {
    type: GET_FILTER_PARAMS_SUCCESS,
    payload
  };
}

function getFilterParamsFailure(payload) {
  return {
    type: GET_FILTER_PARAMS_FAILURE,
    payload
  };
}

export function getFilterParams(searchParams) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getFilterParamsInit());

    let courseService = serviceManager.get("CourseService");

    courseService
      .getFilterParams(searchParams)
      .then(payload => dispatch(getFilterParamsSuccess({ filters: payload })))
      .catch(err => {
        dispatch(getFilterParamsFailure(err));
      });
  };
}

function getFeaturedCoursesInit() {
  return {
    type: GET_FEATURED_COURSES_INIT
  };
}

function getFeaturedCoursesSuccess(payload) {
  return {
    type: GET_FEATURED_COURSES_SUCCESS,
    payload
  };
}

function getFeaturedCoursesFailure(payload) {
  return {
    type: GET_FEATURED_COURSES_FAILURE,
    payload
  };
}

export function getFeaturedCourses() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getFeaturedCoursesInit());

    let courseService = serviceManager.get("CourseService");

    courseService
      .getFeaturedCourses()
      .then(response =>
        dispatch(
          getFeaturedCoursesSuccess({
            featuredCourses: response.courses
          })
        )
      )
      .catch(err => {
        dispatch(getFeaturedCoursesFailure(err));
      });
  };
}

function getRelatedCoursesSuccess(payload) {
  return {
    type: GET_RELATED_COURSES_SUCCESS,
    payload
  };
}

function getRelatedCoursesFailure(payload) {
  return {
    type: GET_RELATED_COURSES_FAILURE,
    payload
  };
}

export function getRelatedCourses(filters) {
  return (dispatch, getState, serviceManager) => {
    const {
      course: { relatedCourses }
    } = getState();

    if (
      relatedCourses.status !== ASYNC_STATUS.LOADING &&
      !areObjectsEqual(relatedCourses.filters, filters)
    ) {
      dispatch({
        type: GET_RELATED_COURSES_INIT,
        payload: { filters }
      });

      let courseService = serviceManager.get("CourseService");
      courseService
        .getRelatedCourses(filters)
        .then(({ courses: list }) =>
          dispatch(
            getRelatedCoursesSuccess({
              list
            })
          )
        )
        .catch(err => {
          dispatch(getRelatedCoursesFailure(err));
        });
    }
  };
}

function getCourseActivitiesInit() {
  return {
    type: GET_COURSE_ACTIVITIES_INIT
  };
}

function getCourseActivitiesSuccess(payload) {
  return {
    type: GET_COURSE_ACTIVITIES_SUCCESS,
    payload
  };
}

function getCourseActivitiesFailure(payload) {
  return {
    type: GET_COURSE_ACTIVITIES_FAILURE,
    payload
  };
}

export function getCourseActivities(filter) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getCourseActivitiesInit());

    let courseService = serviceManager.get("CourseService");

    courseService
      .getCourseActivities(filter)
      .then(response => dispatch(getCourseActivitiesSuccess(response)))
      .catch(err => {
        dispatch(getCourseActivitiesFailure(err));
      });
  };
}

function addCourseToBookmarkListInit() {
  return {
    type: ADD_COURSE_TO_BOOKMARK_LIST_INIT
  };
}

function addCourseToBookmarkListSuccess(payload) {
  return {
    type: ADD_COURSE_TO_BOOKMARK_LIST_SUCCESS,
    payload
  };
}

function addCourseToBookmarkListFailure(payload) {
  return {
    type: ADD_COURSE_TO_BOOKMARK_LIST_FAILURE,
    payload
  };
}

export function addCourseToBookmarkList(courseId) {
  return (dispatch, getState, serviceManager) => {
    dispatch(addCourseToBookmarkListInit());

    let courseService = serviceManager.get("CourseService");

    courseService
      .addCourseToBookmarkList({ list: [courseId] })
      .then(() => dispatch(addCourseToBookmarkListSuccess({ id: courseId })))
      .catch(err => {
        dispatch(addCourseToBookmarkListFailure(err));
      });
  };
}

function removeCourseFromBookmarkListInit() {
  return {
    type: REMOVE_COURSE_FROM_BOOKMARK_LIST_INIT
  };
}

function removeCourseFromBookmarkListSuccess(payload) {
  return {
    type: REMOVE_COURSE_FROM_BOOKMARK_LIST_SUCCESS,
    payload
  };
}

function removeCourseFromBookmarkListFailure(payload) {
  return {
    type: REMOVE_COURSE_FROM_BOOKMARK_LIST_FAILURE,
    payload
  };
}

export function removeCourseFromBookmarkList(courseId) {
  return (dispatch, getState, serviceManager) => {
    dispatch(removeCourseFromBookmarkListInit());

    let courseService = serviceManager.get("CourseService");

    courseService
      .removeCourseFromBookmarkList({ list: [courseId] })
      .then(() =>
        dispatch(removeCourseFromBookmarkListSuccess({ id: courseId }))
      )
      .catch(err => {
        dispatch(removeCourseFromBookmarkListFailure(err));
      });
  };
}

function getBookmarkCourseListInit() {
  return {
    type: GET_BOOKMARK_COURSE_LIST_INIT
  };
}

function getBookmarkCourseListSuccess(payload) {
  return {
    type: GET_BOOKMARK_COURSE_LIST_SUCCESS,
    payload
  };
}

function getBookmarkCourseListFailure(payload) {
  return {
    type: GET_BOOKMARK_COURSE_LIST_FAILURE,
    payload
  };
}

export function getBookmarkCourseList() {
  return (dispatch, getState, serviceManager) => {
    const {
      course: { bookmarkList }
    } = getState();
    if (bookmarkList === null) {
      dispatch(getBookmarkCourseListInit());

      let courseService = serviceManager.get("CourseService");

      courseService
        .getBookmarkCourseList()
        .then(response => {
          dispatch(getBookmarkCourseListSuccess(response));
        })
        .catch(err => {
          dispatch(getBookmarkCourseListFailure(err));
        });
    }
  };
}

export function loadCoursesInCompare() {
  return {
    type: LOAD_COURSES_IN_COMPARE
  };
}

export function addCourseToCompare(payload) {
  return {
    type: ADD_COURSE_TO_COMPARE,
    payload
  };
}

export function removeCourseFromCompare(payload) {
  return {
    type: REMOVE_COURSE_FROM_COMPARE,
    payload
  };
}

function getComparedCourseListSuccess(payload) {
  return {
    type: GET_COMPARED_COURSE_LIST_SUCCESS,
    payload
  };
}

export function getComparedCourseList(payload) {
  return (dispatch, getState, serviceManager) => {
    let courseService = serviceManager.get("CourseService");
    courseService
      .getComparedCourseList(payload)
      .then(({ courses: compareListCourse }) => {
        dispatch(getComparedCourseListSuccess({ compareListCourse }));
      });
  };
}

function getCourseFacilitiesInit() {
  return {
    type: GET_COURSE_FACILITIES_INIT
  };
}

function getCourseFacilitiesSuccess(payload) {
  return {
    type: GET_COURSE_FACILITIES_SUCCESS,
    payload
  };
}

function getCourseFacilitiesFailure(payload) {
  return {
    type: GET_COURSE_FACILITIES_FAILURE,
    payload
  };
}

export function getCourseFacilities(filter) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getCourseFacilitiesInit());

    let courseService = serviceManager.get("CourseService");

    courseService
      .getCourseFacilities(filter)
      .then(response => dispatch(getCourseFacilitiesSuccess(response)))
      .catch(err => {
        dispatch(getCourseFacilitiesFailure(err));
      });
  };
}

function notifySchoolInit() {
  return {
    type: NOTIFY_SCHOOL_INIT
  };
}

function notifySchoolSuccess(payload) {
  return {
    type: NOTIFY_SCHOOL_SUCCESS,
    payload
  };
}

function notifySchoolFailure(payload) {
  return {
    type: NOTIFY_SCHOOL_FAILURE,
    payload
  };
}

export function notifySchool(payload, id) {
  return (dispatch, getState, serviceManager) => {
    dispatch(notifySchoolInit());
    const {
      course: { expressInterestList }
    } = getState();
    let courseService = serviceManager.get("CourseService");

    if (expressInterestList.includes(id)) {
      dispatch(notifySchoolFailure("Already added."));
    } else {
      courseService
        .notifySchool(payload)
        .then(() => dispatch(notifySchoolSuccess({ id })))
        .catch(err => dispatch(notifySchoolFailure(err)));
    }
  };
}

function getFaqInit() {
  return {
    type: GET_FAQ_INIT
  };
}

function getFaqSuccess(payload) {
  return {
    type: GET_FAQ_SUCCESS,
    payload
  };
}

function getFaqFailure(payload) {
  return {
    type: GET_FAQ_FAILURE,
    payload
  };
}

export function getFaq(filter) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getFaqInit());

    let courseService = serviceManager.get("CourseService");
    courseService
      .getFaq(filter)
      .then(response => dispatch(getFaqSuccess(response)))
      .catch(err => dispatch(getFaqFailure(err)));
  };
}
