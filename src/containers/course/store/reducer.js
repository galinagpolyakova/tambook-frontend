// @flow
import { type Action } from "shared/types/ReducerAction";
import { type PaginationType } from "shared/components/Pagination";
import { saveData, loadData } from "shared/kernel/localStorage";
import { ASYNC_STATUS } from "constants/async";
import { type AsyncStatusType } from "types/general";

import {
  GET_COURSE_INIT,
  GET_COURSE_SUCCESS,
  GET_COURSE_FAILURE,
  FILTER_COURSES_INIT,
  FILTER_COURSES_SUCCESS,
  FILTER_COURSES_FAILURE,
  GET_FILTER_PARAMS_INIT,
  GET_FILTER_PARAMS_SUCCESS,
  GET_FILTER_PARAMS_FAILURE,
  GET_FEATURED_COURSES_INIT,
  GET_FEATURED_COURSES_SUCCESS,
  GET_FEATURED_COURSES_FAILURE,
  GET_RELATED_COURSES_INIT,
  GET_RELATED_COURSES_SUCCESS,
  GET_RELATED_COURSES_FAILURE,
  GET_COURSE_ACTIVITIES_SUCCESS,
  GET_COURSE_FACILITIES_SUCCESS,
  ADD_COURSE_TO_BOOKMARK_LIST_INIT,
  ADD_COURSE_TO_BOOKMARK_LIST_FAILURE,
  ADD_COURSE_TO_BOOKMARK_LIST_SUCCESS,
  GET_BOOKMARK_COURSE_LIST_SUCCESS,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_INIT,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_FAILURE,
  REMOVE_COURSE_FROM_BOOKMARK_LIST_SUCCESS,
  LOAD_COURSES_IN_COMPARE,
  ADD_COURSE_TO_COMPARE,
  REMOVE_COURSE_FROM_COMPARE,
  GET_COMPARED_COURSE_LIST_SUCCESS,
  NOTIFY_SCHOOL_INIT,
  NOTIFY_SCHOOL_SUCCESS,
  NOTIFY_SCHOOL_FAILURE,
  GET_FAQ_INIT,
  GET_FAQ_SUCCESS,
  GET_FAQ_FAILURE
} from "./actionTypes";

export type CourseState = {
  submitting: boolean,
  notifications: null | String[],
  bookmarkList: String[] | null,
  course: null | Object,
  status: AsyncStatusType,
  courses: Object[] | null,
  filters: Object | null,
  coursesTotal: number,
  relatedCourses: {
    list: Array<any>,
    status: AsyncStatusType,
    filters: null | Object
  },
  pagination: PaginationType,
  compareList: Object[],
  faq: Array<any>,
  faqLoading: boolean
};

const initialState: CourseState = {
  submitting: false,
  status: ASYNC_STATUS.INIT,
  isFeaturedCoursesLoaded: false,
  isBookmarkLoading: false,
  bookmarkList: null,
  notifications: null,
  course: null,
  relatedCourses: {
    list: [],
    status: ASYNC_STATUS.INIT,
    filters: null
  },
  courses: null,
  filters: null,
  coursesTotal: 0,
  pagination: null,
  compareList: [],
  expressInterestList: [],
  compareListCourse: [],
  notifySchoolStatus: ASYNC_STATUS.INIT,
  faq: [],
  faqLoading: false
};

function getCourseInit(state: CourseState, { id }) {
  return {
    ...state,
    status: ASYNC_STATUS.LOADING,
    course: {
      id,
      data: null,
      activities: [],
      facilities: []
    }
  };
}

function getCourseSuccess(state: CourseState, { data }) {
  return {
    ...state,
    status: ASYNC_STATUS.SUCCESS,
    notifySchoolStatus: ASYNC_STATUS.INIT,
    course: {
      ...state.course,
      data
    }
  };
}

function getCourseFailure(state: CourseState, { notifications }) {
  return {
    ...state,
    notifications,
    status: ASYNC_STATUS.FAILURE,
    pagination: null,
    coursesTotal: 0
  };
}

function fetchCoursesInit(state: CourseState) {
  return {
    ...state,
    status: ASYNC_STATUS.LOADING,
    courses: null
  };
}

function fetchCoursesSuccess(
  state: CourseState,
  { courses, coursesTotal, pagination }
) {
  return {
    ...state,
    status: ASYNC_STATUS.SUCCESS,
    courses,
    pagination,
    coursesTotal
  };
}

function fetchCoursesFailure(state: CourseState) {
  return {
    ...state,
    courses: [],
    status: ASYNC_STATUS.FAILURE
  };
}

function getFilterParamsInit(state: CourseState) {
  return {
    ...state,
    status: ASYNC_STATUS.LOADING,
    isLoaded: false,
    filters: null
  };
}

function getFilterParamsSuccess(state: CourseState, { filters }) {
  return {
    ...state,
    status: ASYNC_STATUS.SUCCESS,
    filters
  };
}

function getFilterParamsFailure(state: CourseState, { notifications }) {
  return {
    ...state,
    notifications,
    status: ASYNC_STATUS.FAILURE,
    isLoaded: true
  };
}

function getFeaturedCoursesInit(state: CourseState) {
  return {
    ...state,
    status: ASYNC_STATUS.LOADING,
    isFeaturedCoursesLoaded: false
  };
}

function getFeaturedCoursesSuccess(state: CourseState, { featuredCourses }) {
  return {
    ...state,
    status: ASYNC_STATUS.SUCCESS,
    isFeaturedCoursesLoaded: true,
    featuredCourses
  };
}

function getFeaturedCoursesFailure(state: CourseState, { notifications }) {
  return {
    ...state,
    notifications,
    status: ASYNC_STATUS.FAILURE,
    isFeaturedCoursesLoaded: true
  };
}

function getRelatedCoursesInit(state: CourseState, { filters }) {
  return {
    ...state,
    relatedCourses: {
      filters,
      list: [],
      status: ASYNC_STATUS.LOADING
    }
  };
}

function getRelatedCoursesSuccess(state: CourseState, { list }) {
  return {
    ...state,
    relatedCourses: {
      list,
      status: ASYNC_STATUS.SUCCESS
    }
  };
}

function getRelatedCoursesFailure(state: CourseState, { notifications }) {
  return {
    ...state,
    notifications,
    status: ASYNC_STATUS.FAILURE
  };
}

function getCourseActivitiesSuccess(state: CourseState, { activities }) {
  return {
    ...state,
    status: ASYNC_STATUS.SUCCESS,
    course: {
      ...state.course,
      activities
    }
  };
}

function getCourseFacilitiesSuccess(state: CourseState, { facilities }) {
  return {
    ...state,
    status: ASYNC_STATUS.SUCCESS,
    course: {
      ...state.course,
      facilities
    }
  };
}

function addCourseToBookmarkListInit(state: CourseState) {
  return {
    ...state,
    isBookmarkLoading: true
  };
}

function addCourseToBookmarkListFailure(state: CourseState) {
  return {
    ...state,
    isBookmarkLoading: false
  };
}

function addCourseToBookmarkListSuccess(state: CourseState, { id }) {
  const bookmarkList =
    state.bookmarkList === null ? [id] : [...state.bookmarkList, id];
  return {
    ...state,
    bookmarkList,
    isBookmarkLoading: false
  };
}

function getBookmarkCourseListSuccess(state: CourseState, bookmarkList) {
  return {
    ...state,
    bookmarkList
  };
}

function removeCourseFromBookmarkListInit(state: CourseState) {
  return {
    ...state,
    isBookmarkLoading: true
  };
}

function removeCourseFromBookmarkListFailure(state: CourseState) {
  return {
    ...state,
    isBookmarkLoading: false
  };
}

function removeCourseFromBookmarkListSuccess(state: CourseState, { id }) {
  return {
    ...state,
    // $FlowFixMe
    bookmarkList: state.bookmarkList.filter(courseId => courseId !== id),
    isBookmarkLoading: false
  };
}

function loadCoursesInCompare(state: CourseState) {
  const compareList =
    loadData("compareList") !== undefined ? loadData("compareList") : [];
  const expressInterestList =
    loadData("expressInterestList") !== undefined
      ? loadData("expressInterestList")
      : [];

  return {
    ...state,
    compareList,
    expressInterestList
  };
}

function addCourseToCompare(state: CourseState, { id }) {
  const compareList = [...state.compareList, id];
  saveData("compareList", compareList);
  return {
    ...state,
    compareList
  };
}

function removeCourseFromCompare(state: CourseState, { id }) {
  let compareList = [];
  if (id !== "ALL") {
    // $FlowFixMe
    compareList = state.compareList.filter(courseId => courseId !== id);
  }
  saveData("compareList", compareList);
  return {
    ...state,
    compareList
  };
}

function getComparedCourseListSuccess(state, { compareListCourse }) {
  return {
    ...state,
    compareListCourse
  };
}

function notifySchoolInit(state) {
  return {
    ...state,
    notifySchoolStatus: ASYNC_STATUS.LOADING
  };
}

function notifySchoolSuccess(state, { id }) {
  const expressInterestList = [...state.expressInterestList, id];
  saveData("expressInterestList", expressInterestList);
  return {
    ...state,
    expressInterestList,
    notifySchoolStatus: ASYNC_STATUS.SUCCESS
  };
}

function notifySchoolFailure(state) {
  return {
    ...state,
    notifySchoolStatus: ASYNC_STATUS.FAILURE
  };
}

function getFaqInit(state) {
  return {
    ...state,
    faqLoading: true
  };
}

function getFaqSuccess(state, payload) {
  return {
    ...state,
    faqLoading: false,
    faq: payload
  };
}

function getFaqFailure(state) {
  return {
    ...state,
    faqLoading: false
  };
}

const reducer = (
  state: CourseState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case GET_COURSE_INIT:
      return getCourseInit(state, payload);
    case GET_COURSE_SUCCESS:
      return getCourseSuccess(state, payload);
    case GET_COURSE_FAILURE:
      return getCourseFailure(state, payload);
    case GET_FEATURED_COURSES_INIT:
      return getFeaturedCoursesInit(state);
    case GET_FEATURED_COURSES_SUCCESS:
      return getFeaturedCoursesSuccess(state, payload);
    case GET_FEATURED_COURSES_FAILURE:
      return getFeaturedCoursesFailure(state, payload);
    case FILTER_COURSES_INIT:
      return fetchCoursesInit(state);
    case FILTER_COURSES_SUCCESS:
      return fetchCoursesSuccess(state, payload);
    case FILTER_COURSES_FAILURE:
      return fetchCoursesFailure(state);
    case GET_FILTER_PARAMS_INIT:
      return getFilterParamsInit(state);
    case GET_FILTER_PARAMS_SUCCESS:
      return getFilterParamsSuccess(state, payload);
    case GET_FILTER_PARAMS_FAILURE:
      return getFilterParamsFailure(state, payload);
    case GET_RELATED_COURSES_INIT:
      return getRelatedCoursesInit(state, payload);
    case GET_RELATED_COURSES_SUCCESS:
      return getRelatedCoursesSuccess(state, payload);
    case GET_RELATED_COURSES_FAILURE:
      return getRelatedCoursesFailure(state, payload);
    case GET_COURSE_ACTIVITIES_SUCCESS:
      return getCourseActivitiesSuccess(state, payload);
    case GET_COURSE_FACILITIES_SUCCESS:
      return getCourseFacilitiesSuccess(state, payload);
    case ADD_COURSE_TO_BOOKMARK_LIST_INIT:
      return addCourseToBookmarkListInit(state);
    case ADD_COURSE_TO_BOOKMARK_LIST_FAILURE:
      return addCourseToBookmarkListFailure(state);
    case ADD_COURSE_TO_BOOKMARK_LIST_SUCCESS:
      return addCourseToBookmarkListSuccess(state, payload);
    case GET_BOOKMARK_COURSE_LIST_SUCCESS:
      return getBookmarkCourseListSuccess(state, payload);
    case REMOVE_COURSE_FROM_BOOKMARK_LIST_INIT:
      return removeCourseFromBookmarkListInit(state);
    case REMOVE_COURSE_FROM_BOOKMARK_LIST_FAILURE:
      return removeCourseFromBookmarkListFailure(state);
    case REMOVE_COURSE_FROM_BOOKMARK_LIST_SUCCESS:
      return removeCourseFromBookmarkListSuccess(state, payload);
    case LOAD_COURSES_IN_COMPARE:
      return loadCoursesInCompare(state);
    case ADD_COURSE_TO_COMPARE:
      return addCourseToCompare(state, payload);
    case REMOVE_COURSE_FROM_COMPARE:
      return removeCourseFromCompare(state, payload);
    case GET_COMPARED_COURSE_LIST_SUCCESS:
      return getComparedCourseListSuccess(state, payload);
    case NOTIFY_SCHOOL_INIT:
      return notifySchoolInit(state);
    case NOTIFY_SCHOOL_SUCCESS:
      return notifySchoolSuccess(state, payload);
    case NOTIFY_SCHOOL_FAILURE:
      return notifySchoolFailure(state);
    case GET_FAQ_INIT:
      return getFaqInit(state);
    case GET_FAQ_SUCCESS:
      return getFaqSuccess(state, payload);
    case GET_FAQ_FAILURE:
      return getFaqFailure(state);
    default:
      return state;
  }
};

export default reducer;
