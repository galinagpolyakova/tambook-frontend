// @flow
import { createSelector } from "reselect";
import { type ApplicationState } from "store/reducers";
import { type CourseDetailsProps } from "../pages/details";
import { type ConsolidatedCourseType } from "../types";
import { getUserPassport } from "containers/profile/store/selectors";

export const getCourses = (state: ApplicationState) => state.course.courses;

export const getRelatedCourses = (state: ApplicationState) =>
  state.course.relatedCourses.list;

export const getCourseParams = (state: ApplicationState) =>
  state.course.filters;

export const isCourseParamLoaded = (createSelector(
  [getCourseParams],
  (courseParams): boolean => {
    return courseParams !== null;
  }
): (state: ApplicationState) => boolean);

export const getCourse = (state: ApplicationState) => state.course.course;

export const getCourseData = (state: ApplicationState) =>
  state.course.course !== null ? state.course.course.data : null;

export const getCourseId = (
  state: ApplicationState,
  props: CourseDetailsProps
) => props.match.params.courseId;

export const isCourseParamsLoaded = (createSelector(
  [getCourseParams],
  (filters): boolean => {
    if (filters === null) {
      return false;
    }
    return true;
  }
): (state: ApplicationState) => boolean);

export const getCourseBookmarkList = (state: ApplicationState) =>
  state.course.bookmarkList;

export const getCourseCompareList = (state: ApplicationState) =>
  state.course.compareList;

export const isCourseBookmarksListLoaded = (createSelector(
  [getCourseBookmarkList],
  (bookmarkList): boolean => {
    return bookmarkList !== null;
  }
): (state: ApplicationState) => boolean);

function getDiscountedCoursePrice(discountedPriceArray, passport) {
  const discountedPrices = [];

  discountedPriceArray.map(discountItem => {
    if (discountItem.countryOfOrigin.includes(passport)) {
      discountedPrices.push(discountItem.discountedPrice);
    }
    return true;
  });

  const discountedPrice =
    discountedPrices.length > 0 ? Math.min(...discountedPrices) : null;

  return discountedPrice;
}

function getConsolidatedCourseFromStore(
  course,
  bookmarkedCourses,
  comparedCourses,
  passport
) {
  if (bookmarkedCourses !== null && bookmarkedCourses.length >= 0) {
    let bookmarkedCourse = bookmarkedCourses.find(courseItem => {
      return course.id === courseItem.toString();
    });
    course.isBookmarked = bookmarkedCourse !== undefined;
  }
  let comparedCourse = comparedCourses.find(courseItem => {
    return course.id === courseItem;
  });
  course.isAddedToCompare = comparedCourse !== undefined;

  course.discountedPrice = getDiscountedCoursePrice(
    course.discountList,
    passport
  );
  return course;
}

function getConsolidatedCoursesFromStore(
  courses,
  bookmarkedCourses,
  comparedCourses,
  passport
) {
  const consolidatedCourses = [];

  if (courses === null) {
    return null;
  }

  for (let course of courses) {
    consolidatedCourses.push(
      getConsolidatedCourseFromStore(
        course,
        bookmarkedCourses,
        comparedCourses,
        passport
      )
    );
  }
  return consolidatedCourses;
}

export const getConsolidatedCourses = createSelector(
  [getCourses, getCourseBookmarkList, getCourseCompareList, getUserPassport],
  (courses, bookmarkedCourses, comparedCourses, passport) => {
    return getConsolidatedCoursesFromStore(
      courses,
      bookmarkedCourses,
      comparedCourses,
      passport
    );
  }
);

export const getConsolidatedRelatedCourses = createSelector(
  [
    getRelatedCourses,
    getCourseBookmarkList,
    getCourseCompareList,
    getUserPassport
  ],
  (courses, bookmarkedCourses, comparedCourses, passport) => {
    return getConsolidatedCoursesFromStore(
      courses,
      bookmarkedCourses,
      comparedCourses,
      passport
    );
  }
);

export const getConsolidatedCourse = (createSelector(
  [getCourseData, getCourseBookmarkList, getCourseCompareList, getUserPassport],
  (course, bookmarkedCourses, comparedCourses, passport) => {
    if (course !== null) {
      return getConsolidatedCourseFromStore(
        course,
        bookmarkedCourses,
        comparedCourses,
        passport
      );
    } else {
      return null;
    }
  }
): (state: ApplicationState) => ConsolidatedCourseType | typeof undefined);

export const getCourseExpressInterestList = (state: ApplicationState) =>
  state.course.expressInterestList;

export const getCourseIdFromProps = (state, props) =>
  props.match.params.courseId;

export const isInterestExpressed = (createSelector(
  [getCourseExpressInterestList, getCourseIdFromProps],
  (expressInterestList, id) => {
    return expressInterestList.includes(id);
  }
): (state: ApplicationState) => ConsolidatedCourseType | typeof undefined);
