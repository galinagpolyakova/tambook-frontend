import React from "react";

import PageHeader from "components/PageHeader";

import DetailsSection from "./partials/DetailsSection";

import "./styles.scss";
import translate from "translations/translate";

export default function aboutUs() {
  return (
    <div>
      <PageHeader
        title={translate("TAMBOOK - WE MAKE LEARNING LANGUAGES")}
        subtitle={translate("AN AFFORDABLE ADVENTURE")}
        breadcrumbs={[
          {
            title: translate("ABOUT")
          }
        ]}
      />
      <DetailsSection />
    </div>
  );
}
