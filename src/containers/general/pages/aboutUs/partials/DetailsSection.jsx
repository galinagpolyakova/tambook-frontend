import React from "react";

import Section, { SectionBody } from "shared/components/Section";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Button from "shared/components/Button";
import Icon from "shared/components/Icon";

import Sidebar from "../../components/sidebar";
import translate from "translations/translate";

export default function detailsSection() {
  return (
    <div className="container detail-content">
      <Row>
        <Col sm="12" md="8">
          <Section className="section-advantages">
            <SectionBody>
              <div className="advantage-item">
                <Icon icon="user-secret" />
                {translate(
                  "Want to learn a new skill on the adventure of a lifetime?"
                )}
              </div>
              <div className="advantage-item">
                <Icon icon="dollar" />
                {translate(
                  "Tambook makes booking language courses around the world easy and affordable."
                )}
              </div>
              <div className="advantage-item">
                <Icon icon="graduation-cap" />
                {translate(
                  "Hundreds of schools, thousands of courses, and amazing best price offers."
                )}
              </div>
              <div className="advantage-item">
                <Icon icon="compass" />
                {translate(
                  "Everything you need to get your adventure started."
                )}
              </div>
            </SectionBody>
          </Section>
          <Section>
            <SectionBody>
              <p>
                {translate(
                  "At Tambook, we believe in globally-conscious learners. We help ambitious people set their own rules for life, and become the most outstanding versions of themselves in the process."
                )}
              </p>

              <p>
                {translate(
                  "As a team of expats from Eurasia and South America, we learned firsthand how frustrating, time-consuming, and expensive it can be to find a language course in a different country. After scrolling through hundreds of websites, we paid hefty fees to haggle with money-grabbing agents. Talking directly with schools was difficult - especially as we didn’t speak the language yet."
                )}
              </p>

              <p>
                {translate(
                  "In 2015, we built Tambook to flip this status quo. Trusted by students and language schools around the globe, we designed Tambook to be a state-of-the-art booking platform that anyone could use to compare language courses, score the best prices directly from schools, and pay in their local currency within a single browsing session."
                )}
              </p>

              <p>
                {translate(
                  "Driven by a pioneering spirit, we’re always working hard to make things better for eager learners. Our team will keep innovating until studying a language abroad becomes an easy, affordable option for everyone."
                )}
              </p>

              <p>
                {translate(
                  "Tambook HQ is currently located in New Zealand. We have a lot of team members working here, and a lot of team members working all around the world. We’re here to make booking a language course easy for you, so if you have any questions, please reach out."
                )}
              </p>
            </SectionBody>
          </Section>
          <Section className="cta">
            <SectionBody>
              <Button
                type={Button.TYPE.SECONDARY}
                size={Button.SIZE.SMALL}
                htmlType={Button.HTML_TYPE.LINK}
                link={`/filter?country=&language=`}
              >
                {translate("START YOUR ADVENTURE NOW")}
              </Button>
            </SectionBody>
          </Section>
        </Col>
        <Col sm="12" md="4">
          <Section>
            <Sidebar />
          </Section>
        </Col>
      </Row>
    </div>
  );
}
