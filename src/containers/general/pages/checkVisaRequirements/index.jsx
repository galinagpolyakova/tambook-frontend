// @flow
import React, { Component, Fragment } from "react";

import PageHeader from "components/PageHeader";
import translate from "translations/translate";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Select from "shared/components/Select";

import Sidebar from "../components/sidebar";
import RelatedCourses from "containers/course/components/RelatedCourses";

import "./styles.scss";

const COUNTRIES = {
  IRELAND: "ireland",
  UK_ENG_SCT_NIR: "uk-eng-sct-nir",
  MALTA: "malta",
  SOUTH_AFRICA: "south-africa",
  CANADA: "canada",
  USA: "usa",
  NEW_ZEALAND: "new-zealand",
  AUSTRALIA: "australia"
};

type CheckVisaRequirementsState = {
  country: | typeof COUNTRIES.IRELAND
    | typeof COUNTRIES.UK_ENG_SCT_NIR
    | typeof COUNTRIES.MALTA
    | typeof COUNTRIES.SOUTH_AFRICA
    | typeof COUNTRIES.CANADA
    | typeof COUNTRIES.USA
    | typeof COUNTRIES.NEW_ZEALAND
    | typeof COUNTRIES.AUSTRALIA
};

class CheckVisaRequirements extends Component<{}, CheckVisaRequirementsState> {
  constructor() {
    super();
    this.state = {
      country: COUNTRIES.IRELAND
    };
  }

  getCountryContent = () => {
    const content = {
      [COUNTRIES.IRELAND]: (
        <Fragment>
          <p>
            <strong>Applying for Ireland Visa</strong>
          </p>

          <p>
            If you&apos;re a non-EU/EEA and non-Swiss citizen, and your course
            is less than three months in duration, you&apos;ll need to apply for
            a Visitor&apos;s (Tourist) Visa before travelling to Ireland.
          </p>

          <p>
            If your course is more than three months in duration, you&apos;ll
            need to apply for a Study Visa before travelling to Ireland, and
            you&apos;ll need to apply for &apos;Permission To Stay&apos;.
          </p>

          <p>
            You can obtain Permission to Stay for the period of up to one
            year.&nbsp;
            <a href="http://www.inis.gov.ie/en/INIS/Pages/registration-offices">
              Apply
            </a>
            &nbsp;for Permission To Stay at the immigration office nearest to
            where you will stay in Ireland.
          </p>

          <p>
            <strong>To apply, you&apos;ll need to:</strong>
          </p>
          <ol>
            <li>
              Fill out and print the&nbsp;
              <a href="https://www.visas.inis.gov.ie/avats/OnlineHome2.aspx">
                application form
              </a>
              . Fill in the field &quot;reason for travel&quot; =
              &quot;study&quot;.
            </li>
            <li>Pay the visa fee.</li>
            <li>
              Send all your documents to the Irish embassy in your home country
              (you must do this within 30 days of creating your online
              application).
            </li>
          </ol>

          <p>
            <strong>
              What do I need to apply for an Ireland Student Visa?
            </strong>
          </p>

          <p>
            You can apply for a study visa up to 3 months before your planned
            visit to Ireland.
          </p>

          <p>This is what you&apos;ll need to apply:</p>

          <ol>
            <li>
              Passport that will remain valid for six months after your
              departure date from Ireland.
            </li>
            <li>Copies of each page from your previous passports.</li>
            <li>
              Two recent 35*45 mm color photos. You&apos;ll need to write your
              name and your visa application number on the back.
            </li>
            <li>An offer of a placement from a language school.</li>
            <li>
              An original bank statement with incoming and outgoing transactions
              on headed paper. In the case of a Short-Term Visa there is no
              minimum bank balance you need to prove, but the visa officer
              decides in each case. In case of a Long-Term Visa, you need to
              have at least 7000 euros.
            </li>
            <li>
              Evidence of your tight connection with your home country: a job
              certificate or study certificate, and confirmed family status
              (marriage certificate, birth certificate of your children).
            </li>
            <li>Private medical insurance.</li>
            <li>Evidence you&apos;ve paid for the course.</li>
            <li>Information about any previous visa refusals.</li>
          </ol>

          <p>
            All documents should be originals accompanied by photocopies.
            Original documents will not be returned (except birth/marriage
            certificates), if you want to take some of them back, you&apos;ll
            need to provide a list of these documents.
          </p>

          <p>
            A certified translator must translate all documents that aren&apos;t
            in English, with date and personal details of the translator.
          </p>

          <p>
            In some cases, you&apos;ll need to provide your fingerprints or
            other biometric data as part of the application process. You can
            check details&nbsp;
            <a href="http://www.inis.gov.ie/en/INIS/Pages/Biometrics">here</a>.
          </p>

          <p>
            If you&apos;re a citizen of EU/EEA countries, you don&apos;t need a
            visa to study in Ireland. Check your eligibility&nbsp;
            <a href="http://www.inis.gov.ie/en/INIS/Pages/check-irish-visa">
              here
            </a>
            .
          </p>

          <p>
            If you have any questions, you can contact INIS&nbsp;
            <a href="mailto:visamail@justice.ie">visamail@justice.ie</a>.
          </p>
        </Fragment>
      ),
      [COUNTRIES.UK_ENG_SCT_NIR]: (
        <Fragment>
          <p>
            <strong>
              Applying for The United Kingdom: England, Scotland & Northern
              Ireland Visa
            </strong>
          </p>
          <p>
            <strong>&nbsp;</strong>
          </p>
          <p>
            if you&apos;re a non-EU/EEA and non-Swiss citizen you&apos;ll need a
            Short-Term Student Visa to study in the UK (Scotland, England &
            Northern Ireland). You need to get your Visa before entering any UK
            countries.
          </p>

          <p>
            Check your eligibility&nbsp;
            <a href="https://www.gov.uk/check-uk-visa/y">here.</a>
          </p>

          <p>
            To receive a Short-Term Student Visa, your course must be less than
            six months in duration if you&apos;re 16-18 years old, and less than
            11 months in duration if you&apos;re older than 18 years old.
          </p>

          <p>
            <strong>
              To obtain a Short-Term Student Visa, you&apos;ll need to:
            </strong>
          </p>
          <ol>
            <li>
              Fill out and print the online&nbsp;
              <a href="https://apply.uq.edu.au/">application form</a>.
            </li>
            <li>
              Make an appointment in an application office in your local
              country. Check for your nearest office&nbsp;
              <a href="https://www.gov.uk/find-a-visa-application-centre">
                here
              </a>
              .
            </li>
            <li>Collect all the required documents.</li>
            <li>
              Provide all the required documents to the application office.
            </li>
          </ol>

          <p>
            If you&apos;re a citizen of EU/EEA countries, you don&apos;t need a
            visa to study a language in the UK.
          </p>

          <h3>What do I need to apply for a UK student visa?</h3>

          <p>
            You need to apply for a study visa up to 3 months before you&apos;re
            planning to visit the UK.
          </p>

          <p>To apply, you&apos;ll need:</p>

          <ol>
            <li>
              Passport valid one month after your departure date from the UK.
            </li>
            <li>Previous passports with previous travel shown.</li>
            <li>Two recent passport-sized color photos.</li>
            <li>
              An offer of a place from a language school from a recognized
              school. Check&nbsp;
              <a href="https://www.gov.uk/government/publications/register-of-licensed-sponsors-students">
                here
              </a>
              .
            </li>
            <li>An original bank statement.</li>
            <li>Proof of job or study history in your home country.</li>
            <li>Fingerprints.</li>
            <li>Complete and printed application form.</li>
          </ol>

          <p>
            There are no other mandatory documents, but you can increase your
            chances of success if you provide other relevant and supporting
            documents. It&apos;s recommended to prove you have enough funds in
            your account to cover all expenses twice.
          </p>

          <p>
            All documents that aren&apos;t in English must be translated by a
            certified translator, with the date and the personal details of the
            translator.
          </p>

          <p>
            It isn&apos;t necessary to provide originals of documents except
            your bank statement and your proof of employment/study.
          </p>
        </Fragment>
      ),
      [COUNTRIES.MALTA]: (
        <Fragment>
          <p>
            <strong>Applying for Malta Visa</strong>
          </p>

          <p>
            If you&apos;re an EU-citizen and your course is 90 days or less, you
            don&apos;t need a visa.
          </p>

          <p>
            Students from Australia, Canada and the USA don&apos;t need a visa
            to stay in Malta for up to 90 days.
          </p>

          <p>
            Citizens of the EU, Australia, Canada or USA will need a Long-Term
            Study Permit if you want to stay more than 90 days.
          </p>

          <p>
            If you&apos;re not an EU citizen and your course is less than 90
            days long, you&apos;ll need to apply for a Maltese Schengen visa
            before traveling to Malta.
          </p>

          <p>
            If you&apos;re not an EU citizen and your course is longer than 90
            days, you&apos;ll need to apply for a National Long-Stay Visa.
          </p>

          <p>
            <strong>To apply for a visa, you&apos;ll need to:</strong>
          </p>
          <ol>
            <li>Complete all documents.</li>
            <li>
              Personally deliver all documents to a Malta Embassy. Or, in the
              case of a 90 day visa, you can also apply in the embassy of
              Austria, Italy or Spain in your country (you can check
              embassies&nbsp;
              <a href="https://identitymalta.com/where-to-apply/">here</a>)
            </li>
          </ol>

          <h3>What do I need to apply for a Malta student visa?</h3>
          <ol>
            <li>
              Passport that will be valid for 3 months from the day you plan to
              depart from Malta.
            </li>
            <li>Two recent passport-sized photos.</li>
            <li>
              An original bank or credit card statement with proof you enough
              money to cover your costs.
            </li>
            <li>Proof you have accommodation in Malta.</li>
            <li>A return ticket.</li>
            <li>Invitation from your school.</li>
            <li>Medical or travel Insurance.</li>
          </ol>

          <p>
            A certified translator must translate all documents that are not in
            English.
          </p>

          <p>The process of consideration usually takes 7-15 days.</p>
        </Fragment>
      ),
      [COUNTRIES.SOUTH_AFRICA]: (
        <Fragment>
          <p>
            <strong>What do I need to apply for a student visa?</strong>
          </p>

          <ol>
            <li>
              A passport valid for up to 30 days after your departure from South
              Africa, with at least 2 empty pages.
            </li>
            <li>Two recent passport-sized photos.</li>
            <li>
              An original bank statement with proof you have money covered your
              costs (ZAR6000 per month).
            </li>
            <li>Proof you have paid the course.</li>
            <li>A return ticket.</li>
            <li>Invitation from your school.</li>
            <li>
              A radiological certificate (download&nbsp;
              <a href="https://uctlanguagecentre.com/wp-content/uploads/2016/06/Radiological-report.pdf">
                here
              </a>
              ,&nbsp;print and go to your doctor or clinic in your country).
            </li>
            <li>
              A medical certificate (download&nbsp;
              <a href="https://uctlanguagecentre.com/wp-content/uploads/2016/06/Medical-report.pdf">
                here
              </a>
              ,&nbsp;print and go to your doctor or clinic in your country).
            </li>
            <li>
              A police clearance certificate from your home country or country
              of residence.
            </li>
            <li>Medical Insurance.</li>
          </ol>
        </Fragment>
      ),
      [COUNTRIES.CANADA]: (
        <Fragment>
          <p>
            <strong>Applying for Canada Visa</strong>
          </p>

          <p>
            If you are not a citizen of Canada, and your course is more than six
            months long, you&apos;ll need a Study Permit. However, having a
            Study Permit doesn&apos;t allow you to enter in Canada; you also
            need a Visa or Travel Authority to enter Canada.
          </p>

          <p>
            You can apply for a study permit at the port of entry if you&apos;re
            a citizen or permanent resident of U.S, Greenland, Saint-Pierre or
            Miquelon.
          </p>

          <p>
            You can check the requirements for your country&nbsp;
            <a href="https://www.canadianetavisa.org/visa-information">here</a>
            &nbsp;and&nbsp;
            <a href="http://www.cic.gc.ca/english/visit/visas.asp">here</a>.
          </p>

          <p>
            If your course is less than six months in duration, you do not
            require a Study Permit, but do require a Travel Authority or
            Visitor&apos;s visa.
          </p>

          <p>
            You&apos;ll need to apply for a Study Permit and Travel Authority /
            Visa before travelling to Canada.
          </p>

          <p>
            <strong>
              To apply for a Canadian Study permit, you&apos;ll need to:
            </strong>
          </p>
          <ol>
            <li>
              Create and fill an online application form or apply on paper in
              your Visa Application Office.
            </li>
            <li>Pay visa fee.</li>
            <li>
              Provide your biometrical data. See&nbsp;
              <a href="http://www.cic.gc.ca/english/information/where-to-give-biometrics.asp">
                here
              </a>
              .
            </li>
            <li>Obtain a letter from the embassy of Canada.</li>
            <li>Provide additional information.</li>
            <li>Pass an interview, if necessary.</li>
          </ol>

          <h3>What do I need to apply to study Canada Study Visa/Permit?</h3>

          <ol>
            <li>Valid passport.</li>
            <li>Two recent passport-sized photos.</li>
            <li>
              An original bank statement with proof you have money covered your
              costs ($917 in Quebec and $833 anywhere else).
            </li>
            <li>
              Letter with an explanation of why you want to take a course in
              Canada.
            </li>
            <li>
              Proof you are enrolled in the course, and the school is in the
              &apos;Designated Learning Institutions&apos;&nbsp;
              <a href="https://www.canada.ca/en/immigration-refugees-citizenship/services/study-canada/study-permit/prepare/designated-learning-institutions-list.html">
                list
              </a>
              .
            </li>
            <li>
              Police Clearance Certificate from your home country and country of
              residence.
            </li>
            <li>
              Certificate from the Quebec government if you&apos;ll study there
              (your school will inform you about details).
            </li>
          </ol>

          <p>
            After completing your application, you&apos;ll need to pay the study
            permit fee C$150. Detailed information about it you can find&nbsp;
            <a href="http://www.cic.gc.ca/english/information/fees/pay.asp">
              here
            </a>
            .
          </p>
          <p>
            Don&apos;t forget to print the payment receipt as proof of payment.
          </p>

          <p>
            All documents that are not in English or French must be translated
            by a person fluent in English or French, and the second language of
            the translation. You&apos;ll need to provide information on the
            translator as well.
          </p>
          <p>
            <br /> &nbsp;If you have any questions you can contact the
            information center&nbsp;
            <a href="https://services.priv.gc.ca/q-s/allez-go/eng/8b62761b-7100-4016-886c-0279a78670d6">
              here
            </a>
            .
          </p>
        </Fragment>
      ),
      [COUNTRIES.USA]: (
        <Fragment>
          <p>
            <strong>Applying for USA Visa</strong>
          </p>

          <p>
            If you&apos;re not a citizen of Canada and you want study in the
            USA, you&apos;ll usually need an F-1 visa.
          </p>

          <p>
            You can apply personally in your local USA embassy or consulate, or
            you can apply online&nbsp;
            <a href="https://ceac.state.gov/GenNIV/Default.aspx">here</a>.
          </p>

          <p>
            You can apply for a visa 120 days before your course starts. You
            cannot travel to the USA earlier than 30 days before your course
            starts.
          </p>

          <h3>What do I need for apply for an F-1 visa?</h3>
          <ol>
            <li>
              Passport valid for six months after the date your planned
              departure from the USA.
            </li>
            <li>
              All old passports with information about your previous travel.
            </li>
            <li>One recent passport-sized photo.</li>
            <li>
              DS160 US Visa application form with a stamp from the Visa
              Application Center.
            </li>
            <li>Receipt of visa fee payment.</li>
            <li>Printed interview appointment letter.</li>
            <li>Proof of student status.</li>
          </ol>

          <p>
            <strong>To apply for the visa, you&apos;ll need to:</strong>
          </p>
          <p>
            1)&nbsp;Register yourself in The Student and Exchange Visitor
            Information System (
            <a href="https://www.ice.gov/sevis/overview">SEVIS</a>);&nbsp;
            USD$200
          </p>
          <p>
            2) Fill and print the DS-160 form&nbsp;
            <a href="https://ceac.state.gov/GenNIV/Default.aspx">here</a>
            .&nbsp;
            <a href="https://travel.state.gov/content/travel/en/us-visas/visa-information-resources/forms/ds-160-online-nonimmigrant-visa-application.html">
              Here
            </a>
            &nbsp;you can find instructions on how to fill it out.
          </p>
          <p>3) Pay visa-fee.</p>
          <p>4) Make an appointment for an interview.</p>
          <p>5) Go to the interview and provide all the documents there.</p>

          <p>
            After applying, you&apos;ll need to pass the interview. The visa
            interview determines if you&apos;ll obtain a visa or not. Go to the
            interview with the documents well prepared. You can find standard
            questions asked in the interview&nbsp;
            <a href="https://www.internationalstudent.com/immigration/f1-student-visa/f1-visa-interview-questions/">
              here
            </a>
            .
          </p>
        </Fragment>
      ),
      [COUNTRIES.NEW_ZEALAND]: (
        <Fragment>
          <p>
            <strong>Applying for New Zealand Visa</strong>
          </p>

          <p>
            Citizens of some countries don&apos;t need a visa to study in New
            Zealand, if their course is less than 3 months. Check your
            eligibility&nbsp;
            <a href="https://www.immigration.govt.nz/new-zealand-visas/apply-for-a-visa/about-visa/visa-waiver">
              here
            </a>
            .
          </p>

          <p>
            If you&apos;re a citizen of the UK, you won&apos;t need a visa for
            courses up to 6 months in duration.
          </p>

          <p>
            If you&apos;re a citizen of another country, and you want to
            undertake a course more than three months in duration, you&apos;ll
            need to apply to for a Student Visa or a Visitor&apos;s Visa. You
            can check what type of visa you&apos;ll need&nbsp;
            <a href="https://www.immigration.govt.nz/new-zealand-visas/options/study/explore-student-visas">
              here
            </a>
            .
          </p>
          <p>
            <br />
            &nbsp;
          </p>
          <h3>What do I need to apply for a New Zealand Student Visa?</h3>

          <ol>
            <li>Valid passport.</li>
            <li>A recent passport-sized photo.</li>
            <li>An offer of a place from a language school.</li>
            <li>
              A bank statement in PDF format which confirms that you have
              sufficient funds to cover the cost of your course and your living
              expenses (1,250 NZD per month/15,000 NZD per year). Sponsorship
              options are available.
            </li>
            <li>
              A job certificate (in replacement of proof of funds, or
              sponsorship).
            </li>
            <li>
              Certificates from your previous English courses. If you have this
              evidence, it will help you a lot.
            </li>
            <li>
              Evidence of your tight connections with your home country (a
              marriage certificate, birth certificates of your children, a real
              estate certificate, etc.)
            </li>
          </ol>

          <p>
            <strong>To apply for the visa, you&apos;ll need:</strong>
          </p>
          <ol>
            <li>
              Choose your type of visa and register&nbsp;
              <a href="https://www.immigration.govt.nz/new-zealand-visas/apply-for-a-visa/about-visa/pathway-student-visa">
                here
              </a>
              .
            </li>
            <li>
              Apply after&nbsp;
              <a href="https://www.immigration.govt.nz/new-zealand-visas/apply-for-a-visa/about-visa/pathway-student-visa">
                registration
              </a>
              .
            </li>
            <li>Attach all the required documents.</li>
            <li>Pay the immigration levy online.</li>
            <li>
              Print or save the confirmation of the submission of your
              application. It&apos;ll contain information about whether you
              should send your passport to the nearest INZ office or not.
            </li>
          </ol>
        </Fragment>
      ),
      [COUNTRIES.AUSTRALIA]: (
        <Fragment>
          <p>
            <strong>Applying for Australian Visa</strong>
          </p>

          <p>
            If you want to study in Australia, you&apos;ll need a visa. There
            are three types of visa you can apply for.
          </p>

          <p>
            If your course is less than three months in duration, you can apply
            for a Visitor&apos;s Visa. If your course is less than four months
            in duration, you can apply for a Working Holiday Visa. And if your
            course is more than four months in duration, you can apply for a
            Study Visa.
          </p>

          <p>
            Learn more&nbsp;
            <a href="https://www.studyinaustralia.gov.au/english/apply-to-study/visas">
              here
            </a>
            .
          </p>
          <h3>What do I need for apply for an Australia Student Visa?</h3>

          <p>In most cases you&apos;ll need to provide:</p>
          <ol>
            <li>Valid passport.</li>
            <li>A recent passport sized photo: 3.5cmx4.5 cm.</li>
            <li>
              An offer of a place and confirmation of payment (CoE) from a
              language school.
            </li>
            <li>
              A bank statement in PDF format which confirms that you have
              sufficient funds to cover the cost of your course + your living
              expenses (1,655 AUD per month/19,830 AUD per year) + return air
              tickets.
            </li>
            <li>
              OSHC insurance for the entire period of your stay in Australia.
              You can buy it via Tambook or your education provider.
            </li>
            <li>A job certificate if you are an employee.</li>
            <li>Certificates from your previous English courses.</li>
            <li>
              Evidence of your tight connections with your home country (a
              marriage certificate, birth certificates of your children, a real
              estate certificate, etc).
            </li>
            <li>Doctor&apos;s examination.</li>
          </ol>

          <p>
            To apply for the visa online, you need to create your ImmiAccount on
            the DIBP&apos;s{" "}
            <a href="https://online.immi.gov.au/lusc/login">website</a> and
            follow the instructions. There is an online application process, and
            you&apos;ll get your visa by e-mail.
          </p>

          <p>
            For the working holiday visa, other information can be required.
            Check the{" "}
            <a href="https://immi.homeaffairs.gov.au/visas/getting-a-visa/visa-listing/work-holiday-417">
              website
            </a>
            .
          </p>

          <p>
            All documents must be translated into English by a certified
            translator who must sign and stamp the translation.
          </p>
        </Fragment>
      )
    };
    return content[this.state.country];
  };

  render() {
    return (
      <Fragment>
        <PageHeader
          title={translate("Visa Requirements")}
          breadcrumbs={[
            {
              title: translate("VISA")
            }
          ]}
        />
        <div className="container check-visa-requirement-page">
          <Row>
            <Col md={8} sm={12}>
              <Row>
                <Col md={6} sm={12}>
                  <Select
                    selected={this.state.country}
                    options={[
                      {
                        value: COUNTRIES.IRELAND,
                        name: "Ireland"
                      },
                      {
                        value: COUNTRIES.UK_ENG_SCT_NIR,
                        name:
                          "The United Kingdom: England, Scotland & Northern Ireland"
                      },
                      {
                        value: COUNTRIES.MALTA,
                        name: "Malta"
                      },
                      {
                        value: COUNTRIES.SOUTH_AFRICA,
                        name: "South Africa"
                      },
                      {
                        value: COUNTRIES.CANADA,
                        name: "Canada"
                      },
                      {
                        value: COUNTRIES.USA,
                        name: "USA"
                      },
                      {
                        value: COUNTRIES.NEW_ZEALAND,
                        name: "New Zealand"
                      },
                      {
                        value: COUNTRIES.AUSTRALIA,
                        name: "Australia"
                      }
                    ]}
                    onChange={country => this.setState({ country })}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="country-content">
                    {this.getCountryContent()}
                  </div>
                </Col>
              </Row>
            </Col>
            <Col md={4} sm={12}>
              <Sidebar />
            </Col>
          </Row>
          <RelatedCourses />
        </div>
      </Fragment>
    );
  }
}

export default CheckVisaRequirements;
