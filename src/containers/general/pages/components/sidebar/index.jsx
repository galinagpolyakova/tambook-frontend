// @flow
import React from "react";

import Sidebar, { Widget } from "components/Sidebar";
import Link from "components/Link";

import Map from "shared/components/Map.jsx";
import Icon from "shared/components/Icon";

import translate from "translations/translate";

import "./styles.scss";

export default function sidebar() {
  const LATITUDE = -36.861844;
  const LONGITUDE = -185.237911;
  return (
    <Sidebar>
      <Widget
        className="detail-container"
        title={translate("NEED HELP?")}
        type={Widget.types.LIGHT}
      >
        <p>
          {translate("Look up")}&nbsp;
          <Link to="/faq">{translate("frequently asked questions")}</Link>&nbsp;
          {translate("for a hint")}.
        </p>
        <div className="number-line" />
        <p>
          {translate(
            "Our friendly and experienced student advisors will help you with all your questions"
          )}
        </p>
        <div className="contact-details">
          <div className="contact-option">
            <Icon icon="envelope" />
            OFFICE@TAMBOOK.COM
          </div>
        </div>
        <div className="number-line" />
        <h4>{translate("New Zealand Head Office")}</h4>
        <div className="center-details">
          <div className="center-option">
            <Icon icon="map-marker-alt" />3 Glenside Crescent, Eden Terrace,
            Auckland, NZ.
          </div>
          <div className="center-option">
            <Icon icon="envelope" />
            office@tambook.com
          </div>
        </div>
      </Widget>
      <Widget>
        <div style={{ minHeight: "200px", width: "100%" }}>
          <Map lat={LATITUDE} lng={LONGITUDE} />
        </div>
      </Widget>
    </Sidebar>
  );
}
