// @flow
import React, { PureComponent } from "react";

import { connect } from "react-redux";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Icon from "shared/components/Icon";
import Section, { SectionHeader } from "shared/components/Section";
import Select from "shared/components/Select";

import translate from "translations/translate";
import PageHeader from "components/PageHeader";
import Sidebar, { Widget } from "components/Sidebar";
import ContactForm from "./components/ContactForm";
import { sendContactMessage } from "../../store/actions";

import "./styles.scss";

type ContactUsProps = {
  isContactMessageSendSuccess: boolean,
  isLoading: boolean,

  sendContactMessage: Function
};

class ContactUs extends PureComponent<ContactUsProps> {
  render() {
    const {
      isContactMessageSendSuccess,
      sendContactMessage,
      isLoading
    } = this.props;

    return (
      <div>
        <PageHeader
          title={translate("Contact The Tambook Team")}
          breadcrumbs={[
            {
              title: translate("Contact Us")
            }
          ]}
        />
        <div className="contact-container">
          <Row>
            <Col sm="12" md="8">
              <ContactForm
                sendContactMessage={sendContactMessage}
                isContactMessageSendSuccess={isContactMessageSendSuccess}
                isLoading={isLoading}
              />
            </Col>
            <Col sm="12" md="4">
              <Sidebar>
                <Widget>
                  <div className="center-details">
                    <h4>{translate("TAMBOOK HEAD OFFICE")}</h4>
                    <div className="center-option">
                      <Icon icon="map-marker-alt" />3 Glenside Crescent, Eden
                      Terrace, Auckland, NZ.
                    </div>
                    <div className="center-option">
                      <Icon icon="envelope" />
                      office@tambook.com
                    </div>
                    <div className="center-option">
                      <Icon icon="clock-o" />
                      {translate(
                        "To schedule a meeting please send us an email"
                      )}
                    </div>
                  </div>
                </Widget>
                <Widget>
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3192.1893877878433!2d174.76002131529157!3d-36.8618853799359!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d47c2c7e7724d%3A0x42f55637547b3f7d!2s3+Glenside+Cres%2C+Eden+Terrace%2C+Auckland+1010%2C+New+Zealand!5e0!3m2!1sen!2slk!4v1560404184599!5m2!1sen!2slk"
                    width="100%"
                    height="200px"
                    frameBorder="0"
                    title="TAMBOOK HEAD OFFICE"
                  />
                </Widget>
              </Sidebar>
            </Col>
          </Row>
          <Row>
            <Col>
              <Section className="agent-filter-container">
                <SectionHeader
                  heading={translate("Find your local Tambook agent")}
                  position="left"
                />
                <Row>
                  <Col sm="12" md="3" className="agent-filter">
                    <Select
                      options={[
                        {
                          name: translate("All Countries"),
                          value: "All Countries"
                        },
                        { name: "Argentina", value: "Argentina" }
                      ]}
                      selected={"All Countries"}
                    />
                  </Col>
                </Row>
              </Section>
            </Col>
          </Row>
          <Row>
            <Col>
              <Section className="agent-detail-container">
                <Row>
                  <Col sm="12" md="4">
                    <div className="center-details">
                      <h4>BUENOS AIRES - EVT PIN A MAP</h4>
                      <div className="center-option">
                        <Icon icon="map-marker-alt" />
                        Costa Rica 3975, CABA (1176), Argentina.
                      </div>
                      <div className="center-option">
                        <Icon icon="file-text" />
                        License: 16620
                      </div>
                      <div className="center-option">
                        <Icon icon="envelope" />
                        latam‌@pamtravel.com
                      </div>
                      <div className="center-option">
                        <Icon icon="clock-o" />
                        {translate(
                          "To schedule a meeting please send us an email"
                        )}
                      </div>
                    </div>
                    <Widget>
                      <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3284.502169395527!2d-58.42091908477064!3d-34.59146118046268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcca7e387b8401%3A0x321d18e704e4dfbb!2sCosta+Rica+3975%2C+C1176ADA+CABA%2C+Argentina!5e0!3m2!1sen!2slk!4v1560404305203!5m2!1sen!2slk"
                        width="100%"
                        height="200px"
                        frameBorder="0"
                        title="BUENOS AIRES - EVT PIN A MAP"
                      />
                    </Widget>
                  </Col>
                  <Col sm="12" md="8" />
                </Row>
              </Section>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isContactMessageSendSuccess: state.general.isContactMessageSendSuccess,
    isLoading: state.general.isLoading
  };
};

const Actions = {
  sendContactMessage
};

export default connect(
  mapStateToProps,
  Actions
)(ContactUs);
