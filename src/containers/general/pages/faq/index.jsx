// @flow
import { type ConsolidatedFaqType } from "../../types";

import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Parser from "html-react-parser";

import Loader from "components/Loader";
import PageHeader from "components/PageHeader";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Accordion from "shared/components/Accordion";
import translate from "translations/translate";
import RelatedCourses from "containers/course/components/RelatedCourses";

import { getAllFaq } from "../../store/actions";

import "./styles.scss";

type FaqPropsType = {
  getAllFaq: Function,
  isGetFaqLoading: boolean,
  isFaqLoaded: boolean,
  faq: Array<ConsolidatedFaqType>
};

type FaqStateType = {
  form: {
    input: string
  },
  result: {
    category: string,
    questions: Array<any>
  }
};
class Faq extends PureComponent<FaqPropsType, FaqStateType> {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        input: ""
      },
      result: {
        category: "",
        questions: []
      }
    };
    // $FlowFixMe
    this.setInitialFaq = this.setInitialFaq.bind(this);
    // $FlowFixMe
    this.onClickListItem = this.onClickListItem.bind(this);
    // $FlowFixMe
    this.getQuestionsList = this.getQuestionsList.bind(this);
  }

  componentDidMount() {
    const { getAllFaq, isFaqLoaded } = this.props;

    if (!isFaqLoaded) getAllFaq();

    this.setInitialFaq();
  }

  componentDidUpdate(prevProps) {
    const { isFaqLoaded } = this.props;

    if (prevProps.isFaqLoaded !== isFaqLoaded && isFaqLoaded) {
      this.setInitialFaq();
    }
  }

  setInitialFaq() {
    const { faq } = this.props;

    if (faq.length) {
      const { category, questions } = faq[0];
      this.setState(({ result }) => ({
        result: {
          ...result,
          category,
          questions
        }
      }));
    }
  }

  onClickListItem({ category, questions }) {
    this.setState(({ result }) => ({
      result: {
        ...result,
        category,
        questions
      }
    }));
  }

  getQuestionsList() {
    const {
      result: { questions }
    } = this.state;

    return questions.map(question => ({
      title: question.question,
      content: <div>{Parser(question.answer)}</div>
    }));
  }

  render() {
    const { isFaqLoaded, faq } = this.props;
    const { result } = this.state;

    const items = this.getQuestionsList();
    if (!isFaqLoaded) {
      return <Loader isLoading />;
    }

    return (
      <div>
        <PageHeader
          title={translate("FREQUENTLY ASKED QUESTIONS")}
          breadcrumbs={[
            {
              title: "FAQ"
            }
          ]}
        />
        <div className="faq-container">
          <Row>
            <Col sm="12" md="4">
              <div className="category-container">
                <h2>{translate("CATEGORIES")}</h2>
                <ul type="none">
                  {faq.map(({ category, questions }, key) => (
                    <li
                      key={key}
                      className={`country-item ${
                        category === result.category ? "active" : ""
                      }`}
                      onClick={() =>
                        this.onClickListItem({ category, questions })
                      }
                    >
                      {category}
                    </li>
                  ))}
                </ul>
              </div>
            </Col>
            <Col sm="12" md="8">
              <h1>{result.category}</h1>
              <Accordion items={items} />
            </Col>
          </Row>
          <Row>
            <Col>{isFaqLoaded && <RelatedCourses />}</Col>
          </Row>
        </div>
      </div>
    );
  }
}

const Actions = {
  getAllFaq
};

const mapStateToProps = state => {
  return {
    isGetFaqLoading: state.general.isGetFaqLoading,
    isFaqLoaded: state.general.isFaqLoaded,
    faq: state.general.faq
  };
};

export default connect(
  mapStateToProps,
  Actions
)(Faq);
