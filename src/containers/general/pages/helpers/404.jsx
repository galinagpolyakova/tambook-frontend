import React from "react";

import Section from "shared/components/Section";

import notFound from "assets/404.svg";

import "./styles.scss";

export default function NotFound() {
  return (
    <div className="container">
      <Section className="helper-page">
        <img src={notFound} alt="Not Found" />
        <div className="heading-2">Oops!</div>
        <div className="heading-4">Page not found.</div>
      </Section>
    </div>
  );
}
