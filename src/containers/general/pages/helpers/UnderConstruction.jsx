import React from "react";

import Section from "shared/components/Section";

import underConstruction from "assets/under-construction.svg";

import "./styles.scss";

export default function UnderConstruction() {
  return (
    <div className="container">
      <Section className="helper-page">
        <img src={underConstruction} alt="Under Construction" />
        <div className="heading-4">This page is currently</div>
        <div className="heading-2">under construction</div>
      </Section>
    </div>
  );
}
