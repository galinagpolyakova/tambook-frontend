// @flow
import React, { PureComponent } from "react";

import Currency from "components/Currency";

// import Link from "components/Link";

import "./styles.scss";

type CityCardProps = {
  name: string,
  price: number,
  image: string,
  link: string,
  currencyType: string
};

class CityCard extends PureComponent<CityCardProps> {
  render() {
    const { name, price, image, currencyType } = this.props;
    return (
      // <Link to={link}>
      <div
        className="city-card"
        style={{
          background: `linear-gradient(
                      rgba(0, 0, 0, 0.1),
                      rgba(0, 0, 0, 0.6)
                      )
                      , center / cover no-repeat url(${image})`
        }}
      >
        <div className="name">{name}</div>
        <div className="price">
          <span>
            FROM <Currency currencyType={currencyType}>{price}</Currency>
          </span>
          &nbsp; PER WEEK
        </div>
      </div>
      // </Link>
    );
  }
}

export default CityCard;
