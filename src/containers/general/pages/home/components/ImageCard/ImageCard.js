// @flow
import React, { PureComponent } from "react";

import "./styles.scss";

type ImageCardProps = {
  subHeading: string,
  heading: string,
  image: string
};

class ImageCard extends PureComponent<ImageCardProps> {
  getSubHeading = () => {
    const { subHeading } = this.props;
    return subHeading ? (
      <span className="sub-heading">{subHeading}</span>
    ) : null;
  };
  render() {
    const { heading, image } = this.props;
    return (
      <div
        className="image-card"
        style={{
          background: `linear-gradient(
                      rgba(0, 0, 0, 0.6),
                      rgba(0, 0, 0, 0.6)
                      )
                      , center / cover no-repeat url(${image})`
        }}
      >
        <span className="heading">{heading}</span>
        {this.getSubHeading()}
      </div>
    );
  }
}

export default ImageCard;
