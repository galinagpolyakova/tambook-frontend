// @flow
import type { ReviewType } from "containers/review/types";
import React, { Fragment } from "react";

import Rate from "shared/components/Rate";
import Card, { CardBody } from "shared/components/Card";

import { textTruncate } from "shared/utils";

import "./styles.scss";

export default function ReviewCard(props: ReviewType) {
  const { profilePic, rating, comment, name, country } = props;
  return (
    <Card className="review-card">
      <div className="review-card-container">
        <CardBody>
          <div className="reviewer-image">
            <img alt={name} src={profilePic} />
          </div>
          <div className="reviewer-rating">
            <Rate value={rating} size="small" />
          </div>
          <div className="reviewer-name">{name}</div>
          <div className="country-name">
            {country !== "" && country !== null ? (
              `(${country})`
            ) : (
              <Fragment>&nbsp;</Fragment>
            )}
          </div>
          <div className="review-comment">{textTruncate(comment, 200)}</div>
          <br />
        </CardBody>
      </div>
    </Card>
  );
}
