// @flow
import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import Helmet from "react-helmet";

import MainSlider from "./partials/MainSlider";
import LanguageCourses from "./partials/LanguageCourses";
import WhatPeopleAreSaying from "./partials/WhatPeopleAreSaying";

import { getFilterParams } from "containers/course/store/actions";
import { getFeaturedReviews } from "containers/review/store/actions";
import { getCourseParams } from "containers/course/store/selectors";

import type { ReviewsListType } from "containers/review/types";

import "./styles.scss";

type HomeProps = {
  getFeaturedCourses: Function,
  getFeaturedReviews: Function,
  featuredReviews: ReviewsListType,
  filters: Object | null,
  getFilterParams: Function
};

class Home extends PureComponent<HomeProps> {
  componentDidMount() {
    this.props.getFeaturedReviews();
    this.props.getFilterParams();
  }

  render() {
    const { featuredReviews } = this.props;
    return (
      <Fragment>
        <Helmet
          title={"Tambook"}
          meta={[{ property: "og:title", content: "Tambook" }]}
        />
        <MainSlider
          countries={
            this.props.filters === null
              ? [
                  { country: "NEW ZEALAND" },
                  { country: "AUSTRALIA" },
                  { country: "UNITED STATES OF AMERICA" },
                  { country: "UNITED KINGDOM" },
                  { country: "MALTA" },
                  { country: "CANADA" },
                  { country: "IRELAND" },
                  { country: "SOUTH AFRICA" },
                  { country: "SWITZERLAND" },
                  { country: "SCOTLAND" }
                ]
              : this.props.filters.countries
          }
          languages={
            this.props.filters === null
              ? [{ language: "ENGLISH" }, { language: "FRENCH" }]
              : this.props.filters.languages
          }
        />
        <div>
          <LanguageCourses />
          <div className="promo-video bg-secondary">
            <div className="video-container">
              <iframe
                title="Promo Video"
                width="100%"
                height="500"
                src="https://www.youtube.com/embed/4YgoYL2qMtA"
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              />
            </div>
          </div>
          <WhatPeopleAreSaying featuredReviews={featuredReviews} />
        </div>
      </Fragment>
    );
  }
}
const Actions = {
  getFeaturedReviews,
  getFilterParams
};
function mapStateToProps(state) {
  return {
    featuredReviews: state.review.featuredReviews,
    filters: getCourseParams(state)
  };
}
export default connect(
  mapStateToProps,
  Actions
)(Home);
