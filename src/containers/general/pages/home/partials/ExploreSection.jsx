import React from "react";

import Section from "shared/components/Section/Section";
import SectionHeader from "shared/components/Section/SectionHeader";
import SectionBody from "shared/components/Section/SectionBody";
import ImageCard from "../components/ImageCard";
import Row from "shared/components/Row";
import Col from "shared/components/Col";

import Link from "components/Link";

import experiencesImage from "assets/home/experiences.jpg";
import eligibleForWorkImage from "assets/home/eligible-for-work.jpg";
import languagesImage from "assets/home/languages.jpg";
import countriesImage from "assets/home/countries.jpg";

export default function exploreSection() {
  return (
    <Section className="expore-section container">
      <SectionHeader
        heading="Explore"
        subheading="You select what matters to you, and we’ll show you language schools and destinations that match your interests."
      />
      <SectionBody>
        <Row equalHeight>
          <Col md="6">
            <Row className="has-margin-bottom">
              <Col>
                <Link to="/under-construction">
                  <ImageCard image={experiencesImage} heading="EXPERIENCES" />
                </Link>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <Link to="/under-construction">
                  <ImageCard image={languagesImage} heading="LANGUAGES" />
                </Link>
              </Col>
              <Col md="6">
                <Link to="/under-construction">
                  <ImageCard image={countriesImage} heading="COUNTRIES" />
                </Link>
              </Col>
            </Row>
          </Col>
          <Col md="6" className="flex-box">
            <Link to="/under-construction">
              <ImageCard
                image={eligibleForWorkImage}
                heading="ELIGIBLE FOR WORK"
                subHeading="Click here to start a search for overseas language courses that make you eligible for a work visa."
              />
            </Link>
          </Col>
        </Row>
      </SectionBody>
    </Section>
  );
}
