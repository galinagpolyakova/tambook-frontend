import React from "react";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Section from "shared/components/Section";
import translate from "translations/translate";

import europeImage from "assets/home/europe.png";
import africaImage from "assets/home/africa.png";
import asiaImage from "assets/home/asia.png";
import australiaImage from "assets/home/australia.png";
import northAmericaImage from "assets/home/north-america.png";
import southAmericaImage from "assets/home/south-america.png";

export default function languageCourses() {
  return (
    <Section className="courses-all-around-section">
      <div className="container">
        <Row>
          <Col md={3} className="section-title-container">
            <p className="heading-3">
              {translate("Language Courses All Around The World")}
            </p>
          </Col>
          <Col md={9} className="section-body-container">
            <Row>
              <Col className="country-card">
                <img alt="EUROPE" src={europeImage} />
                <span>{translate("Europe")}</span>
              </Col>
              <Col className="country-card">
                <img alt="ASIA" src={asiaImage} />
                <span>{translate("Asia")}</span>
              </Col>
              <Col className="country-card">
                <img alt="AFRICA" src={africaImage} />
                <span>{translate("Africa")}</span>
              </Col>
              <Col className="country-card">
                <img alt="NORTH AMERICA" src={northAmericaImage} />
                <span>{translate("North America")}</span>
              </Col>
              <Col className="country-card">
                <img alt="SOUTH AMERICA" src={southAmericaImage} />
                <span>{translate("South America")}</span>
              </Col>
              <Col className="country-card">
                <img alt="AUSTRALIA" src={australiaImage} />
                <span>{translate("Australia")}</span>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Section>
  );
}
