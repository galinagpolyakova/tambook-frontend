// @flow
import React, { PureComponent } from "react";
import { Redirect } from "react-router-dom";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Button from "shared/components/Button";
import Select from "shared/components/Select";
import translate from "translations/translate";

import { getLanguageFromUrl } from "shared/helpers/translations";

type MainSliderProps = {
  countries: any[],
  languages: any[]
};

type MainSliderState = {
  country: string,
  language: string,
  isFormSubmitted: boolean
};

class MainSlider extends PureComponent<MainSliderProps, MainSliderState> {
  constructor(props: MainSliderProps) {
    super(props);

    this.state = {
      country: "",
      language: "",
      isFormSubmitted: false
    };
    // $FlowFixMe
    this.submitSearchForm = this.submitSearchForm.bind(this);
  }

  submitSearchForm = () => {
    this.setState({
      isFormSubmitted: true
    });
  };

  render() {
    const { countries, languages } = this.props;
    const { country, language, isFormSubmitted } = this.state;

    const currentLanguage = getLanguageFromUrl();

    if (isFormSubmitted) {
      return (
        <Redirect
          to={{
            pathname: `/${currentLanguage}/filter`,
            search: `?country=${country}&language=${language}`
          }}
        />
      );
    }

    return (
      <div className="hero-slider">
        <div className="container">
          <span className="hero-heading">
            {translate(
              "Find, compare and book the best priced language courses"
            )}
          </span>
          <div className="search-box">
            <Row>
              <Col md={5}>
                <div className="form-group">
                  <label>{translate("Where do you want to study?")}</label>
                  <input
                    type="hidden"
                    name="country"
                    value={this.state.country}
                  />
                  <Select
                    defaultSelected={true}
                    onChange={country => {
                      this.setState({ country });
                    }}
                    selected={this.state.country}
                    options={[
                      { name: translate("All Countries"), value: "" },
                      ...countries.map(({ country }) => country)
                    ]}
                  />
                </div>
              </Col>
              <Col md={5}>
                <div className="form-group">
                  <label>
                    {translate("What language do you want to learn?")}
                  </label>
                  <input
                    type="hidden"
                    name="language"
                    value={this.state.language}
                  />
                  <Select
                    defaultSelected={true}
                    onChange={language => {
                      this.setState({ language });
                    }}
                    selected={this.state.language}
                    options={[
                      { name: translate("All Languages"), value: "" },
                      ...languages.map(({ language }) => language)
                    ]}
                  />
                </div>
              </Col>
              <Col md={2} className="form-group search-button">
                <Button
                  type={Button.TYPE.SECONDARY}
                  onClick={this.submitSearchForm}
                >
                  {translate("Search now")}
                </Button>
              </Col>
            </Row>
          </div>
          <span className="hero-subheading">
            {translate(
              "Search hundreds of top rated, government approved language schools from around the world"
            )}
            .
          </span>
        </div>
      </div>
    );
  }
}

export default MainSlider;
