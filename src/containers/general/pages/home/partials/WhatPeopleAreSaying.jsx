// @flow
import React, { PureComponent } from "react";

import { type ReviewsListType } from "containers/review/types";

import ReviewCard from "../components/ReviewCard";

import Section, { SectionHeader, SectionBody } from "shared/components/Section";

import translate from "translations/translate";
import Button from "shared/components/Button";
import Icon from "shared/components/Icon";

type WhatPeopleAreSayingProps = {
  featuredReviews: ReviewsListType
};

type WhatPeopleAreSayingState = {
  slidesPerPage: number,
  totalSlides: number,
  activeSlide: number
};

class WhatPeopleAreSaying extends PureComponent<
  WhatPeopleAreSayingProps,
  WhatPeopleAreSayingState
> {
  state = {
    slidesPerPage: 5,
    activeSlide: 0,
    totalSlides: 0
  };

  componentDidMount() {
    this._handleResize();
    window.addEventListener("resize", this._handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this._handleResize);
  }

  _handleResize = () => {
    const slidesPerPage = parseInt((window.innerWidth / 320).toFixed());

    this.setState({
      slidesPerPage,
      totalSlides: Math.floor(4 / slidesPerPage) + 1
    });
  };

  _goToNextSlide = () => {
    this.setState(state => ({
      activeSlide: state.activeSlide + 1
    }));
  };

  _goToPreviousSlide = () => {
    this.setState(state => ({
      activeSlide: state.activeSlide - 1
    }));
  };

  getReviews(reviews: ReviewsListType) {
    if (reviews !== undefined && reviews.length > 0) {
      // $FlowFixMe
      return reviews.map((review, reviewKey) => (
        <ReviewCard {...review} key={reviewKey} />
      ));
    }
  }

  render() {
    const { featuredReviews } = this.props;
    return (
      <Section className="what-people-saying-section">
        <div className="container">
          <SectionHeader heading={translate("What People Are Saying")} />
          <SectionBody>
            <div className="reviews-container">
              {this.getReviews(featuredReviews)}
            </div>
            <div className="view-more-container">
              <Button
                size={Button.SIZE.MEDIUM}
                htmlType={Button.HTML_TYPE.LINK}
                link="/reviews"
                className="btn-with-icon"
              >
                {translate("VIEW MORE")}&nbsp;
                <Icon className="icon" icon="chevron-right" />
              </Button>
            </div>
          </SectionBody>
        </div>
      </Section>
    );
  }
}

export default WhatPeopleAreSaying;
