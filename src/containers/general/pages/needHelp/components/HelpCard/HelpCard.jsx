// @flow
import React, { PureComponent } from "react";

import "./styles.scss";

type HelpCardProps = {
  title: string,
  data: string
};
class HelpCard extends PureComponent<HelpCardProps> {
  render() {
    return (
      <div className="help-card-container">
        <div className="help-card-title">{this.props.title}</div>
        <div className="help-card-Data">{this.props.data}</div>
      </div>
    );
  }
}

export default HelpCard;
