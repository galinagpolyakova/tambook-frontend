// @flow
import React, { PureComponent } from "react";

import PageHeader from "components/PageHeader";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import HelpCard from "./components/HelpCard";
import Sidebar from "../components/sidebar";
import translate from "translations/translate";
import RelatedCourses from "containers/course/components/RelatedCourses";

import "./styles.scss";

type NeedHelpProps = {};

type NeedHelpState = {
  selected: string
};

class NeedHelp extends PureComponent<NeedHelpProps, NeedHelpState> {
  constructor() {
    super();

    this.state = {
      selected: translate("Australia")
    };
    // $FlowFixMe
    this.getDetails = this.getDetails.bind(this);
    // $FlowFixMe
    this.onClickListItem = this.onClickListItem.bind(this);
  }

  countryDetails = [
    {
      country: translate("Australia"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate(
                  "Here you’ll learn how to apply for a visa. The web page will tell what documents are required, what your work options are, and further details about the application process."
                )}
              </p>
              <a href="https://www.studyinaustralia.gov.au/english/apply-to-study/visas">
                https://www.studyinaustralia.gov.au/english/apply-to-study/visas
              </a>
              <p>
                {translate(
                  "The application form for the student visa is available here:"
                )}
              </p>
              <a href="https://online.immi.gov.au/lusc/login">
                https://online.immi.gov.au/lusc/login
              </a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate(
                  "Use these trusted websites to find student accommodation in any part of Australia."
                )}
              </p>
              <a href="https://www.student.com/au">
                https://www.student.com/au
              </a>
              &nbsp; - {translate("flats, studios and rooms in shared")}
              flats.
              <br />
              <br />
              <a href="https://flatmates.com.au">
                https://flatmates.com.au
              </a> – {translate("here you can find your future flatmates.")}
              <br />
            </div>
          )
        },
        {
          title: translate("Buy and sell on-line"),
          data: (
            <div>
              <p>
                {translate(
                  "Buy and sell whatever you want on these Australian websites."
                )}
              </p>
              <a href="https://www.gumtree.com.au/s-sydney/l3003435">
                https://www.gumtree.com.au/s-sydney/l3003435
              </a>
              <br />
              <a href="http://buy-and-sell.freeadsaustralia.com">
                http://buy-and-sell.freeadsaustralia.com
              </a>
              <br />
              <a href="http://www.sellbuyswap.com.au">
                http://www.sellbuyswap.com.au
              </a>
            </div>
          )
        },
        {
          title: translate("Student work in Australia"),
          data: (
            <div>
              <a href="https://www.seek.com.au/uni-student-jobs">
                https://www.seek.com.au/uni-student-jobs
              </a>
              &nbsp; - {translate("job offers for students.")}
              <br />
              <br />
              <a href="https://www.adzuna.com.au">https://www.adzuna.com.au</a>
              &nbsp; –&nbsp;
              {translate("here you can find and apply for any kind of job.")}
              <br />
              <br />
              <a href="https://www.careerone.com.au">
                https://www.careerone.com.au
              </a>
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>
                {translate(
                  "Here you can find a car for hire anywhere in Australia."
                )}
              </p>
              <a href="https://www.vroomvroomvroom.com.au">
                https://www.vroomvroomvroom.com.au
              </a>
              <br />
              <a href="https://www.easyrentcars.com">
                https://www.easyrentcars.com
              </a>
            </div>
          )
        },
        {
          title: translate("Moving companies in Australia"),
          data: (
            <div>
              <p>
                {translate(
                  "These companies will help you move within Australia, or internationally. They’re useful companies for moving personal items, or unique heavy items."
                )}
              </p>
              <a href="https://www.grace.com.au/removals/interstate-removals">
                https://www.grace.com.au/removals/interstate-removals
              </a>
              <br />
              <br />
              <a href="https://www.alliedpickfords.com.au">
                https://www.alliedpickfords.com.au
              </a>
            </div>
          )
        },
        {
          title: translate("Emergency number in Australia"),
          data: (
            <div>
              <p>{translate("National emergency call service number – 000")}</p>
              <p>
                {translate(
                  "Secondary emergency call service numbers – 112 and 106"
                )}
              </p>
            </div>
          )
        }
      ]
    },
    {
      country: translate("Scotland"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate(
                  "As Scotland is a part of the UK, you’ll probably need a UK visa to study there. Check your eligibility here:"
                )}
              </p>
              <a href="https://www.gov.uk/check-uk-visa">
                https://www.gov.uk/check-uk-visa
              </a>
              <p>
                {translate(
                  "Application form for the student visa is available here:"
                )}
              </p>
              <a href="https://apply.uq.edu.au">https://apply.uq.edu.au</a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate(
                  "Use these websites to find a room in a student residence or in a shared flat:"
                )}
              </p>
              <a href="https://apply.uq.edu.au">https://apply.uq.edu.au</a>
              <br />
              <a href="https://www.student.com">https://www.student.com</a>
              <br />
              <a href="https://www.downingstudents.com/student-accommodation">
                https://www.downingstudents.com/student-accommodation
              </a>
            </div>
          )
        },
        {
          title: translate("Buy and sell online"),
          data: (
            <div>
              <p>
                {translate(
                  "Buy all you’ll need during your stay in Scotland on the following websites:"
                )}
              </p>
              <a href="https://www.freeads.co.uk/scotland/buy-sell">
                https://www.freeads.co.uk/scotland/buy-sell
              </a>
              <br />
              <a href="https://www.preloved.co.uk/classifieds/all/scotland">
                https://www.preloved.co.uk/classifieds/all/scotland
              </a>
            </div>
          )
        },
        {
          title: translate("Student work in Scotland"),
          data: (
            <div>
              <p>
                {translate(
                  "If you want to work while studying, you can find a part-time job here:"
                )}
              </p>
              <a href="https://jobs.trovit.co.uk">https://jobs.trovit.co.uk</a>
              <a href="https://www.indeed.co.uk/Ideal-Student-jobs-in-Scotland">
                https://www.indeed.co.uk/Ideal-Student-jobs-in-Scotland
              </a>
              <br />
              <br />
              <a href="https://www.totaljobs.com/jobs/student/in-scotland">
                https://www.totaljobs.com/jobs/student/in-scotland
              </a>
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>
                {translate("Use these national providers for car rentals:")}
              </p>
              <a href="https://www.easycar.com/car-hire/scotland">
                https://www.easycar.com/car-hire/scotland
              </a>
              <br />
              <a href="https://www.sixt.com/car-rental/united-kingdom/scotland">
                https://www.sixt.com/car-rental/united-kingdom/scotland
              </a>
            </div>
          )
        },
        {
          title: translate("Moving companies in Scotland"),
          data: (
            <div>
              <p>
                {translate(
                  "Here are moving companies that operate over the entire country:"
                )}
              </p>
              <a href="http://www.headingtoncarriers.co.uk">
                http://www.headingtoncarriers.co.uk
              </a>
              <br />
              <a href="https://www.pickfords.co.uk/removals-and-storage-scotland">
                https://www.pickfords.co.uk/removals-and-storage-scotland
              </a>
            </div>
          )
        },
        {
          title: translate("Emergency number in Scotland"),
          data: (
            <div>
              <p>{translate("In case of emergency call 999 or 112")}</p>
              <p>{translate("For gas emergencies, call 0800111999")}</p>
            </div>
          )
        }
      ]
    },
    {
      country: translate("United States of America"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate(
                  "If you aren’t a citizen of the USA or Canada you’ll need to apply for a student visa. The online form is available here"
                )}
              </p>
              <a href="https://ceac.state.gov/GenNIV/Default.aspx">
                https://ceac.state.gov/GenNIV/Default.aspx
              </a>
              <p>
                {translate(
                  "You’ll need to do an in-person interview to get your visa. Check this website to find your nearest embassy."
                )}
              </p>
              <a href="https://www.usembassy.gov">https://www.usembassy.gov</a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate(
                  "Find a shared room or a room in a shared flat on the following websites:"
                )}
              </p>
              <a href="https://www.findroommate.com">
                https://www.findroommate.com
              </a>
              <br />
              <a href="https://www.iroommates.com/region_rental/united_states">
                https://www.iroommates.com/united_states
              </a>
              <br />
              <a href="https://www.spareroom.com/roommates">
                https://www.spareroom.com/roommates
              </a>
              <br />
              <a href="https://www.spareroom.com/roommates">
                https://www.spareroom.com/roommates
              </a>
            </div>
          )
        },
        {
          title: translate("Buy and sell online"),
          data: (
            <div>
              <p>{translate("Buy or sell used things here")}</p>
              <a href="https://poshmark.com">https://poshmark.com</a>–&nbsp;
              {translate("specialty clothes selling")}:
              <br />
              <a href="https://buysaleandtrade.com/index.php?region=1">
                https://buysaleandtrade.com/index.php?region=1
              </a>
              <br />
              <a href="http://us.classifieds.sulekha.com/buy-sell">
                http://us.classifieds.sulekha.com/buy-sell
              </a>
            </div>
          )
        },
        {
          title: translate("Student work in the USA"),
          data: (
            <div>
              <p>{translate("You can find seasonal student work here:")}</p>
              <a href="https://www.seasonworkers.com">
                https://www.seasonworkers.com
              </a>
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>
                {translate(
                  "Here you can find a rental car in any state in the USA:"
                )}
              </p>
              <a href="https://www.usacarsrental.com">
                https://www.usacarsrental.com
              </a>
              <br />
              <a href="https://www.americacarrental.com/en">
                https://www.americacarrental.com/en
              </a>
              <br />
              <a href="https://www.economybookings.com">
                https://www.economybookings.com
              </a>
            </div>
          )
        },
        {
          title: translate("Moving companies in the USA"),
          data: (
            <div>
              <p>{translate("Use these nationwide moving companies:")}</p>
              <a href="http://getmovingusa.com">http://getmovingusa.com</a>
              <br />
              <a href="https://bestmoverquote.com/fullservicemoving.html?gclid=EAIaIQobChMImrKfucbh4AIVg4XVCh27jAx6EAAYAiAAEgJ_cfD_BwE">
                https://bestmoverquote.com/fullservicemoving.html?gclid=EAIaIQobChMImrKfucbh4AIVg4XVCh27jAx6EA
                <br />
                AYAiAAEgJ_cfD_BwE
              </a>
              <br />
              <br />
              <a href="http://gentlemoving.net">http://gentlemoving.net</a>
            </div>
          )
        },
        {
          title: translate("Emergency number in USA"),
          data: (
            <div>
              <p>{translate("In case of emergency, call 911")}</p>
            </div>
          )
        }
      ]
    },
    {
      country: translate("United Kingdom"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>{translate("You can apply for the UK visa here:")}</p>
              <a href="https://apply.uq.edu.au">https://apply.uq.edu.au</a>
              <p>{translate("Check your eligibility here:")}</p>
              <a href="https://www.gov.uk/check-uk-visa">
                https://www.gov.uk/check-uk-visa
              </a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate(
                  "Find accommodation for your stay using these websites:"
                )}
              </p>
              <a href="https://www.unitestudents.com">
                https://www.unitestudents.com
              </a>
              <br />
              <a href="https://www.padsforstudents.co.uk/index.php">
                https://www.padsforstudents.co.uk/index.php
              </a>
              <br />
              <a href="https://www.campusboard.co.uk">
                https://www.campusboard.co.uk
              </a>
              <br />
              <a href="https://www.accommodationforstudents.com">
                https://www.accommodationforstudents.com
              </a>
            </div>
          )
        },
        {
          title: translate("Buy and sell online"),
          data: (
            <div>
              <p>
                {translate("Buy or sell second hand goods on these pages:")}
              </p>
              <a href="https://www.preloved.co.uk">
                https://www.preloved.co.uk
              </a>
              <br />
              <a href="https://gb.letgo.com/uk">https://gb.letgo.com/uk</a>
            </div>
          )
        },
        {
          title: translate("Student work in the UK"),
          data: (
            <div>
              <p>
                {translate(
                  "If you want to work while studying, use these websites:"
                )}
              </p>
              <a href="https://www.studentjob.co.uk/part-time-job">
                https://www.studentjob.co.uk/part-time-job
              </a>
              <br />
              <a href="https://jobs.trovit.co.uk">https://jobs.trovit.co.uk</a>
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>
                {translate("Use one of these nationwide rental companies:")}
              </p>
              <a href="https://www.autoeurope.com/car-rental-uk">
                https://www.autoeurope.com/car-rental-uk
              </a>
              <br />
              <a href="https://www.enterprise.co.uk">
                https://www.enterprise.co.uk
              </a>
            </div>
          )
        },
        {
          title: translate("Moving companies in the UK"),
          data: (
            <div>
              <p>{translate("Use one of these nationwide movers:")}</p>
              <a href="https://www.pickfords.co.uk/home-moving">
                https://www.pickfords.co.uk/home-moving
              </a>
              <br />
              <a href="https://bestmoverquote.com">
                https://bestmoverquote.com
              </a>
              <br />
              <a href="http://www.london-houseremovals.co.uk">
                http://www.london-houseremovals.co.uk
              </a>
            </div>
          )
        },
        {
          title: translate("Emergency numbers in the UK"),
          data: (
            <div>
              <p>{translate("In case of any emergency call 999 or 112")}</p>
              <p>{translate("If it’s a gas emergency, call 0800111999")}</p>
            </div>
          )
        }
      ]
    },
    {
      country: translate("Canada"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate(
                  "You can find full information about Canadian visas, and eligibility, here:"
                )}
              </p>
              <a href="https://www.canadianetavisa.org/visa-information">
                https://www.canadianetavisa.org/visa-information
              </a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate(
                  "Find a flat, room in a student residence, or a shared flat on the following websites:"
                )}
              </p>
              <a href="http://canada.accommodationforstudents.com">
                http://canada.accommodationforstudents.com
              </a>
              <br />
              <a href="https://www.universityrooms.com">
                https://www.universityrooms.com
              </a>
              <br />
              <a href="https://www.search4studenthousing.com/search">
                https://www.search4studenthousing.com/search
              </a>
            </div>
          )
        },
        {
          title: translate("Buy and sell online"),
          data: (
            <div>
              <p>
                {translate(
                  "Here you have links to the most popular online platforms for second hand goods:"
                )}
              </p>
              <a href="https://www.kijiji.ca/b-buy-sell/canada/c10l0">
                https://www.kijiji.ca/b-buy-sell/canada/c10l0
              </a>
              <br />
              <a href="https://ca.letgo.com/en">https://ca.letgo.com/en</a>
              <br />
              <a href="https://www.used.ca">https://www.used.ca</a>
              <br />
              <a href="https://buyandsell.gc.ca">https://buyandsell.gc.ca</a>
            </div>
          )
        },
        {
          title: translate("Student work in Canada"),
          data: (
            <div>
              <p>{translate("Find part time student jobs here:")}</p>
              <a href="https://talentegg.ca">https://talentegg.ca</a>
              <br />
              <a href="https://ca.indeed.com/m/jobs?q=Part+Time">
                https://ca.indeed.com/m/jobs?q=Part+Time
              </a>
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>
                {translate(
                  "If you want to rent a car in Canada, we recommend you use one of these companies:"
                )}
              </p>
              <a href="https://www.rentalcars.com">
                https://www.rentalcars.com
              </a>
              <br />
              <a href="https://www.ca.kayak.com/Canada-Car-Rentals.43.crc.html">
                https://www.ca.kayak.com/Canada-Car-Rentals.43.crc.html
              </a>
            </div>
          )
        },
        {
          title: translate("Moving companies in the Canada"),
          data: (
            <div>
              <p>
                {translate(
                  "Here are two major moving companies, operating throughout Canada:"
                )}
              </p>
              <a href="https://www.santaferelo.com">
                https://www.santaferelo.com
              </a>
              <br />
              <a href="https://www.alliedvanlines.ca">
                https://www.alliedvanlines.ca
              </a>
            </div>
          )
        },
        {
          title: translate("Emergency number in Canada"),
          data: (
            <div>
              <p>{translate("In case of emergency situation call 911")}</p>
            </div>
          )
        }
      ]
    },
    {
      country: translate("South Africa"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate(
                  "For detailed information about students visas, please contact the Local South African Mission in your country. You can check for your nearest Mission here:"
                )}
              </p>
              <a href="http://www.dirco.gov.za/foreign/sa_abroad/index.htm">
                http://www.dirco.gov.za/foreign/sa_abroad/index.htm
              </a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate("Find your new home away from home on these pages:")}
              </p>
              <a href="http://southafrica.accommodationforstudents.com">
                http://southafrica.accommodationforstudents.com
              </a>
              <br />
              <a href="http://www.studentaccommodation.co.za">
                http://www.studentaccommodation.co.za
              </a>
              <br />
              <a href="https://www.theroomlink.co.za">
                https://www.theroomlink.co.za
              </a>
            </div>
          )
        },
        {
          title: translate("Buy and sell online"),
          data: (
            <div>
              <p>
                {translate(
                  "Use these websites for buying and selling second hand goods:"
                )}
              </p>
              <a href="https://www.ananzi.co.za/ads/stuff/buy-or-sell-south-africa.html">
                https://www.ananzi.co.za/ads/stuff/buy-or-sell-south-africa.html
              </a>
              <br />
              <br />
              <a href="https://www.olx.co.za">https://www.olx.co.za</a>
              <br />
              <br />
              <a href="https://www.bidorbuy.co.za/article/24/South_Africas_Largest_Online_Shopping_and_Auction_Marketplace">
                https://www.bidorbuy.co.za/article/24/South_Africas_Largest_Online_Shopping_and_Auction_Marketplace
              </a>
            </div>
          )
        },
        {
          title: translate("Student work"),
          data: (
            <div>
              <p>
                {translate(
                  "Use these websites to find student work in South Africa:"
                )}
              </p>
              <a href="https://www.adzuna.co.za/student-part-time">
                https://www.adzuna.co.za/student-part-time
              </a>
              <br />
              <a href="https://www.careerjet.co.za/student-jobs.html">
                https://www.careerjet.co.za/student-jobs.html
              </a>
              <br />
              <a href="https://www.rent-a-student.co.za">
                https://www.rent-a-student.co.za
              </a>
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>
                {translate(
                  "These are the major car rental companies operating in South Africa:"
                )}
              </p>
              <a href="https://www.thrifty.co.za">https://www.thrifty.co.za</a>
              <br />
              <a href="https://www.avis.co.za">https://www.avis.co.za</a>
              <br />
              <a href="https://www.drivesouthafrica.com/za/rentals/car-hire/south-africa">
                https://www.drivesouthafrica.com/za/rentals/car-hire/south-africa
              </a>
              <br />
              <a href="https://www.budget.co.za">https://www.budget.co.za</a>
            </div>
          )
        },
        {
          title: translate("Moving companies in the South Africa"),
          data: (
            <div>
              <p>
                {translate("These South African companies will help you move:")}
              </p>
              <a href="https://www.pickfords.co.za">
                https://www.pickfords.co.za
              </a>
              <br />
              <a href="https://www.pickfords.co.za">
                https://www.pickfords.co.za
              </a>
            </div>
          )
        },
        {
          title: translate("Emergency number in South Africa"),
          data: (
            <div>
              <p>{translate("Emergency - Ambulance – 10177")}</p>
              <p>{translate("Emergency - Cell phone – 112")}</p>
              <p>{translate("Emergency - National - 10111")}</p>
            </div>
          )
        }
      ]
    },
    {
      country: translate("Malta"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate(
                  "If you are not an EU citizen, you’ll probably need a long-term student visa. For more information contact the nearest embassy to you:"
                )}
              </p>
              <a href="https://identitymalta.com/where-to-apply">
                https://identitymalta.com/where-to-apply
              </a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate("Find your new home in Malta on these web pages:")}
              </p>
              <a href="https://www.universityrooms.com/en-GB/city/malta/home">
                https://www.universityrooms.com/en-GB/city/malta/home
              </a>
              <br />
              <br />
              <a href="http://universityresidence.com/accomodation/accommodation-choices/student-accommodation">
                http://universityresidence.com/accomodation/accommodation-choices/student-accommodation
              </a>
              <br />
              <br />
              <a href="https://sunnycoast.com.mt">https://sunnycoast.com.mt</a>
            </div>
          )
        },
        {
          title: translate("Buy or sell used things here"),
          data: (
            <div>
              <p>
                {translate(
                  "Buy or sell everything you’ll need using the following websites:"
                )}
              </p>
              <a href="https://www.fairsale.com.mt">
                https://www.fairsale.com.mt
              </a>
              <br />
              <a href="https://www.sellinmalta.com">
                https://www.sellinmalta.com
              </a>
              <br />
              <a href="https://easysell.com.mt">https://easysell.com.mt</a>
              <br />
              <a href="http://www.secondhand.com.mt">
                http://www.secondhand.com.mt
              </a>
            </div>
          )
        },
        {
          title: translate("Student work"),
          data: (
            <div>
              <p>
                {translate(
                  "If you want to find a student job in Malta, use these websites:"
                )}
              </p>
              <a href="https://www.totaljobs.com/jobs/student/in-malta">
                https://www.totaljobs.com/jobs/student/in-malta
              </a>
              <br />
              <a href="https://jobsinmalta.com/malta-job-vacancies/part-time">
                https://jobsinmalta.com/malta-job-vacancies/part-time
              </a>
              <br />
              <br />
              <a href="https://www.indeed.com/q-For-Student-l-Malta,-NY-jobs.html">
                https://www.indeed.com/q-For-Student-l-Malta,-NY-jobs.html
              </a>
              <br />
              <br />
              <a href="https://www.careerjet.com.mt">
                https://www.careerjet.com.mt
              </a>
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>
                {translate(
                  "These are the leading car rental companies in Malta:"
                )}
              </p>
              <a href="https://www.maltacar.com">https://www.maltacar.com</a>
              <br />
              <a href="https://www.kayak.co.uk/Malta-Car-hire.151.crc.html">
                https://www.kayak.co.uk/Malta-Car-hire.151.crc.html
              </a>
              <br />
              <a href="https://www.maltauncovered.com/malta-car-hire">
                https://www.maltauncovered.com/malta-car-hire
              </a>
            </div>
          )
        },
        {
          title: translate("Moving companies in the Malta"),
          data: (
            <div>
              <p>{translate("Use this established moving company:")}</p>
              <a href="http://www.domtransportmalta.com">
                http://www.domtransportmalta.com
              </a>
            </div>
          )
        },
        {
          title: translate("Emergency number in Malta"),
          data: (
            <data>
              <p>{translate("In case of emergency, call 112")}</p>
            </data>
          )
        }
      ]
    },
    {
      country: translate("Ireland"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate(
                  "Use this website to learn about everything you’ll need to get a student Visa in Ireland:"
                )}
              </p>
              <a href="http://www.inis.gov.ie/en/inis/pages/study">
                http://www.inis.gov.ie/en/inis/pages/study
              </a>
              <p>
                {translate(
                  "Use this website to find the closest application office in your country:"
                )}
              </p>
              <a href="http://www.inis.gov.ie/en/INIS/Pages/registration-offices">
                http://www.inis.gov.ie/en/INIS/Pages/registration-offices
              </a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate(
                  "Use these websites to find student accommodation in Ireland:"
                )}
              </p>
              <a href="https://ie.roomeez.com">https://ie.roomeez.com</a>&nbsp;
              -&nbsp;
              {translate("Rooms in shared flats")}
              <br />
              <a href="https://www.rent.ie/student-accommodation">
                https://www.rent.ie/student-accommodation
              </a>
              &nbsp; -&nbsp;
              {translate(
                "Flats and rooms in shared apartments. You can <also find parking spaces here."
              )}
              <br />
              <br />
              <a href="https://www.stays.io">https://www.stays.io</a> –
              {translate("Short-term accommodation in Ireland")}.
            </div>
          )
        },
        {
          title: translate("Buy and sell online"),
          data: (
            <div>
              <p>
                {translate(
                  "Use these websites to buy and sell second hand goods:"
                )}
              </p>
              <a href="https://www.littlewoodsireland.ie">
                https://www.littlewoodsireland.ie
              </a>
              <br />
              <a href="https://www.debenhams.ie">https://www.debenhams.ie</a>
              <br />
              <a href="https://www.adverts.ie">https://www.adverts.ie</a>
            </div>
          )
        },
        {
          title: translate("Work in Ireland"),
          data: (
            <div>
              <p>
                {translate(
                  "The following websites will help you to find a job in Ireland:"
                )}
              </p>
              <a href="https://www.internationalstudents.ie">
                https://www.internationalstudents.ie
              </a>
              &nbsp; – {translate("contains useful information for students.")}
              <br />
              <br />
              <a href="https://www.jobs.ie">https://www.jobs.ie</a> –&nbsp;
              {translate("part-time")}
              <br />
              {translate("and full-time job offers.")}
              <br />
              <br />
              <a href="https://www.irishjobs.ie/Student-Jobs">
                https://www.irishjobs.ie/Student-Jobs
              </a>
              &nbsp; – {translate("find a")} <br />
              {translate("job in a large, established company.")}
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>{translate("Find the car you want here:")}</p>
              <a href="https://www.carhire.ie">https://www.carhire.ie</a>
              <br />
              <a href="https://www.budget.ie">https://www.budget.ie</a>
              <br />
              <a href="https://www.kayak.com/Ireland-Car-Rentals.116.crc.html">
                https://www.kayak.com/Ireland-Car-Rentals.116.crc.html
              </a>
            </div>
          )
        },
        {
          title: translate("Moving companies in Ireland"),
          data: (
            <div>
              <p>
                {translate(
                  "For moving furniture and other large items, one of the following moving companies will help you out:"
                )}
              </p>
              <a href="https://www.anyvan.ie">https://www.anyvan.ie</a>
              <br />
              <a href="https://www.getamover.ie">https://www.getamover.ie</a>
              <br />
              <a href="https://www.getcracking.ie">
                https://www.getcracking.ie
              </a>
            </div>
          )
        },
        {
          title: translate("Emergency number in Ireland"),
          data: (
            <div>
              <p>{translate("In case of emergency, call 112 or 999")}</p>
            </div>
          )
        }
      ]
    },
    {
      country: translate("Switzerland"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate("Requirement to get a Student Visa in Switzerland")}
              </p>
              <a
                href={
                  "https://www.eda.admin.ch/eda/en/home/entry-switzerland-residence/visa-requirements-application-form.html"
                }
              >
                https://www.eda.admin.ch/eda/en/home/entry-switzerland-residence/visa-requirements-application-form.html
              </a>
            </div>
          )
        },
        {
          title: translate("Buy and Sell online"),
          data: (
            <div>
              <a href="www.ricardo.ch">www.ricardo.ch</a>
              <br />
              <a href="www.anibis.ch">www.anibis.ch</a>
              <br />
              <a href="www.tutti.ch">www.tutti.ch</a>
            </div>
          )
        },
        {
          title: translate("Car rentals"),
          data: (
            <div>
              <a href="www.liligo.fr/location-voiture-suisse.html">
                www.liligo.fr/location-voiture-suisse.html
              </a>
              <br />
              <a href="www.mobility.ch">www.mobility.ch</a>
            </div>
          )
        },
        {
          title: translate("Student Work in Switzerland"),
          data: (
            <div>
              <a href="https://www.students.ch/">https://www.students.ch/</a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <a href="www.youthhostel.ch/fr/">www.youthhostel.ch/fr/</a>
              <br />
              <a href="https://fr-fr.roomlala.com/colocation/CH-Suisse">
                https://fr-fr.roomlala.com/colocation/CH-Suisse
              </a>
              <br />
              <a href="https://www.students.ch/">https://www.students.ch/</a>
            </div>
          )
        },
        {
          title: translate("Moving companies"),
          data: (
            <div>
              <a href="www.devis.ch/f/demenagement-nettoyage/devis-demenagement/8/44">
                www.devis.ch/f/demenagement-nettoyage/devis-demenagement/8/44
              </a>
              <br />
              <a href="www.demenagement-365.ch/">www.demenagement-365.ch/</a>
            </div>
          )
        },
        {
          title: translate("Emergency Numbers"),
          data: (
            <div>
              <p>117- {translate("Police")}</p>
              <p>144- {translate("Emergency")}</p>
              <p>118- {translate("Firemen")}</p>
            </div>
          )
        }
      ]
    },
    {
      country: translate("New Zealand"),
      details: [
        {
          title: translate("Visa"),
          data: (
            <div>
              <p>
                {translate(
                  "Here you can check out information about visas, including a list of the required documentation, work options and the details about application process."
                )}
              </p>
              <a href="https://www.immigration.govt.nz/new-zealand-visas/options/study/explore-student-visas">
                https://www.immigration.govt.nz/new-zealand-visas/options/study/explore-student-visas
              </a>
            </div>
          )
        },
        {
          title: translate("Accommodation"),
          data: (
            <div>
              <p>
                {translate(
                  "Use these websites to find your new home away from home in New Zealand."
                )}
              </p>
              <a href="https://www.student.com/nz">
                https://www.student.com/nz
              </a>
              <br />
              <a href="https://studentflat.co.nz">https://studentflat.co.nz</a>
              <br />
              <a href="https://www.otago.ac.nz/accommodation/index.html">
                https://www.otago.ac.nz/accommodation/index.html
              </a>
            </div>
          )
        },
        {
          title: translate("Buy and sell on-line"),
          data: (
            <div>
              <p>{translate("Buy and sell used goods on these websites.")}</p>
              <a href="https://nz.letgo.com/en ">https://nz.letgo.com/en</a>
              <br />
              <a href="https://www.trademe.co.nz">https://www.trademe.co.nz</a>
              <br />
              <a href="https://www.mightyape.co.nz/used">
                https://www.mightyape.co.nz/used
              </a>
            </div>
          )
        },
        {
          title: translate("Student work in New Zealand"),
          data: (
            <div>
              <p>
                {translate("Check out these websites to find a part-time job.")}
              </p>
              <a href="https://www.seek.co.nz/student-jobs ">
                https://www.seek.co.nz/student-jobs
              </a>
              <br />
              <a href="https://www.sjs.co.nz">https://www.sjs.co.nz</a>
              <br />
              <a href="https://nz.jobrapido.com">https://nz.jobrapido.com</a>
            </div>
          )
        },
        {
          title: translate("Car rent"),
          data: (
            <div>
              <p>
                {translate(
                  "Here you can find a car to rent while traveling in New Zealand"
                )}
              </p>
              <a href="https://www.gorentals.co.nz">
                https://www.gorentals.co.nz
              </a>
              <br />
              <a href="https://www.nzrentacar.co.nz">
                https://www.nzrentacar.co.nz
              </a>
            </div>
          )
        },
        {
          title: translate("Moving companies in New Zealand"),
          data: (
            <div>
              <p>
                {translate(
                  "These companies will help you in case you need to transport heavy luggage to New Zealand."
                )}
              </p>
              <a href="https://www.newzealandmovers.co.nz">
                https://www.newzealandmovers.co.nz
              </a>
              <br />
              <a href="https://www.conroy.co.nz/moving-around-new-zealand">
                https://www.conroy.co.nz/moving-around-new-zealand
              </a>
              <br />
              <a href="https://www.transworld.co.nz">
                https://www.transworld.co.nz
              </a>
            </div>
          )
        },
        {
          title: translate("Emergency number in New Zealand"),
          data: (
            <div>
              <p>{translate("in case of emergency call 111.")}</p>
            </div>
          )
        }
      ]
    }
  ];

  onClickListItem(country: string) {
    this.setState({
      selected: country
    });
  }

  getDetails() {
    const { selected } = this.state;

    let countryDetail = [];

    this.countryDetails.map(({ country, details }) => {
      if (selected === country) {
        countryDetail.push(...details);
      }
      return true;
    });
    return countryDetail;
  }

  render() {
    const { selected } = this.state;

    let details = this.getDetails();
    return (
      <div>
        <PageHeader
          title={translate("HELPING YOU GET READY TO GO")}
          breadcrumbs={[
            {
              title: translate("NEED HELP")
            }
          ]}
        />
        <div className="container need-help-container">
          <Row>
            <Col>
              <div className="country-container">
                <ul className="country-list">
                  {this.countryDetails.map(({ country }) => {
                    return (
                      <li
                        key={country}
                        className={`country-item ${
                          selected === country ? "active" : ""
                        }`}
                        onClick={() => this.onClickListItem(country)}
                      >{`${translate("Going to")} ${country}`}</li>
                    );
                  })}
                </ul>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12" md="8">
              <div className="card-container">
                {details
                  ? details.map(detail => {
                      return (
                        <HelpCard
                          key={detail.title}
                          title={detail.title}
                          data={detail.data}
                        />
                      );
                    })
                  : ""}
              </div>
            </Col>
            <Col sm="12" md="4">
              <div className="side-bar-container">
                <div className="side-title">
                  {translate("HELPING YOU GET READY TO GO")}
                </div>
                <div className="side-details">
                  <p>
                    {translate(
                      "Tambook isn't just about finding you a language course, it's about making sure your overseas adventure goes smoothly. The links below will help. You'll find useful and reliable info about Visas and immigration, options for where to stay, driver licensing - pretty much anything you need to know before you move."
                    )}
                  </p>
                </div>
                <Sidebar />
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <RelatedCourses />
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default NeedHelp;
