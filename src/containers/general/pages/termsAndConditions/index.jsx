// @flow
import React, { PureComponent, Fragment } from "react";

import RelatedCourses from "containers/course/components/RelatedCourses";

import PageHeader from "components/PageHeader";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Accordion from "shared/components/Accordion";
import Sidebar from "../components/sidebar";

import translate from "translations/translate";

import "./styles.scss";

type TermsAndConditionsProps = {};
class TermsAndConditions extends PureComponent<TermsAndConditionsProps> {
  getTermsAndConditions = () => {
    return [
      {
        title: translate("DEFINITIONS AND INTERPRETATION"),
        content: (
          <Fragment>
            <div>
              1.1&nbsp;
              {translate(
                "Unless expressly stated otherwise, the following capitalized expressions and their derivatives in this Agreement shall have the meanings assigned thereto herein below, it being understood that such expressions in the lower case shall have their common meaning as the context requires"
              )}
              :
              <ol type="a">
                <li>
                  <strong>{translate("Agent")}</strong>&nbsp;
                  {translate(
                    "means a person or entity approved by tambook who acts on your behalf in order to search and book Language Courses on the Platform and assist you with making payment for the chosen Language Course"
                  )}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("Content")}</strong>&nbsp;
                  {translate(
                    "includes all material and information displayed on the Platform, including information about Language Schools and Language School Terms, real time or other information, notices, data, text, materials, graphics, software, tools, results, names, logos and trade marks on the Platform"
                  )}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("includes")}</strong>&nbsp;
                  {translate("or similar terms or phrases imply no limitation")}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("Language Course")}</strong>&nbsp;
                  {translate(
                    "means any English or other language educational course provided by a relevant Language School and includes any other products or services provided to you in conjunction with that course (for example, accommodation or )"
                  )}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("Language School")}</strong>&nbsp;
                  {translate(
                    "means any independent third party English or other language school whose Language Courses Tambook offers on the Site"
                  )}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("Language School Terms")}</strong>
                  {translate(
                    "means the terms and conditions of enrolment of any Language School you enrol in through the Site"
                  )}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("Loss")}</strong>&nbsp;
                  {translate(
                    "means any loss or damage, including any direct, indirect, punitive or consequential loss or damages, or any loss of income, profits, goodwill, data, contracts, use of money, or loss or damages arising from or connected in any way to business interruption, and whether in tort (including negligence), contract or otherwise in connection with"
                  )}
                  :
                  <ol type="i">
                    <li>{translate("the Platform or the Content; or")}</li>
                    <br />
                    <li>
                      (ii)&nbsp;
                      {translate(
                        "the access or use, inability to access or use, or the results of use of, the Site or the Content, any websites linked to the Site or the material on such websites, including loss or damage due to viruses that may infect your computer equipment, software, data or other property on account of your access to, use or browsing of, the Site or your downloading of Content from the Site or content from any websites linked to the Site"
                      )}
                      ;
                    </li>
                    <br />
                  </ol>
                </li>
                <br />
                <li>
                  <strong>{translate("Platform")}</strong>&nbsp;
                  {translate(
                    "means an online platform Tambook accessible through following websites"
                  )}
                  &nbsp;<a href="www.tambook.com">www.tambook.com</a> and&nbsp;
                  <a href="www.tambook.co.nz">www.tambook.co.nz</a>&nbsp;
                  {translate(
                    "and/or mobile, tablet and other smart device applications"
                  )}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("Site")}</strong>&nbsp;
                  {translate(
                    "means the Tambook English language course site, which is accessed from"
                  )}
                  &nbsp;<a href="www.tambook.com">www.tambook.com</a> and&nbsp;
                  <a href="www.tambook.co.nz">www.tambook.co.nz</a>;
                </li>
                <br />
                <li>
                  <strong>{translate("Tambook")}</strong>&nbsp;
                  {translate(
                    "means Tambook Limited and includes any related company providing or hosting this Site"
                  )}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("Terms")}</strong>&nbsp;
                  {translate("means these terms and conditions")};
                </li>
                <br />
                <li>
                  <strong>{translate("Testimonial")}</strong>&nbsp;
                  {translate(
                    "means any testimonial or other expression of opinion, or recommendation, tip, announcement, report, or discussion, whether made in a Testimonial, or by any other blog or facility on the Site"
                  )}
                  ;
                </li>
                <br />
                <li>
                  <strong>{translate("you")}</strong>&nbsp;
                  {translate(
                    "means you, the person who has accessed the Platform, including your representatives and Agents acting on your behalf, and your has a corresponding meaning"
                  )}
                  .
                </li>
              </ol>
            </div>
          </Fragment>
        )
      },
      {
        title: translate("SCOPE AND NATURE OF TAMBOOK SERVICES"),
        content: (
          <Fragment>
            <p>
              2.1&nbsp;
              {translate(
                "Tambook is an online marketplace operating as an intermediary between Language Schools and users in order to provide our Platforms users with the possibility to search and book Language Courses and other related services online"
              )}
              .
            </p>
            <br />
            <p>
              2.2&nbsp;
              {translate(
                "You and (if applicable) an Agent acting on your behalf acknowledge and agree that Tambook being the provider of the Platform does not own, create, sell, resell, provide, control, manage, offer, deliver, or supply any Language Courses and/or services or function as a Language School and that all such Language Courses and other related products and/or services are provided by relevant Language School being an independent third party contractor. Therefore, by using the Platform, booking, paying for a relevant Language Course you enter into a direct (legally binding) contractual relationship with the Language School with which you make a reservation or purchase a Language Course (as applicable)"
              )}
              .
            </p>
            <br />
            <p>
              2.3&nbsp;
              {translate(
                "Tambook acts solely as an intermediary between you and the Language School, transmitting the relevant details of your reservation to the relevant Language School(s) and sending you or your Agent acting on your behalf a confirmation email for and on behalf of the Language School"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("AGENTS"),
        content: (
          <Fragment>
            <div>
              3.1&nbsp;
              {translate(
                "Tambook may in its sole discretion appoint Agents to assist with searching and booking of Language Courses on the Platform and making payment for the chosen Language Course. In order to be appointed as Agent a person or entity must"
              )}
              :
              <ol type="a">
                <li>
                  {translate(
                    "submit an online application to become Agent and accept and agree to be bound by the Terms"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "provide Tambook with copies of documents evidencing that it is duly registered and licensed (if required) to perform functions of an Agent in the relevant jurisdiction(s)"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <div>
              3.2&nbsp;
              {translate(
                "Tambook may in its sole discretion suspend or revoke the appointment of any Agent, who"
              )}
              :
              <ol type="a">
                <li>
                  {translate("wilfully or negligently breaches the Terms")};
                </li>
                <br />
                <li>
                  {translate(
                    "does any act or omission which is unlawful or may otherwise put Tambook's reputation under threat"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <p>
              3.3&nbsp;
              {translate(
                "An Agent is entitled to receive remuneration for its services calculated as a discount on the relevant Language Course price indicated in the Agent's section of the Platform. An Agent shall not charge any other fees from the students or alter the Language Course prices quoted to students compared to the prices available to direct users of the Platform. No remuneration shall be payable in respect of any Language Course for which a refund or charge back is issued. An agent is not entitled to any reimbursement by Tambook of its costs or expenses related the Agent's business activities"
              )}
              .
            </p>
            <br />
            <div>
              3.4 {translate("The Platform enables Agents to")}:
              <ol type="a">
                <li>
                  {translate(
                    "access the Platform in order to search and book Language Courses on behalf of the potential students"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "market the Platform to its customers – potential students"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "make bookings of Language Courses on behalf of the those customers"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <div>
              3.5&nbsp;
              {translate(
                "Prior to booking the Language Course on behalf of its customer, Agent shall"
              )}
              :
              <ol type="a">
                <li>
                  {translate(
                    "communicate to its customer and obtain evidence that the Customer has read and accepted the Terms"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "make clear to its customers provision specified in clause 2.2 above and shall not display or make any statement which expressly or by implication contradicts this proposition"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "make clear to its customers regarding any fees charged by the Agent above the Language Course price published on the Platform"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "communicate fully and accurately to its customer the description of the chosen Language Course and Language School as made available on the Platform"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "communicate fully and accurately to its customer any limitations or restrictions relating to the relevant Language Course including, without limitation, whether cancellations are permitted and the consequences of same"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <p>
              3.6&nbsp;
              {translate(
                "Agent undertakes not to make any verbal or written assurances to its customer which are additional to or contradict the Terms, including without limitation by promising that any special requests will be met. Agent shall be solely responsible and liable in respect of any representations, or special request confirmed or made to its customer without Tambook's prior written authorization and approval"
              )}
              .
            </p>
            <br />
            <p>
              3.7&nbsp;
              {translate(
                "Agent shall provide its customer with information specified in clause 4.5 below (payment confirmation, booking confirmation) within 24 hours from its receipt from Tambook"
              )}
              .
            </p>
            <br />
            <p>
              3.8&nbsp;
              {translate(
                "Agent shall comply with all applicable laws in force from time to time relevant to the performance of its obligations under the Terms including, without limitation, any legislation relating to the licensing of agency services"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("APPLYING FOR LANGUAGE COURSES"),
        content: (
          <Fragment>
            <div>
              4.1&nbsp;
              {translate(
                "Prior to using the Platform, you and (if applicable) your Agent acting on your behalf warrant that"
              )}
              :
              <ol type="a">
                <li>
                  {translate(
                    "you have the legal capacity and power to agree to be bound by these Terms and perform your obligations"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "information and other works posted on the Platform by you do not breach the intellectual property rights of any third party"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "all data delivered to Tambook will be free of infection or viruses"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "you will not use or make available any Content in any way that is offensive, unlawful, harassing, or threatening, or is otherwise, in Tambook's sole opinion, objectionable or damaging to Tambook, users of the Platform or anyone else"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <div>
              4.2 {translate("You must not")}:
              <ol type="a">
                <li>
                  {translate(
                    "use any feature of the Platform to send any unsolicited commercial electronic messages to any person"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "make any public statement about, or release any other form of publicity relating to, Tambook without Tambook's prior written approval"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <p>
              4.3&nbsp;
              {translate(
                "Language Courses on the Platform may not always be available even if they are advertised on the Platform. The availability to you of a Language Course is subject to a relevant Language School acceptance into that course"
              )}
              .
            </p>
            <br />
            <p>
              4.4&nbsp;
              {translate(
                "When you apply for a Language Course(s) you or your Agent acting on your behalf warrant to Tambook and the relevant Language School that all information provided by you is true, accurate, complete and up-to-date. Due to difficulty of user verification, Tambook does not assume any responsibility for the confirmation of your identity. Your failure to maintain true, accurate, complete and up-to-date information, including having an invalid or expired payment method on file, may result in your inability to access to and use of a Language Course"
              )}
              .
            </p>
            <br />
            <div>
              4.5&nbsp;
              {translate(
                "After you submit an application for a Language Course, you or your Agent acting on your behalf shall pay a 100% advance payment of the Language Course price via Stripe payment platform integrated with the Platform. After that you or your Agent acting on your behalf will receive on e-mail the following"
              )}
              :
              <ol type="a">
                <li>
                  {translate("a receipt from Stripe confirming your payment")};
                </li>
                <br />
                <li>
                  {translate(
                    "an e-mail from Tambook confirming your payment, booking confirmation and guidance how to log-in in your personal account at the Platform and manage your booking"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "an e-mail from the Language School together with documents and Language Course booking confirmation (i.e. official acceptance to the Language Course)"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <p>
              4.6&nbsp;
              {translate(
                "Tambook will submit an application for a Language Course you make on this Platform to the relevant Language School on your behalf. Submission of an application does not guarantee acceptance into any Language Course. You should not make any travel or other plans until you have received written confirmation from the Language School of your acceptance into a Language Course by e-mail. For the avoidance of doubt, an e-mail from Tambook with booking confirmation shall not be deemed as final confirmation and your acceptance to the Language Course"
              )}
              .
            </p>
            <br />
            <p>
              4.7&nbsp;
              {translate(
                "It is your responsibility to arrange all applicable travel permits or visas and to have a valid passport. Tambook is not responsible for determining whether or not you need a travel permit or visa (as applicable) to a country where a Language Course takes place. Immigration and visa regulations change from time to time therefore we strongly advise you to contact the relevant Embassy/Consulate in your country for up to date information"
              )}
              .
            </p>
            <br />
            <p>
              4.8&nbsp;
              {translate(
                "If you indicate that you require accommodation, insurance or any other related product or service on your application form, your request will be passed on to the relevant Language School via your application, and the Language School (or its agents) will be responsible for providing those products and services to you (subject to their acceptance of your application)"
              )}
              .
            </p>
            <br />
            <div>
              4.9&nbsp;
              {translate(
                "Any reference on the Platform to a 'best price' (or similar) guarantee is subject to the following conditions"
              )}
              :
              <ol type="a">
                <li>
                  {translate(
                    "Tambook will match any price offered by a competing reseller of a language course that is the same as a Language Course ('Competing Course')"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "The price for the Competing Course must be current at the relevant time"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "The Competing Course must be available in a city/town/locality where the same Language Course takes place"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "The Competing Course must contain the same elements as a Language Course being offered by Tambook at the relevant time (including, but not limited to, period of a Language Course, teachers, academic hours quantity, etc.). The 'best price guarantee' does not apply to the price of individual elements of a course, but to the price for the course as a whole (i.e. you cannot 'cherry-pick' elements of a Competing Course)"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "The Competing Course shall not be a part of loyalty, rewards programme or a special promotion or deal"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "You must have notified Tambook in writing of the existence and pricing of a Competing Course prior to you completing an application for a Language Course"
                  )}
                  ;
                </li>
                <br />
                <li>
                  (g)&nbsp;
                  {translate(
                    "Any refund may, at Tambook’s discretion, be reduced by the amount of any reduction on Language Course fees given for a 'best price' guarantee"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "You are only entitled to claim one 'best price' guarantee from Tambook"
                  )}
                  .
                </li>
              </ol>
            </div>
          </Fragment>
        )
      },
      {
        title: translate("PAYMENT"),
        content: (
          <Fragment>
            <p>
              5.1&nbsp;
              {translate(
                "In order to finish the Language Course booking on the Platform, you or your Agent acting on your behalf shall make a 100% advance payment of the Language Course price via Stripe payment platform integrated with the Platform"
              )}
              .
            </p>
            <br />
            <p>
              5.2&nbsp;
              {translate(
                "Where you are provided with an option as to the currency in which your Language Course fees are charged, the Platform will show those fees as being payable in the currency you have selected ('Selected Currency'). You or your Agent acting on your behalf must ensure that payment is made to, and received by, Tambook, in full, in the Selected Currency. You, not Tambook, must bear any currency exchange rate risk"
              )}
              .
            </p>
            <br />
            <p>
              5.3&nbsp;
              {translate(
                "Where you or your Agent acting on your behalf use any online payment facility (for example, Stripe) to pay Tambook, Tambook may charge you a fee if Tambook is charged a fee by the payment facility provider"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("REFUNDS"),
        content: (
          <Fragment>
            <p>
              6.1&nbsp;
              {translate(
                "If you cannot attend, or wish to withdraw from, any Language Course, you may be entitled to a refund in cases specified below. If you wish to request a refund for any reason, you must notify Tambook in writing. Such notification shall be forwarded by Tambook to the Language School for approval"
              )}
              .
            </p>
            <br />
            <p>
              6.2&nbsp;
              {translate(
                "You shall be entitled to refund of 100% of the Language Course price (net of any applicable fees (including transaction fees and school registration fee) and currency exchange rates) if cancellation of a Language Course reservation takes place at least Fourteen (14) calendar days before a Language Course commencement date"
              )}
              .
            </p>
            <br />
            <p>
              6.3&nbsp;
              {translate(
                "Refund process may take up to Three (3) weeks due to administrative reasons (communication between Tambook and the Language School, approval of such refund, etc.). In case of refund, the Student may be required to submit relevant document(s) requested by the Language School (e.g. a signed approval for release of funds from the trustee where the Language Course price is accumulated under the laws of the New Zealand). Tambook shall be entitled to deduct, from any refund payable to you, Tambook’s reasonable administrative and other costs of processing the refund, including any payment Tambook is required to make to the relevant Language School as a result of you obtaining a refund"
              )}
              .
            </p>
            <br />
            <p>
              6.4&nbsp;
              {translate(
                "If you decide to terminate your Language Course at any point between Thirteen (13) and One (1) calendar days prior to a Language Course commencement date and after its commencement date, you shall settle all and any disputes and issues related to refund with the Language School directly subject to relevant Language School's refund rules. Tambook does not accept any liability or responsibility for the consequences of cancellation of the Language Course whether such cancellation is made by you or the Language School within the said period"
              )}
              .
            </p>
            <br />
            <p>
              6.5&nbsp;
              {translate(
                "Tambook’s decision as to your entitlement (or otherwise) to a refund shall be final and binding on you"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("CONTENT"),
        content: (
          <Fragment>
            <p>
              7.1&nbsp;
              {translate(
                "All Content is owned by Tambook, the relevant Language School or Tambook’s licensors"
              )}
              .
            </p>
            <br />
            <p>
              7.2&nbsp;
              {translate(
                "If the owner of any Content is not Tambook, your rights in respect of that Content will be as defined by the copyright owner"
              )}
              .
            </p>
            <br />
            <p>
              7.3&nbsp;
              {translate(
                "Tambook grants you a non-exclusive licence to view and print Content and make copies of the Content provided that any copies of Content that you make must not be used for any commercial purpose and must retain all applicable copyright and other notices on the Platform. Except as provided above, you may not use, copy, change, transfer to a third party or do anything else with, any Content. You must not use any form of data gathering or extraction tool on this Platform for any purpose"
              )}
              .
            </p>
            <br />
            <p>
              7.4&nbsp;
              {translate(
                "Tambook uses Content provided to it by Language Schools. Content on the Platform and any site operated by a Language School may not be consistent. If you observe any inconsistency between Content on the Platform and content on a Language School site, you should advise Tambook by in written specifying such inconsistency"
              )}
              .
            </p>
            <br />
            <p>
              7.5&nbsp;
              {translate(
                "Where the Platform contains any Content in a language other than English, the English version of that Content shall prevail in the case of any inconsistency"
              )}
              .
            </p>
            <br />
          </Fragment>
        )
      },
      {
        title: translate("DISCLAIMER"),
        content: (
          <Fragment>
            <p>
              8.1&nbsp;
              {translate(
                "You acknowledge that each Language Course is provided to you by the relevant Language School and Tambook shall not be liable to provide any Language Course to you"
              )}
              .
            </p>
            <br />
            <p>
              8.2&nbsp;
              {translate(
                "Tambook is not responsible (and disclaims any liability) for the use, validity, quality, suitability, fitness and due disclosure of the Language Courses and makes no representations, warranties or conditions of any kind in this respect, whether implied, statutory or otherwise, including any implied warranties of merchantability, title, non-infringement or fitness for a particular purpose. You acknowledge and agree that the relevant Language School is solely responsible and assumes all responsibility and liability in respect of the Language Course (including any warranties and representations made by the Language School). Tambook is not a (re)seller of the Trip. Complaints or claims in respect of the Language Course are to be dealt with by the Language School. Tambook is not responsible for and disclaims any liability in respect of such complaints, claims and (product) liabilities"
              )}
              .
            </p>
            <br />
            <p>
              8.3&nbsp;
              {translate(
                "You acknowledge that Content may not be accurate or complete. Tambook will use reasonable endeavour, but shall not be liable, to rectify any material error in any of the Content as soon as reasonably practicable after being notified of that error. You are solely responsible for any actions you take in reliance on the Content. If you want to rely on any Content, you should contact Tambook first"
              )}
              .
            </p>
            <br />
            <p>
              8.4&nbsp;
              {translate(
                "Tambook gives no warranty as to the truth or accuracy of, and accepts no responsibility or liability for any errors in, any advertisement, discount, terms of trade or other information posted by any Language School or other supplier on, or provided to you from or via, the Platform"
              )}
              .
            </p>
            <br />
            <p>
              8.5&nbsp;
              {translate(
                "Tambook does not endorse or recommend any of the Language Schools or any other suppliers who appear on the Platform and you acknowledge that Tambook does not vet, nor is it responsible for vetting any representation made by any person on the Platform"
              )}
              .
            </p>
            <br />
            <div>
              8.6&nbsp;
              {translate(
                "Tambook and its directors, shareholders, officers, agents, employees or contractors, to the maximum extent permitted by law"
              )}
              :
              <ol type="a">
                <li>
                  {translate(
                    "exclude all representations, warranties, conditions and other terms (including any conditions implied by law of satisfactory quality, fitness for purpose and the use of reasonable care and skill) which but for these Terms might have effect in relation to the Platform and the Content (including any Testimonial); and"
                  )}
                </li>
                <br />
                <li>
                  {translate(
                    "do not warrant that any of the functions contained in the Platform or your access to the Platform will be uninterrupted or error-free, and Tambook will have no liability arising from your access (or lack of access) to, or use of, the Platform or the Content, any Language Course or any services provided to you by Tambook"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <p>
              8.7&nbsp;
              {translate(
                "Tambook may revise these Terms at any time without prior notice to you. If we make changes to these Terms, we will post the revised Terms on the Platform and update the 'Last Updated' date at the top of these Terms. It is your responsibility to ensure you review and understand the Terms applicable to the use of the Platform every time you access the Platform. If you do not wish to accept any new terms and conditions, you should not continue to use the Platform"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("LIABILITY AND INDEMNITY"),
        content: (
          <Fragment>
            <p>
              9.1&nbsp;
              {translate(
                "Tambook, its officers, directors, employees, shareholders, contractors and agents of any of them (whether or not involved in creating, producing, maintaining or delivering the Platform), exclude all liability and responsibility for any Loss that may result to you or any third party"
              )}
              .
            </p>
            <br />
            <p>
              9.2&nbsp;
              {translate(
                "Nothing in these Terms shall exclude or limit Tambook’s liability to you for fraud by Tambook or any person for whom Tambook is legally responsible, or any liability which cannot be lawfully excluded or limited"
              )}
              .
            </p>
            <br />
            <p>
              9.3&nbsp;
              {translate(
                "To the extent Tambook becomes liable to you for any reason (including under the law of contract, tort (including negligence) or otherwise) Tambook’s total liability to you shall not exceed an amount equivalent to any fees paid by you to Tambook, or NZD$100.00, whichever is the lesser"
              )}
              .
            </p>
            <div>
              9.4&nbsp;
              {translate(
                "You agree to fully indemnify Tambook, and to defend and hold Tambook, its officers, directors, employees, shareholders and agents, harmless from and against all claims (including claims made by third parties against Tambook), liability, damages, losses, costs (including legal fees on a solicitor-client basis) arising out of"
              )}
              :
              <ol type="a">
                <li>{translate("any breach of these Terms by you")};</li>
                <br />
                <li>
                  {translate(
                    "your access to, or use of, the Platform or the Content, or the access to, or use of, the Platform or the Content by any other person for whom you are legally responsible"
                  )}
                  ;
                </li>
                <br />
                <li>{translate("any Refund paid or payable to you")}.</li>
              </ol>
            </div>
            <br />
            <p>
              9.5&nbsp;
              {translate(
                "You warrant and represent that, if you use the Platform for the purposes of a business then, to the maximum extent permitted by law, any statutory consumer guarantees or legislation intended to protect non-business consumers in any jurisdiction does not apply to your use of the Platform, to the provision of any Language Course by a Language School, or to any services provided to you by Tambook"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate(
          "TESTIMONIALS AND INFORMATION POSTED TO PLATFORM, ETC"
        ),
        content: (
          <Fragment>
            <p>
              10.1&nbsp;
              {translate(
                "The Platform may provide facilities for you and other students to post Testimonials. Any view expressed in a Testimonial is not necessarily the view of Tambook or any Language School. Without limiting anything in these Terms, nothing contained on the Platform or in any Testimonial shall be construed as the provision of advice to you or any other person and information contained in them should not be relied on. If you require advice, you should contact Tambook"
              )}
              .
            </p>
            <br />
            <p>
              10.2&nbsp;
              {translate(
                "Tambook is not responsible for and does not endorse the content of any message posted in a Testimonial. Tambook reserves the right to delete, move or edit any message posted in a Testimonial at its sole discretion, without giving notice to you or any other person"
              )}
              .
            </p>
            <br />
            <p>
              10.3&nbsp;
              {translate(
                "If you submit any material, data or information (Material) to the Platform or to Tambook (whether through a Testimonial or otherwise) you agree that Tambook may use that Material for any purpose without out any further consent from you. You agree that Tambook may use your name, voice, image, likeness, user information and any representations made by you, in any media (online or otherwise), world-wide, for the purposes of advertising, promoting, reporting and disseminating information about Tambook, its business or the Platform"
              )}
              .
            </p>
            <br />
            <div>
              10.4&nbsp;
              {translate(
                "In using the Platform, including posting, commenting on, or contributing to, any Testimonial, you must not"
              )}
              :
              <ol type="a">
                <li>
                  {translate(
                    "do anything that infringes or violates any third party rights, including any other person’s intellectual property rights or proprietary information"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "impersonate any person or otherwise misrepresent your affiliation with any person"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate("do anything that, in Tambook’s sole discretion")}:
                  <ol type="i">
                    <li>
                      {translate(
                        "violates any law or regulation, or any contractual or fiduciary relationship"
                      )}
                      ;
                    </li>
                    <br />
                    <li>
                      {translate(
                        "is harmful, offensive, objectionable, unlawful, harassing or threatening, or that constitutes 'stalking' or 'trolling'"
                      )}
                      ;
                    </li>
                    <br />
                    <li>
                      {translate(
                        "constitutes unsolicited or unauthorized advertising, or promotional material, that are in the nature of 'junk mail' ,'spam', 'chain letters', 'pyramid schemes' or any other form of solicitation"
                      )}
                      ;
                    </li>
                    <br />
                    <li>
                      {translate(
                        "interferes with or disrupts the Platform or Tambook’s servers or networks"
                      )}
                      ;
                    </li>
                  </ol>
                </li>
                <br />
                <li>
                  {translate(
                    "disobey or attempt to disobey any requirement, procedure or policy of Tambook"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "attempt to gain access to any other Platform user's account or password"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate("charge any person for use of the Platform")};
                </li>
                <br />
                <li>
                  {translate(
                    "modify, adapt, reverse engineer, de-compile or attempt to discover the source code of the Platform or any of its algorithms that are utilized on it"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <p>
              10.5&nbsp;
              {translate(
                "By registering on the Platform, you consent to Tambook using your personal information to promote advertising to you generally, or for specific products and/or services, when you visit the Site, via email or by other means"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("LINKS"),
        content: (
          <Fragment>
            <p>
              11.1&nbsp;
              {translate(
                "The Platform may from time to time contain links to Language School and other third party web sites (Other Websites). Other Websites may not be under the control of Tambook and Tambook takes no responsibility for the content of the Other Websites. The inclusion on the Platform of a link to any Other Website does not imply any endorsement by Tambook of that Other Website"
              )}
              .
            </p>
            <br />
            <p>
              11.2&nbsp;
              {translate(
                "Tambook prohibits caching, unauthorized hypertext links (including deep linking) to the Platform and the framing of any content without Tambook’s prior written consent. Tambook reserves the right to disable any unauthorized frames or links to or from the Platform"
              )}
              .
            </p>
            <br />
          </Fragment>
        )
      },
      {
        title: translate("COOKIES"),
        content: (
          <Fragment>
            <p>
              12.1&nbsp;
              {translate(
                "Tambook may use cookies to track Platform usage patterns or display content that is more relevant to you based on non-identifying information Tambook may collect when you visit the Platform"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("VIRUSES, ETC."),
        content: (
          <Fragment>
            <p>
              13.1&nbsp;
              {translate(
                "In accessing or using the Platform, you warrant to Tambook that your access to, or use of, the Platform or Content will not introduce anything infectious, including any virus, trojan horse or computer programming code (Virus), which would have the effect of disrupting, impairing, disabling or otherwise adversely affecting or shutting down the Platform, or denying access to any other user of the Platform or Content"
              )}
              .
            </p>
            <br />
            <p>
              13.2&nbsp;
              {translate(
                "Tambook makes no warranty that files available for downloading through the Platform or delivered via email through the Platform will be free of Viruses that manifest contaminating or destructive properties. You are responsible for implementing sufficient security procedures and checkpoints to prevent damage to your computer system, or damage to or loss of data"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("BREACH OF TERMS"),
        content: (
          <Fragment>
            <div>
              14.1&nbsp;
              {translate(
                "Tambook may, without prior notice to you, block your access to the Platform, including if"
              )}
              :
              <ol type="a">
                <li>
                  {translate(
                    "you or any person gaining access to the Platform through you, or on your behalf, breaches any of these Terms (or Tambook reasonably believes that to be the case); or"
                  )}
                </li>
                <br />
                <li>
                  {translate(
                    "Tambook otherwise determines that you are accessing or using the Platform or any Content in an illegal, inappropriate or unacceptable manner"
                  )}
                  .
                </li>
              </ol>
              <ul type="none">
                <li>
                  {translate(
                    "If we block your access to the Platform for any reason, you will not be entitled to a refund of any fees paid to us"
                  )}
                  .
                </li>
              </ul>
            </div>
            <br />
            <p>
              14.2&nbsp;
              {translate(
                "Tambook may pursue all other remedies available at law for breach of the Terms"
              )}
              .
            </p>
            <br />
            <p>
              14.3&nbsp;
              {translate(
                "If you become aware of any breach of these Terms, or any misuse of the Platform or the Content by any person you must contact and notify Tambook immediately in writing"
              )}
              .
            </p>
          </Fragment>
        )
      },
      {
        title: translate("PERSONAL DATA"),
        content: (
          <Fragment>
            <p>
              15.1&nbsp;
              {translate(
                "We will use all reasonable endeavour to keep your personal information confidential except to the extent disclosure is required by law or necessary to enable us to enrol you in Language Courses or do anything else contemplated by these Terms. We will comply with our obligations under the Privacy Act 1993 and/or Regulation (EU) 2016/679 (General Data Protection Regulation)"
              )}
              .
            </p>
            <br />
            <p>
              15.2&nbsp;
              {translate(
                "In order to apply for the Language Course, you shall provide Tambook with your personal data. That said, you give us your consent to process your personal data which is necessary to make it possible for you to search and book Language Courses and other related services online"
              )}
              .
            </p>
            <br />
            <div>
              15.3&nbsp;
              {translate(
                "Tambook controls the processing of your personal data as contemplated by this clause 14 and is deemed as a controller of your personal data. Our contact details are as follows"
              )}
              :
              <ol type="a">
                <li>office@tambook.com;</li>
                <br />
                <li>tel.: +64 22 699 1235;</li>
                <br />
                <li>
                  {translate("address: 3 Glenside Cres, Auckland, New Zealand")}
                  .
                </li>
              </ol>
            </div>
            <br />
            <p>
              15.4&nbsp;
              {translate(
                "Tambook collects and processes personal data you provide Tambook with, namely, your full name, date of birth, nationality, residential address, phone number, e-mail. Tambook may keep your bank card details in secure manner only if you specifically authorize it by checking the relevant box on the Platform"
              )}
              .
            </p>
            <br />
            <p>
              15.5&nbsp;
              {translate(
                "Tambook also may collect and process certain information automatically, namely, IP address, the date and time you accessed our services, the hardware, software or internet browser you use and information about your computer’s operating system, like application versions and your language settings"
              )}
              .
            </p>
            <br />
            <div>
              15.6&nbsp;
              {translate(
                "Tambook collects and processes information about you mentioned above for the following purposes"
              )}
              :
              <ol type="a">
                <li>
                  {translate(
                    "provision of services related to search and booking of Language Courses and other related services"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "marketing activities which include newsletters and advertisement related to Language Courses and Language Schools"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "communication with you, including, but not limited to, confirmation of your booking, reminders on unfinished reservations, etc"
                  )}
                  .
                </li>
              </ol>
            </div>
            <br />
            <div>
              15.7&nbsp;
              {translate(
                "You have the following rights in relation to personal data and information you provide to us"
              )}
              :
              <ol type="a">
                <li>
                  {
                    "right of access to your personal data (i.e. copy of processed personal data)"
                  }
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "right to rectification (i.e. you may ask us to correct any of personal data we process about you)"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "right to erasure (right to be forgotten) which means that in certain cases you may ask us to delete (erase) all personal data we process about you"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "right to restriction or object of processing which means that you may ask us to block or restrict processing of personal data we process about you"
                  )}
                  ;
                </li>
                <br />
                <li>
                  {translate(
                    "right to data portability which means that you may ask us to transfer you personal data to a third party"
                  )}
                  .
                </li>
              </ol>
              <ul type="none">
                <li>
                  {translate(
                    "Any request you may have in relation to the above shall be sent to office@tambook.com"
                  )}
                  .
                </li>
              </ul>
            </div>
            <br />
            <p>
              15.8&nbsp;
              {translate(
                "We will process your personal data until either you have an active account at the Platform or we deem it necessary in order to be ready to provide our services to you (including maintaining the online user account (if created)), to comply with applicable laws, resolve disputes with any parties and otherwise as necessary to allow us to conduct our business, including to detect and prevent fraud or other illegal activities"
              )}
              .
            </p>
            <br />
            <div>
              15.9&nbsp;
              {translate(
                "In the course of provision our services to you, we may share your personal data to the following parties"
              )}
              :
              <ol type="a">
                <li>{translate("Language Schools")};</li>
                <li>
                  {translate(
                    "Third-party service providers (payment services, marketing services, accommodation services, insurance services) subject to obligation of such third-parties to adequately safeguard your personal data"
                  )}
                  ;
                </li>
                <li>{translate("Competent authorities")};</li>
                <li>{translate("Tambook affiliates")}.</li>
              </ol>
            </div>
          </Fragment>
        )
      },
      {
        title: translate("OTHER"),
        content: (
          <Fragment>
            <p>
              16.1&nbsp;
              {translate(
                "Tambook may alter the functionality and/or appearance of the Site, including the way advertisements are represented on the Site or mobile communication devices"
              )}
              .
            </p>
            <br />
            <p>
              16.2&nbsp;
              {translate(
                "You may not assign or transfer any rights and obligations pursuant to these Terms to any other person or entity without Tambook's prior written approval (which will not be unreasonably withheld). If you are a company, any change in your effective control shall be deemed an assignment for the purpose of this clause"
              )}
              .
            </p>
            <br />
            <div>
              16.3&nbsp;
              {translate(
                "All data uploaded to the Platform by you (Data) shall remain, as between you and Tambook, your property. You grant Tambook a royalty-free, worldwide, perpetual, non-exclusive, irrevocable licence to use any Data to"
              )}
              :
              <ol type="a">
                <li>
                  {translate("manage its internal reporting requirements")};
                </li>
                <br />
                <li>
                  {translate(
                    "collate statistical information about use of the Platform"
                  )}
                  ;
                </li>
                <br />
                <li>{translate("analyse user behavior on the Platform")};</li>
                <br />
                <li>
                  {translate("generally improve the Platform user experience")}.
                </li>
              </ol>
            </div>
            <br />
            <p>
              16.4&nbsp;
              {translate(
                "In the event of a dispute about the contents or interpretation of these Terms or inconsistency or discrepancy between the English version and any other language version of these Terms, the English language version to the extent permitted by laws of New Zealand shall apply, prevail and be conclusive"
              )}
            </p>
            <br />
            <p>
              16.5&nbsp;
              {translate(
                "If any provision of these Terms is or becomes invalid, unenforceable or non-binding, you shall remain bound by all other provisions hereof. In such event, such invalid provision shall nonetheless be enforced to the fullest extent permitted by laws of New Zealand, and you will at least agree to accept a similar effect as the invalid, unenforceable or non-binding provision, given the contents and purpose of these Terms"
              )}
              .
            </p>
            <br />
            <p>
              16.6&nbsp;
              {translate(
                "If you dispute any of these Terms, you must notify Tambook immediately in writing. You agree that you will co-operate in good faith to resolve the dispute with Tambook. If we cannot resolve the dispute between us, either of us may refer the dispute to the competent court. All matters relating to these Terms shall be governed by, construed and enforced in accordance with the laws of New Zealand and be subject to the exclusive jurisdiction of the courts of New Zealand"
              )}
              .
            </p>
          </Fragment>
        )
      }
    ];
  };
  render() {
    const items = this.getTermsAndConditions();

    return (
      <div>
        <PageHeader
          title={translate("TERMS & CONDITIONS")}
          breadcrumbs={[
            {
              title: translate("TERMS & CONDITIONS")
            }
          ]}
        />
        <div className="terms-conditions-container">
          <Row>
            <Col sm="12" md="8">
              <h2>{translate("TERMS & CONDITIONS")}</h2>
              <p>
                {translate(
                  "If you are having difficulties understanding the information provided on this site, Tambook strongly advises you to seek assistance of your language interpreter particularly during your booking process or go to one of authorized agents in your country"
                )}
              </p>
              <Accordion items={items} />
            </Col>
            <Col sm="12" md="4">
              <div className="terms-conditions-sidebar">
                <Sidebar />
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12">
              <RelatedCourses />
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default TermsAndConditions;
