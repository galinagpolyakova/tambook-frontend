// import lib
import { lazy } from "react";

export default [
  {
    path: "/",
    exact: true,
    className: "home-page",
    component: lazy(() => import("./pages/home"))
  },
  {
    path: "/about-us",
    exact: true,
    className: "about-us-page",
    component: lazy(() => import("./pages/aboutUs"))
  },
  {
    path: "/faq",
    exact: true,
    className: "faq-page",
    component: lazy(() => import("./pages/faq"))
  },
  {
    path: "/under-construction",
    exact: true,
    component: lazy(() => import("./pages/helpers/UnderConstruction"))
  },
  {
    path: "/contact-us",
    exact: true,
    component: lazy(() => import("./pages/contactUs"))
  },
  {
    path: "/terms-and-conditions",
    exact: true,
    component: lazy(() => import("./pages/termsAndConditions"))
  },
  {
    path: "/need-help",
    exact: true,
    component: lazy(() => import("./pages/needHelp"))
  },
  {
    path: "/check-visa-requirements",
    exact: true,
    component: lazy(() => import("./pages/checkVisaRequirements"))
  },
  {
    path: "/404",
    exact: true,
    component: lazy(() => import("./pages/helpers/404"))
  }
];
