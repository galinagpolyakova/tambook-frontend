// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";
import type { ApiFaqListType, ConsolidatedFaqListType } from "./types";

export class GeneralService {
  api: ApiServiceInterface;

  endpoint: string = "/schools/contact";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  sendContactMessage(payload: Object) {
    return this.api.post(this.endpoint, payload);
  }

  getAllFaq() {
    return this.api.get("/faq/all").then(response => {
      return this._normalizeApiFaq(response.data);
    });
  }

  _normalizeApiFaq(apiFaq: ApiFaqListType): ConsolidatedFaqListType {
    const data = apiFaq.map(faq => this._transformFaqData(faq));
    return data;
  }

  _transformFaqData({ category, list }: any): any {
    const selectedFaq = [];
    list.map(question => {
      selectedFaq.push({
        question: question.question,
        answer: question.answer
      });
      return true;
    });
    return {
      category,
      questions: selectedFaq
    };
  }
}
