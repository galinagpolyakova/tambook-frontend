// @flow
import { type Action } from "shared/types/ReducerAction";

import {
  SEND_CONTACT_MESSAGE_INIT,
  SEND_CONTACT_MESSAGE_SUCCESS,
  SEND_CONTACT_MESSAGE_FAILURE,
  GET_ALL_FAQ_INIT,
  GET_ALL_FAQ_SUCCESS,
  GET_ALL_FAQ_FAILURE
} from "./actionTypes";

export type GeneralState = {
  isLoading: boolean,
  isContactMessageSendSuccess: boolean
};

const initialState: GeneralState = {
  isLoading: false,
  isContactMessageSendSuccess: false,
  isGetFaqLoading: false,
  isFaqLoaded: false,
  faq: []
};

function sendContactMessageInit(state) {
  return {
    ...state,
    isLoading: true,
    isContactMessageSendSuccess: false
  };
}

function sendContactMessageSuccess(state) {
  return {
    ...state,
    isLoading: false,
    isContactMessageSendSuccess: true
  };
}

function sendContactMessageFailure(state) {
  return {
    ...state,
    isLoading: false,
    isContactMessageSendSuccess: false
  };
}

function getAllFaqInit(state) {
  return {
    ...state,
    isGetFaqLoading: true
  };
}

function getAllFaqSuccess(state, payload) {
  return {
    ...state,
    isGetFaqLoading: false,
    isFaqLoaded: true,
    faq: payload
  };
}

function getAllFaqFailure(state) {
  return {
    ...state,
    isGetFaqLoading: false
  };
}

const reducer = (
  state: GeneralState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case SEND_CONTACT_MESSAGE_INIT:
      return sendContactMessageInit(state);
    case SEND_CONTACT_MESSAGE_SUCCESS:
      return sendContactMessageSuccess(state);
    case SEND_CONTACT_MESSAGE_FAILURE:
      return sendContactMessageFailure(state);
    case GET_ALL_FAQ_INIT:
      return getAllFaqInit(state);
    case GET_ALL_FAQ_SUCCESS:
      return getAllFaqSuccess(state, payload);
    case GET_ALL_FAQ_FAILURE:
      return getAllFaqFailure(state);
    default:
      return state;
  }
};

export default reducer;
