// @flow
import React, { Component } from "react";
import { Storage } from "aws-amplify";
import InputMask from "react-input-mask";
import PhoneInput from "react-phone-input-2";
import classnames from "classnames";
import "react-phone-input-2/dist/style.css";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Button from "shared/components/Button";
import Input from "shared/components/Input";
import Select from "shared/components/Select";

import countries from "shared/data/countries";
import locales from "shared/data/locales";
import translate from "translations/translate";
import { isEmail } from "shared/kernel/cast";
import isValid from "shared/utils/dates/isValid";
import compareAsc from "shared/utils/dates/compareAsc";

type EditProfileProps = {
  fullName: string,
  userEmail: string,
  phoneNo: string,
  gender: string,
  birthDate: string,
  citizenship: string,
  nativeLanguage: string,
  isOnEdit: boolean,
  toggleEdit: Function,
  onCompleteEditing: Function
};

type EditProfileState = {
  values: {
    fullName: string,
    userEmail: string,
    phoneNo: string,
    gender: string,
    birthDate: string,
    citizenship: string,
    nativeLanguage: string
  },
  errors: {
    fullName: null,
    userEmail: null,
    phoneNo: null,
    gender: null,
    birthDate: null,
    citizenship: null,
    nativeLanguage: null
  },
  uploading: boolean
};

class EditProfile extends Component<EditProfileProps, EditProfileState> {
  constructor(props: EditProfileProps) {
    super(props);

    const {
      fullName,
      userEmail,
      phoneNo,
      gender,
      birthDate,
      citizenship,
      nativeLanguage
    } = this.props;

    this.state = {
      values: {
        fullName,
        userEmail,
        phoneNo,
        gender,
        birthDate,
        citizenship,
        nativeLanguage
      },
      errors: {
        fullName: null,
        userEmail: null,
        phoneNo: null,
        gender: null,
        birthDate: null,
        citizenship: null,
        nativeLanguage: null
      },
      uploading: false
    };
    // $FlowFixMe
    this.handleFormFieldChange = this.handleFormFieldChange.bind(this);
    // $FlowFixMe
    this.handleInputChange = this.handleInputChange.bind(this);
    // $FlowFixMe
    this.updateProfileHandler = this.updateProfileHandler.bind(this);
    // $FlowFixMe
    this.resetFormErrors = this.resetFormErrors.bind(this);
    // $FlowFixMe
    this.setFormErrors = this.setFormErrors.bind(this);
  }

  handleFormFieldChange(name: string, value: string) {
    this.setState(({ values }) => ({
      values: {
        ...values,
        [name]: value
      }
    }));
  }

  handleInputChange(event: SyntheticInputEvent<HTMLInputElement>) {
    event.persist();
    this.setState(({ values }) => ({
      values: {
        ...values,
        [event.target.id]: event.target.value
      }
    }));
  }

  onChange = async (e: any) => {
    const file = e.target.files[0];
    const fileName = "uuid()";
    this.setState({ uploading: true });
    await Storage.put(fileName, file, {
      customPrefix: { public: "uploads/profile/" },
      metadata: { email: this.props.userEmail }
    });
    this.setState({ uploading: false });
  };

  resetFormErrors() {
    this.setState({
      errors: {
        fullName: null,
        userEmail: null,
        phoneNo: null,
        gender: null,
        birthDate: null,
        citizenship: null,
        nativeLanguage: null
      }
    });
  }

  setFormErrors(field: string, message: string) {
    this.setState(state => {
      return {
        errors: {
          ...state.errors,
          [field]: message
        }
      };
    });
  }

  validateForm() {
    const {
      values: {
        fullName,
        userEmail,
        phoneNo,
        gender,
        birthDate,
        citizenship,
        nativeLanguage
      }
    } = this.state;
    let hasError = false;

    this.resetFormErrors();

    if (userEmail === "") {
      this.setFormErrors("userEmail", translate("Email is required."));
      hasError = true;
    } else if (!isEmail(userEmail)) {
      this.setFormErrors("userEmail", translate("Email is invalid."));
      hasError = true;
    }

    if (fullName === "") {
      this.setFormErrors("fullName", translate("Name is required."));
      hasError = true;
    }

    if (gender === "") {
      this.setFormErrors("gender", translate("Gender is required."));
      hasError = true;
    }

    if (birthDate === "") {
      this.setFormErrors("birthDate", translate("Date of birth is required."));
      hasError = true;
    } else if (!isValid(new Date(birthDate))) {
      this.setFormErrors(
        "birthDate",
        translate("Date of birth format is incorrect.")
      );
      hasError = true;
    } else if (1 !== compareAsc(new Date(), birthDate)) {
      this.setFormErrors("birthDate", translate("Date of birth is invalid."));
      hasError = true;
    } else if (nativeLanguage === "") {
      this.setFormErrors(
        "nativeLanguage",
        translate("Native nativeLanguage is required.")
      );
      hasError = true;
    }

    if (phoneNo === "" || phoneNo === "+") {
      this.setFormErrors("phoneNo", translate("Phone number is required."));
      hasError = true;
    }

    if (citizenship === "") {
      this.setFormErrors("citizenship", translate("Citizenship is required."));
      hasError = true;
    }
    return hasError;
  }

  updateProfileHandler() {
    const { isOnEdit } = this.props;

    if (isOnEdit) {
      if (!this.validateForm()) {
        this.props.onCompleteEditing(this.state.values);
      }
    } else {
      this.props.toggleEdit();
    }
  }

  render() {
    const { isOnEdit } = this.props;
    const {
      values: {
        fullName,
        userEmail,
        phoneNo,
        gender,
        birthDate,
        citizenship,
        nativeLanguage
      },
      errors
    } = this.state;

    const countryList = [];
    for (const key in countries) {
      if (countries.hasOwnProperty(key)) {
        countryList.push({
          value: key,
          name: countries[key]
        });
      }
    }

    const localeList = [];
    for (const key in locales) {
      if (locales.hasOwnProperty(key)) {
        localeList.push({
          value: key,
          name: locales[key]
        });
      }
    }

    return (
      <Row>
        {/* {!isOnEdit && (
          <Col sm={12} md={4}>
            <img src={profileImage} />
          </Col>
        )} */}
        <Col className="user-details">
          {isOnEdit ? (
            <Row>
              <Col sm={12} md={4}>
                {translate("Name")}
              </Col>
              <Col sm={12} md={8}>
                <Input
                  placeholder={translate("Enter your name")}
                  id="fullName"
                  text={fullName}
                  onChange={this.handleInputChange}
                  error={errors.fullName}
                />
              </Col>
            </Row>
          ) : (
            <Row>
              <Col sm={12} md={12}>
                <div className="name">{fullName}</div>
              </Col>
            </Row>
          )}
          <Row>
            <Col sm={12} md={4}>
              {translate("Email")}
            </Col>
            <Col sm={12} md={8}>
              {userEmail}
            </Col>
          </Row>
          <Row>
            <Col sm={12} md={4}>
              {translate("Phone")}
            </Col>
            {isOnEdit ? (
              <Col sm={12} md={8}>
                <div
                  className={classnames("form-input", {
                    "has-errors": errors.phoneNo !== null
                  })}
                >
                  <PhoneInput
                    id="phoneNo"
                    defaultCountry={"us"}
                    inputClass="tel-input"
                    buttonClass={"dropdown"}
                    value={phoneNo}
                    onChange={phoneNo =>
                      this.handleFormFieldChange("phoneNo", phoneNo)
                    }
                  />
                  {errors.phoneNo !== null && (
                    <ul className="form-errors">
                      <li>{errors.phoneNo}</li>
                    </ul>
                  )}
                </div>
              </Col>
            ) : (
              <Col sm={12} md={8}>
                {phoneNo}
              </Col>
            )}
          </Row>
          <Row>
            <Col sm={12} md={4}>
              {translate("Gender")}
            </Col>
            {isOnEdit ? (
              <Col sm={12} md={8}>
                <Select
                  options={[
                    {
                      name: translate("Male"),
                      value: "male"
                    },
                    {
                      name: translate("Female"),
                      value: "female"
                    }
                  ]}
                  id="gender"
                  placeholder={translate("Select the gender")}
                  onChange={gender =>
                    this.handleFormFieldChange("gender", gender)
                  }
                  selected={gender}
                  error={errors.gender}
                />
              </Col>
            ) : (
              <Col sm={12} md={6} className="text-capitalize">
                {gender}
              </Col>
            )}
          </Row>
          <Row>
            <Col sm={12} md={4}>
              {translate("Birth date")}
            </Col>
            {isOnEdit ? (
              <Col sm={12} md={8} className="date-of-birth">
                <div
                  className={classnames("form-input", {
                    "has-errors": errors.birthDate !== null
                  })}
                >
                  <InputMask
                    mask="9999-99-99"
                    maskChar="_"
                    placeholder="yyyy-mm-dd"
                    onChange={event =>
                      this.handleFormFieldChange(
                        "birthDate",
                        event.target.value
                      )
                    }
                    value={birthDate}
                  />
                  {errors.birthDate !== null && (
                    <ul className="form-errors">
                      <li>{errors.birthDate}</li>
                    </ul>
                  )}
                </div>
              </Col>
            ) : (
              <Col sm={12} md={8}>
                {birthDate}
              </Col>
            )}
          </Row>
          <Row>
            <Col sm={12} md={4}>
              {translate("Citizenship")}
            </Col>
            {isOnEdit ? (
              <Col sm={12} md={8}>
                <Select
                  options={countryList}
                  placeholder={translate("Select country")}
                  onChange={citizenship =>
                    this.handleFormFieldChange("citizenship", citizenship)
                  }
                  selected={citizenship}
                  searchable
                  error={errors.citizenship}
                />
              </Col>
            ) : (
              <Col sm={12} md={8}>
                {countries[citizenship]}
              </Col>
            )}
          </Row>
          <Row>
            <Col sm={12} md={4}>
              {translate("Native language")}
            </Col>
            {isOnEdit ? (
              <Col sm={12} md={8}>
                <Select
                  options={localeList}
                  placeholder={translate("Select language")}
                  onChange={nativeLanguage =>
                    this.handleFormFieldChange("nativeLanguage", nativeLanguage)
                  }
                  selected={nativeLanguage}
                  searchable
                  error={errors.nativeLanguage}
                />
              </Col>
            ) : (
              <Col sm={12} md={8}>
                {locales[nativeLanguage]}
              </Col>
            )}
          </Row>
          {/* {isOnEdit && (
            <Row>
              <Col sm={12} md={4}>Profile Image</Col>
              <Col sm={12} md={8}>
                <div>
                  <input
                    id="add-image-file-input"
                    type="file"
                    accept="image/*"
                    onChange={this.onChange}
                  />
                </div>
              </Col>
            </Row>
          )} */}
          <Button
            className="update-profile"
            onClick={this.updateProfileHandler}
          >
            {isOnEdit ? translate("Update Profile") : translate("Edit Profile")}
          </Button>
        </Col>
      </Row>
    );
  }
}

export default EditProfile;
