// @flow
import { type notificationSettings } from "../types";

import React, { PureComponent, Fragment } from "react";

import Checkbox from "shared/components/Checkbox";
import Button from "shared/components/Button";
import Icon from "shared/components/Icon";
import translate from "translations/translate";
import Alert from "shared/components/Alert";
import Notifications from "containers/profile/pages/Notifications";

type NotificationSettingsFormProps = {
  notificationSettings: notificationSettings,
  updateNotificationSettings: Function,
  backLink: string,
  isNotificationsUpdated: boolean,
  error: null | string
};
type NotificationSettingsFormState = {
  form: {
    allNotification: boolean,
    changeFavCourse: boolean,
    schoolUpdates: boolean,
    schoolUpdateRegardSchool: boolean,
    specialOffers: boolean,
    promotion: boolean,
    newSchools: boolean,
    newCourse: boolean,
    bookingStatusUpdate: boolean,
    tambookEmail: boolean
  }
};
class NotificationSettingsForm extends PureComponent<
  NotificationSettingsFormProps,
  NotificationSettingsFormState
> {
  constructor(props: Object) {
    super(props);

    const { notificationSettings } = props;

    this.state = {
      form: {
        ...notificationSettings
      }
    };
    // $FlowFixMe
    this.onClickCheckbox = this.onClickCheckbox.bind(this);
    // $FlowFixMe
    this.onClickCancel = this.onClickCancel.bind(this);
    // $FlowFixMe
    this.updateNotificationSettings = this.updateNotificationSettings.bind(
      this
    );
    // $FlowFixMe
    this.checkAllNotifications = this.checkAllNotifications.bind(this);
  }

  onClickCheckbox(field: string, value: boolean) {
    if (Notifications.NOTIFICATION_TYPE.ALL_NOTIFICATION === field) {
      this.setState(({ form }) => ({
        form: {
          ...form,
          changeFavCourse: !value,
          schoolUpdates: !value,
          schoolUpdateRegardSchool: !value,
          specialOffers: !value,
          promotion: !value,
          newSchools: !value,
          newCourse: !value,
          bookingStatusUpdate: !value,
          tambookEmail: !value
        }
      }));
    } else {
      this.setState(({ form }) => ({
        form: {
          ...form,
          [field]: !value
        }
      }));
    }
  }

  onClickCancel() {
    const { notificationSettings } = this.props;

    this.setState({
      form: { ...notificationSettings }
    });
  }

  updateNotificationSettings() {
    const {
      changeFavCourse,
      schoolUpdates,
      schoolUpdateRegardSchool,
      specialOffers,
      promotion,
      newSchools,
      newCourse,
      bookingStatusUpdate,
      tambookEmail
    } = this.state.form;
    const allNotification = this.checkAllNotifications();

    this.props.updateNotificationSettings({
      all_notification: allNotification,
      change_fav_course: changeFavCourse,
      school_updates: schoolUpdates,
      school_update_regard_booking: schoolUpdateRegardSchool,
      special_offers: specialOffers,
      promotion: promotion,
      new_schools: newSchools,
      new_course: newCourse,
      booking_status_update: bookingStatusUpdate,
      tambook_email: tambookEmail
    });
  }

  checkAllNotifications() {
    const {
      changeFavCourse,
      schoolUpdates,
      schoolUpdateRegardSchool,
      specialOffers,
      promotion,
      newSchools,
      newCourse,
      bookingStatusUpdate,
      tambookEmail
    } = this.state.form;
    return (
      changeFavCourse &&
      schoolUpdates &&
      schoolUpdateRegardSchool &&
      specialOffers &&
      promotion &&
      newSchools &&
      newCourse &&
      bookingStatusUpdate &&
      tambookEmail
    );
  }

  render() {
    const { backLink, isNotificationsUpdated, error } = this.props;
    const { form } = this.state;
    const allNotification = this.checkAllNotifications();
    return (
      <Fragment>
        <div className="button-container">
          <div className="back-button">
            <Button
              htmlType={Button.HTML_TYPE.LINK}
              link={backLink}
              className="btn-with-icon"
            >
              <Icon className="icon" icon="chevron-left" />
              {translate("BACK TO NOTIFICATIONS")}
            </Button>
          </div>
          <div className="action-buttons">
            <Button
              type={Button.TYPE.PRIMARY}
              onClick={this.updateNotificationSettings}
            >
              {translate("SAVE")}
            </Button>
            <Button
              type={Button.TYPE.INFO}
              outline={true}
              onClick={this.onClickCancel}
            >
              {translate("CANCEL")}
            </Button>
          </div>
        </div>
        {isNotificationsUpdated ? (
          <Alert type={Alert.TYPE.SUCCESS}>
            {translate("Settings updated successfully")}
          </Alert>
        ) : error ? (
          <Alert type={Alert.TYPE.ERROR}>{translate(error)}</Alert>
        ) : null}
        <div className="notification-body">
          <Checkbox
            text={translate("I want to receive all notifications")}
            isChecked={allNotification}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.ALL_NOTIFICATION,
                allNotification
              )
            }
          />
          <div className="number-line" />
          <div className="notifications-type">{translate("Common")}</div>
          <Checkbox
            text={translate("I want to receive emails from Tambook")}
            isChecked={form.tambookEmail}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.TAMBOOK_EMAIL,
                form.tambookEmail
              )
            }
          />
          <Checkbox
            text={translate(
              "I want to receive information about promo from Tambook"
            )}
            isChecked={form.promotion}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.PROMOTION,
                form.promotion
              )
            }
          />
          <div className="notifications-type">{translate("Courses")}</div>
          <Checkbox
            text={translate(
              "I want to receive notifications about new courses"
            )}
            isChecked={form.newCourse}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.NEW_COURSE,
                form.newCourse
              )
            }
          />
          <Checkbox
            text={translate(
              "I want to receive notifications about changing favorite course information"
            )}
            isChecked={form.changeFavCourse}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.CHANGE_FAV_COURSE,
                form.changeFavCourse
              )
            }
          />
          <Checkbox
            text={translate(
              "I want to receive notifications about special offers"
            )}
            isChecked={form.specialOffers}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.SPECIAL_OFFERS,
                form.specialOffers
              )
            }
          />
          <div className="notifications-type">{translate("Schools")}</div>
          <Checkbox
            text={translate(
              "I want to receive notifications about new schools"
            )}
            isChecked={form.newSchools}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.NEW_SCHOOLS,
                form.newSchools
              )
            }
          />
          <Checkbox
            text={translate(
              "I want to receive notifications about schools' updates"
            )}
            isChecked={form.schoolUpdates}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.SCHOOL_UPDATES,
                form.schoolUpdates
              )
            }
          />
          <div className="notifications-type">{translate("Bookings")}</div>
          <Checkbox
            text={translate(
              "I want to receive notifications with the order status updates"
            )}
            isChecked={form.bookingStatusUpdate}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.BOOKING_STATUS_UPDATE,
                form.bookingStatusUpdate
              )
            }
          />
          <Checkbox
            text={translate(
              "I want to receive notifications with school updates regarding the booking"
            )}
            isChecked={form.schoolUpdateRegardSchool}
            onChange={() =>
              this.onClickCheckbox(
                Notifications.NOTIFICATION_TYPE.school_update_regard_booking,
                form.schoolUpdateRegardSchool
              )
            }
          />
        </div>
      </Fragment>
    );
  }
}

export default NotificationSettingsForm;
