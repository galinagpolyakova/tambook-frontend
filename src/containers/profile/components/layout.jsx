// @flow
import type { Node } from "react";

import React, { PureComponent } from "react";
import classnames from "classnames";

import Link from "components/Link";
import PageHeader from "components/PageHeader";
import translate from "translations/translate";

import "../styles.scss";

type LayoutProps = {
  currentLink: string,
  currentTitle: string,
  children: Node
};

class Layout extends PureComponent<LayoutProps> {
  render() {
    const { currentLink, currentTitle, children } = this.props;
    const pages = [
      { link: "bookings", title: translate("Bookings") },
      { link: "bookmarks", title: translate("Bookmarks") },
      { link: "user-information", title: translate("User information") },
      // { link: "payment-method", title: translate("Payment method") }
      { link: "notifications", title: translate("Notifications") }
    ];
    let title;
    let activeLink;
    if (currentLink === "booking-details") {
      title = translate("Booking Details");
      activeLink = "bookings";
    } else if (currentLink === "notification-settings") {
      title = translate("notification settings");
      activeLink = "notifications";
    } else {
      title = currentTitle;
      activeLink = currentLink;
    }
    return (
      <div className="profile-page">
        <PageHeader
          title={translate(title)}
          breadcrumbs={[
            {
              title: translate("Profile"),
              link: "/profile"
            },
            {
              title: translate(title)
            }
          ]}
        />
        <div className="container">
          <div className="profile-content">
            <div className="header-menu">
              <ul>
                {pages.map(({ title, link }, key) => (
                  <li
                    key={key}
                    className={classnames({ active: link === activeLink })}
                  >
                    <Link to={`/profile/${link}`}>{title}</Link>
                  </li>
                ))}
              </ul>
            </div>
            {children}
          </div>
        </div>
      </div>
    );
  }
}

export default Layout;
