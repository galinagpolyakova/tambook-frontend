export const BOOKING_STATUS = {
  ACTIVE: "Active",
  UNFINISHED: "Unfinished",
  ARCHIVE: "Archive"
};
