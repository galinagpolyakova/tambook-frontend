// @flow
import React, { Component, Fragment } from "react";

import { type BookingType } from "containers/agent/types";

import Accordion from "shared/components/Accordion";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Currency from "components/Currency";

import translate from "translations/translate";
import AccommodationForm from "containers/booking/pages/CourseInfo/components/AccommodationForm";

type ActiveOrderProps = {
  booking: BookingType,
  isAgent: boolean
};

class ActiveOrder extends Component<ActiveOrderProps> {
  render() {
    const {
      booking: {
        bookingSummery,
        bookingStatus,
        paymentLog: {
          paymentParams: {
            currency,
            description,
            application_fee,
            amount,
            receipt_email
          }
        },
        studentInfo,
        accommodation,
        paymentStatus,
        course
      },
      isAgent
    } = this.props;
    return (
      <Accordion
        items={[
          {
            title: translate("Order summary"),
            content: (
              <Fragment>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Start Date")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {bookingSummery.startDate}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Booked By")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {bookingSummery.bookedBy}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Booking Owner")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {bookingSummery.bookingOwner}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Booking Status")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {bookingStatus}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Payment Type")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {bookingSummery.paymentType}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Weeks")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {bookingSummery.weeks}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Currency")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {bookingSummery.currency}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Week Price")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    <Currency currencyType={bookingSummery.currency}>
                      {bookingSummery.weekPrice}
                    </Currency>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Week Discount")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    <Currency currencyType={bookingSummery.currency}>
                      {bookingSummery.weekDiscount}
                    </Currency>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Enrolment Fee")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    <Currency currencyType={bookingSummery.currency}>
                      {bookingSummery.enrolmentFee}
                    </Currency>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Stripe Charges")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    <Currency currencyType={bookingSummery.currency}>
                      {bookingSummery.stripeCharges}
                    </Currency>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Tambook Charges")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    <Currency currencyType={bookingSummery.currency}>
                      {bookingSummery.tambookCharge}
                    </Currency>
                  </Col>
                </Row>
                {isAgent === true && (
                  <Row>
                    <Col sm={12} md={4} className="field-heading">
                      {translate("Agent Discount")}&nbsp;:
                    </Col>
                    <Col sm={12} md={8}>
                      <Currency currencyType={bookingSummery.currency}>
                        {bookingSummery.agentCommission}
                      </Currency>
                    </Col>
                  </Row>
                )}
              </Fragment>
            )
          },
          {
            title: translate("Order total"),
            content: (
              <Fragment>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Total")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    <Currency currencyType={currency}>{amount / 100}</Currency>
                  </Col>
                </Row>
              </Fragment>
            )
          },
          {
            title: translate("Student information"),
            content: (
              <Fragment>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Student Name")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {studentInfo.firstName} {studentInfo.lastName}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Email")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {studentInfo.email}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Date of Birth")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {studentInfo.dob}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Phone No")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {studentInfo.phoneNo}
                  </Col>
                </Row>
              </Fragment>
            )
          },
          {
            title: translate("Accommodation information"),
            content: (
              <Fragment>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Start Date")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {accommodation.startDate !== null
                      ? accommodation.startDate
                      : translate("NOT SPECIFIED")}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("End Date")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {accommodation.endDate !== null &&
                    accommodation.endDate !== undefined
                      ? accommodation.endDate
                      : translate("NOT SPECIFIED")}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Allergies")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {accommodation.alergies}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Food Restrictions")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {accommodation.foodRestrictions}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("HomeStay Weeks")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {accommodation.homeStayWeeks}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Smoke")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {accommodation.smoke}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Type")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {translate(
                      AccommodationForm.ACCOMMODATION_VALUES[accommodation.type]
                    )}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Young Children")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {accommodation.youngChildren}
                  </Col>
                </Row>
              </Fragment>
            )
          },
          {
            title: translate("Transfer & Flight information"),
            content: (
              <Fragment>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Type")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {translate(
                      "I'll arrange transportation from the airport myself"
                    )}
                  </Col>
                </Row>
              </Fragment>
            )
          },
          {
            title: translate("Payment information"),
            content: (
              <Fragment>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Receipt Email")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {receipt_email}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Description")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {description}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Status")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {paymentStatus}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Currency")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {currency}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Application Fee")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    <Currency currencyType={currency}>
                      {application_fee / 100}
                    </Currency>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Amount")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    <Currency currencyType={currency}>{amount / 100}</Currency>
                  </Col>
                </Row>
              </Fragment>
            )
          },
          {
            title: translate("Course information"),
            content: (
              <Fragment>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Course Name")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {course.courseShortName}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Language")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {course.language}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Country")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {course.country}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("City")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {course.city}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Course Discount")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {bookingSummery.weekDiscount > 0 &&
                    parseFloat(bookingSummery.weeks) > 0 ? (
                      <Currency currencyType={course.currency}>
                        {bookingSummery.weekDiscount *
                          parseFloat(bookingSummery.weeks)}
                      </Currency>
                    ) : (
                      "-"
                    )}
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={4} className="field-heading">
                    {translate("Enrollment Fee")}&nbsp;:
                  </Col>
                  <Col sm={12} md={8}>
                    {course.enrollmentFee > 0 ? (
                      <Currency currencyType={course.currency}>
                        {course.enrollmentFee}
                      </Currency>
                    ) : (
                      "-"
                    )}
                  </Col>
                </Row>
              </Fragment>
            )
          }
        ]}
      />
    );
  }
}

export default ActiveOrder;
