// @flow
import React, { Component, Fragment } from "react";

import { type AsyncStatusType, type NotificationType } from "types/general";
import { type UserReviewType } from "containers/review/types";
import { type BookingType } from "containers/agent/types";

import Button from "shared/components/Button";
import Accordion from "shared/components/Accordion";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Currency from "components/Currency";
import IconButton from "shared/components/IconButton";
import format from "shared/utils/dates/format";
import Loader from "components/Loader";
import FeedbackForm from "./FeedbackForm";

import translate from "translations/translate";
import { MEDIA_URL } from "config/app";

type ArchivedOrderProps = {
  booking: BookingType,
  bookingId: string,
  isAgent: boolean,
  status: AsyncStatusType,
  notification: NotificationType,
  saveFeedback: Function,
  updateFeedback: Function,
  resetNotification: Function,
  review: UserReviewType,
  validateUserFeedback: Function
};

type ArchivedOrderState = {
  feedbackFormType: null | string
};

class ArchivedOrder extends Component<ArchivedOrderProps, ArchivedOrderState> {
  static FEEDBACK_FORM_TYPE = {
    COURSE: "course",
    TAMBOOK: "tambook"
  };

  constructor(props: ArchivedOrderProps) {
    super(props);
    this.state = {
      feedbackFormType: null
    };
    // $FlowFixMe
    this.showFeedbackForm = this.showFeedbackForm.bind(this);
    // $FlowFixMe
    this.hideFeedbackForm = this.hideFeedbackForm.bind(this);
    // $FlowFixMe
    this._onIconButtonClick = this._onIconButtonClick.bind(this);
  }

  componentDidMount() {
    const {
      validateUserFeedback,
      booking: {
        course: { id: courseId }
      }
    } = this.props;
    validateUserFeedback({
      courseId
    });
  }

  componentDidUpdate(prevProps) {
    const {
      validateUserFeedback,
      bookingId,
      booking: {
        course: { id: courseId }
      }
    } = this.props;
    if (prevProps.bookingId !== bookingId) {
      validateUserFeedback({
        courseId
      });
    }
  }

  getDetails() {
    const {
      booking: {
        bookingSummery,
        paymentLog: {
          paymentParams: { currency, amount }
        },
        studentInfo,
        course
      }
    } = this.props;
    let items = [
      {
        title: translate("Course information"),
        content: (
          <Fragment>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Intensity (hours per week)")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                {course.intensity}
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Minimum age")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                {course.minStudentAge}
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Start date")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                {course.schedule.map(
                  (schedule, key) =>
                    key === 0 && (
                      <div key={key}>{format(schedule.from, "yyyy-MM-dd")}</div>
                    )
                )}
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Schedule")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                {course.schedule.map((schedule, key) => (
                  <div key={key}>
                    {schedule.day}&nbsp;: {format(schedule.from, "HH:mm")}
                    &nbsp;-&nbsp;
                    {format(schedule.to, "HH:mm")}
                  </div>
                ))}
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("School")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                {course.schoolDetail.name}
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("English entry level")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                {course.minEnglishLevel}
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Work rights")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                {course.workEligibility !== null
                  ? course.workEligibility
                  : translate("NOT SPECIFIED")}
              </Col>
            </Row>
          </Fragment>
        )
      },
      {
        title: translate("Order total"),
        content: (
          <Fragment>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Course")}({bookingSummery.weeks}
                {translate("Weeks")})&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                <Currency currencyType={currency}>
                  {bookingSummery.weekPrice * bookingSummery.weeks}
                </Currency>
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Enrolment fee")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                <Currency currencyType={currency}>
                  {bookingSummery.enrolmentFee}
                </Currency>
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Tambook discount")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                &nbsp;-&nbsp;
                <Currency currencyType={currency}>
                  {bookingSummery.weekDiscount}
                </Currency>
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Bank Transfer fee")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                <Currency currencyType={currency}>
                  {bookingSummery.stripeCharges}
                </Currency>
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4} className="field-heading">
                {translate("Total")}&nbsp;:
              </Col>
              <Col sm={12} md={8}>
                <Row>
                  <Col sm={12} md={4}>
                    <Currency currencyType={currency}>{amount / 100}</Currency>
                  </Col>
                  <Col sm={12} md={4} className="strike-through">
                    {bookingSummery.weekDiscount > 0 && (
                      <Currency currencyType={currency}>
                        {bookingSummery.weekPrice * bookingSummery.weeks +
                          bookingSummery.stripeCharges +
                          bookingSummery.enrolmentFee}
                      </Currency>
                    )}
                  </Col>
                </Row>
              </Col>
            </Row>
            {bookingSummery.weekDiscount > 0 && (
              <Row>
                <Col sm={12} md={4} className="field-heading">
                  {translate("You save")}&nbsp;:
                </Col>
                <Col sm={12} md={8}>
                  <Currency currencyType={currency}>
                    {bookingSummery.weekDiscount}
                  </Currency>
                </Col>
              </Row>
            )}
          </Fragment>
        )
      },
      {
        title: translate("Package of documents"),
        content: (
          <Fragment>
            <Row>
              <Col sm={12} md={4}>
                {translate("DOCUMENTS FROM SCHOOL")}
              </Col>
              <Col sm={12} md={4}>
                {translate("COURSE INFORMATION")}
              </Col>
              <Col sm={12} md={4}>
                {translate("USER DOCUMENTS")}
                <div className="file-download">
                  <IconButton
                    className="button-close"
                    onClick={() => {
                      this._onIconButtonClick(studentInfo.passportImage);
                    }}
                    icon="file-text"
                  />
                  {studentInfo.passportImage.substring(
                    studentInfo.passportImage.lastIndexOf("/") + 1
                  )}
                </div>
              </Col>
            </Row>
          </Fragment>
        )
      }
    ];
    return items;
  }

  showFeedbackForm(feedbackFormType: string) {
    this.setState({ feedbackFormType });
  }

  hideFeedbackForm() {
    const {
      resetNotification,
      validateUserFeedback,
      booking: {
        course: { id: courseId }
      }
    } = this.props;
    this.setState({ feedbackFormType: null });
    resetNotification();
    validateUserFeedback({
      courseId
    });
  }

  _onIconButtonClick(key: any) {
    let url = `${MEDIA_URL}/${key}`;
    setTimeout(() => {
      const response = {
        file: url
      };
      window.open(response.file);
    }, 1000);
  }

  render() {
    const {
      isAgent,
      review: { courseReview, tambookReview },
      booking: { course, studentInfo },
      saveFeedback,
      updateFeedback,
      status,
      notification,
      validateUserFeedback
    } = this.props;
    const { feedbackFormType } = this.state;

    if (courseReview === null && tambookReview === null) {
      return <Loader />;
    }
    if (feedbackFormType !== null) {
      return (
        <FeedbackForm
          isCourseReview={
            feedbackFormType === ArchivedOrder.FEEDBACK_FORM_TYPE.COURSE
          }
          onClose={this.hideFeedbackForm}
          courseId={course.id}
          schoolId={course.school}
          countryCode={studentInfo.country}
          onDefaultFormSubmit={saveFeedback}
          onUpdateFormSubmit={updateFeedback}
          status={status}
          notification={notification}
          review={
            feedbackFormType === ArchivedOrder.FEEDBACK_FORM_TYPE.COURSE
              ? courseReview.length > 0
                ? courseReview[0]
                : null
              : tambookReview.length > 0
              ? tambookReview[0]
              : null
          }
          validateUserFeedback={validateUserFeedback}
        />
      );
    }

    return (
      <Fragment>
        <Accordion items={this.getDetails()} />
        {isAgent !== true && (
          <div>
            {translate("Add a feedback about")}
            <div className="review-buttons">
              <Button
                htmlType={Button.HTML_TYPE.BUTTON}
                type={Button.TYPE.DEFAULT}
                onClick={() => this.showFeedbackForm("tambook")}
              >
                {translate("Tambook operations")}
              </Button>
              <Button
                htmlType={Button.HTML_TYPE.BUTTON}
                type={Button.TYPE.DEFAULT}
                onClick={() => this.showFeedbackForm("course")}
              >
                {translate("Course")}
              </Button>
            </div>
          </div>
        )}
      </Fragment>
    );
  }
}

export default ArchivedOrder;
