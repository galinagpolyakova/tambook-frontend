// @flow
import React, { Component } from "react";

import { type UserReviewType } from "containers/review/types";
import { type ReviewState } from "containers/review/store/reducer";
import type { AsyncStatusType } from "types/general";

import StarRatingInput from "shared/components/StarRatingInput";
import Modal from "shared/components/Modal";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Button from "shared/components/Button";
import TextArea from "shared/components/Textarea";

import translate from "translations/translate";
import { getLanguageFromUrl } from "shared/helpers/translations";
import { ASYNC_STATUS } from "constants/async";
import Alert from "shared/components/Alert/Alert";

type FeedbackFormProps = {
  notification: $PropertyType<ReviewState, "notification">,
  isCourseReview: boolean,
  status: AsyncStatusType,
  courseId: string,
  schoolId: string,
  countryCode: string,
  onClose: Function,
  onDefaultFormSubmit: Function,
  onUpdateFormSubmit: Function,
  review: UserReviewType,
  validateUserFeedback: Function
};

type FeedbackFormState = {
  errors: { comment: null | string },
  values: { ranking: number, comment: string },
  isFeedbackEdit: boolean
};

class FeedbackForm extends Component<FeedbackFormProps, FeedbackFormState> {
  static FORM_ACTION_TYPE = {
    CREATE: "create",
    UPDATE: "update"
  };

  constructor(props: FeedbackFormProps) {
    super(props);
    this.state = {
      values: {
        ranking: props.review !== null ? props.review.rate : 0,
        comment: props.review !== null ? props.review.reviewText : ""
      },
      errors: {
        comment: null
      },
      isFeedbackEdit: props.review !== null
    };

    // $FlowFixMe
    this.handleFormFieldChange = this.handleFormFieldChange.bind(this);
    // $FlowFixMe
    this.validateForm = this.validateForm.bind(this);
    // $FlowFixMe
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    // $FlowFixMe
    this.setFormErrors = this.setFormErrors.bind(this);
    // $FlowFixMe
    this.resetFormErrors = this.resetFormErrors.bind(this);
    // $FlowFixMe
    this.getAlert = this.getAlert.bind(this);
  }

  handleFormFieldChange(field: string, value: string) {
    this.setState(({ values }) => ({
      values: {
        ...values,
        [field]: value
      }
    }));
  }

  resetFormErrors() {
    this.setState({
      errors: {
        comment: null
      }
    });
  }

  setFormErrors(field: any, error: string) {
    this.setState(({ errors }) => ({
      errors: {
        ...errors,
        [field]: error
      }
    }));
  }

  validateForm() {
    const {
      values: { comment }
    } = this.state;

    let hasError = false;

    this.resetFormErrors();

    if (comment === "") {
      this.setFormErrors("comment", translate("Feedback is required"));
      hasError = true;
    }
    return hasError;
  }

  handleFormSubmit() {
    if (!this.validateForm()) {
      const {
        values: { comment, ranking }
      } = this.state;
      const { isFeedbackEdit } = this.state;

      const { isCourseReview, courseId, schoolId, countryCode } = this.props;

      const lanuage = getLanguageFromUrl();

      if (isFeedbackEdit) {
        if (isCourseReview) {
          this.props.onUpdateFormSubmit(
            {
              comment,
              courseId,
              ranking,
              lanuage,
              countryCode
            },
            isCourseReview
          );
        } else {
          this.props.onUpdateFormSubmit(
            {
              comment,
              ranking,
              schoolId,
              countryCode,
              reviewId: this.props.review.id
            },
            isCourseReview
          );
        }
      } else {
        this.props.onDefaultFormSubmit(
          {
            comment,
            courseId,
            schoolId,
            countryCode,
            ranking,
            lanuage
          },
          isCourseReview
        );
      }
    }
  }

  getAlert() {
    const { isFeedbackEdit } = this.state;
    if (isFeedbackEdit) {
      return (
        <Alert type={Alert.TYPE.SUCCESS}>
          {translate("You have already added a feedback, Please update it")}.
        </Alert>
      );
    }
  }

  render() {
    const { errors, values, isFeedbackEdit } = this.state;
    const { isCourseReview, onClose, notification, status } = this.props;

    return (
      <Modal showModal={true} onClose={onClose}>
        <div className="feedback-form">
          <div className="form-content">
            {isCourseReview ? (
              <h3>{translate("Feedback about course")}</h3>
            ) : (
              <h3>{translate("Feedback about Tambook operations")}</h3>
            )}
            {this.getAlert()}
            <Row>
              <Col sm={12} md={4}>
                {translate("Your vote")}
              </Col>
              <Col sm={12} md={8}>
                <StarRatingInput
                  size={5}
                  value={values.ranking}
                  onChange={ranking =>
                    this.handleFormFieldChange("ranking", ranking)
                  }
                />
              </Col>
            </Row>
            <Row>
              <Col sm={12} md={4}>
                {translate("Your feedback")}
              </Col>
              <Col sm={12} md={8}>
                <TextArea
                  id="comment"
                  type={"text"}
                  text={values.comment}
                  onChange={event =>
                    this.handleFormFieldChange("comment", event.target.value)
                  }
                  error={errors.comment}
                  autoComplete={false}
                />
              </Col>
            </Row>
          </div>
          {notification !== null && (
            <Alert type={notification.type}>{notification.message}</Alert>
          )}
          <div className="form-action">
            <Button
              type={Button.TYPE.PRIMARY}
              onClick={() => this.handleFormSubmit()}
              disabled={status === ASYNC_STATUS.SUCCESS}
              loading={status === ASYNC_STATUS.LOADING}
            >
              {translate(isFeedbackEdit ? "UPDATE" : "SEND")}
            </Button>
            <Button type={Button.TYPE.DEFAULT} onClick={onClose}>
              {translate("CANCEL")}
            </Button>
          </div>
        </div>
      </Modal>
    );
  }
}

export default FeedbackForm;
