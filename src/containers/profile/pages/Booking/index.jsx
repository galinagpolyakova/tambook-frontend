// @flow
import { type BookingType } from "containers/agent/types";
import { type UserReviewType } from "containers/review/types";
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import type { Match, Location } from "react-router-dom";

import { type AsyncStatusType } from "types/general";

import translate from "translations/translate";
import Layout from "../../components/layout";
import Loader from "components/Loader";
import Button from "shared/components/Button";
import Icon from "shared/components/Icon";
import AgentLayout from "containers/agent/components/layout";

import { getBooking, validateUserFeedback } from "../../store/actions";
import {
  saveFeedback,
  resetNotification,
  updateFeedback
} from "containers/review/store/actions";
import ActiveOrder from "./components/ActiveOrder";
import ArchivedOrder from "./components/ArchivedOrder";
import { USER_TYPE } from "containers/auth/constants";
import { type ReviewState } from "containers/review/store/reducer";
import { type AuthState } from "containers/auth/store/reducer";

import "./styles.scss";

export type BookingDetailsProps = {
  isLoaded: boolean,
  booking: BookingType,
  bookingId: string,
  validateUserFeedback: Function,
  review: UserReviewType,
  match: Match,
  getBooking: Function,
  saveFeedback: Function,
  updateFeedback: Function,
  location: Location,
  role: $PropertyType<AuthState, "role">,
  asyncStatus: AsyncStatusType,
  resetNotification: Function,
  notification: $PropertyType<ReviewState, "notification">
};

class BookingDetails extends PureComponent<BookingDetailsProps> {
  componentDidMount() {
    const {
      match: {
        params: { id }
      },
      isLoaded,
      getBooking
    } = this.props;
    if (!isLoaded) getBooking(id);
  }

  componentDidUpdate(prevProps) {
    const {
      match: {
        params: { id }
      },
      getBooking
    } = this.props;
    if (prevProps.match.params.id !== id) {
      getBooking(id);
    }
  }

  getOrderDetails() {
    const {
      booking,
      bookingId,
      review,
      validateUserFeedback,
      role,
      asyncStatus,
      match: {
        params: { status }
      },
      saveFeedback,
      updateFeedback,
      resetNotification,
      notification
    } = this.props;
    switch (status) {
      case "archived":
        return (
          <ArchivedOrder
            booking={booking}
            bookingId={bookingId}
            review={review}
            validateUserFeedback={validateUserFeedback}
            saveFeedback={saveFeedback}
            updateFeedback={updateFeedback}
            status={asyncStatus}
            resetNotification={resetNotification}
            notification={notification}
            isAgent={role === USER_TYPE.AGENT}
          />
        );
      default:
        return (
          <ActiveOrder booking={booking} isAgent={role === USER_TYPE.AGENT} />
        );
    }
  }

  getButton() {
    const { role } = this.props;
    return (
      <Button
        htmlType={Button.HTML_TYPE.LINK}
        link={
          role === USER_TYPE.AGENT
            ? "/agent-profile/orders"
            : "/profile/bookings"
        }
        className="btn-with-icon"
      >
        <Icon className="icon" icon="chevron-left" />
        {translate("BACK TO BOOKINGS")}
      </Button>
    );
  }

  render() {
    const { role, booking } = this.props;
    if (booking === null) {
      return <Loader />;
    }
    if (role === USER_TYPE.AGENT) {
      return (
        <AgentLayout
          currentLink="order-details"
          currentTitle={translate("Order Details")}
        >
          <div className="order-details">
            <div className="order-header">
              <h3>{booking.course.courseShortName}</h3>
              <div className="back-button">{this.getButton()}</div>
            </div>
            <div className="order-details-list">{this.getOrderDetails()}</div>
          </div>
        </AgentLayout>
      );
    } else {
      return (
        <Layout
          currentLink="booking-details"
          currentTitle={translate("Booking Details")}
        >
          <div className="order-details">
            <div className="order-header">
              <h3>{booking.course.courseShortName}</h3>
              <div className="back-button">{this.getButton()}</div>
            </div>
            <div className="order-details-list">{this.getOrderDetails()}</div>
          </div>
        </Layout>
      );
    }
  }
}

const Actions = {
  getBooking,
  saveFeedback,
  updateFeedback,
  resetNotification,
  validateUserFeedback
};

function mapStateToProps(state) {
  return {
    booking: state.profile.booking.data,
    bookingId: state.profile.booking.bookingId,
    review: state.profile.review,
    getCurrencyList: state.profile.currencies,
    asyncStatus: state.review.status,
    notification: state.review.notification,
    role: state.auth.role
  };
}

export default connect(
  mapStateToProps,
  Actions
)(BookingDetails);
