// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Row from "shared/components/Row";
import Col from "shared/components/Col";
import format from "shared/utils/dates/format";
import translate from "translations/translate";
import Button from "shared/components/Button";

import Loader from "components/Loader/Loader";

import { getBookings } from "containers/profile/store/actions";
import Pagination from "shared/components/Pagination";
import { BOOKING_STATUS } from "containers/profile/constants";

type CourseBookingProps = {
  activeBookings: null | [],
  archivedBookings: null | [],
  unfinishedBookings: null | [],
  type: | typeof BOOKING_STATUS.ACTIVE
    | typeof BOOKING_STATUS.UNFINISHED
    | typeof BOOKING_STATUS.ARCHIVE,
  getBookings: Function
};

class CourseBooking extends PureComponent<CourseBookingProps> {
  constructor(props) {
    super(props);
    // $FlowFixMe
    this.getBookings = this.getBookings.bind(this);
  }

  componentDidMount() {
    this.getBookings();
  }

  getBookings({ page } = { page: 1 }) {
    const { type, getBookings } = this.props;

    getBookings(type, page);
  }

  render() {
    const {
      type,
      activeBookings,
      archivedBookings,
      unfinishedBookings
    } = this.props;

    let link;
    let data = null;

    switch (type) {
      case BOOKING_STATUS.ACTIVE:
        link = "/profile/bookings/active/";
        data = activeBookings;
        break;
      case BOOKING_STATUS.ARCHIVE:
        link = "/profile/bookings/archived/";
        data = archivedBookings;
        break;
      case BOOKING_STATUS.UNFINISHED:
        data = unfinishedBookings;
        break;
      default:
        link = "";
        break;
    }

    if (!data || data === null) {
      return <Loader />;
    }

    return (
      <div className="course-booking">
        <div className="course-booking-header">
          <Row>
            <Col sm={12} md={3}>
              {translate("Title")}
            </Col>
            <Col sm={12} md={3}>
              {translate("Duration (weeks)")}
            </Col>
            <Col sm={12} md={2}>
              {translate("Date")}
            </Col>
            <Col sm={12} md={2}>
              {translate("Status")}
            </Col>
            <Col sm={12} md={2}>
              {translate("Details")}
            </Col>
          </Row>
        </div>
        <div className="course-booking-body">
          {data.list.map((booking, key) => (
            <Row key={key} className="course-booking-row">
              <Col sm={12} md={3}>
                {booking.title}
              </Col>
              <Col sm={12} md={3}>
                {type === BOOKING_STATUS.UNFINISHED
                  ? "-"
                  : `${booking.duration} (weeks)`}
              </Col>
              <Col sm={12} md={2}>
                {format(booking.date, "yyyy-MM-dd")}
              </Col>
              <Col sm={12} md={2} className="status text-capitalize">
                {booking.status}
              </Col>
              <Col sm={12} md={2}>
                {type === BOOKING_STATUS.UNFINISHED ? (
                  <Button
                    size={Button.SIZE.SMALL}
                    htmlType={Button.HTML_TYPE.LINK}
                    link={`/booking/course/${booking.id}/${
                      booking.courseID
                    }?selectedWeek=0&selectedRange=0`}
                  >
                    {translate("BOOK")}
                  </Button>
                ) : (
                  <Button
                    size={Button.SIZE.SMALL}
                    htmlType={Button.HTML_TYPE.LINK}
                    link={link + booking.id}
                  >
                    {translate("SHOW")}
                  </Button>
                )}
              </Col>
            </Row>
          ))}
        </div>
        <Pagination
          totalPages={data.pagination.totalPages}
          currentPage={data.pagination.current}
          onPageChanged={this.getBookings}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    activeBookings: state.profile.activeBookings,
    archivedBookings: state.profile.archivedBookings,
    unfinishedBookings: state.profile.unfinishedBookings
  };
}

const Actions = {
  getBookings
};

export default connect(
  mapStateToProps,
  Actions
)(CourseBooking);
