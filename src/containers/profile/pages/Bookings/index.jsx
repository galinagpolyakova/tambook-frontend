// @flow
import React, { Component } from "react";

import translate from "translations/translate";

import Tabs from "shared/components/Tabs";

import Layout from "../../components/layout";
import CourseBooking from "./components/CourseBooking";
import { BOOKING_STATUS } from "containers/profile/constants";

class Bookings extends Component {
  render() {
    return (
      <Layout currentLink="bookings" currentTitle={translate("Bookings")}>
        <div className="bookings">
          <Tabs
            items={[
              {
                title: translate("Active"),
                content: <CourseBooking type={BOOKING_STATUS.ACTIVE} />
              },
              {
                title: translate("Unfinished"),
                content: <CourseBooking type={BOOKING_STATUS.UNFINISHED} />
              },
              {
                title: translate("Archived"),
                content: <CourseBooking type={BOOKING_STATUS.ARCHIVE} />
              }
            ]}
          />
        </div>
      </Layout>
    );
  }
}

export default Bookings;
