// @flow
import { type notificationSettings } from "../types";

import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Loader from "components/Loader";
import Layout from "../components/layout";
import translate from "translations/translate";
import NotificationSettingsForm from "../components/NotificationSettingsForm";

import {
  getNotificationSettings,
  updateNotificationSettings
} from "../store/actions";

type NotificationSettingsProps = {
  getNotificationSettings: Function,
  updateNotificationSettings: Function,
  loading: boolean,
  notificationSettings: notificationSettings,
  isNotificationsUpdated: boolean,
  error: null | string
};
class NotificationSettings extends PureComponent<NotificationSettingsProps> {
  BACK_LINK = "/profile/notifications";

  constructor(props: Object) {
    super(props);

    // $FlowFixMe
    this.updateNotificationSettings = this.updateNotificationSettings.bind(
      this
    );
  }

  componentDidMount() {
    const { getNotificationSettings } = this.props;

    getNotificationSettings();
  }

  updateNotificationSettings(payload) {
    const { updateNotificationSettings } = this.props;

    updateNotificationSettings(payload);
  }

  render() {
    const {
      loading,
      notificationSettings,
      isNotificationsUpdated,
      error
    } = this.props;
    return (
      <Layout
        currentLink="notification-settings"
        currentTitle={translate("Notification Settings")}
      >
        <div className="notification-settings">
          {loading ? (
            <Loader isLoading />
          ) : (
            <NotificationSettingsForm
              notificationSettings={notificationSettings}
              updateNotificationSettings={this.updateNotificationSettings}
              backLink={this.BACK_LINK}
              isNotificationsUpdated={isNotificationsUpdated}
              error={error}
            />
          )}
        </div>
      </Layout>
    );
  }
}

const Actions = {
  getNotificationSettings,
  updateNotificationSettings
};

function mapStateToProps(state) {
  return {
    loading: state.profile.loading,
    notificationSettings: state.profile.notificationSettings,
    isNotificationsUpdated: state.profile.isNotificationsUpdated,
    error: state.profile.error
  };
}

export default connect(
  mapStateToProps,
  Actions
)(NotificationSettings);
