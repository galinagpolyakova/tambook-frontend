// @flow
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Layout from "../components/layout";
import Loader from "components/Loader";
import Alert from "shared/components/Alert/Alert";
import Button from "shared/components/Button";
import translate from "translations/translate";
import { getNotifications, markAllNotificationsRead } from "../store/actions";
import format from "shared/utils/dates/format";

type NotificationsProps = {
  getNotifications: Function,
  markAllNotificationsRead: Function,
  notifications: Array<any>,
  loading: boolean
};
class Notifications extends PureComponent<NotificationsProps> {
  static NOTIFICATION_TYPE = {
    ALL_NOTIFICATION: "allNotification",
    CHANGE_FAV_COURSE: "changeFavCourse",
    SCHOOL_UPDATES: "schoolUpdates",
    school_update_regard_booking: "schoolUpdateRegardSchool",
    SPECIAL_OFFERS: "specialOffers",
    PROMOTION: "promotion",
    NEW_SCHOOLS: "newSchools",
    NEW_COURSE: "newCourse",
    BOOKING_STATUS_UPDATE: "bookingStatusUpdate",
    TAMBOOK_EMAIL: "tambookEmail"
  };

  componentDidMount() {
    const { getNotifications } = this.props;

    getNotifications();
  }

  render() {
    const { notifications, loading, markAllNotificationsRead } = this.props;

    return (
      <Layout currentLink="notifications" currentTitle="Notifications">
        <div className="notifications">
          <div className="notifications-header">
            <Button
              type={Button.TYPE.PRIMARY}
              onClick={markAllNotificationsRead}
            >
              {translate("Mark all as read")}
            </Button>
            <Button
              htmlType={Button.HTML_TYPE.LINK}
              size={Button.SIZE.SMALL}
              link={"/profile/notifications/notification-settings"}
            >
              {translate("Notifications Settings")}
            </Button>
          </div>
          {loading ? (
            <Loader isLoading />
          ) : (
            <div className="notifications-body">
              {notifications.length > 0 ? (
                notifications.map(({ message, isView, createdAt }, index) => (
                  <Alert
                    key={index}
                    type={
                      isView === "true" ? Alert.TYPE.LIGHT : Alert.TYPE.INFO
                    }
                  >
                    {message}{" "}
                    <span>{format(new Date(createdAt), "yyyy-MM-dd")}</span>
                  </Alert>
                ))
              ) : (
                <Alert isFullWidth={true} type={Alert.TYPE.INFO}>
                  {translate("No Notifications")}
                </Alert>
              )}
            </div>
          )}
        </div>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.profile.loading,
    notifications: state.profile.notifications
  };
}

const Actions = {
  getNotifications,
  markAllNotificationsRead
};

export default connect(
  mapStateToProps,
  Actions
)(Notifications);
