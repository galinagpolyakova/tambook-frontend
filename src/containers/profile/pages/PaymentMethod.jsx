import React from "react";
import Layout from "../components/layout";
import Alert from "shared/components/Alert";

export default function PaymentMethod() {
  const hasPaymentMethod = false;
  return (
    <Layout currentLink="payment-method" currentTitle="Payment method">
      {hasPaymentMethod ? (
        <div className="payment-method">
          <div className="card">
            <div className="chip">
              <div className="side left" />
              <div className="side right" />
              <div className="vertical top" />
              <div className="vertical bottom" />
            </div>
            <div className="data">
              <div className="pan" title="4123 4567 8910 1112">
                4123 4567 8910 1112
              </div>
              <div className="exp-date-wrapper">
                <div className="left-label">EXPIRES END</div>
                <div className="exp-date">
                  <div className="date" title="01/17">
                    01/17
                  </div>
                </div>
              </div>
              <div className="name-on-card" title="John Doe">
                John Doe
              </div>
            </div>
            <div className="lines-up" />
          </div>
        </div>
      ) : (
        <Alert>You have not added a payment method.</Alert>
      )}
    </Layout>
  );
}
