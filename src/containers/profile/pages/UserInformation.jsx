// @flow
import React, { Component } from "react";
import { connect } from "react-redux";

import { getUserProfile, updateProfile } from "../store/actions";

import Layout from "../components/layout";
import EditProfile from "../components/EditProfile";
import Loader from "components/Loader";
import Alert from "shared/components/Alert";
import translate from "translations/translate";

type UserInformationProps = {
  user: Object,
  updateProfile: Function,
  error: null | string,
  loading: boolean,
  isLoaded: boolean,
  getUserProfile: Function
};

type UserInformationState = {
  isOnEdit: boolean
};

class UserInformation extends Component<
  UserInformationProps,
  UserInformationState
> {
  state = {
    isOnEdit: false
  };
  componentDidMount() {
    const { isLoaded, getUserProfile } = this.props;

    if (!isLoaded) {
      getUserProfile();
    }
  }

  toggleEdit() {
    this.setState({ isOnEdit: true });
  }
  updateProfile(payload) {
    this.props.updateProfile(payload);
    this.setState({ isOnEdit: false });
  }
  render() {
    const { isOnEdit } = this.state;
    const { user, loading, error } = this.props;
    return (
      <Layout
        currentLink="user-information"
        currentTitle={translate("User information")}
      >
        <div className="user-information">
          {user !== null &&
            (loading ? (
              <Loader isLoading />
            ) : (
              <EditProfile
                {...user}
                isOnEdit={isOnEdit}
                toggleEdit={this.toggleEdit.bind(this)}
                onCompleteEditing={this.updateProfile.bind(this)}
              />
            ))}
          {error && <Alert type={Alert.TYPE.ERROR}>{error}</Alert>}
        </div>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.profile.userProfile,
    error: state.profile.error,
    loading: state.profile.loading,
    isLoaded: state.profile.isLoaded
  };
}

const Actions = {
  getUserProfile,
  updateProfile
};

export default connect(
  mapStateToProps,
  Actions
)(UserInformation);
