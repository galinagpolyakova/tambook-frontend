// import lib
import { lazy } from "react";
import { USER_TYPE } from "containers/auth/constants";

export default [
  {
    path: "/profile/",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT],
    component: lazy(() => import("./pages/Bookings"))
  },
  {
    path: "/profile/bookings",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT],
    component: lazy(() => import("./pages/Bookings"))
  },
  {
    path: "/profile/bookings/:status/:id",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT],
    component: lazy(() => import("./pages/Booking"))
  },
  {
    path: "/profile/bookmarks",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT],
    component: lazy(() => import("./pages/Bookmarks"))
  },
  {
    path: "/profile/user-information",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT],
    component: lazy(() => import("./pages/UserInformation"))
  },
  {
    path: "/profile/payment-method",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT],
    component: lazy(() => import("./pages/PaymentMethod"))
  },
  {
    path: "/profile/notifications",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT],
    component: lazy(() => import("./pages/Notifications"))
  },
  {
    path: "/profile/notifications/notification-settings",
    exact: true,
    auth: true,
    roles: [USER_TYPE.STUDENT],
    component: lazy(() => import("./pages/NotificationSettings"))
  }
];
