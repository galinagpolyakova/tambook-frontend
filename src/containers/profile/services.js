// @flow
import type { ApiServiceInterface } from "shared/services/ApiServiceInterface";
export class ProfileService {
  api: ApiServiceInterface;

  endpoint: string = "/user";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  _normalizeProfileInformation({
    userEmail,
    fullName,
    gender,
    phoneNo,
    birthDate,
    citizenship,
    nativeLanguage
  }: any) {
    return {
      userEmail,
      gender,
      phoneNo: phoneNo === null ? "" : phoneNo,
      fullName: fullName ? fullName : "",
      birthDate: birthDate === null ? "" : birthDate,
      citizenship,
      nativeLanguage
    };
  }

  _normalizeNotificationSettings({
    all_notification,
    change_fav_course,
    school_updates,
    school_update_regard_booking,
    special_offers,
    promotion,
    new_schools,
    new_course,
    booking_status_update,
    tambook_email
  }: any) {
    return {
      notificationSettings: {
        allNotification: all_notification,
        changeFavCourse: change_fav_course,
        schoolUpdates: school_updates,
        schoolUpdateRegardSchool: school_update_regard_booking,
        specialOffers: special_offers,
        promotion: promotion,
        newSchools: new_schools,
        newCourse: new_course,
        bookingStatusUpdate: booking_status_update,
        tambookEmail: tambook_email
      }
    };
  }

  _normalizeNotifications(ApiNotification: any) {
    let notifications = [];

    if (ApiNotification.length > 0) {
      ApiNotification.map(({ isView, message, createdAt }) => {
        return notifications.push({ message, isView, createdAt });
      });
    }
    return {
      notifications
    };
  }

  updateUser(payload: Object) {
    return this.api.patch(`${this.endpoint}/update`, payload);
  }

  getUser(payload: Object) {
    return this.api.post(`/user/search`, payload).then(response => {
      return {
        ...this._normalizeProfileInformation(response)
      };
    });
  }

  getNotificationSettings() {
    return this.api
      .get(`${this.endpoint}/notification/setting`)
      .then(response => {
        return {
          ...this._normalizeNotificationSettings(response.result)
        };
      });
  }

  updateNotificationSettings(payload: Object) {
    return this.api
      .post(`${this.endpoint}/notification/setting`, {
        ...payload,
        user_type: "student"
      })
      .then(response => {
        return {
          ...this._normalizeNotificationSettings(response.result)
        };
      });
  }

  getNotifications() {
    return this.api.get(`${this.endpoint}/notification`).then(response => {
      return {
        ...this._normalizeNotifications(response.result)
      };
    });
  }

  markAllNotificationsRead() {
    return this.api
      .post(`${this.endpoint}/notification/readall`)
      .then(response => {
        return {
          ...this._normalizeNotifications(response.result)
        };
      });
  }

  notifyPaymentConfirmation(payload) {
    return this.api.post(`${this.endpoint}/notification/send`, payload);
  }
}
