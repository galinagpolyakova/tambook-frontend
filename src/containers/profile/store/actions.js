import {
  PROFILE_ASYNC_INIT,
  GET_BOOKMARKED_COURSES_SUCCESS,
  GET_BOOKMARKED_COURSES_FAILURE,
  REMOVE_BOOKMARKED_COURSE_SUCCESS,
  GET_USER_PROFILE_SUCCESS,
  GET_USER_PROFILE_FAILURE,
  GET_USER_PROFILE_INIT,
  UPDATE_USER_PROFILE_INIT,
  UPDATE_USER_PROFILE_SUCCESS,
  UPDATE_USER_PROFILE_FAILURE,
  GET_BOOKINGS_SUCCESS,
  GET_CURRENCY_LIST_SUCCESS,
  SAVE_USER_PREFERENCES,
  GET_CURRENCY_RATE_INIT,
  GET_CURRENCY_RATE_SUCCESS,
  GET_CURRENCY_RATE_FAILURE,
  GET_USER_PREFERENCES,
  CLEAR_STUDENT_DATA,
  GET_COOKIE_CONSENT_STATUS,
  UPDATE_COOKIE_CONSENT_STATUS,
  GET_BOOKING_INIT,
  GET_BOOKINGS_INIT,
  GET_BOOKING_SUCCESS,
  GET_BOOKING_FAILURE,
  GET_NOTIFICATION_SETTINGS_INIT,
  GET_NOTIFICATION_SETTINGS_SUCCESS,
  GET_NOTIFICATION_SETTINGS_FAILURE,
  UPDATE_NOTIFICATION_SETTINGS_INIT,
  UPDATE_NOTIFICATION_SETTINGS_SUCCESS,
  UPDATE_NOTIFICATION_SETTINGS_FAILURE,
  GET_NOTIFICATIONS_INIT,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAILURE,
  MARK_ALL_NOTIFICATIONS_READ_INIT,
  MARK_ALL_NOTIFICATIONS_READ_SUCCESS,
  MARK_ALL_NOTIFICATIONS_READ_FAILURE,
  VALIDATE_FEEDBACK_SUCCESS
} from "./actionTypes";
import { ASYNC_STATUS } from "constants/async";

import { Auth } from "aws-amplify";

export function profileAsyncInit() {
  return {
    type: PROFILE_ASYNC_INIT
  };
}

function getBookmarkedCoursesSuccess(payload) {
  return {
    type: GET_BOOKMARKED_COURSES_SUCCESS,
    payload
  };
}

function getBookmarkedCoursesFailure() {
  return {
    type: GET_BOOKMARKED_COURSES_FAILURE
  };
}

export function getBookmarkedCourses(filter) {
  return (dispatch, getState, serviceManager) => {
    dispatch(profileAsyncInit());

    let courseService = serviceManager.get("CourseService");
    courseService
      .getBookmarkedCourses(filter)
      .then(({ courses, pagination }) =>
        dispatch(
          getBookmarkedCoursesSuccess({
            courses,
            pagination
          })
        )
      )
      .catch(err => {
        dispatch(getBookmarkedCoursesFailure(err));
      });
  };
}

function removeBookmarkedCourseSuccess(payload) {
  return {
    type: REMOVE_BOOKMARKED_COURSE_SUCCESS,
    payload
  };
}

export function removeBookmarkedCourse(courseId) {
  return (dispatch, getState, serviceManager) => {
    let courseService = serviceManager.get("CourseService");
    let {
      auth: { email }
    } = getState();

    courseService
      .removeCourseFromBookmarkList({ email, list: [courseId] })
      .then(() => dispatch(removeBookmarkedCourseSuccess({ id: courseId })));
  };
}

function getUserProfileSuccess(payload) {
  return {
    type: GET_USER_PROFILE_SUCCESS,
    payload
  };
}

function getUserProfileFailure(payload) {
  return {
    type: GET_USER_PROFILE_FAILURE,
    payload
  };
}

function getUserProfileInit() {
  return {
    type: GET_USER_PROFILE_INIT
  };
}

export function getUserProfile() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getUserProfileInit());

    let ProfileService = serviceManager.get("ProfileService");

    Auth.currentAuthenticatedUser().then(({ attributes: { email } }) => {
      ProfileService.getUser({ email: email })
        .then(user => dispatch(getUserProfileSuccess(user)))
        .catch(err => dispatch(getUserProfileFailure(err)));
    });
  };
}

function updateUserProfileInit() {
  return {
    type: UPDATE_USER_PROFILE_INIT
  };
}

function updateUserProfileSuccess(payload) {
  return {
    type: UPDATE_USER_PROFILE_SUCCESS,
    payload
  };
}

function updateUserProfileFailure(payload) {
  return {
    type: UPDATE_USER_PROFILE_FAILURE,
    payload
  };
}

export function updateProfile(payload) {
  return (dispatch, getState, serviceManager) => {
    dispatch(updateUserProfileInit());

    let ProfileService = serviceManager.get("ProfileService");
    ProfileService.updateUser(payload)
      .then(() =>
        dispatch(
          updateUserProfileSuccess({
            ...payload
          })
        )
      )
      .catch(err => dispatch(updateUserProfileFailure(err)));
  };
}

function getBookingsSuccess(payload) {
  return {
    type: GET_BOOKINGS_SUCCESS,
    payload
  };
}

export function getBookings(type, page) {
  return (dispatch, getState, serviceManager) => {
    let bookingService = serviceManager.get("BookingService");
    dispatch({
      type: GET_BOOKINGS_INIT,
      payload: { type }
    });
    bookingService.getBookings({ type, page }).then(bookings => {
      dispatch(getBookingsSuccess({ type, bookings }));
    });
  };
}

function getCurrencyListSuccess(payload) {
  return {
    type: GET_CURRENCY_LIST_SUCCESS,
    payload
  };
}

export function getCurrencyList() {
  return (dispatch, getState, serviceManager) => {
    let courseService = serviceManager.get("CourseService");
    courseService
      .getCurrencyList()
      .then(({ data: currencies }) =>
        dispatch(getCurrencyListSuccess({ currencies }))
      );
  };
}

function validateFeedbackSuccess(payload) {
  return {
    type: VALIDATE_FEEDBACK_SUCCESS,
    payload
  };
}

export function validateUserFeedback(data) {
  return (dispatch, getState, serviceManager) => {
    let reviewService = serviceManager.get("ReviewService");
    reviewService.validateUserFeedback(data).then(({ reviews }) => {
      dispatch(validateFeedbackSuccess(reviews));
    });
  };
}

export function getUserPreferences() {
  return {
    type: GET_USER_PREFERENCES
  };
}

export function saveUserPreferences(payload) {
  return {
    type: SAVE_USER_PREFERENCES,
    payload
  };
}

export function changeUserPassport(passport) {
  return (dispatch, getState) => {
    const {
      profile: { settings }
    } = getState();
    dispatch(saveUserPreferences({ ...settings, passport }));
  };
}

export function getCurrencyRate(currency) {
  return (dispatch, getState, serviceManager) => {
    let courseService = serviceManager.get("CourseService");
    const {
      profile: { rates }
    } = getState();
    if (
      (rates.list === null || rates.currency !== currency) &&
      rates.status !== ASYNC_STATUS.LOADING
    ) {
      dispatch({
        type: GET_CURRENCY_RATE_INIT,
        payload: { currency }
      });
      courseService
        .getCurrencyRates(currency)
        .then(({ data: { rates: list } }) =>
          dispatch({
            type: GET_CURRENCY_RATE_SUCCESS,
            payload: { list }
          })
        )
        .catch(() =>
          dispatch({
            type: GET_CURRENCY_RATE_FAILURE
          })
        );
    }
  };
}

export function clearStudentData() {
  return {
    type: CLEAR_STUDENT_DATA
  };
}

export function getCookieConsentStatus() {
  return {
    type: GET_COOKIE_CONSENT_STATUS
  };
}
export function updateCookieConsentStatus() {
  return {
    type: UPDATE_COOKIE_CONSENT_STATUS
  };
}

function getBookingInit() {
  return {
    type: GET_BOOKING_INIT
  };
}

function getBookingSuccess(payload) {
  return {
    type: GET_BOOKING_SUCCESS,
    payload
  };
}

function getBookingFailure(err) {
  return {
    type: GET_BOOKING_FAILURE,
    err
  };
}

export function getBooking(bookingId) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getBookingInit());
    let bookingService = serviceManager.get("BookingService");
    bookingService
      .getBooking(bookingId)
      .then(response => {
        let data = response.result;
        dispatch(
          getBookingSuccess({
            bookingId,
            data
          })
        );
      })
      .catch(err => dispatch(getBookingFailure(err)));
  };
}

function getNotificationSettingsInit() {
  return {
    type: GET_NOTIFICATION_SETTINGS_INIT
  };
}

function getNotificationSettingsSuccess(payload) {
  return {
    type: GET_NOTIFICATION_SETTINGS_SUCCESS,
    payload
  };
}

function getNotificationSettingsFailure(err) {
  return {
    type: GET_NOTIFICATION_SETTINGS_FAILURE,
    err
  };
}

export function getNotificationSettings() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getNotificationSettingsInit());

    let profileService = serviceManager.get("ProfileService");

    profileService
      .getNotificationSettings()
      .then(response => dispatch(getNotificationSettingsSuccess(response)))
      .catch(err => dispatch(getNotificationSettingsFailure(err)));
  };
}

function updateNotificationSettingsInit() {
  return {
    type: UPDATE_NOTIFICATION_SETTINGS_INIT
  };
}

function updateNotificationSettingsSuccess(payload) {
  return {
    type: UPDATE_NOTIFICATION_SETTINGS_SUCCESS,
    payload
  };
}

function updateNotificationSettingsFailure(payload) {
  return {
    type: UPDATE_NOTIFICATION_SETTINGS_FAILURE,
    payload
  };
}

export function updateNotificationSettings(payload) {
  return (dispatch, getState, serviceManager) => {
    dispatch(updateNotificationSettingsInit());

    let profileService = serviceManager.get("ProfileService");

    profileService
      .updateNotificationSettings(payload)
      .then(response => dispatch(updateNotificationSettingsSuccess(response)))
      .catch(err => dispatch(updateNotificationSettingsFailure(err)));
  };
}

function getNotificationsInit() {
  return {
    type: GET_NOTIFICATIONS_INIT
  };
}

function getNotificationsSuccess(payload) {
  return {
    type: GET_NOTIFICATIONS_SUCCESS,
    payload
  };
}

function getNotificationsFailure(payload) {
  return {
    type: GET_NOTIFICATIONS_FAILURE,
    payload
  };
}

export function getNotifications() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getNotificationsInit());

    let profileService = serviceManager.get("ProfileService");

    profileService
      .getNotifications()
      .then(response => dispatch(getNotificationsSuccess(response)))
      .catch(err => dispatch(getNotificationsFailure(err)));
  };
}

function markAllNotificationsReadInit() {
  return {
    type: MARK_ALL_NOTIFICATIONS_READ_INIT
  };
}

function markAllNotificationsReadSuccess(payload) {
  return {
    type: MARK_ALL_NOTIFICATIONS_READ_SUCCESS,
    payload
  };
}

function markAllNotificationsReadFailure(payload) {
  return {
    type: MARK_ALL_NOTIFICATIONS_READ_FAILURE,
    payload
  };
}

export function markAllNotificationsRead() {
  return (dispatch, getState, serviceManager) => {
    dispatch(markAllNotificationsReadInit());

    let profileService = serviceManager.get("ProfileService");

    profileService
      .markAllNotificationsRead()
      .then(response => dispatch(markAllNotificationsReadSuccess(response)))
      .catch(err => dispatch(markAllNotificationsReadFailure(err)));
  };
}
