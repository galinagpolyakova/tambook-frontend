// @flow
import { type Action } from "shared/types/ReducerAction";
import { type PaginationType } from "shared/components/Pagination";
import { type notificationSettings } from "../types";
import { saveData, loadData } from "shared/kernel/localStorage";
import { BOOKING_STATUS } from "containers/profile/constants";

import {
  GET_BOOKMARKED_COURSES_SUCCESS,
  REMOVE_BOOKMARKED_COURSE_SUCCESS,
  GET_USER_PROFILE_SUCCESS,
  GET_USER_PROFILE_FAILURE,
  GET_USER_PROFILE_INIT,
  GET_BOOKINGS_SUCCESS,
  GET_CURRENCY_LIST_SUCCESS,
  SAVE_USER_PREFERENCES,
  GET_CURRENCY_RATE_SUCCESS,
  GET_USER_PREFERENCES,
  PROFILE_ASYNC_INIT,
  UPDATE_USER_PROFILE_INIT,
  UPDATE_USER_PROFILE_SUCCESS,
  UPDATE_USER_PROFILE_FAILURE,
  CLEAR_STUDENT_DATA,
  GET_COOKIE_CONSENT_STATUS,
  UPDATE_COOKIE_CONSENT_STATUS,
  GET_BOOKING_INIT,
  GET_BOOKING_SUCCESS,
  GET_BOOKING_FAILURE,
  GET_NOTIFICATION_SETTINGS_INIT,
  GET_NOTIFICATION_SETTINGS_SUCCESS,
  GET_NOTIFICATION_SETTINGS_FAILURE,
  UPDATE_NOTIFICATION_SETTINGS_INIT,
  UPDATE_NOTIFICATION_SETTINGS_SUCCESS,
  UPDATE_NOTIFICATION_SETTINGS_FAILURE,
  GET_NOTIFICATIONS_INIT,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAILURE,
  MARK_ALL_NOTIFICATIONS_READ_INIT,
  MARK_ALL_NOTIFICATIONS_READ_SUCCESS,
  MARK_ALL_NOTIFICATIONS_READ_FAILURE,
  VALIDATE_FEEDBACK_SUCCESS,
  GET_BOOKINGS_INIT,
  GET_CURRENCY_RATE_INIT,
  GET_CURRENCY_RATE_FAILURE
} from "./actionTypes";
import { ASYNC_STATUS } from "constants/async";

export type UserSettingsType = {
  language: string,
  passport: string,
  currency: string
};

export type ProfileState = {
  loading: boolean,
  error: null | string,
  isCookieConsentGiven: null | boolean,
  bookmarkedCourses: {
    pagination: PaginationType,
    courses: Array<any> | null
  },
  activeBookings: Array<Object>,
  review: null | Object,
  archivedBookings: Array<Object>,
  userProfile: {
    name: string,
    email: string,
    gender: string,
    birthDate: string,
    citizenship: string,
    language: string
  } | null,
  shouldCoursesLoad: boolean,
  currencies: null | Object,
  rates: null | Object,
  settings: UserSettingsType,
  booking: Object,
  isBookInfoLoading: boolean,
  isBookingInfoLoaded: boolean,
  notificationSettings: notificationSettings,
  isNotificationsUpdated: boolean,
  notifications: Array<Object | null>
};

const initialState: ProfileState = {
  loading: false,
  isLoaded: false,
  isBookInfoLoading: false,
  isBookingInfoLoaded: false,
  error: null,
  isCookieConsentGiven: null,
  bookmarkedCourses: {
    pagination: null,
    courses: null
  },
  activeBookings: null,
  archivedBookings: null,
  unfinishedBookings: null,
  review: {
    tambookReview: null,
    courseReview: null
  },
  userProfile: null,
  shouldCoursesLoad: false,
  currencies: null,
  rates: {
    status: ASYNC_STATUS.INIT,
    list: null,
    currency: null
  },
  settings: {
    language: "en_US",
    passport: "US",
    currency: "USD"
  },
  booking: {
    bookingId: null,
    data: null
  },
  notificationSettings: {
    allNotification: false,
    changeFavCourse: false,
    schoolUpdateRegardSchool: false,
    specialOffers: false,
    promotion: false,
    tambookEmail: false,
    newSchools: false,
    newCourse: false,
    schoolUpdates: false,
    bookingStatusUpdate: false
  },
  isNotificationsUpdated: false,
  notifications: []
};

function profileAsyncInit(state) {
  return {
    ...state,
    loading: true
  };
}

function getBookmarkedCoursesSuccess(
  state: ProfileState,
  { courses, pagination }
) {
  return {
    ...state,
    loading: false,
    bookmarkedCourses: {
      courses,
      pagination
    },
    shouldCoursesLoad: false
  };
}

function removeBookmarkedCourseSuccess(state: ProfileState) {
  return {
    ...state,
    shouldCoursesLoad: true,
    bookmarkedCourses: {
      pagination: null,
      courses: null
    }
  };
}

function getUserProfileInit(state: ProfileState) {
  return {
    ...state,
    error: null,
    loading: true
  };
}

function getUserProfileSuccess(state: ProfileState, userProfile) {
  return {
    ...state,
    loading: false,
    isLoaded: true,
    userProfile
  };
}

function getUserProfileFailure(state: ProfileState, error) {
  return {
    ...state,
    loading: false,
    error
  };
}

function getBookingsSuccess(state: ProfileState, { type, bookings }) {
  let dataType = "";
  switch (type) {
    case BOOKING_STATUS.ACTIVE:
      dataType = "activeBookings";
      break;
    case BOOKING_STATUS.ARCHIVE:
      dataType = "archivedBookings";
      break;
    case BOOKING_STATUS.UNFINISHED:
      dataType = "unfinishedBookings";
      break;
    default:
      dataType = undefined;
      break;
  }
  return {
    ...state,
    loading: false,
    [dataType]: bookings
  };
}

function getCurrencyListSuccess(state, { currencies }) {
  return {
    ...state,
    currencies
  };
}

function getUserPreferences(state) {
  const settings =
    loadData("userSettings") !== undefined
      ? loadData("userSettings")
      : state.settings;
  return {
    ...state,
    settings
  };
}

function saveUserPreferences(state, settings) {
  saveData("userSettings", settings);
  return {
    ...state,
    settings
  };
}

function getCurrencyRateInit(state, { currency }) {
  return {
    ...state,
    rates: {
      currency,
      list: null,
      status: ASYNC_STATUS.LOADING
    }
  };
}
function getCurrencyRateFailure(state) {
  return {
    ...state,
    rates: {
      ...state.rates,
      list: null,
      status: ASYNC_STATUS.LOADING
    }
  };
}

function getCurrencyRateSuccess(state, { list }) {
  return {
    ...state,
    rates: {
      ...state.rates,
      list,
      status: ASYNC_STATUS.INIT
    }
  };
}

function updateUserProfileInit(state) {
  return {
    ...state,
    loading: true,
    error: null
  };
}

function updateUserProfileSuccess(state, userProfile) {
  return {
    ...state,
    userProfile,
    loading: false
  };
}

function updateUserProfileFailure(state, error) {
  return {
    ...state,
    loading: false,
    error
  };
}

function getCookieConsentStatus(state) {
  const isCookieConsentGiven =
    loadData("CookieConsent") !== undefined ? loadData("CookieConsent") : false;
  return {
    ...state,
    isCookieConsentGiven
  };
}

function updateCookieConsentStatus(state) {
  saveData("CookieConsent", true);
  return {
    ...state,
    isCookieConsentGiven: true
  };
}

function clearStudentData() {
  return initialState;
}

function getBookingInit(state) {
  return {
    ...state,
    isBookInfoLoading: true,
    isBookingInfoLoaded: false
  };
}

function getBookingSuccess(state, { bookingId, data }) {
  return {
    ...state,
    isBookInfoLoading: false,
    isBookingInfoLoaded: true,
    booking: {
      bookingId: bookingId,
      data: data
    }
  };
}

function getBookingFailure(state) {
  return {
    ...state,
    isBookInfoLoading: false
  };
}

function getNotificationSettingsInit(state) {
  return {
    ...state,
    loading: true,
    isNotificationsUpdated: false,
    error: null
  };
}

function getNotificationSettingsSuccess(state, { notificationSettings }) {
  return {
    ...state,
    loading: false,
    notificationSettings
  };
}

function getNotificationSettingsFailure(state) {
  return {
    ...state,
    loading: false
  };
}

function updateNotificationSettingsInit(state) {
  return {
    ...state,
    loading: true,
    isNotificationsUpdated: false,
    error: null
  };
}

function updateNotificationSettingsSuccess(state, { notificationSettings }) {
  return {
    ...state,
    loading: false,
    isNotificationsUpdated: true,
    notificationSettings
  };
}

function updateNotificationSettingsFailure(state) {
  return {
    ...state,
    loading: false,
    isNotificationsUpdated: false,
    error: "Sorry, Something went wrong, Please try again.."
  };
}

function getNotificationsInit(state) {
  return {
    ...state,
    loading: true
  };
}

function getNotificationsSuccess(state, { notifications }) {
  return {
    ...state,
    loading: false,
    notifications
  };
}

function getNotificationsFailure(state) {
  return {
    ...state,
    loading: false
  };
}

function markAllNotificationsReadInit(state) {
  return {
    ...state,
    loading: true
  };
}

function markAllNotificationsReadSuccess(state, { notifications }) {
  return {
    ...state,
    loading: false,
    notifications
  };
}

function markAllNotificationsReadFailure(state) {
  return {
    ...state,
    loading: false
  };
}

function validateFeedbackSuccess(state: ProfileState, review) {
  return {
    ...state,
    review
  };
}

function getBookingsInit(state, { type }) {
  let dataType = "";
  switch (type) {
    case BOOKING_STATUS.ACTIVE:
      dataType = "activeBookings";
      break;
    case BOOKING_STATUS.ARCHIVE:
      dataType = "archivedBookings";
      break;
    case BOOKING_STATUS.UNFINISHED:
      dataType = "unfinishedBookings";
      break;
    default:
      dataType = undefined;
      break;
  }
  return {
    ...state,
    loading: true,
    [dataType]: null
  };
}

const reducer = (
  state: ProfileState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case PROFILE_ASYNC_INIT:
      return profileAsyncInit(state);
    case GET_BOOKMARKED_COURSES_SUCCESS:
      return getBookmarkedCoursesSuccess(state, payload);
    case REMOVE_BOOKMARKED_COURSE_SUCCESS:
      return removeBookmarkedCourseSuccess(state);
    case GET_USER_PROFILE_INIT:
      return getUserProfileInit(state);
    case GET_USER_PROFILE_SUCCESS:
      return getUserProfileSuccess(state, payload);
    case GET_USER_PROFILE_FAILURE:
      return getUserProfileFailure(state, payload);
    case GET_BOOKINGS_SUCCESS:
      return getBookingsSuccess(state, payload);
    case GET_CURRENCY_LIST_SUCCESS:
      return getCurrencyListSuccess(state, payload);
    case GET_USER_PREFERENCES:
      return getUserPreferences(state);
    case SAVE_USER_PREFERENCES:
      return saveUserPreferences(state, payload);
    case GET_CURRENCY_RATE_INIT:
      return getCurrencyRateInit(state, payload);
    case GET_CURRENCY_RATE_FAILURE:
      return getCurrencyRateFailure(state);
    case GET_CURRENCY_RATE_SUCCESS:
      return getCurrencyRateSuccess(state, payload);
    case UPDATE_USER_PROFILE_INIT:
      return updateUserProfileInit(state);
    case UPDATE_USER_PROFILE_SUCCESS:
      return updateUserProfileSuccess(state, payload);
    case UPDATE_USER_PROFILE_FAILURE:
      return updateUserProfileFailure(state, payload);
    case GET_COOKIE_CONSENT_STATUS:
      return getCookieConsentStatus(state);
    case UPDATE_COOKIE_CONSENT_STATUS:
      return updateCookieConsentStatus(state);
    case CLEAR_STUDENT_DATA:
      return clearStudentData();
    case GET_BOOKING_INIT:
      return getBookingInit(state);
    case GET_BOOKING_SUCCESS:
      return getBookingSuccess(state, payload);
    case GET_BOOKING_FAILURE:
      return getBookingFailure(state);
    case GET_NOTIFICATION_SETTINGS_INIT:
      return getNotificationSettingsInit(state);
    case GET_NOTIFICATION_SETTINGS_SUCCESS:
      return getNotificationSettingsSuccess(state, payload);
    case GET_NOTIFICATION_SETTINGS_FAILURE:
      return getNotificationSettingsFailure(state);
    case UPDATE_NOTIFICATION_SETTINGS_INIT:
      return updateNotificationSettingsInit(state);
    case UPDATE_NOTIFICATION_SETTINGS_SUCCESS:
      return updateNotificationSettingsSuccess(state, payload);
    case UPDATE_NOTIFICATION_SETTINGS_FAILURE:
      return updateNotificationSettingsFailure(state);
    case GET_NOTIFICATIONS_INIT:
      return getNotificationsInit(state);
    case GET_NOTIFICATIONS_SUCCESS:
      return getNotificationsSuccess(state, payload);
    case GET_NOTIFICATIONS_FAILURE:
      return getNotificationsFailure(state);
    case MARK_ALL_NOTIFICATIONS_READ_INIT:
      return markAllNotificationsReadInit(state);
    case MARK_ALL_NOTIFICATIONS_READ_SUCCESS:
      return markAllNotificationsReadSuccess(state, payload);
    case MARK_ALL_NOTIFICATIONS_READ_FAILURE:
      return markAllNotificationsReadFailure(state);
    case VALIDATE_FEEDBACK_SUCCESS:
      return validateFeedbackSuccess(state, payload);
    case GET_BOOKINGS_INIT:
      return getBookingsInit(state, payload);
    default:
      return state;
  }
};

export default reducer;
