// @flow
import { type ApplicationState } from "store/reducers";
import { type BookingDetailsProps } from "../pages/Booking";
import { createSelector } from "reselect";

export const getCourseBookmarkList = (state: ApplicationState) =>
  state.profile.bookmarkedCourses.courses;

export const getCourseCompareList = (state: ApplicationState) =>
  state.course.compareList;

export const isCourseBookmarksListLoaded = (createSelector(
  [getCourseBookmarkList],
  (bookmarkedCourses): boolean => {
    return bookmarkedCourses !== null && bookmarkedCourses.length > 0;
  }
): (state: ApplicationState) => boolean);

export const getConsolidatedCourseBookmarkList = createSelector(
  [getCourseBookmarkList, getCourseCompareList],
  (courses, comparedCourses) => {
    const consolidatedCourses = [];
    if (courses === null) {
      return null;
    }
    for (let course of courses) {
      course.isBookmarked = true;
      consolidatedCourses.push(course);
      let comparedCourse = comparedCourses.find(courseItem => {
        return course.id === courseItem;
      });
      course.isAddedToCompare = comparedCourse !== undefined;
    }
    return consolidatedCourses;
  }
);

export const getBookingData = (state: ApplicationState) =>
  state.profile.booking.data;

export const getBooking = (state: ApplicationState) => state.profile.booking;

export const getBookingId = (
  state: ApplicationState,
  props: BookingDetailsProps
) => props.match.params.id;

export const getUserPassport = state => state.profile.settings.passport;
