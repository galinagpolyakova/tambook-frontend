// @flow
export type Booking = {
  id: string,
  title: string,
  duration: number,
  date: string,
  status: string,
  total: number
};
export type BookingList = Array<Booking>;

export type notificationSettings = {
  allNotification: boolean,
  changeFavCourse: boolean,
  schoolUpdateRegardSchool: boolean,
  specialOffers: boolean,
  promotion: boolean,
  tambookEmail: boolean,
  newSchools: boolean,
  newCourse: boolean,
  schoolUpdates: boolean,
  bookingStatusUpdate: boolean
};
