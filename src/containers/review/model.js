// @flow
import type { ReviewType, ApiReviewType } from "./types";
import type { SerializableInterface } from "../../shared/SerializableInterface";
import { isNotEmpty } from "shared/utils";
import missingImage from "../../assets/missing-profile-picture.png";
import { API_URL } from "config/app";

export class Review implements SerializableInterface<ReviewType> {
  name: $PropertyType<ReviewType, "name">;
  profilePic: $PropertyType<ReviewType, "profilePic">;
  tag: $PropertyType<ReviewType, "tag">;
  comment: $PropertyType<ReviewType, "comment">;
  rating: $PropertyType<ReviewType, "rating">;
  country: $PropertyType<ReviewType, "country">;

  constructor(apiReview: ApiReviewType) {
    this.name = apiReview.name;

    this.profilePic = isNotEmpty(apiReview.profile_pic)
      ? `${API_URL}/schools/image${apiReview.profile_pic}?width=110&height=110`
      : missingImage;
    this.tag = apiReview.tag;
    this.comment = apiReview.comment_en;
    this.rating = apiReview.rating;

    this.country = apiReview.country;
  }

  static fromReviewApi(apiReview: ApiReviewType) {
    return new this(apiReview);
  }

  toJSON(): ReviewType {
    return this;
  }
}
