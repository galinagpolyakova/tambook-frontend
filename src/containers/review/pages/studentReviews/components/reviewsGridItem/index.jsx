// @flow
import React from "react";

import Review from "components/Review";

type ReviewGridItemProps = {
  comment: string,
  name: string,
  image: string,
  rating: number
};
function ReviewGridItem(props: ReviewGridItemProps) {
  return (
    <Review
      name={props.name}
      comment={props.comment}
      image={props.image}
      rating={props.rating}
    />
  );
}

export default ReviewGridItem;
