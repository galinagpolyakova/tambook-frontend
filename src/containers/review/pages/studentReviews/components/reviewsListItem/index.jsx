// @flow
import React from "react";

import Review from "components/Review";

type ReviewListItemProps = {
  comment: string,
  name: string,
  image: string,
  rating: number
};

export default function ReviewListItem(props: ReviewListItemProps) {
  return (
    <Review
      name={props.name}
      comment={props.comment}
      image={props.image}
      rating={props.rating}
    />
  );
}
