// @flow
import React, { PureComponent, createRef } from "react";
import { connect } from "react-redux";

import { type StudentReviewType } from "../../types";
import Pagination, { type PaginationType } from "shared/components/Pagination";

import PageHeader from "components/PageHeader";
import Row from "shared/components/Row";
import Col from "shared/components/Col";
import Select from "shared/components/Select";
import SortButton from "shared/components/SortButton";
import { ItemsLayout, ItemsLayoutToggler } from "shared/components/ItemsLayout";
import translate from "translations/translate";
import { getStudentReviews } from "../../store/actions";
import Loader from "components/Loader";
import { UserSettingsConsumer } from "contexts/UserSettings";
import ReviewGridItem from "../studentReviews/components/reviewsGridItem";
import ReviewListItem from "../studentReviews/components/reviewsListItem";
import countries from "shared/data/countries";
import { getLanguageFromUrl } from "shared/helpers/translations";
import { queryParamsParse, stringifyQueryParams } from "shared/helpers/url";

import "./styles.scss";

type StudentReviewsProps = {
  getStudentReviews: Function,
  loading: boolean,
  reviews: Array<Object>,
  pagination: PaginationType,
  location: {
    search: string
  },
  history: any
};

class StudentReviews extends PureComponent<StudentReviewsProps> {
  constructor(props) {
    super(props);
    // $FlowFixMe
    this.filterReviews = this.filterReviews.bind(this);
  }

  myRef = createRef();

  componentDidMount() {
    const {
      getStudentReviews,
      location: { search }
    } = this.props;
    getStudentReviews(queryParamsParse(search));
  }

  filterReviews(filter: Object) {
    const {
      location: { search },
      history,
      getStudentReviews
    } = this.props;
    const language = getLanguageFromUrl();
    filter.page = filter.page ? filter.page : 1;

    const query = {
      ...queryParamsParse(search),
      ...filter
    };
    history.push({
      pathname: `/${language}/reviews`,
      search: stringifyQueryParams(query)
    });
    getStudentReviews(query);
    window.scrollTo(0, this.myRef.current.offsetTop - 40);
  }

  render() {
    const countryList = [{ value: "", name: "All countries" }];
    for (const key in countries) {
      if (countries.hasOwnProperty(key)) {
        countryList.push({
          value: countries[key],
          name: countries[key]
        });
      }
    }
    const {
      reviews,
      loading,
      pagination,
      location: { search }
    } = this.props;
    const { ratingSort, country } = queryParamsParse(search);
    let reviewTotal = pagination ? pagination.items : 0;
    return (
      <div>
        <PageHeader
          title={translate("STUDENT REVIEWS")}
          subheading={`${translate("We Found")}: ${reviewTotal} ${translate(
            "reviews"
          )}`}
          breadcrumbs={[
            {
              title: translate("REVIEWS")
            }
          ]}
        />
        <UserSettingsConsumer>
          {({ viewMode, changeViewMode }) => (
            <div className="container review-page">
              <Row>
                <Col>
                  <div className="page-filters">
                    <div className="filters">
                      <Select
                        defaultSelected={true}
                        onChange={country => this.filterReviews({ country })}
                        selected={country}
                        options={countryList}
                      />
                      <SortButton
                        onClick={this.filterReviews}
                        filter="ratingSort"
                        sorting={ratingSort}
                        selectedSorting={SortButton.SELECTED_SORTING.RATING}
                        sortingType={SortButton.SELECTED_SORTING.RATING}
                      >
                        {translate("Sort by ranking")}
                      </SortButton>
                    </div>
                    <div className="layout-switcher">
                      <div className="label">{translate("View")}: </div>
                      <ItemsLayoutToggler
                        viewMode={viewMode}
                        changeViewMode={changeViewMode}
                      />
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  {loading ? (
                    <Loader isLoading />
                  ) : (
                    <div ref={this.myRef}>
                      <ItemsLayout
                        items={reviews}
                        viewMode={viewMode}
                        gridByLine={2}
                        gridItemComponent={(props: {
                          item: StudentReviewType
                        }) => {
                          return <ReviewGridItem {...props.item} />;
                        }}
                        listItemComponent={(props: {
                          item: StudentReviewType
                        }) => {
                          return <ReviewListItem {...props.item} />;
                        }}
                      />
                      {pagination !== null && (
                        <Pagination
                          totalPages={pagination.pages}
                          currentPage={pagination.currentPage}
                          onPageChanged={this.filterReviews}
                        />
                      )}
                    </div>
                  )}
                </Col>
              </Row>
            </div>
          )}
        </UserSettingsConsumer>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    reviews: state.review.studentReviews,
    loading: state.review.loading,
    pagination: state.review.pagination
  };
}

const Actions = {
  getStudentReviews
};

export default connect(
  mapStateToProps,
  Actions
)(StudentReviews);
