// import lib
import { lazy } from "react";

export default [
  {
    path: "/reviews",
    exact: true,
    component: lazy(() => import("./pages/studentReviews"))
  }
];
