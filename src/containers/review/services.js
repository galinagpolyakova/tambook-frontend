// @flow
import type { ApiServiceInterface } from "../../shared/services/ApiServiceInterface";
import type {
  UserReviewType,
  ReviewsListType,
  ApiStudentReviewListType,
  ApiStudentReviewType
} from "./types";
import { Review } from "./model";
import { API_URL } from "config/app";
import { isCompleteUrl, isNotEmpty } from "shared/utils";
import missingImage from "../../assets/missing-profile-picture.png";

export class ReviewService {
  api: ApiServiceInterface;

  endpoint: string = "/reviews";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  _normalizeReviews(apiReviews: any): ReviewsListType {
    const reviews = apiReviews.map(apiReview => {
      return Review.fromReviewApi(apiReview);
    });
    return reviews;
  }

  getFeaturedReviews() {
    return this.api._fetch("get", this.endpoint).then(response => {
      return this._normalizeReviews(response.result.Items);
    });
  }

  _transformStudentReviewData({
    name,
    profile_pic,
    rating,
    comment
  }: ApiStudentReviewType) {
    const image = isNotEmpty(profile_pic)
      ? isCompleteUrl(profile_pic)
        ? `${API_URL}/schools/image${profile_pic}?width=120&height=120`
        : `${API_URL}schools/image/${profile_pic}?width=120&height=120`
      : missingImage;
    return {
      name,
      comment,
      image,
      rating
    };
  }

  _normalizeStudentReviews(apiReviews: ApiStudentReviewListType) {
    const reviews = [];
    if (apiReviews.length > 0) {
      apiReviews.map(apiReview => {
        let review = this._transformStudentReviewData(apiReview);
        reviews.push(review);
        return true;
      });
    }
    return {
      reviews
    };
  }

  getStudentReviews(filters: Object = {}) {
    return this.api
      .get(`${this.endpoint}/tambook`, filters)
      .then(({ results }) => {
        return {
          ...this._normalizeStudentReviews(results.data),
          pagination: {
            items: results.pagination.total,
            currentPage: results.pagination.current,
            pages: results.pagination.total_pages,
            pageSize: results.pagination.per_page
          }
        };
      });
  }

  createCourseReview(payload: Object) {
    return this.api.post(`${this.endpoint}/course`, payload);
  }

  createTambookReview(payload: Object) {
    return this.api.post(`${this.endpoint}/tambook`, payload);
  }

  _normalizeUserFeedback(userReviews: UserReviewType) {
    let courseReview = [];
    let tambookReview = [];
    if (userReviews.courseReview.length > 0) {
      userReviews.courseReview.map(review => {
        courseReview.push(review);
        return true;
      });
    }
    if (userReviews.tambookReview.length > 0) {
      userReviews.tambookReview.map(review => {
        tambookReview.push(review);
        return true;
      });
    }
    return {
      reviews: {
        courseReview,
        tambookReview
      }
    };
  }

  validateUserFeedback(filters: Object = {}) {
    return this.api
      ._fetch("get", `${this.endpoint}/validate-feedback`, {}, { ...filters })
      .then(response => {
        return {
          ...this._normalizeUserFeedback(response.data)
        };
      });
  }

  updateCourseReview(payload: Object) {
    return this.api.patch(`${this.endpoint}/course`, payload);
  }

  updateTambookReview(payload: Object) {
    return this.api.patch(`${this.endpoint}/tambook`, payload);
  }
}
