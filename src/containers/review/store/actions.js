import {
  GET_FEATURED_REVIEWS_INIT,
  GET_FEATURED_REVIEWS_SUCCESS,
  GET_FEATURED_REVIEWS_FAILURE,
  GET_STUDENT_REVIEWS_INIT,
  GET_STUDENT_REVIEWS_SUCCESS,
  GET_STUDENT_REVIEWS_FAILURE,
  SAVE_FEEDBACK_INIT,
  SAVE_FEEDBACK_SUCCESS,
  SAVE_FEEDBACK_FAILURE,
  RESET_NOTIFICATION,
  UPDATE_FEEDBACK_INIT,
  UPDATE_FEEDBACK_SUCCESS,
  UPDATE_FEEDBACK_FAILURE
} from "./actionTypes";

function getFeaturedReviewsInit() {
  return {
    type: GET_FEATURED_REVIEWS_INIT
  };
}

function getFeaturedReviewsSuccess(payload) {
  return {
    type: GET_FEATURED_REVIEWS_SUCCESS,
    payload
  };
}

function getFeaturedReviewsFailure(payload) {
  return {
    type: GET_FEATURED_REVIEWS_FAILURE,
    payload
  };
}

export function getFeaturedReviews() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getFeaturedReviewsInit());

    let courseService = serviceManager.get("ReviewService");
    courseService
      .getFeaturedReviews()
      .then(data =>
        dispatch(
          getFeaturedReviewsSuccess({
            featuredReviews: data
          })
        )
      )
      .catch(err => {
        dispatch(getFeaturedReviewsFailure(err));
      });
  };
}

function getStudentReviewsInit() {
  return {
    type: GET_STUDENT_REVIEWS_INIT
  };
}

function getStudentReviewsSuccess(payload) {
  return {
    type: GET_STUDENT_REVIEWS_SUCCESS,
    payload
  };
}

function saveFeedbackSuccess(payload) {
  return {
    type: SAVE_FEEDBACK_SUCCESS,
    payload
  };
}

function saveFeedbackFailure(payload) {
  return {
    type: SAVE_FEEDBACK_FAILURE,
    payload
  };
}

function updateFeedbackSuccess(payload) {
  return {
    type: UPDATE_FEEDBACK_SUCCESS,
    payload
  };
}

function updateFeedbackFailure(payload) {
  return {
    type: UPDATE_FEEDBACK_FAILURE,
    payload
  };
}

export function saveFeedback(data, isCourseReview) {
  return (dispatch, getState, serviceManager) => {
    dispatch({
      type: SAVE_FEEDBACK_INIT
    });

    let reviewService = serviceManager.get("ReviewService");

    if (isCourseReview) {
      reviewService
        .createCourseReview(data)
        .then(({ message }) => {
          dispatch(saveFeedbackSuccess({ message }));
        })
        .catch(message => {
          dispatch(saveFeedbackFailure({ message }));
        });
    } else {
      reviewService
        .createTambookReview(data)
        .then(({ message }) => {
          dispatch(saveFeedbackSuccess({ message }));
        })
        .catch(message => {
          dispatch(saveFeedbackFailure({ message }));
        });
    }
  };
}

export function updateFeedback(data, isCourseReview) {
  return (dispatch, getState, serviceManager) => {
    dispatch({
      type: UPDATE_FEEDBACK_INIT
    });

    let reviewService = serviceManager.get("ReviewService");

    if (isCourseReview) {
      reviewService
        .updateCourseReview(data)
        .then(({ message }) => {
          dispatch(updateFeedbackSuccess({ message }));
        })
        .catch(message => {
          dispatch(updateFeedbackFailure({ message }));
        });
    } else {
      reviewService
        .updateTambookReview(data)
        .then(({ message }) => {
          dispatch(updateFeedbackSuccess({ message }));
        })
        .catch(message => {
          dispatch(updateFeedbackFailure({ message }));
        });
    }
  };
}

function getStudentReviewsFailure(payload) {
  return {
    type: GET_STUDENT_REVIEWS_FAILURE,
    payload
  };
}

export function getStudentReviews(filters) {
  return (dispatch, getState, serviceManager) => {
    dispatch(getStudentReviewsInit());

    let reviewService = serviceManager.get("ReviewService");

    reviewService
      .getStudentReviews(filters)
      .then(response => dispatch(getStudentReviewsSuccess(response)))
      .catch(err => dispatch(getStudentReviewsFailure(err)));
  };
}

export function resetNotification() {
  return {
    type: RESET_NOTIFICATION
  };
}
