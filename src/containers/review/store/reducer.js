// @flow
import type { Action } from "shared/types/ReducerAction";
import { type PaginationType } from "shared/components/Pagination";
import { type StudentReviewListType } from "../types";
import { type AsyncStatusType, type NotificationType } from "types/general";
import {
  GET_FEATURED_REVIEWS_INIT,
  GET_FEATURED_REVIEWS_SUCCESS,
  GET_FEATURED_REVIEWS_FAILURE,
  GET_STUDENT_REVIEWS_INIT,
  GET_STUDENT_REVIEWS_SUCCESS,
  GET_STUDENT_REVIEWS_FAILURE,
  SAVE_FEEDBACK_INIT,
  SAVE_FEEDBACK_SUCCESS,
  SAVE_FEEDBACK_FAILURE,
  RESET_NOTIFICATION,
  UPDATE_FEEDBACK_INIT,
  UPDATE_FEEDBACK_SUCCESS,
  UPDATE_FEEDBACK_FAILURE
} from "./actionTypes";
import { ASYNC_STATUS } from "constants/async";
import Alert from "shared/components/Alert/Alert";

export type ReviewState = {
  loading: boolean,
  featuredReviews: Array<any>,
  studentReviews: StudentReviewListType,
  pagination: PaginationType,
  status: AsyncStatusType,
  notification: NotificationType
};

const initialState: ReviewState = {
  loading: false,
  featuredReviews: [],
  pagination: null,
  studentReviews: [],
  status: ASYNC_STATUS.INIT,
  notification: null
};

function getFeaturedReviewsInit(state) {
  return {
    ...state,
    loading: true
  };
}

function getFeaturedReviewsSuccess(state, { featuredReviews }) {
  return {
    ...state,
    loading: false,
    featuredReviews
  };
}

function getFeaturedReviewsFailure(state, { notifications }) {
  return {
    ...state,
    notifications,
    loading: false
  };
}

function getStudentReviewsInit(state) {
  return {
    ...state,
    loading: true
  };
}

function getStudentReviewsSuccess(state, { reviews, pagination }) {
  return {
    ...state,
    loading: false,
    pagination,
    studentReviews: reviews
  };
}

function getStudentReviewsFailure(state) {
  return {
    ...state,
    loading: false
  };
}

function saveFeedbackSuccess(state: ReviewState, { message }) {
  return {
    ...state,
    status: ASYNC_STATUS.SUCCESS,
    notification: {
      message,
      type: Alert.TYPE.SUCCESS
    }
  };
}

function saveFeedbackFailure(state: ReviewState, { message }): ReviewState {
  return {
    ...state,
    status: ASYNC_STATUS.FAILURE,
    notification: {
      message,
      type: Alert.TYPE.ERROR
    }
  };
}

function saveFeedbackInit(state: ReviewState) {
  return {
    ...state,
    status: ASYNC_STATUS.LOADING
  };
}

function resetNotification(state: ReviewState) {
  return {
    ...state,
    status: ASYNC_STATUS.INIT,
    notification: null
  };
}

function updateFeedbackSuccess(state: ReviewState, { message }) {
  return {
    ...state,
    status: ASYNC_STATUS.SUCCESS,
    notification: {
      message,
      type: Alert.TYPE.SUCCESS
    }
  };
}

function updateFeedbackFailure(state: ReviewState, { message }): ReviewState {
  return {
    ...state,
    status: ASYNC_STATUS.FAILURE,
    notification: {
      message,
      type: Alert.TYPE.ERROR
    }
  };
}

function updateFeedbackInit(state: ReviewState) {
  return {
    ...state,
    status: ASYNC_STATUS.LOADING
  };
}

const reducer = (
  state: ReviewState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case GET_FEATURED_REVIEWS_INIT:
      return getFeaturedReviewsInit(state);
    case GET_FEATURED_REVIEWS_SUCCESS:
      return getFeaturedReviewsSuccess(state, payload);
    case GET_FEATURED_REVIEWS_FAILURE:
      return getFeaturedReviewsFailure(state, payload);
    case GET_STUDENT_REVIEWS_INIT:
      return getStudentReviewsInit(state);
    case GET_STUDENT_REVIEWS_SUCCESS:
      return getStudentReviewsSuccess(state, payload);
    case GET_STUDENT_REVIEWS_FAILURE:
      return getStudentReviewsFailure(state);
    case SAVE_FEEDBACK_INIT:
      return saveFeedbackInit(state);
    case SAVE_FEEDBACK_SUCCESS:
      return saveFeedbackSuccess(state, payload);
    case SAVE_FEEDBACK_FAILURE:
      return saveFeedbackFailure(state, payload);
    case RESET_NOTIFICATION:
      return resetNotification(state);
    case UPDATE_FEEDBACK_INIT:
      return updateFeedbackInit(state);
    case UPDATE_FEEDBACK_SUCCESS:
      return updateFeedbackSuccess(state, payload);
    case UPDATE_FEEDBACK_FAILURE:
      return updateFeedbackFailure(state, payload);
    default:
      return state;
  }
};

export default reducer;
