// @flow
export type ReviewType = {
  name: string,
  profilePic: string,
  tag: string,
  comment: string,
  rating: number,
  country: string
};

export type ReviewsListType = Array<ReviewType>;

export type ApiReviewType = {
  active: string,
  comment_es: string,
  rating: number,
  comment_en: string,
  comment_ru: string,
  comment_pt: string,
  tag: string,
  id: number,
  country: string,
  name: string,
  profile_pic: string
};

export type ApiReviewsListType = Array<ApiReviewType>;

export type ApiCourseReviewType = {
  active: string,
  countryCode: string,
  detailPicture: string,
  id: number,
  rate: number,
  reviewText: string,
  school: number,
  studentCountry: string,
  studentName: string,
  tag: string,
  userId: string
};

export type ApiTambookReviewType = {
  active: string,
  country: string,
  id: number,
  name: string,
  profile_pic: string,
  rate: number,
  reviewText: string,
  tag: string
};

export type ApiStudentReviewType = {
  name: string,
  comment: string,
  profile_pic: string,
  rating: number
};

export type ApiStudentReviewListType = Array<ApiStudentReviewType>;

export type StudentReviewType = {
  name: string,
  image: string,
  comment: string,
  rating: number
};

export type StudentReviewListType = Array<StudentReviewType>;

export type UserReviewType = {
  courseReview: Array<ApiCourseReviewType>,
  tambookReview: Array<ApiTambookReviewType>
};
