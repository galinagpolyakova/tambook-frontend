// @flow
import type { Element } from "react";
import React, { Component, createContext } from "react";
import { ItemsLayout } from "shared/components/ItemsLayout";

export type UserSettingsContext = {
  viewMode: typeof ItemsLayout.GRID_LAYOUT | typeof ItemsLayout.LIST_LAYOUT,
  displayAuth: boolean,
  showUserPreference: boolean,
  changeViewMode: Function,
  toggleAuth: Function,
  toggleUserPreference: Function
};

const UserSettings = createContext({
  viewMode: ItemsLayout.GRID_LAYOUT,
  displayAuth: false,
  showUserPreference: false,
  changeViewMode: () => null,
  toggleUserPreference: () => {},
  toggleAuth: () => null
});

export const UserSettingsConsumer = UserSettings.Consumer;

type UserSettingsProviderProps = {
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined
};

type UserSettingsProviderState = {
  viewMode: typeof ItemsLayout.GRID_LAYOUT | typeof ItemsLayout.LIST_LAYOUT,
  displayAuth: boolean,
  showUserPreference: boolean
};

export class UserSettingsProvider extends Component<
  UserSettingsProviderProps,
  UserSettingsProviderState
> {
  state = {
    viewMode: ItemsLayout.GRID_LAYOUT,
    displayAuth: false,
    showUserPreference: false
  };

  changeViewMode(mode: string) {
    this.setState({
      viewMode: mode
    });
  }

  toggleAuth() {
    this.setState(({ displayAuth }) => ({
      displayAuth: !displayAuth
    }));
  }

  toggleUserPreference() {
    this.setState(({ showUserPreference }) => ({
      showUserPreference: !showUserPreference
    }));
  }

  render() {
    return (
      <UserSettings.Provider
        value={{
          viewMode: this.state.viewMode,
          displayAuth: this.state.displayAuth,
          showUserPreference: this.state.showUserPreference,
          changeViewMode: this.changeViewMode.bind(this),
          toggleAuth: this.toggleAuth.bind(this),
          toggleUserPreference: this.toggleUserPreference.bind(this)
        }}
      >
        {this.props.children}
      </UserSettings.Provider>
    );
  }
}
