import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import Amplify from "aws-amplify";

import { registerServices, serviceManager } from "./services/manager";
import { isUserAuthenticated } from "./containers/auth/store/action";

import awsConfig from "./config/amplify";
import initializeStore from "./store";
import Routes from "./routes";
import { API_URL } from "./config/app";

import "./index.scss";
import { loadCoursesInCompare } from "./containers/course/store/actions";
import {
  getUserPreferences,
  getCookieConsentStatus
} from "./containers/profile/store/actions";

Amplify.configure({
  Auth: awsConfig.cognito,
  Storage: awsConfig.storage
});

const settings = {
  api: {
    baseUrl: API_URL
  }
};

if (process.env.NODE_ENV === "development") {
  settings.api.baseUrl = "/api";
}
registerServices(settings);

const store = initializeStore({}, serviceManager);

store.dispatch(isUserAuthenticated());
store.dispatch(loadCoursesInCompare());
store.dispatch(getUserPreferences());
store.dispatch(getCookieConsentStatus());

ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById("root")
);
