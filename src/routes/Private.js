// @flow
import React from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({
  component: Component,
  isAuthenticated,
  isUserInitiated,
  path,
  roles,
  userRole,
  ...rest
}: {
  component: Function,
  isAuthenticated: boolean,
  isUserInitiated: boolean,
  path: string,
  userRole: $PropertyType<AuthState, "role">,
  roles: Array<$PropertyType<AuthState, "role">>,
  location?: Object
}) => {
  if (!isUserInitiated) {
    return null;
  }
  return (
    <Route
      {...rest}
      path={`/:locale${path}`}
      render={props =>
        isAuthenticated && roles.includes(userRole) ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
