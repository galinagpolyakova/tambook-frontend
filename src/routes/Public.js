// @flow
import React from "react";
import { Route } from "react-router-dom";

const PublicRoutes = ({
  component: Component,
  path,
  ...rest
}: {
  component: Function,
  path: string
}) => {
  return (
    <Route
      path={`/:locale${path}`}
      {...rest}
      component={props => <Component {...props} />}
    />
  );
};

export default PublicRoutes;
