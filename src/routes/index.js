// @flow
import React, { PureComponent, Suspense } from "react";
import { connect } from "react-redux";
import { Switch, Redirect } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";

import {
  isAuthenticated,
  isUserInitiated
} from "containers/auth/store/selectors";
import { authSignOut } from "containers/auth/store/action";
import { updateCookieConsentStatus } from "containers/profile/store/actions";
import { type ProfileState } from "containers/profile/store/reducer";

import Layout from "components/Layout";
import Loader from "components/Loader";

import { history } from "store";

import { UserSettingsProvider } from "contexts/UserSettings";

import routes from "./routes";
import PrivateRoute from "./Private";
import PublicRoute from "./Public";

type RoutesProps = {
  isAuthenticated: Boolean,
  isUserInitiated: Boolean,
  compareList: string[],
  userSettings: Object,
  role: $PropertyType<AuthState, "role">,
  authSignOut: Function,
  isCookieConsentGiven: $PropertyType<ProfileState, "isCookieConsentGiven">,
  updateCookieConsentStatus: Function
};

class Routes extends PureComponent<RoutesProps> {
  render() {
    const {
      role,
      isAuthenticated,
      isUserInitiated,
      userSettings,
      authSignOut,
      isCookieConsentGiven,
      updateCookieConsentStatus
    } = this.props;
    return (
      <UserSettingsProvider>
        {
          // $FlowFixMe
          <ConnectedRouter history={history}>
            <Suspense fallback={<Loader />}>
              <Switch>
                <Redirect exact from="/" to="/en" />
                {isUserInitiated ? (
                  <Layout
                    isAuthenticated={isAuthenticated}
                    userSettings={userSettings}
                    authSignOut={authSignOut}
                    isCookieConsentGiven={isCookieConsentGiven}
                    updateCookieConsentStatus={updateCookieConsentStatus}
                  >
                    {routes.map((route, i) => {
                      if (route.auth) {
                        return (
                          <PrivateRoute
                            isAuthenticated={isAuthenticated}
                            isUserInitiated={isUserInitiated}
                            key={i}
                            userRole={role}
                            {...route}
                          />
                        );
                      }
                      return <PublicRoute key={i} {...route} />;
                    })}
                  </Layout>
                ) : (
                  <Loader />
                )}
              </Switch>
            </Suspense>
          </ConnectedRouter>
        }
      </UserSettingsProvider>
    );
  }
}

const Actions = {
  authSignOut,
  updateCookieConsentStatus
};

function mapStateToProps(state) {
  return {
    isAuthenticated: isAuthenticated(state),
    isUserInitiated: isUserInitiated(state),
    role: state.auth.role,
    userSettings: state.profile.settings,
    isCookieConsentGiven: state.profile.isCookieConsentGiven
  };
}

export default connect(
  mapStateToProps,
  Actions
)(Routes);
