const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  app.use(
    proxy("/api", {
      target: "https://qa.tambook-dev.co.nz/qa",
      pathRewrite: { "^/api": "" },
      secure: true,
      changeOrigin: true
    })
  );
};
