// eslint-disable-next-line
/* global google */
// @flow
import React, { Component } from "react";
import classNames from "classnames";
import Icon from "../Icon";
import { isNotEmpty } from "../../utils";
import translate from "translations/translate";

declare var google;

type AddressAutocompleteProps = {
  icon: string,
  placeholder: string,
  id?: string,
  error: null | string,
  className: string,
  onPlaceChanged: Function,
  autocomplete: any,
  autocompleteInput: any,
  handlePlaceChanged: any
};

export type AddressAutocompleteResponseType = {
  address_components: Array<Object>,
  formatted_address: string,
  geometry: { location: { lat: Function, lng: Function } }
};

class AddressAutocomplete extends Component<AddressAutocompleteProps> {
  autocompleteInput = React.createRef();
  autocomplete: any;

  componentDidMount() {
    // $FlowFixMe
    this.autocomplete = new google.maps.places.Autocomplete(
      this.autocompleteInput.current,
      { types: ["geocode"] }
    );
    this.autocomplete.addListener(
      "place_changed",
      this.handlePlaceChanged.bind(this)
    );
  }

  handlePlaceChanged = () => {
    const place = this.autocomplete.getPlace();
    this.props.onPlaceChanged(place);
  };

  static defaultProps = {
    error: null,
    className: ""
  };

  getFieldErrors(error: string | null) {
    return error !== null ? (
      <ul className="form-errors">
        <li>{error}</li>
      </ul>
    ) : (
      ""
    );
  }

  render() {
    const { icon, error, className } = this.props;
    const hasErrors = error !== null;
    return (
      <div
        className={classNames(
          "form-input",
          { "has-errors": hasErrors },
          className
        )}
      >
        {isNotEmpty(icon) ? <Icon className="input-icon" icon={icon} /> : ""}
        <input
          ref={this.autocompleteInput}
          id="autocomplete"
          placeholder={translate("Enter your address")}
          type="text"
        />
        {this.getFieldErrors(error)}
      </div>
    );
  }
}

export default AddressAutocomplete;
