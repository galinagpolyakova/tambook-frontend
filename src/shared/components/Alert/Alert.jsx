// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";

import "./styles.scss";

const TYPE = {
  SUCCESS: "alert-success",
  ERROR: "alert-danger",
  LIGHT: "alert-light",
  INFO: "alert-info"
};

type AlertProps = {
  isFullWidth: boolean,
  children: any,
  type: typeof TYPE.SUCCESS | typeof TYPE.ERROR | typeof TYPE.INFO
};

class Alert extends PureComponent<AlertProps> {
  static TYPE = TYPE;
  static defaultProps = {
    type: TYPE.INFO,
    isFullWidth: false
  };
  render() {
    const { children, type, isFullWidth } = this.props;
    return (
      <div className={classNames("alert", { "full-width": isFullWidth }, type)}>
        {children}
      </div>
    );
  }
}

export default Alert;
