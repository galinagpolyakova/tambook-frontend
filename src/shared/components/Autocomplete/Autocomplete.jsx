// @flow
import React, { PureComponent } from "react";
import "./styles.scss";

type AutocompleteProps = {
  suggestions: Array<string>,
  placeholder?: string,
  name?: string,
  onChange: Function,
  onSelect: Function
};

type AutocompleteState = {
  activeSuggestion: number,
  filteredSuggestions: Array<string>,
  showSuggestions: boolean,
  userInput: string
};

class Autocomplete extends PureComponent<AutocompleteProps, AutocompleteState> {
  static defaultProps = {
    suggestions: [],
    onChange: () => {}
  };
  state = {
    // The active selection's index
    activeSuggestion: 0,
    // The suggestions that match the user's input
    filteredSuggestions: [],
    // Whether or not the suggestion list is shown
    showSuggestions: false,
    // What the user has entered
    userInput: ""
  };

  onChange = (e: SyntheticEvent<HTMLButtonElement>) => {
    const { suggestions, onChange } = this.props;
    const userInput = e.currentTarget.value;

    // Filter our suggestions that don't contain the user's input
    const filteredSuggestions = suggestions.filter(
      suggestion =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value
    });
    onChange(e.currentTarget.value);
  };

  onClick = (e: SyntheticEvent<HTMLButtonElement>) => {
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    });
    this.props.onSelect(e.currentTarget.innerText);
  };

  onKeyDown = (e: SyntheticKeyboardEvent<HTMLElement>) => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    // User pressed the enter key
    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion]
      });
    }
    // User pressed the up arrow
    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }
    // User pressed the down arrow
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput
      }
    } = this;

    const { name, placeholder } = this.props;

    let suggestionsListComponent;

    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {filteredSuggestions.map((suggestion, index) => {
              let className;

              // Flag the active suggestion with a class
              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return (
                <li className={className} key={suggestion} onClick={onClick}>
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <ul className="suggestions no-suggestions">
            <li>No suggestions</li>
          </ul>
        );
      }
    }

    return (
      <div className="auto-complete-input">
        <input
          type="text"
          autoComplete="off"
          name={name}
          placeholder={placeholder}
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
        />
        {suggestionsListComponent}
      </div>
    );
  }
}

export default Autocomplete;
