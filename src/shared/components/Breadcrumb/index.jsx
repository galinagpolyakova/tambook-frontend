// @flow
import React, { PureComponent } from "react";
import Link from "components/Link";

import translate from "translations/translate";

import "./styles.scss";

type BreadcrumbType = {
  title: string,
  link: string
};

export type BreadcrumbListType = Array<BreadcrumbType>;

type BreadcrumbProps = {
  breadcrumbs: Array<{
    title: string,
    link: string
  }>
};

class Breadcrumb extends PureComponent<BreadcrumbProps> {
  render() {
    const { breadcrumbs } = this.props;
    return (
      <ul className="breadcrumb">
        <li>
          <Link to="/">{translate("Home")}</Link>
        </li>
        {breadcrumbs.map(({ title, link }) => (
          <li key={title}>
            {link ? <Link to={link}>{title}</Link> : <span>{title}</span>}
          </li>
        ))}
      </ul>
    );
  }
}

export default Breadcrumb;
