// @flow
import React, { PureComponent } from "react";
import "./styles.scss";

type CheckboxProps = {
  isChecked: boolean,
  text: string,
  onChange: Function,
  children: any
};

type CheckboxState = {
  isChecked: boolean
};

class Checkbox extends PureComponent<CheckboxProps, CheckboxState> {
  static defaultProps = {
    isChecked: false,
    onChange: () => {}
  };

  state = {
    isChecked: false
  };

  toggleModal = (isChecked: boolean) => {
    this.setState(function() {
      return {
        isChecked
      };
    });
  };

  componentDidUpdate(prevProps: CheckboxProps) {
    if (prevProps.isChecked !== this.props.isChecked) {
      this.toggleModal(this.props.isChecked);
    }
  }

  render() {
    const { isChecked, text, onChange, children } = this.props;
    return (
      <div className="checkbox-container">
        <input type="checkbox" checked={isChecked} onChange={onChange} />
        <span className="checkmark" />
        <label>{text}</label>
        {children}
      </div>
    );
  }
}

export default Checkbox;
