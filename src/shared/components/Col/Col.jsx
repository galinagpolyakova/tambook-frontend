// @flow
import type { Element } from "react";

import React, { PureComponent } from "react";
import classNames from "classnames";

import "./styles.scss";

type ColProps = {
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined,
  size?: string,
  xm?: string,
  sm?: string,
  md?: string,
  lg?: string,
  styles?: string,
  className?: string
};
class Col extends PureComponent<ColProps> {
  render() {
    const { xm, sm, md, lg, size, className, styles } = this.props;
    const colClass = [];
    if (size) {
      colClass.push(`col-${size}`);
    }
    if (xm) {
      colClass.push(`col-xm-${xm}`);
    }
    if (sm) {
      colClass.push(`col-sm-${sm}`);
    }
    if (md) {
      colClass.push(`col-md-${md}`);
    }
    if (lg) {
      colClass.push(`col-lg-${lg}`);
    }
    return (
      <div className={classNames("col", className, colClass)} style={styles}>
        {this.props.children}
      </div>
    );
  }
}

export default Col;
