// @flow
import React, { PureComponent } from "react";
import { type CourseType } from "containers/course/types";
import { type RRuleSet } from "rrule";

import { WeekHeader } from "./WeekHeader";
import { Month } from "./Month";
import { CalendarHeader } from "./CalendarHeader";

import compareAsc from "../../utils/dates/compareAsc";

type CalendarProps = {
  dayFullTextFormat: string,
  monthFullTextFormat: string,
  minDate: Date,
  maxDate: Date,
  selectedDate: Date | null,
  onDayClick: (day: Date) => void,
  onMonthChange: (direction: number) => void,
  readonly: boolean,
  weekStartOn: "monday" | "sunday",
  displayedMonth: Date,
  rrule?: RRuleSet,
  startingDates: $PropertyType<CourseType, "startDates">,
  closeDatePicker: Function
};

export class Calendar extends PureComponent<CalendarProps> {
  static defaultProps = {
    disableYearSelection: false,
    weekStartOn: "monday"
  };

  getToolbarInteractions() {
    return {
      prevMonth: compareAsc(this.props.displayedMonth, this.props.minDate) > 0,
      nextMonth: compareAsc(this.props.displayedMonth, this.props.maxDate) < 0
    };
  }

  render() {
    const toolbarInteractions = this.getToolbarInteractions();
    return (
      <div className="datepicker">
        <div className="styled-calendar">
          <div className="datepicker-content">
            <CalendarHeader
              month={this.props.displayedMonth}
              onMonthChange={this.props.onMonthChange}
              prevMonth={toolbarInteractions.prevMonth}
              nextMonth={toolbarInteractions.nextMonth}
              monthFullTextFormat={this.props.monthFullTextFormat}
              closeDatePicker={this.props.closeDatePicker}
            />

            <WeekHeader weekStartOn={this.props.weekStartOn} />

            <Month
              month={this.props.displayedMonth}
              onDayClick={this.props.onDayClick}
              readonly={this.props.readonly}
              selectedDate={this.props.selectedDate}
              weekStartOn={this.props.weekStartOn}
              rrule={this.props.rrule}
              startingDates={this.props.startingDates}
            />
          </div>
        </div>
      </div>
    );
  }
}
