// @flow
import React, { PureComponent } from "react";
import format from "../../utils/dates/format";
import IconButton from "../IconButton";

type CalendarHeaderProps = {
  month: Date,
  nextMonth: boolean,
  prevMonth: boolean,
  monthFullTextFormat: string,
  onMonthChange: (direction: -1 | 1) => void,
  closeDatePicker: Function
};

export class CalendarHeader extends PureComponent<CalendarHeaderProps> {
  static defaultProps = {
    nextMonth: true,
    prevMonth: true,
    onMonthChange: () => {}
  };

  constructor(props: CalendarHeaderProps) {
    super(props);

    //$FlowFixMe Perfs
    this.handleClickPrevMonth = this.handleClickPrevMonth.bind(this);
    //$FlowFixMe Perfs
    this.handleClickNextMonth = this.handleClickNextMonth.bind(this);
  }

  handleClickPrevMonth() {
    this.props.onMonthChange(-1);
  }

  handleClickNextMonth() {
    this.props.onMonthChange(1);
  }

  render() {
    return (
      <div className="calendar-header">
        <IconButton
          className="close-datepicker"
          icon="times"
          onClick={this.props.closeDatePicker}
        />
        <div className="calendar-header-container">
          <IconButton
            className="month-toggler"
            disabled={!this.props.prevMonth}
            icon="angle-left"
            onClick={this.handleClickPrevMonth}
          />

          <div className="displayed-month">
            {format(this.props.month, this.props.monthFullTextFormat)}
          </div>

          <IconButton
            className="month-toggler"
            disabled={!this.props.nextMonth}
            icon="angle-right"
            onClick={this.handleClickNextMonth}
          />
        </div>
      </div>
    );
  }
}
