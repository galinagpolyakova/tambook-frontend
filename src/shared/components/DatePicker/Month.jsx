// @flow
import React, { PureComponent } from "react";
import { type CourseType } from "containers/course/types";
import { type RRuleSet } from "rrule";

import { Week } from "./Week";

import eachDay from "../../utils/dates/eachDay";
import addDays from "../../utils/dates/addDays";
import lastDayOfMonth from "../../utils/dates/lastDayOfMonth";

type MonthProps = {
  month: Date,
  onDayClick: (day: Date) => void,
  selectedDate: Date | null,
  readonly: boolean,
  weekStartOn: "monday" | "sunday",
  startingDates: $PropertyType<CourseType, "startDates">,
  rrule?: RRuleSet
};

export class Month extends PureComponent<MonthProps> {
  getDaysMatrix(): Array<Array<null | Date>> {
    const month = this.props.month;
    const monthDays = eachDay(
      new Date(month.getFullYear(), month.getMonth(), 1),
      lastDayOfMonth(month)
    );
    const weekMatrix = [];
    const weekStartAt = this.props.weekStartOn === "monday" ? 1 : 0; //0 = Sunday, 1 = Monday

    const padWeek = (week: Date[]): Array<Date | null> => {
      const emptyDays = 7 - week.length;
      let weekClone = [...week];
      for (let i = 0; i < emptyDays; i++) {
        if (weekMatrix.length === 0) {
          weekClone.unshift(null);
          continue;
        }

        weekClone.push(null);
      }

      return weekClone;
    };

    let week = [];
    for (let day of monthDays) {
      if (week.length > 0 && day.getDay() === weekStartAt) {
        weekMatrix.push(padWeek(week));
        week = [];
      }

      week.push(day);
    }
    weekMatrix.push(padWeek(week));
    return weekMatrix;
  }

  getAvailableDates(): Date[] {
    const firstDay = new Date(
      this.props.month.getFullYear(),
      this.props.month.getMonth(),
      1
    );
    const lastDay = addDays(lastDayOfMonth(this.props.month), 1);

    if (this.props.rrule === undefined || this.props.rrule === null) {
      if (this.props.startingDates.length > 0) {
        return this.props.startingDates;
      } else {
        return eachDay(firstDay, lastDay);
      }
    }

    return this.props.rrule.between(firstDay, lastDay, true);
  }

  render() {
    const daysMatrix = this.getDaysMatrix();
    const availableDates = this.getAvailableDates();
    return (
      <div className="month">
        {daysMatrix.map((week: Array<null | Date>, i) => (
          <Week
            key={i}
            week={week}
            readonly={this.props.readonly}
            onDayClick={this.props.onDayClick}
            selectedDate={this.props.selectedDate}
            availableDates={availableDates}
          />
        ))}
      </div>
    );
  }
}
