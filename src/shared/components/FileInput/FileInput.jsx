// @flow
import React, { Component } from "react";
import translate from "translations/translate";

type FileInputProps = {
  onChange: Function,
  multiple: boolean,
  placeholder: string,
  className: string,
  inputClass: string
};

class FileInput extends Component<FileInputProps> {
  fileInput = React.createRef();

  render() {
    const {
      onChange,
      multiple,
      placeholder,
      className,
      inputClass
    } = this.props;
    return (
      <div className={className}>
        {translate("Choose File")}
        <input
          type="file"
          placeholder={placeholder}
          className={inputClass}
          multiple={multiple}
          onChange={onChange}
        />
      </div>
    );
  }
}
export default FileInput;
