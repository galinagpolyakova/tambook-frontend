// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";

import "./styles.scss";

export type IconProps = {
  icon: string,
  className: string,
  color: string
};

class Icon extends PureComponent<IconProps> {
  static defaultProps = {
    className: "",
    color: ""
  };
  render() {
    const { icon, className, color } = this.props;
    return (
      <i
        className={classNames("icon", `icon-${icon}`, className)}
        style={{ color }}
      />
    );
  }
}

export default Icon;
