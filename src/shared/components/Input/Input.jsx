// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";

import Icon from "../Icon";

import { isNotEmpty } from "../../utils";

import "./styles.scss";

type InputProps = {
  icon: string,
  text: string,
  placeholder: string,
  name?: string,
  id?: string,
  onChange: Function,
  type: string,
  error: null | string,
  className: string,
  onBlur: Function,
  onFocus: Function,
  autoComplete: boolean,
  disabled: boolean
};

class Input extends PureComponent<InputProps> {
  static defaultProps = {
    type: "text",
    error: null,
    className: "",
    onChange: () => {},
    onBlur: () => {},
    onFocus: () => {},
    autoComplete: true,
    disabled: false
  };

  getFieldErrors(error: string | null) {
    return error !== null && <div className="form-errors">{error}</div>;
  }

  render() {
    const {
      icon,
      text,
      placeholder,
      name,
      id,
      onChange,
      type,
      error,
      className,
      onBlur,
      onFocus,
      autoComplete,
      disabled
    } = this.props;
    const hasErrors = error !== null;
    return (
      <div
        className={classNames(
          "form-input",
          { "has-errors": hasErrors },
          className
        )}
      >
        {isNotEmpty(icon) ? <Icon className="input-icon" icon={icon} /> : ""}
        <input
          autoComplete={autoComplete.toString()}
          name={name}
          id={id}
          placeholder={placeholder}
          value={text === null ? "" : text}
          onChange={onChange}
          type={type}
          onBlur={onBlur}
          onFocus={onFocus}
          disabled={disabled}
        />
        {this.getFieldErrors(error)}
      </div>
    );
  }
}

export default Input;
