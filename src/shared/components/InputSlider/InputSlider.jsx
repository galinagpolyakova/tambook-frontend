// @flow
import React, { PureComponent } from "react";
import IconButton from "../IconButton";

import "./styles.scss";

type InpulSilderOptionObject = {
  value: string,
  name: string
};

type InpulSilderOption = InpulSilderOptionObject | string;

export type InpulSilderOptionList = Array<InpulSilderOption>;

type InputSliderProps = {
  onChange: Function,
  options: InpulSilderOptionList,
  name?: string
};

type InputSliderState = {
  activeValue: number
};
class InputSlider extends PureComponent<InputSliderProps, InputSliderState> {
  static defaultProps = {
    onChange: () => {}
  };
  state = {
    activeValue: 0
  };
  onChange = (
    value: $PropertyType<InpulSilderOptionObject, "value"> | string
  ) => {
    this.props.onChange(value);
  };
  getOptions = () => {
    const { options } = this.props;
    const { activeValue } = this.state;
    if (options && options.length > 0) {
      return typeof options[activeValue] === "string"
        ? options[activeValue]
        : options[activeValue].name;
    }
    return "No values";
  };
  selectNext = () => {
    const { activeValue } = this.state;
    if (this.props.options.length - 1 > activeValue) {
      this.setState({
        activeValue: this.state.activeValue + 1
      });
    }
  };
  selectPrev = () => {
    const { activeValue } = this.state;
    if (activeValue !== 0) {
      this.setState({
        activeValue: this.state.activeValue - 1
      });
    }
  };
  render() {
    return (
      <div className="input-slider-container">
        <div className="input-slider">
          <IconButton
            icon="chevron-left"
            onClick={this.selectPrev}
            className="input-slider-controls"
          />
          <div className="input-slider-value">{this.getOptions()}</div>
          <IconButton
            icon="chevron-right"
            className="input-slider-controls"
            onClick={this.selectNext}
          />
          <input
            type="text"
            value={this.state.activeValue}
            name={this.props.name}
            onChange={this.onChange}
          />
        </div>
      </div>
    );
  }
}

export default InputSlider;
