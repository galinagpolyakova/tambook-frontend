// @flow
import React, { PureComponent } from "react";
import { RepeatableItemsGrid } from "./RepeatableItemsGrid";
import { RepeatableItemsList } from "./RepeatableItemsList";
import IconButton from "./IconButton";
import classNames from "classnames";
import Alert from "./Alert";
import translate from "translations/translate";

const GRID_LAYOUT = "GRID_LAYOUT";
const LIST_LAYOUT = "LIST_LAYOUT";

export type displayModeType = typeof LIST_LAYOUT | typeof GRID_LAYOUT;

type ItemsLayoutProps = {
  items: Array<Object>,
  listItemComponent: any,
  gridItemComponent: any,
  gridByLine: number,
  viewMode: displayModeType
};

export class ItemsLayout extends PureComponent<ItemsLayoutProps> {
  static GRID_LAYOUT = GRID_LAYOUT;
  static LIST_LAYOUT = LIST_LAYOUT;

  static defaultProps = {
    viewMode: ItemsLayout.GRID_LAYOUT,
    gridByLine: 4
  };

  getItems() {
    if (this.props.items.length === 0) {
      return (
        <Alert isFullWidth={true} type={Alert.TYPE.INFO}>
          {translate("No search results were found")}
        </Alert>
      );
    }
    if (this.props.viewMode === ItemsLayout.LIST_LAYOUT) {
      return (
        <RepeatableItemsList
          items={this.props.items}
          itemComponent={this.props.listItemComponent}
        />
      );
    } else {
      return (
        <RepeatableItemsGrid
          byLine={this.props.gridByLine}
          items={this.props.items}
          itemComponent={this.props.gridItemComponent}
        />
      );
    }
  }

  render() {
    return <div className="items-container">{this.getItems()}</div>;
  }
}

type ItemsLayoutTogglerProps = {
  viewMode: typeof ItemsLayout.GRID_LAYOUT | typeof ItemsLayout.LIST_LAYOUT,
  changeViewMode: Function
};

export class ItemsLayoutToggler extends PureComponent<ItemsLayoutTogglerProps> {
  static defaultProps = {
    viewMode: ItemsLayout.GRID_LAYOUT
  };

  viewModeToggle(selected: string) {
    if (this.props.changeViewMode !== undefined) {
      this.props.changeViewMode(selected);
    }
  }

  render() {
    return (
      <div className="toggle-view">
        <div
          className={classNames("toggle-button", {
            active: ItemsLayout.GRID_LAYOUT === this.props.viewMode
          })}
        >
          <IconButton
            icon="th-large"
            onClick={() => this.viewModeToggle(ItemsLayout.GRID_LAYOUT)}
          />
        </div>
        <div
          className={classNames("toggle-button", {
            active: ItemsLayout.LIST_LAYOUT === this.props.viewMode
          })}
        >
          <IconButton
            icon="bars"
            onClick={() => this.viewModeToggle(ItemsLayout.LIST_LAYOUT)}
          />
        </div>
      </div>
    );
  }
}
