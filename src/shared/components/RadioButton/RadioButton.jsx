// @flow
import React, { PureComponent } from "react";

import "./styles.scss";

type RadioButtonProps = {
  label: string,
  name: string,
  value: string,
  onChange: Function,
  checked?: boolean
};

class RadioButton extends PureComponent<RadioButtonProps> {
  static defaultProps = {
    checked: false,
    onChange: () => { }
  };

  render() {
    const { label, name, value, checked, onChange } = this.props;
    return (
      <label className="radio-button">
        {label}
        <input
          type="radio"
          name={name}
          value={value}
          onChange={onChange}
          checked={checked}
        />
        <span className="checkmark" />
      </label>
    );
  }
}

export default RadioButton;
