// @flow
import type { Element } from "react";
import React, { PureComponent } from "react";
import Row from "./Row";
import Col from "./Col";

export type RepeatableItemsGridProps = {
  items: Array<Object>,
  itemComponent: any,
  byLine: number
};

export class RepeatableItemsGrid extends PureComponent<RepeatableItemsGridProps> {
  static defaultProps = {
    byLine: 3
  };

  getLines(): Array<Array<Object>> {
    let lines = [];

    for (let i = 0; i < this.props.items.length; i++) {
      let lineIndex = Math.floor(i / this.props.byLine);
      if (lines[lineIndex] === undefined) {
        lines[lineIndex] = [];
      }
      lines[lineIndex].push(this.props.items[i]);
    }

    return lines;
  }

  render(): Array<Element<any>> {
    let ItemComponent = this.props.itemComponent;
    return this.getLines().map((line: Array<Object>, index: number) => {
      return (
        <Row key={index}>
          {line.map((item: Object, index: number) => {
            return (
              <Col key={index} sm={12} md={Math.floor(12 / this.props.byLine)}>
                <ItemComponent item={item} />
              </Col>
            );
          })}
        </Row>
      );
    });
  }
}
