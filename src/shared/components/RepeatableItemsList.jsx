// @flow
import React, { PureComponent } from "react";
import Row from "./Row";
import Col from "./Col";

export type RepeatableItemsListProps = {
  items: Array<Object>,
  itemComponent: any
};

export class RepeatableItemsList extends PureComponent<RepeatableItemsListProps> {
  render() {
    let ItemComponent = this.props.itemComponent;
    return (
      <Row>
        {this.props.items.map((item: Object, index: number) => {
          return (
            <Col sm={12} md={12} key={index}>
              <ItemComponent item={item} />
            </Col>
          );
        })}
      </Row>
    );
  }
}
