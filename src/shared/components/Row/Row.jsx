// @flow
import type { Element } from "react";

import React, { PureComponent } from "react";
import classNames from "classnames";

import "./styles.scss";

type RowProps = {
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined,
  equalHeight?: boolean,
  fullHeight?: boolean,
  fullWidth?: boolean,
  columnPosition?: "string",
  className?: string
};

class Row extends PureComponent<RowProps> {
  static defaultProps = {
    equalHeight: false,
    fullWidth: false
  };
  render() {
    const { equalHeight, fullWidth, className } = this.props;
    return (
      <div
        className={classNames(
          "row",
          { "eq-height": equalHeight },
          { "full-width": fullWidth },
          className
        )}
      >
        {this.props.children}
      </div>
    );
  }
}

export default Row;
