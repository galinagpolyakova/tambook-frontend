// @flow
import type { Element } from "react";

import React, { PureComponent } from "react";
import classNames from "classnames";

import "./styles.scss";

type SectionProps = {
  className?: string,
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined
};

class Section extends PureComponent<SectionProps> {
  render() {
    const { className, children } = this.props;
    return (
      <section className={classNames("section", className)}>{children}</section>
    );
  }
}

export default Section;
