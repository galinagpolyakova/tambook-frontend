// @flow
import React, { PureComponent } from "react";
import classnames from "classnames";

import Button from "../Button";
import Icon from "../Icon";

import "./styles.scss";
import IconButton from "../IconButton";

const SELECTED_SORTING = {
  PRICE: "price",
  RATING: "rating"
};

type SortButtonProps = {
  onClick: Function,
  filter: string,
  children: Node,
  sorting: string,
  selectedSorting: | typeof SELECTED_SORTING.PRICE
    | typeof SELECTED_SORTING.RATING,
  sortingType: typeof SELECTED_SORTING.PRICE | typeof SELECTED_SORTING.RATING
};

class SortButton extends PureComponent<SortButtonProps> {
  static TYPE = {
    ASC: "asc",
    DESC: "desc"
  };

  static SELECTED_SORTING = SELECTED_SORTING;

  toggleSort = () => {
    const { sorting, sortingType } = this.props;
    const sort =
      sorting === SortButton.TYPE.ASC || sorting === undefined
        ? SortButton.TYPE.DESC
        : SortButton.TYPE.ASC;
    this.props.onClick({
      selectedSorting: sortingType,
      [this.props.filter]: sort
    });
  };

  getIcon = () => {
    const { sorting } = this.props;
    switch (sorting) {
      case SortButton.TYPE.ASC:
        return (
          <div className="sort">
            <Icon icon="sort" />
            <Icon icon="sort-down" />
          </div>
        );
      case SortButton.TYPE.DESC:
        return (
          <div className="sort">
            <Icon icon="sort" />
            <Icon icon="sort-up" />
          </div>
        );
      default:
        return (
          <div className="sort">
            <Icon icon="sort" />
          </div>
        );
    }
  };

  render() {
    const { children, selectedSorting, sorting, sortingType } = this.props;
    const active =
      (sorting === SortButton.TYPE.ASC || sorting === SortButton.TYPE.DESC) &&
      selectedSorting === sortingType;

    return (
      <div
        className={classnames("sort-button", {
          active
        })}
      >
        <Button
          type={Button.TYPE.LIGHT}
          size={Button.SIZE.SMALL}
          onClick={() => this.toggleSort()}
        >
          {children}
          {this.getIcon()}
        </Button>
        {active && (
          <IconButton
            icon="times"
            className="close-button"
            onClick={() =>
              this.props.onClick({
                selectedSorting: "",
                [this.props.filter]: ""
              })
            }
          />
        )}
      </div>
    );
  }
}

export default SortButton;
