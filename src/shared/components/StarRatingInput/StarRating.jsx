// @flow
import * as React from "react";
import starRange from "./starRange";

export interface Props {
  value: number;
}
export default function StarRating(props: Props) {
  return (
    <div className="star-rating-input">
      {starRange(props.value).map(i => (
        <Star key={i} />
      ))}
    </div>
  );
}

function Star() {
  return (
    <div className="star-rating-star-container">
      <a
        className={`star-rating-star on`}
        href=""
        onClick={(e: ClickEvent) => {
          e.preventDefault();
        }}
      />
    </div>
  );
}

interface ClickEvent {
  preventDefault: () => void;
}
