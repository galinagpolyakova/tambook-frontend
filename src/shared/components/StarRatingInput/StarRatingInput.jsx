// @flow
import React, { Component, Fragment } from "react";
import classNames from "classnames";
import starRange from "./starRange";
import "./styles.scss";
import Icon from "../Icon";

const DEFAULT_SIZE = 5;

export interface StarRatingInputProps {
  size?: number;
  value: number;
  error: null | string;
  className: string;
  onChange: (value: number) => void;
}

export interface StarRatingInputState {
  prospectiveValue: number;
}

interface StarProps {
  value: number;
  mode: string;
  onMouseEnter: () => void;
  onMouseLeave: () => void;
  onClick: () => void;
}

interface ClickEvent {
  preventDefault: () => void;
}

export default class StarRatingInput extends Component<
  StarRatingInputProps,
  StarRatingInputState
> {
  _promiseSetStateDone: Promise<void>;

  get promiseSetStateDone() {
    return this._promiseSetStateDone;
  }

  constructor(props: Props) {
    super(props);
    this._promiseSetStateDone = Promise.resolve();
    this.state = { prospectiveValue: 0 };
  }

  render() {
    const { size, error, className } = this.props;
    const range = starRange(size ? size : DEFAULT_SIZE);
    const hasErrors = error !== null;
    return (
      <Fragment>
        <div
          className={classNames(
            "star-rating-input",
            { "has-errors": hasErrors },
            className
          )}
        >
          {range.map(i => (
            <Star {...this.starProps(i)} key={i} />
          ))}
        </div>
        <div>{this.getFieldErrors(error)}</div>
      </Fragment>
    );
  }

  getFieldErrors(error: string | null) {
    return error !== null && <div className="errors">{error}</div>;
  }
  starProps(value: number): StarProps {
    return {
      value,
      mode: this.anchorMode(value),
      onClick: this.handleStarClick.bind(this, value),
      onMouseEnter: this.handleStarMouseEnter.bind(this, value),
      onMouseLeave: this.handleStarMouseLeave.bind(this)
    };
  }

  anchorMode(value: number): string {
    if (this.state.prospectiveValue > 0) {
      return value <= this.state.prospectiveValue ? "suggested" : "off";
    }

    return value <= this.props.value ? "on" : "off";
  }

  handleStarMouseEnter(value: number): void {
    this._promiseSetStateDone = new Promise(resolve => {
      this.setState({ prospectiveValue: value }, resolve);
    });
  }

  handleStarMouseLeave(): void {
    this._promiseSetStateDone = new Promise(resolve => {
      this.setState({ prospectiveValue: 0 }, resolve);
    });
  }

  handleStarClick(value: number): void {
    this._promiseSetStateDone = new Promise(resolve => {
      this.setState({ prospectiveValue: 0 }, resolve);
    });

    this.props.onChange(value);
  }
}

function Star(props: StarProps) {
  return (
    <a
      className={`star-rating-star ${props.mode}`}
      title={`${props.value}`}
      href="#rating"
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}
      onClick={(e: ClickEvent) => {
        e.preventDefault();
        props.onClick();
      }}
    >
      <Icon icon="star" />
    </a>
  );
}
