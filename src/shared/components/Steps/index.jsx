// @flow
import React, { Component, type Node } from "react";
import classNames from "classnames";

import Icon from "../Icon";

import "./styles.scss";

type StepsProps = {
  steps: Array<{
    title: string,
    content: Node
  }>,
  current: number
};

class Steps extends Component<StepsProps> {
  render() {
    const { steps, current } = this.props;
    return (
      <div className="steps-container">
        <div className="step-header">
          {steps.map(({ title }, key) => (
            <div
              key={key}
              className={classNames("step", {
                current: key + 1 === current,
                done: key + 1 < current
              })}
            >
              {key + 1 < current ? (
                <span className="step-icon done">
                  <Icon icon="check" />
                </span>
              ) : (
                <span className="step-icon">{key + 1}</span>
              )}
              <div className="step-header-content">{title}</div>
            </div>
          ))}
        </div>
        <div className="step-content">{steps[current - 1].content}</div>
      </div>
    );
  }
}

export default Steps;
