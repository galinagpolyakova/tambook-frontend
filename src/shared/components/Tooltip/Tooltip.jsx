// @flow
import React, { Component, type Element } from "react";
import "./styles.scss";

type TooltipProps = {
  message: string,
  position: string,
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined
};

type TooltipState = {
  displayTooltip: boolean
};

class Tooltip extends Component<TooltipProps, TooltipState> {
  static position = {
    TOP: "top",
    BOTTOM: "bottom",
    LEFT: "left",
    RIGHT: "right"
  };

  constructor(props: TooltipProps) {
    super(props);

    this.state = {
      displayTooltip: false
    };

    // $FlowFixMe
    this.hideTooltip = this.hideTooltip.bind(this);
    // $FlowFixMe
    this.showTooltip = this.showTooltip.bind(this);
  }

  hideTooltip() {
    this.setState({ displayTooltip: false });
  }
  showTooltip() {
    this.setState({ displayTooltip: true });
  }

  render() {
    let { message, position } = this.props;
    return (
      <span className="tooltip" onMouseLeave={this.hideTooltip}>
        {this.state.displayTooltip && (
          <div className={`tooltip-bubble tooltip-${position}`}>
            <div className="tooltip-message">{message}</div>
          </div>
        )}
        <span className="tooltip-trigger" onMouseOver={this.showTooltip}>
          {this.props.children}
        </span>
      </span>
    );
  }
}

export default Tooltip;
