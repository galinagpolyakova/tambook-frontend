export const translationOptions = [
  { name: "English", value: "en" },
  { name: "Portuguese", value: "pt" },
  { name: "Spanish", value: "es" },
  { name: "Russian", value: "ru" }
];
