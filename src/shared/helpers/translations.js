export function getLanguageFromUrl(url = window.location.pathname) {
  const path = url.split("/");
  return path.length >= 1 && path[1] !== "" ? path[1] : "en";
}

export function getLocaleRedirectUrl(language) {
  const path = window.location.pathname.split("/");
  if (path.length >= 1) {
    path[1] = language;
  }
  let url = "";
  path.forEach(segment => {
    if (segment !== "") url = `${url}/${segment}`;
  });
  return url;
}
