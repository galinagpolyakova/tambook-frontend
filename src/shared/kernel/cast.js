// @flow

export const isInt = (val: any): boolean =>
  !isNaN(val) &&
  !/^0.+/gi.test(val) &&
  (Number.isInteger(val) ||
    (typeof val === "string" && "" + parseInt(val, 10) === val));

export const isFloat = (val: any): boolean =>
  !isNaN(val) &&
  !isInt(val) &&
  !/^0[^.]+/gi.test(val) &&
  val.toString().length > 0;

export const isColor = (val: any): boolean => /^#?[0-9A-F]{6}$/i.test(val);

export const autoCast = (value: any): any => {
  if (!value) {
    return value;
  }

  if (isColor(value)) {
    return `#${value}`.replace("##", "#").toUpperCase();
  }

  if (isInt(value)) {
    return parseInt(value, 10);
  }

  if (isFloat(value)) {
    return parseFloat(value);
  }

  if (value.toLowerCase() === "true") {
    return true;
  }

  if (value.toLowerCase() === "false") {
    return false;
  }

  return value;
};

export const castString = (str: any): string => {
  if (typeof str !== "string" && !(str instanceof String)) {
    str = `${str}`;
  }
  return str.toLowerCase();
};

export const isString = (str: string) => {
  return typeof str === "string" && str instanceof String;
};

export const isName = (str: string) => {
  return /^[A-Za-z ]+$/.test(str);
};

export const isEmail = (email: string) => {
  const regex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  return regex.test(email);
};

export const changeToTitleCase = (str: string) => {
  return str
    .split(" ")
    .map(s => s.slice(0, 1).toUpperCase() + s.slice(1).toLowerCase())
    .join(" ");
};

export function areObjectsEqual(object1: Object, object2: Object) {
  return JSON.stringify(object1) === JSON.stringify(object2);
}
