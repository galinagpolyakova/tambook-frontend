// eslint-disable-next-line
/* global require */
// This file is generated automatically by `scripts/build/indices.js`. Please, don't change it.
import * as enUS from "./en-US";
import * as eo from "./eo";
module.exports = {
  enUS,
  eo
};
