import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";
import { createMiddleware } from "redux-beacon";
import GoogleTagManager from "@redux-beacon/google-tag-manager";

import createRootReducer from "./reducers";
import {
  AUTH_AGENT_SIGN_UP_INIT,
  AUTH_STUDENT_SIGN_UP_INIT
} from "containers/auth/store/actionTypes";
import {
  SEND_CONTACT_MESSAGE_INIT,
  ON_PAGE_CHANGE
} from "containers/general/store/actionTypes";
import {
  BOOKING_COURSE_STEP,
  BOOKING_ACCOMMODATION_STEP,
  BOOKING_TRAVEL_STEP,
  BOOKING_PAYMENT_STEP,
  BOOKING_PERSONAL_INFO_STEP
} from "containers/booking/store/actionTypes";

const pageViewEventDefinition = ({ payload }) => {
  return {
    event: {
      hitType: "pageview",
      ...payload
    }
  };
};

const agentSignUpEventDef = () => {
  return {
    event: {
      hitType: "event",
      eventAction: "send",
      eventCategory: "sign_up_as_agent’"
    }
  };
};

const studentSignUpEventDef = () => {
  return {
    event: {
      hitType: "event",
      eventAction: "send",
      eventCategory: "sign_up_as_student"
    }
  };
};

const sendContactMessageEventDef = () => {
  return {
    event: {
      hitType: "event",
      eventAction: "send",
      eventCategory: "contact_us"
    }
  };
};

const sendBookingCourseStepEventDef = () => {
  return {
    event: {
      hitType: "event",
      eventAction: "send",
      eventCategory: "booking_course_step"
    }
  };
};

const sendBookingAccommodationStepEventDef = () => {
  return {
    event: {
      hitType: "event",
      eventAction: "send",
      eventCategory: "booking_accommodation_step"
    }
  };
};

const sendBookingTravelStepEventDef = () => {
  return {
    event: {
      hitType: "event",
      eventAction: "send",
      eventCategory: "booking_travel_step"
    }
  };
};

const sendBookingPaymentStepEventDef = () => {
  return {
    event: {
      hitType: "event",
      eventAction: "send",
      eventCategory: "booking_payment_step"
    }
  };
};

const sendBookingPersonalInfoStepEventDef = () => {
  return {
    event: {
      hitType: "event",
      eventAction: "send",
      eventCategory: "booking_personal_info_step"
    }
  };
};

const eventsMap = {
  [ON_PAGE_CHANGE]: pageViewEventDefinition,
  [AUTH_AGENT_SIGN_UP_INIT]: agentSignUpEventDef,
  [AUTH_STUDENT_SIGN_UP_INIT]: studentSignUpEventDef,
  [SEND_CONTACT_MESSAGE_INIT]: sendContactMessageEventDef,
  [BOOKING_COURSE_STEP]: sendBookingCourseStepEventDef,
  [BOOKING_ACCOMMODATION_STEP]: sendBookingAccommodationStepEventDef,
  [BOOKING_TRAVEL_STEP]: sendBookingTravelStepEventDef,
  [BOOKING_PAYMENT_STEP]: sendBookingPaymentStepEventDef,
  [BOOKING_PERSONAL_INFO_STEP]: sendBookingPersonalInfoStepEventDef
};

const gaMiddleware = createMiddleware(eventsMap, GoogleTagManager);

export const history = createBrowserHistory();

export default function(initialState = {}, serviceManager) {
  const middleware = [
    thunk.withExtraArgument(serviceManager),
    routerMiddleware(history),
    gaMiddleware
  ];

  const middlewareEnhancer = applyMiddleware(...middleware);

  const composedEnhancers = composeWithDevTools(middlewareEnhancer);
  const store = createStore(
    createRootReducer(history),
    initialState,
    composedEnhancers
  );

  return store;
}
