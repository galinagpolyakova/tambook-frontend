export default {
    "Contact Us": "Contáctanos ",
    "Address": "Dirección",
    "3 Glenside Crescent, Eden Terrace, Auckland, NZ.": "3 Glenside Crescent Eden Terrace Auckland",
    "Follow us": "Síguenos",
    "Resources": "Recursos",
    "Terms and Conditions": "Términos y Condiciones",
    "About Us": "Sobre Nosotros",
    "FAQ": "Preguntas Más Frecuentes ",
    "All rights reserved Tambook": "Todos los Derechos Reservados Tambook",
    "Email us": "Envíenos un correo electrónico",
    "Sign Up As School": "Registrarse como Escuela",
    "Sign In As School": "Iniciar sesión como Escuela",
    "Blog": "Blog",
    "Got it!": "¡Hecho!",
    "Tambook Reviews": "Opiniones de Tambook"
};