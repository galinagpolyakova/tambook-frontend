export default {
    "About Us": "Sobre Nosotros",
    "Need Help?": "¿Necesitas ayuda?",
    "Profile": "Perfil",
    "Logout": "Salir",
    "Login / Sign up": "Iniciar sesión / Registrarse",
    "Compare": "Comparar",
};