export default {
  "BLOG": "BLOG",
  "DETAIL": "DETALLE ",
  "Back to Blog": "Volver al Blog",
  "SHARE": "COMPARTIR",
  "DON'T MISS A THING:": "NO TE PIERDAS NADA:",
  "Subscribe to get our latest best offers, promotions and news": "Subscríbete para recibir nuestras mejores ofertas, promociones y últimas noticias",
  "SUBSCRIBE": "SUSCRIBIRSE",
  "Subscribed successfully. Thank you.": "Suscrito con éxito. Gracias.",
  "All": "Todo",
  "WATCH HOW TAMBOOK HELPS YOU STUDY OVERSEAS": "MIRA COMO TAMBOOK TE AYUDA CON TUS ESTUDIOS EN EL EXTRANJERO",
  "Welcome to our blog! Here you'll find useful tips and tricks that'll help you plan your next adventure. To get started, take a look at our short video 'How to book a course using Tambook": "¡Bienvenido a nuestro blog! Aquí podrás encontrar consejos útiles que te ayudarán a organizar tu próxima aventura.Para empezar, echa un vistazo a nuestro breve vídeo 'Cómo reservar un curso a través de Tambook'",
  "SEARCH FOR COURSES NOW": "BUSCAR CURSOS AHORA",
  "search": "buscar",
  "MORE INFO":"Más información"
};