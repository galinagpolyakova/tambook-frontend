export default {
    "Price": "Precio",
    "Destination": "Destino",
    "Intensity": "Intensidad",
    "Duration": "Duración",
    "Ability to work": "Capacidad para trabajar",
    "Course type": "Tipo de Curso",
    "Compare Courses": "Comparar cursos",
    "No courses in compare list":"No hay cursos en la lista",
    "Clear All":"Limpiar Todo"
};