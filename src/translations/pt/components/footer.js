export default {
    "Contact Us": "Entre em Contato Conosco",
    "Address": "Endereço",
    "3 Glenside Crescent, Eden Terrace, Auckland, NZ.": "3 Glenside Crescent, Eden Terrace, Auckland",
    "Follow us": "Siga-nos",
    "Resources": "Recursos",
    "Terms and Conditions": "Termos e Condições",
    "About Us": "Sobre Nós",
    "FAQ": "FAQ",
    "All rights reserved Tambook": "Todos os direitos reservados Tambook",
    "Email us": "Envia-nos um email",
    "Sign Up As School": "Inscreva-se Como Escola",
    "Sign In As School": "Entrar Como Escola",
    "Blog": "Blog",
    "Got it!": "Entendi!",
    "Tambook Reviews": "Avaliações da Tambook"
};