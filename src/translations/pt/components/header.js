export default {
    "About Us": "Sobre Nós",
    "Need Help?": "Precisa de Ajuda?",
    "Profile": "Perfil",
    "Logout": "Sair",
    "Login / Sign up": "Entrar / Inscreva-se",
    "Compare": "Comparar",
};