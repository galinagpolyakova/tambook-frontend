export default {
  "BLOG": "BLOG",
  "DETAIL": "DETALHES",
  "Back to Blog": "Voltar ao Blog",
  "SHARE": "COMPARTILHAR",
  "DON'T MISS A THING:": "NÃO PERCA NADA:",
  "Subscribe to get our latest best offers, promotions and news": "Inscreva-se para receber nossas últimas ofertas, promoções e novidades",
  "SUBSCRIBE": "INSCREVA-SE",
  "Subscribed successfully. Thank you.": "Inscrito com sucesso. Obrigado.",
  "All": "Todos",
  "WATCH HOW TAMBOOK HELPS YOU STUDY OVERSEAS": "ASSISTA COMO A TAMBOOK AJUDA VOCÊ A ESTUDAR NO EXTERIOR",
  "Welcome to our blog! Here you'll find useful tips and tricks that'll help you plan your next adventure. To get started, take a look at our short video 'How to book a course using Tambook": "Bem-vindo(a) ao nosso blog! Aqui você encontrará dicas e truques úteis que ajudarão você a planejar sua próxima aventura. Para começar, assista ao nosso pequeno vídeo 'Como reservar um curso usando a Tambook",
  "SEARCH FOR COURSES NOW": "PESQUISAR CURSOS AGORA",
  "search": "pesquisar",
  "MORE INFO":"MAIS INFORMAÇÕES"
};