export default {
    "Price": "Preço",
    "Destination": "Destino",
    "Intensity": "Intensidade",
    "Duration": "Duração",
    "Ability to work": "Possibilidade de Trabalhar",
    "Course type": "Tipo de curso",
    "Compare Courses": "Comparar Cursos",
    "No courses in compare list":"Nenhum curso na lista de comparação",
    "Clear All":"Limpar tudo"
};