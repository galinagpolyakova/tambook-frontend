import aboutUs from "./aboutUs";
import auth from "./auth";
import blog from "./blog";
import booking from "./booking";
import compare from "./compare";
import contact from "./contact";
import course from "./course";
import home from "./home";
import needHelp from "./needHelp";
import profile from "./profile";

export default { ...aboutUs, ...auth, ...blog, ...booking, ...compare, ...contact, ...course, ...home, ...needHelp, ...profile};