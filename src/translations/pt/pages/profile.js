export default {
    "Home": "Página Inicial",
    "Profile": "Perfil",
    "Bookings": "Reservas",
    "Bookmarks": "Favoritos",
    "User information": "Informações do Usuário",
    "Payment method": "Método de Pagamento",
    "Duration (weeks)": "Duração (semanas)",
    "Date": "Data",
    "Status": "Estado",
    "Details": "Detalhes",
    "Course Booking": "Reserva de Curso",
    "Phone":"Telefone",
    "Gender": "Gênero",
    "Birth date": "Data de Nascimento",
    "Citizenship": "Nacionalidade",
    "Native language": "Idioma Nativo",
    "Title": "Título",
    "Booking": "Reserva",
    "Male": "Masculino",
    "Female": "Feminino",
    "Select the gender": "Selecione seu gênero",
    "Update Profile": "Atualizar Perfil",
    "Edit Profile": "Editar Perfil",
    "This website uses cookies to ensure you get the best experience on our website.": "Este site utiliza cookies para garantir que você tenha a melhor experiência em nosso site.",
    "Sorry, there was a problem loading the page.": "Desculpe, ocorreu um problema ao carregar a página.",
    "Choose File": "Escolher Arquivo",
    "Mark all as read": "Marcar tudo como lido",
    "Notifications Settings": "Configurações das notificações",
    "BACK TO NOTIFICATIONS": "VOLTAR ÀS NOTIFICAÇÕES",
    "SAVE": "SALVAR",
    "CANCEL": "CANCELAR",
    "I want to receive all notifications": "Eu quero receber todas as notificações",
    "Common": "Comum",
    "I want to receive emails from Tambook": "Eu quero receber os e-mails da Tambook",
    "I want to receive information about promo from Tambook": "Eu quero receber notificações sobre promoções da Tambook",
    "Courses": "Cursos",
    "I want to receive notifications about new courses": "Eu quero receber notificações sobre novos cursos",
    "I want to receive notifications about changing favorite course information": "Eu quero receber notificações sobre alterações das informação do curso favorito",
    "I want to receive notifications about special offers": "Eu quero receber notificações sobre ofertas especiais",
    "Schools": "Escolas",
    "I want to receive notifications about new schools": "Eu quero receber notificações sobre novas escolas",
    "I want to receive notifications about schools' updates": "Eu quero receber notificações sobre atualizações das escolas",
    "I want to receive notifications with the order status updates": "Eu quero receber notificações com atualizações do status do pedido",
    "I want to receive notifications with school updates regarding the booking": "Eu quero receber notificações com atualizações da escola sobre a reserva",
    "Settings updated successfully": "Configurações atualizadas com sucesso",
    "No Notifications": "Não há notificações",
    "Package of documents": "Pacote de documentos",
    "DOCUMENTS FROM SCHOOL": "DOCUMENTOS DA ESCOLA",
    "COURSE INFORMATION": "INFORMAÇÕES DO CURSO",
    "USER DOCUMENTS": "DOCUMENTOS DO USUÁRIO",
    "Add a feedback about": "Adicionar um comentário sobre", 
    "Course": "Curso",
    "Feedback about course": "Comentários sobre o curso",
    "BACK TO BOOKINGS": "VOLTAR ÀS RESERVAS",
    "Order summary": "Resumo do pedido",
    "Start Date": "Data de início",
    "Booked By": "Reservado Por",
    "Booking Owner": "Proprietário da Reserva",
    "Booking Status": "Status da Reserva",
    "Payment Type": "Tipo de Pagamento",
    "Weeks": "Semanas",
    "Currency": "Moeda",
    "Week Price": "Preço da Semana",
    "Week Discount": "Desconto da Semana",
    "Enrolment Fee": "Taxa de Matrícula",
    "Stripe Charges": "Tarifas Stripe",
    "Tambook Charges": "Tarifas Tambook",
    "Agent Discount": "Desconto de Agente",
    "Order total": "Total da compra",
    "Total": "Total",
    "Student information": "Informações do aluno",
    "Student Name": "Nome do Aluno",
    "Email": "E-mail",
    "Date of Birth": "Data de Nascimento",
    "Phone No": "Número de Telefone",
    "Accommodation information": "Informações de Hospedagem",
    "End Date": "Data Final",
    "Allergies": "Alergias",
    "Food Restrictions": "Restrições Alimentares",
    "HomeStay Weeks": "Semanas de Acomodação em casa de família",
    "Smoke": "Fumante",
    "Type": "Tipo",
    "Young Children": "Crianças Pequenas",
    "Transfer & Flight information": "Traslado e Informações do Voo",
    "Payment information": "Informações de pagamento",
    "Receipt Email": "Recibo de Email",
    "Description": "Descrição",
    "Application Fee": "Taxa de Inscrição",
    "Amount": "Valor",
    "Course information": "Informações do Curso",
    "Course Name": "Nome do Curso",
    "Language": "Idioma",
    "Country": "País",
    "City": "Cidade",
    "Course Discount": "Desconto do Curso",
    "Enrollment Fee": "Taxa de Matrícula",
    "SHOW": "MOSTRAR",
    "Vote is required": "A votação é obrigatória",
    "Feedback is required": "O comentário é obrigatório",
    "Your vote": "Sua votação",
    "Your feedback": "Seu comentário",
    "SEND": "ENVIAR",
    "booking-details": "detalhes da reserva",
    "Booking Details": "Detalhes da Reserva",
    "Sorry, Something went wrong, Please try again.": "Desculpe, ocorreu um erro. Por favor, tente novamente.",
    "Thank you, Your feedback has successfully added to the Tambook": "Obrigado, seu comentário foi adicionado com sucesso na Tambook",
    "Tambook operations": "Operações da Tambook",
    "You save": "Você economiza",
    "NOT SPECIFIED": "Não Especificado",
    "Work rights": "Direito de trabalhar",
    "English entry level": "Nível Mínimo de Inglês",
    "School": "Escola",
    "Schedule": "Horário",
    "Minimum age": "Idade mínima",
    "Intensity (hours per week)": "Intensidade (horas por semana)",
    "Notifications": "Notificações",
    "Active": "Ativo",
    "Unfinished": "Pendente",
    "Archived": "Arquivado",
    "No search results were found": "Nenhum resultado encontrado",
    "Feedback about Tambook operations": "Comentários sobre as operações da Tambook",
    "Order Details":"Detalhes da Reserva",
    "notification settings": "Configurações das notificações",
    "BOOK": "RESERVAR",
    "Date of birth is invalid.":"A data de nascimento é inválida.",
    "You have already added a feedback, Please update it":"Você já adicionou um comentário. Por favor, é necessário atualizá-lo",
    "UPDATE":"ATUALIZAR",
    "Tambook discount":"Desconto Tambook",
    "Bank Transfer fee":"Taxa de Transferência Bancária"
};