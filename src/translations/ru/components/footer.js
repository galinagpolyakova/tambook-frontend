export default {
    "Contact Us": "Контакты ",
    "Address": "Адрес",
    "3 Glenside Crescent, Eden Terrace, Auckland, NZ.": "3 Glenside Crescent Eden Terrace Auckland",
    "Follow us": "Подписывайтесь на нас",
    "Resources": "Ресурсы",
    "Terms and Conditions": "Положения и условия",
    "About Us": "О нас",
    "FAQ": "Часто задаваемые вопросы",
    "All rights reserved Tambook": "Все права защищены Tambook",
    "Email us": "Свяжитесь с нами по электронной почте",
    "Sign Up As School": "Зарегистрироваться как Школа",
    "Sign In As School": "Войти как Школа",
    "Blog": "Блог",
    "Got it!": "Понятно!",
    "Tambook Reviews": "Отзывы о Tambook"
};