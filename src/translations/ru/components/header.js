export default {
    "About Us": "О нас",
    "Need Help?": "Нужна помощь?",
    "Profile": "Профиль",
    "Logout": "Выйти",
    "Login / Sign up": "Вход / Регистрация",
    "Compare": "Сравнить",
};