export default {
  "Want to learn a new skill on the adventure of a lifetime?": "Хотели бы вы приобрести новый навык во время уникального путешествия?",
  "Tambook makes booking language courses around the world easy and affordable.": "С помощью платформы Tambook бронирование языковых курсов становится простым и доступным.",
  "Hundreds of schools, thousands of courses, and amazing best price offers.": "Сотни школ, тысячи курсов и привлекательные ценовые предложения.",
  "Everything you need to get your adventure started.": "Это все, что нужно для нового приключения.",
  "At Tambook, we believe in globally-conscious learners. We help ambitious people set their own rules for life, and become the most outstanding versions of themselves in the process.": "На Tambook мы являемся приверженными сторонниками учеников с глобально-ориентированным мышлением. Мы помогаем амбициозным людям устанавливать собственные правила жизни и становиться первоклассными версиями себя в этом процессе.",
  "As a team of expats from Eurasia and South America, we learned firsthand how frustrating, time-consuming, and expensive it can be to find a language course in a different country. After scrolling through hundreds of websites, we paid hefty fees to haggle with money-grabbing agents. Talking directly with schools was difficult - especially as we didn’t speak the language yet.": "Как команда эмигрантов из Евразии и Южной Америки, мы не понаслышке знаем, насколько досадным, времязатратным и дорогостоящим может быть процесс поиска языковых курсов в другой стране. После прокрутки сотен веб-сайтов, мы заплатили изрядную сумму денег, торгуясь с агентами. Вести переговоры со школами напрямую было сложно - тем более что мы еще не говорили на английском.",
  "In 2015, we built Tambook to flip this status quo. Trusted by students and language schools around the globe, we designed Tambook to be a state-of-the-art booking platform that anyone could use to compare language courses, score the best prices directly from schools, and pay in their local currency within a single browsing session.": "В 2015 году мы создали Tambook с целью изменения этого статуса-кво. Благодаря доверию учеников и языковых школ по всему миру, мы разработали Tambook как современную платформу бронирования, которую каждый может использовать для сравнения языковых курсов, получать предложения по самым выгодным ценам напрямую от школ и платить в местной валюте в течение одного сеанса просмотра.",
  "Driven by a pioneering spirit, we’re always working hard to make things better for eager learners. Our team will keep innovating until studying a language abroad becomes an easy, affordable option for everyone.": "Вдохновленные духом новаторства, мы всегда упорно работаем над тем, чтобы облегчить жизнь амбициозным студентам. Наша команда продолжит внедрять инновации, пока обучение за рубежом не станет простым и недорогостоящим вариантом для каждого.",
  "Tambook HQ is currently located in New Zealand. We have a lot of team members working here, and a lot of team members working all around the world. We’re here to make booking a language course easy for you, so if you have any questions, please reach out.": "Штаб-квартира Tambook в настоящее время находится в Новой Зеландии. Наша большая команда состоит из сотрудников, работающих как внутри страны, так и за рубежом. Мы здесь, чтобы сделать языковые курсы легкодоступными для вас, поэтому, если у вас есть какие-либо вопросы или предложения по улучшению платформы, пишите!",
  "TAMBOOK - WE MAKE LEARNING LANGUAGES": "TAMBOOK - МЫ ПОМОГАЕМ УЧИТЬ ЯЗЫКИ",
  "AN AFFORDABLE ADVENTURE": "ПУТЕШЕСТВИЕ ПО ДОСТУПНОЙ ЦЕНЕ",
  "ABOUT": "ПРО",
  "START YOUR ADVENTURE NOW": "НАЧНИ ПУТЕШЕСТВИЕ ПРЯМО СЕЙЧАС",
  "NEED HELP?": "НУЖНА ПОМОЩЬ?",
  "Look up": "Искать",
  "frequently asked questions": "Часто задаваемые вопросы",
  "for a hint": "подсказка",
  "Our friendly and experienced student advisors will help you with all your questions": "Наши внимательные и опытные консультанты помогут вам с любыми вопросами",
  "New Zealand Head Office": "Штаб-квартира в Новой Зеландии",
  "FREQUENTLY ASKED QUESTIONS": "ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ",
  "CATEGORIES": "Категории",
  "SEARCH: NO RESULT FOUND": "ПОИСК: РЕЗУЛЬТАТОВ НЕ НАЙДЕНО",
  "SEARCH: ": "Поиск: "
};