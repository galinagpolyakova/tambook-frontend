export default {
  "BLOG": "БЛОГ",
  "DETAIL": "ПОДРОБНОСТИ",
  "Back to Blog": "Вернуться к Блогу",
  "SHARE": "ПОДЕЛИТЬСЯ",
  "DON'T MISS A THING:": "НЕ ПРОПУСТИТЕ:",
  "Subscribe to get our latest best offers, promotions and news": "Подпишитесь, чтобы получать лучшие предложения, акции и оставаться в курсе новостей",
  "SUBSCRIBE": "ПОДПИСАТЬСЯ",
  "Subscribed successfully. Thank you.": "Подписка успешно завершена. Спасибо.",
  "All": "Все",
  "WATCH HOW TAMBOOK HELPS YOU STUDY OVERSEAS": "СМОТРЕТЬ КАК TAMBOOK ПОМОГАЕТ УЧИТЬСЯ ЗА ГРАНИЦЕЙ",
  "Welcome to our blog! Here you'll find useful tips and tricks that'll help you plan your next adventure. To get started, take a look at our short video 'How to book a course using Tambook": "Добро пожаловать в наш блог! Здесь вы найдете полезные сответы и рекомендации, которые помогут вам распланировать ваше следующее приключение.Чтобы начать, посмотрите наше короткое видео 'Как забронировать курс с помощью Tambook'",
  "SEARCH FOR COURSES NOW": "ИСКАТЬ КУРСЫ ПРЯМО СЕЙЧАС",
  "search": "искать",
  "MORE INFO":"БОЛЬШЕ ИНФОРМАЦИИ"
};