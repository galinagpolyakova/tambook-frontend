export default {
    "Price": "Стоимость",
    "Destination": "Направление",
    "Intensity": "Интенсивность",
    "Duration": "Продолжительность",
    "Ability to work": "Право на работу",
    "Course type": "Тип курса",
    "Compare Courses": "Сравнить курсы",
    "No courses in compare list":"В списке сравнения курсов нет",
    "Clear All":"Очистить все"
};