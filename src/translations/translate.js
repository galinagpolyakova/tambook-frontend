// @flow
import translations from "../translations";
import { getLanguageFromUrl } from "../shared/helpers/translations";

export type TranslateType = {
  title: string
};

function translate(title: $PropertyType<TranslateType, "title">) {
  const language = getLanguageFromUrl();
  if (
    translations === undefined ||
    translations[language] === undefined ||
    translations[language][title] === undefined
  ) {
    return title;
  } else {
    return translations[language][title];
  }
}

export default translate;
