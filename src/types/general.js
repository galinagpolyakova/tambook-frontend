import { ASYNC_STATUS } from "../constants/async";
import Alert from "shared/components/Alert/Alert";

// @flow
export type AsyncStatusType =
  | typeof ASYNC_STATUS.INIT
  | typeof ASYNC_STATUS.LOADING
  | typeof ASYNC_STATUS.SUCCESS
  | typeof ASYNC_STATUS.FAILURE;

export type NotificationType = null | {
  message: string,
  type: | null
    | typeof Alert.TYPE.ERROR
    | typeof Alert.TYPE.SUCCESS
    | typeof Alert.TYPE.INFO
};
